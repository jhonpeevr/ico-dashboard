'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const bonusSchema = new Schema({
  startDate: {
    type: Date,
    required: true,
    unique: true,
  },
  endDate: {
    type: Date,
    required: true,
    unique: true,
  },
  percentage: {
    type: Number,
    required: true,
    default: 0,
  },
  priceInUSD: {
    type: Number,
    required: true,
    default: 1,
  },
  description: {
    type: String,
    default: '',
  },
});

const model = db.model('Bonus', bonusSchema);
module.exports = model;
