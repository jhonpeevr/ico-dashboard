'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const SchemaTypes = mongoose.Schema.Types;

const financialHistoriesSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
        required: true,
    },
    amount: {
        type: SchemaTypes.Double,
        required: true,
    },
    coinAddressId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CoinAddresses',
    },
    exchangeRateValue: {
        type: SchemaTypes.Double,
        required: true,
    },
    currencyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Currencies',
        required: true,
    },
    currencyAbbr: {
        type: String,
        required: true,
    },
    currencyName: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        enum: [
            'deposit',
            'invest',
            'admin_deposit',
            'withdraw',
            'referral_payment',
            'referral_payment_canceled',
            'bonus',
        ],
        required: true,
    },
    timestamp: {type: Date, default: Date.now},
});

financialHistoriesSchema.plugin(mongoosePaginate);
const model = db.model('FinancialHistories', financialHistoriesSchema);
module.exports = model;
