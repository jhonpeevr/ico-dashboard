'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
// const SchemaTypes = mongoose.Schema.Types;

/**
 * Stores generated coin addresses from bitgo
 */
const createAddressSchema = new Schema({
    coinName: {
        type: String,
        required: true,
    },
    coinAddress: {
        type: String,
        required: true,
        unique: true,
    },
    addressStatus: {
        type: String,
        enum: ['notused', 'used'],
        default: 'notused',
    },
    timestamp: {
        type: Date,
        default: Date.now,
    },
});

const model = db.model('CreateAddress', createAddressSchema);
module.exports = model;
