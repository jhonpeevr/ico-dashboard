'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const captchaSchema = new Schema({
  sessionId: {
    type: String,
    required: true,
    unique: true,
  },
  ip: {
    type: String,
    required: true,
  },
  value: {
    type: String,
    required: true,
  },
  timestamp: {type: Date, default: Date.now},
});

const model = db.model('CaptchaSessions', captchaSchema);
module.exports = model;
