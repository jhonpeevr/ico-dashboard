'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const feedSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
    },
    subject: {
        type: String,
    },
    description: {
        type: String,
    },
    status: {
        type: Boolean,
        default: true,
    },
    visibility: {
        type: String,
        enum: ['private', 'public'],
    },
    timestamp: {type: Date, default: Date.now},
});

const model = db.model('Feeds', feedSchema);
module.exports = model;
