'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const notificationsSchema = new Schema({
    isForAll: {
        type: Boolean,
        default: false,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
    },
    subject: {
        type: String,
    },
    message: {
        type: String,
    },
    seen: {
        type: Boolean,
        default: false,
    },
    timestamp: {type: Date, default: Date.now},
});

const model = db.model('Notifications', notificationsSchema);
module.exports = model;
