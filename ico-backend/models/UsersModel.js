'use strict';
/* Include necessary libs */
const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const randomstring = require('randomstring');
const sha512 = require('js-sha512').sha512;
const emitter = require('../lib/emitter');

/* Mongoose Schema Object*/

const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;


// Error handler
const AccountBlockedError = require('../errors/AccountBlockedError');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const EmailAddressAlreadyExistsError = require(
    '../errors/EmailAddressAlreadyExistsError');


const usersSchema = new Schema({
    email: {
        type: String,
        lowercase: true,
        required: true,
        unique: true,
        match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    },
    fullName: {
        type: String,
    },
    balanceTKN: {
        type: SchemaTypes.Double,
        default: 0.0,
    },
    referralTKN: {
        type: SchemaTypes.Double,
        default: 0.0,
    },
    purchasedTKN: {
        type: SchemaTypes.Double,
        default: 0.0,
    },
    role: {
        type: String,
        enum: ['admin', 'user'],
        default: 'user',
    },
    password: {
        type: String,
        required: true,
    },
    country: {
        type: String,
    },
    twoFaEnabled: Boolean,
    twoFaCode: String,
    emailOTP: String,
    forgotPasswordCode: String,
    forgotPasswordTried: Number,
    authCount: {
        type: Number,
        default: 0,
    },
    active: {
        type: Boolean,
        default: false,
    },
    activationCode: String,
    kycConfirmed: {
        type: Boolean,
        default: false,
    },
    kycSubmitted: {
        type: Boolean,
        default: false,
    },
    kycStatus: {
        type: String,
        enum: ['REJECTED', 'SUCCESS', 'SUBMITTED', 'PENDING', 'PROGRESS', 'INCOMPLETE'],
        default: 'PENDING',
    },
    amlConfirmed: {
        type: Boolean,
        default: false,
    },
    amlRecord: {type: mongoose.Schema.Types.ObjectId, ref: 'Aml'},
    withdrawalStatus: {
        type: Boolean,
        default: true,
    },
    ethAddress: {
        type: String,
    },
    withdrawType: {
        type: String,
        enum: ['manual', 'automatic'],
        default: 'manual',
    },
    referralCode: {
        type: String,
        unique: true,
    },
    provider: {
        type: String,
        enum: ['local', 'facebook', 'google'],
        default: 'local',
    },
    providerData: {
        type: {
            id: String,
            token: String,
            obj: String,
        },
        select: false,
    },
    isUserNew: {
        type: Boolean,
        default: false,
    },
    referBy: {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
    isMailVerification: {
        type: Boolean,
        default: false,
    },
    timestamp: {type: Date, default: Date.now},
});

/* Facebook user registration  and login hook*/

usersSchema.statics.upsertFbUser = (accessToken, refreshToken, profile, cb) => {
    let userEmail = profile._json.email;
    return model.findOne({
        'provider': 'facebook',
        'providerData.id': profile.id,
    }, (err, user) => {
        // no user was found, lets create a new one [in case of new user registration]
        if (!user) {

            return model.findOne({
                'provider': {$in: ['google', 'local']},
                'email': userEmail,
            }, (err, user) => {
                if (user) {
                    return cb(new EmailAddressAlreadyExistsError(), user);
                }
                // check referral


                let newUser = new model({
                    email: profile.emails[0].value,
                    providerData: {
                        id: profile.id,
                        token: accessToken,
                        obj: JSON.stringify(profile),
                    },
                    fullName: profile._json.name || 'anonymous',
                    provider: 'facebook',
                    password: sha512(profile.id),
                    active: true,
                    isUserNew: true,
                    referralCode: randomstring.generate({length: 64}),
                    // firstName:profile.name
                });

                newUser.save((error, savedUser) => {
                    if (error) {
                        console.log(error);
                    }
                    emitter.emit('user-count-changed');
                    return cb(error, savedUser);
                });
            });
        } else {
            if (!user.active) {
                throw new AccountBlockedError(user.email);
            }
            return cb(err, user);
        }
    });
};

/* Google user registration login hook */

usersSchema.statics.upsertGoogleUser =
    (accessToken, refreshToken, profile, cb) => {
        let userEmail = profile._json.email;


        return model.findOne({
            'provider': 'google',
            'providerData.id': profile.id,
        }, (err, user) => {
            if (err) {
                throw new InvalidInputParametersError();
            }
            // no user was found, lets create a new one
            if (!user) {

                return model.findOne({
                    'provider': {$in: ['facebook', 'local']},
                    'email': userEmail,
                }, (err, user) => {
                    if (user) {
                        return cb(new EmailAddressAlreadyExistsError(), user);
                    }

                    let newUser = new model({
                        email: profile._json.email,
                        providerData: {
                            id: profile.id,
                            token: accessToken,
                        },
                        fullName: profile._json.name || 'anonymous',
                        provider: 'google',
                        password: sha512(profile.id),
                        active: true,
                        isUserNew: true,
                        referralCode: randomstring.generate({length: 64}),
                    });

                    newUser.save((error, savedUser) => {
                        if (error) {
                            console.log(error);
                        }
                        emitter.emit('user-count-changed');
                        return cb(error, savedUser);
                    });
                });
            } else {
                if (!user.active) {
                    throw new AccountBlockedError(user.email);
                }
                return cb(err, user);
            }

        });
    };

const model = db.model('Users', usersSchema);
module.exports = model;
