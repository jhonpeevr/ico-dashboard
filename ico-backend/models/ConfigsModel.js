'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const configsSchema = new Schema({
  key: {
    type: String,
    required: true,
    unique: true,
  },
  valueString: {
    type: String,
  },
  valueDouble: {
    type: SchemaTypes.Double,
  },
  valueDate: {
    type: Date,
  },
  valueBoolean: {
    type: Boolean,
  },
  editable: {
    type: Boolean,
    default: false,
  },
});

const model = db.model('Configs', configsSchema);
module.exports = model;
