'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const exchangeRatesSchema = new Schema({
  currencyIdFirst: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Currencies',
    required: true,
  },
  currencyIdFirstAbbr: {
    type: String,
    required: true,
  },
  currencyIdFirstName: {
    type: String,
    required: true,
  },
  currencyIdFirstMinAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
  currencyIdFirstMinAmountCompany: {
    type: SchemaTypes.Double,
    required: true,
  },
  currencyIdFirstMaxAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
  currencyIdFirstType: {
    type: String,
    required: true,
  },
  currencyIdSecond: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Currencies',
    required: true,
  },
  currencyIdSecondAbbr: {
    type: String,
    required: true,
  },
  currencyIdSecondName: {
    type: String,
    required: true,
  },
  currencyIdSecondMinAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
  currencyIdSecondMinAmountCompany: {
      type: SchemaTypes.Double,
      required: true,
  },
  currencyIdSecondMaxAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
  currencyIdSecondType: {
    type: String,
    required: true,
  },
  exchangeRate: {
    type: SchemaTypes.Double,
    required: true,
  },
  type: {
    type: String,
    enum: ['invest', 'withdraw'],
    required: true,
  },
});

const model = db.model('ExchangeRates', exchangeRatesSchema);
module.exports = model;
