'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const kycSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
    contactno: {
    type: String,
    required: true,
  },
  dateOfBirth: {
    type: Date,
    required: true,
  },
  address: {
    type: String,
    required: true,
  },
  state: {
    type: String,
  },
  countryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Countries',
    required: true,
  },
  passportNumber: {
    type: String,
  },
  SSN: {
    type: String,
  },
  status: {
    type: String,
    enum: ['pending', 'declined', 'confirmed'],
  },
  passportImage: String,
  utilityImage: String,
  timestamp: {type: Date, default: Date.now},
});

const model = db.model('KYCDetails', kycSchema);
module.exports = model;
