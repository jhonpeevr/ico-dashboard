'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const bonusPerStageSchema = new Schema({
    bonusStage: {
        type: String,
        required: true,
    },
    minTokens: {
        type: Number,
        required: true,
    },
    maxTokens: {
        type: Number,
        required: true,
    },
    percentage: {
        type: Number,
        required: true,
        default: 0,
    },
    priceInUSD: {
        type: Number,
        required: true,
        default: 1,
    },
    description: {
        type: String,
        default: '',
    },
});

const model = db.model('BonusPerStage', bonusPerStageSchema);
module.exports = model;
