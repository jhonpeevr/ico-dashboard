'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const currentRatesUsdSchema = new Schema({
    currencyId: {
        type: String,
        required: true,
    },
    valueInUSD: {
        type: SchemaTypes.Double,
        required: true,
    },
});

const model = db.model('CurrentRatesUsd', currentRatesUsdSchema);
module.exports = model;
