'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const btcTransfersSchema = new Schema({
  id: String,
  coin: String,
  wallet: String,
  txid: String,
  height: String,
  date: Date,
  confirmations: Number,
  value: Number,
  bitgoFee: Number,
  usd: SchemaTypes.Double,
  state: String,
  tags: [String],
  internalSequenceId: String,
  history: [
    {
      date: Date,
      action: String,
    }],
  entries: [
    {
      address: String,
      value: Number,
      wallet: String,
    }],
  confirmedTime: Date,
  createdTime: Date,
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

const model = db.model('CoinTransfers', btcTransfersSchema);
module.exports = model;
