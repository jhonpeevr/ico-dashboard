'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const coinTransferHooksSchema = new Schema({
    coinAddressId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'CoinAddresses',
    },
    type: {
        type: String,
        required: true,
    },
    wallet: {
        type: String,
        required: true,
    },
    hash: {
        type: String,
        required: true,
    },
    transfer: {
        type: String,
        required: true,
    },
    coin: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum: ['processing', 'processed', 'insert-error', 'update-error'],
        required: true,
    },
    details: {
        type: String,
    },
    timestamp: {type: Date, default: Date.now},
});

const model = db.model('CoinTransferHooks', coinTransferHooksSchema);
module.exports = model;
