'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const subscriptionSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  confirmed: {
    type: Boolean,
    default: false,
  },
  timestamp: {type: Date, default: Date.now},
});

const model = db.model('Subscriptions', subscriptionSchema);
module.exports = model;
