'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const currenciesSchema = new Schema({
  abbr: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
    unique: true,
  },
  type: {
    type: String,
    enum: ['crypto', 'fiat'],
    required: true,
  },
  minAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
  minAmountCompany: {
      type: SchemaTypes.Double,
      required: true,
  },
  maxAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
});

const model = db.model('Currencies', currenciesSchema);
module.exports = model;
