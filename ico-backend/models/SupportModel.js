'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;

const supportSchema = new Schema({
    email: {
        type: String,
        lowercase: true,
        required: true,
    },
    supportIssue: {
        type: String,
        required: true,
    },
    supportDesc: {
        type: String,
        required: true,
    },
    timestamp: {type: Date, default: Date.now},
});

const model = db.model('Support', supportSchema);
module.exports = model;
