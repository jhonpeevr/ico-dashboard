'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

/**
 * Stores generated coin addresses
 */
const coinAddressesSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
  },
  status: {
    type: String,
    enum: ['pending', 'processed', 'expired'],
    default: 'pending',
  },
  coinAmount: {
    type: SchemaTypes.Double,
    required: true,
  },
  coinResult: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  tknAmount: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  address: {
    type: String,
    required: true,
    unique: true,
  },
  bonusPercentage: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  afterBonusTokens: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  currencyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Currencies',
    required: true,
  },
  currencyAbbr: {
    type: String,
    required: true,
  },
  exchangeRateValue: {
    type: SchemaTypes.Double,
    required: true,
  },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

const model = db.model('CoinAddresses', coinAddressesSchema);
module.exports = model;
