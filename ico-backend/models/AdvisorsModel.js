'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const advisorsSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    required: true,
    unique: true,
  },
  amountBTC: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  amountETH: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  referralAmountTKN: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  referralAmountBTC: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  referralAmountETH: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  timestamp: {type: Date, default: Date.now},
});

const model = db.model('Advisors', advisorsSchema);
module.exports = model;
