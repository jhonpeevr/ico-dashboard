'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const amlSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  recommendation: Number,
  balance: Number,
  whitelist: String,
  cscore: Number,
  usd_balance: Number,
});
module.exports = db.model('Aml', amlSchema);

