'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const liveChatsSchema = new Schema({
  sessionId: {
    type: String,
    required: true,
  },
  messages: [
    {
      message: String,
      timestamp: {type: Date, default: Date.now},
      name: {
        type: String,
        required: true,
      },
    },
  ],

  timestamp: {type: Date, default: Date.now},
});

const model = db.model('LiveChats', liveChatsSchema);
module.exports = model;
