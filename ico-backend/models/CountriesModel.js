'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const countriesSchema = new Schema({
    countryName: {
        type: String,
        required: true,
        unique: true,
    },
    code: {
        type: String,
        required: true,
        unique: true,
    },
});

const model = db.model('Countries', countriesSchema);
module.exports = model;
