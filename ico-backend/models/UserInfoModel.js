'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

/**
 * Stores generated coin addresses
 */
const userInfoSchema = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users',
    },
    city: {
        type: String,
    },
    countryCode: {
        type: String,
    },
    countryName: {
        type: String,
    },
    ipAddress: {
        type: String,
    },
    latitude: {
        type: SchemaTypes.Double,
    },
    longitude: {
        type: SchemaTypes.Double,
    },
    browser: {
        type: String,
    },
    timestamp: {
        type: Date,
        default: Date.now,
    },
});

const model = db.model('UserInfo', userInfoSchema);
module.exports = model;
