'use strict';

const db = require('../lib/maindb');
const mongoose = require('mongoose');
require('mongoose-double')(mongoose);
const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;

const usdInvestsSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
  },
  currencyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Currencies',
    required: true,
  },
  exchangeRateValue: {
    type: SchemaTypes.Double,
    required: true,
  },
  fullname: {
    type: String,
  },
  country: {
    type: String,
  },
  amountUsd: {
    type: SchemaTypes.Double,
    required: true,
  },
  resultUsd: {
    type: SchemaTypes.Double,
    default: 0.0,
  },
  paymentDetails: {
    type: String,
  },
  status: {
    type: String,
    enum: ['processing', 'confirmed', 'canceled'],
  },
  transactionId: String,
  timestamp: {type: Date, default: Date.now},
});

const model = db.model('UsdInvests', usdInvestsSchema);
module.exports = model;
