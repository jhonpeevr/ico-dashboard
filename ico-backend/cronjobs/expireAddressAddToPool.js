'use strict';

const bitgo = require('../lib/bitgo');
const config = require('config');
const CreateAddressModel = require('../lib/maindb').model('CreateAddress');
const CoinAddressesModel = require('../lib/maindb').model('CoinAddresses');
const ConfigsModel = require('../lib/maindb').model('Configs');
const CronJob = require('cron').CronJob;

let job = new CronJob({
    cronTime: '0 0 */1 * * *',
    //cronTime: '0 */01 * * * *',
    onTick: async function() {
        /*
         * Runs everyday 1 hour
         */
        console.log('inside cronjob expire add');
        let ethAddressExpirationTime = await ConfigsModel.findOne({key: 'ethAddressExpirationTime'});
        console.log(ethAddressExpirationTime['valueDouble']['value']);
        const coinType = 'teth';
        let query = {
            status: 'pending',
            timestamp: {
                $lte: new Date(Date.now() - ethAddressExpirationTime['valueDouble']['value']),
            },
        }

        let pendingUserAdd = await CoinAddressesModel.find(query);

        let pendingAdd = pendingUserAdd.map(address => address['address']);

        let pendingQuery = {
            coinAddress:{ '$in': pendingAdd }
        };

        let changePendingAddToNotUsed = await CreateAddressModel.update(pendingQuery, { addressStatus: 'notused' },{ multi: true });

        for(let i =0; i< pendingUserAdd.length; i++){
            // await CoinAddressesModel.update(query,{ address: Date.now() },{ multi: true });  to expire many with one query
            let expireAdd = await CoinAddressesModel.findById(pendingUserAdd[i]['_id']);
            expireAdd['address'] = pendingUserAdd[i]['userId'] + Date.now();
            expireAdd['status'] = 'expired';
            await expireAdd.save();
        }



    },
    onComplete: function() {
        console.log('<== [CronJob] :: Daily point limit reset for all users ==>');
    },
    start: true,
    timeZone: 'America/Los_Angeles',
});
module.exports = job;
