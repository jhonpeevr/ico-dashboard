'use strict';

const bitgo = require('../lib/bitgo');
const config = require('config');
const CreateAddressModel = require('../lib/maindb').model('CreateAddress');
const CoinAddressesModel = require('../lib/maindb').model('CoinAddresses');
const ConfigsModel = require('../lib/maindb').model('Configs');
const CronJob = require('cron').CronJob;

let job = new CronJob({
    cronTime: '0 0 */1 * * *',
    //cronTime: '0 */01 * * * *',
    onTick: async function() {
        /*
         * Runs everyday 1 hour
         */
        console.log('inside cronjob add sync');
        const coinType = config.get('coins')[1];
        bitgo.coin(coinType).wallets().get({
            'id': config.get('bitgo-' + coinType + '-wallet-id'),
        }).then((wallet) => {
            wallet.addresses({
                'limit': 1000,
            }).then(async (addresses) => {
                const etherAddressCount = addresses.count;
                const createdAddressCount = await CreateAddressModel.count({}).exec();
                // console.log('etherAddressCount '+etherAddressCount);
                for (let i = etherAddressCount-1; i > createdAddressCount-1; i--) {
                    //for (let i = etherAddressCount-1; i >= 0; i--) {
                    let ethAddress = addresses.addresses[i].address;
                    CreateAddressModel.findOne({coinAddress: ethAddress}).exec().then((ethAddressFound) => {
                        if (!ethAddressFound) {
                            CoinAddressesModel.findOne({address: ethAddress}).exec().then((coinAddressFound) => {
                                if (!coinAddressFound) {
                                    const ethAddressData = new CreateAddressModel({
                                        coinName: 'teth',
                                        coinAddress: ethAddress,
                                    });
                                    ethAddressData.save(() => {
                                        console.log('Address saved '+ethAddressData.coinAddress);
                                    });
                                } else {
                                    console.log('Address found in coin Address Model.'+coinAddressFound.address);
                                }
                            });
                        } else {
                            console.log('Address found in create Address Model. '+ethAddressFound.coinAddress);
                        }
                    });
                }
            });
        }).catch(err=>{
            console.log('Bitgo Err', err);
            res.send({msg: 'Bitgo service is unavailable'})
        });

    },
    onComplete: function() {
        console.log('<== [CronJob] :: Daily point limit reset for all users ==>');
    },
    start: true,
    timeZone: 'America/Los_Angeles',
});
module.exports = job;

