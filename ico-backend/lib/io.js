'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');
const redis = require('socket.io-redis');
const io = require('socket.io').listen(config.get('io-port'));
 io.adapter(redis({host: 'localhost', port: 6379}));

io.use((socket, next) => {
  const token = socket.handshake.query.token;

  jwt.verify(token, config.get('secret'), function(err, decoded) {
    if (err) {
      if (err instanceof jwt.TokenExpiredError) {
        return next(new Error('EXPIRED'));
      }

      return next(new Error('FORBIDDEN'));
    } else {
      socket.decoded = decoded;
    }
  });
  return next();
});

io.on('connection', (socket) => {

});

module.exports = io;
