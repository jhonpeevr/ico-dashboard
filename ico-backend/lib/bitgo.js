const config = require('config');
const BitGoJS = require('bitgo');

const bitgo = new BitGoJS.BitGo({
  env: config.get('bitgo-env'),
  accessToken: config.get('bitgo-access-token'),
});

module.exports = bitgo;
