const BigNumber = require('bignumber.js');

exports.safeAdd = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.plus(y);

}

exports.safeSubtract = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.minus(y);
}

exports.safeDivide = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.dividedBy(y);
}

exports.safeMultiply = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.multipliedBy(y);
}

exports.valueOf = function (a) {
    return a.valueOf();

}

exports.isGreaterThan = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.isGreaterThan(y);

}

exports.isLessThan = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.isLessThan(y);

}
exports.isGreaterThanOrEqualTo = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.isGreaterThanOrEqualTo(y);

}


exports.isLessThanOrEqualTo = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.isLessThanOrEqualTo(y);

}

exports.toFixed = function (a, b) {
    let x = new BigNumber(a);
    return x.toFixed(b);

}

exports.toFixedNew = function (a, b) {
    let str = a.split(".");
    let str1 = str[0];
    let str2 = str[1];
    if (str2) {
        if (str2.length > b) {
            str2 = str2.slice(0, b);
        }
        else {
            let numofzero = b - str2.length;
            str2 += '0'.repeat(numofzero);
        }
    } else{
        str2 = '0000';
    }

    return str1 + '.' + str2;

}

exports.isEqualTo = function (a, b) {
    let x = new BigNumber(a);
    let y = new BigNumber(b);
    return x.isEqualTo(y);

}


