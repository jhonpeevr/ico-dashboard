'use strict';

const Promise = require('bluebird');
const util = require('util');
const config = require('config');
const mailcomposer = require('mailcomposer');
const mailgun = require('mailgun-js')({
    apiKey: config.get('mailgun-access-key'),
    domain: config.get('mailgun-domain'),
});

const eTemplate = require('./emailTemplate.json');


// Email Template Design
const EMAIL_LOGO = '<p><img width=\'200\' height=\'80\' src=\'' +
    config.get('frontend-host') +
    '/assets/img/logo-blue.png\' alt=\'logo\'> </p>';

const HEAD_LOGO = '<img width=\'320\' height=\'80\' src=\'' +
    config.get('frontend-host') +
    '/assets/img/header-email-confirm.png\' alt=\'logo\'>';

const loggerName = '[Mail]';

module.exports.sendSupportMail = function (email, supportIssue, supportDesc) {
    const methodName = '[SendSupportMail]';
    const emailTo = config.get('zendesk-support-mail');

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: email,
            to: emailTo,
            subject: supportIssue,
            text: supportDesc,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: emailTo,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Support Email sent by %s', email));
                console.log(loggerName, methodName,
                    util.format('Support Email sent to %s', emailTo));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};


module.exports.sendActivationCodeMailGun = function (email, name, activationCode) {
    const methodName = '[SendActivationCode]';

    let subject = eTemplate.sendActivationCode.SUBJECT.replace('[COMPANY_NAME]',
        config.get('company_name'));
    let actUrl = config.get('frontend-host') + '/activate-email/' + email + '/' + activationCode;
    let activationURL = '<span><a href=\'' + actUrl +
        '\' style="background: #ed1e79; padding: 10px 15px; color:#ffffff; text-decoration: none; border-radius: 25px;">Verify email address</a></span>';

    let bodyHtml = eTemplate.sendActivationCode.HTML_MSG.replace(
        '[ACTIVATE_LINK]', activationURL).replace(
        '[ACTIVATE_URL]', actUrl).replace('[HEAD_LOGO]', HEAD_LOGO).replace(
        '[EMAIL]', email).replace('[NAME]', name);
    bodyHtml += eTemplate.DISCLAIMER;

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Message successfully submitted to %s through MailGun Api', email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendForgotPasswordMailGun = function (email, name, activationCode) {
    const methodName = '[SendForgotPassword]';

    let subject = eTemplate.sendForgotPassword.SUBJECT;
    let actUrl = config.get('frontend-host') + '/change-password/' + email + '/' + activationCode;
    let activationLink = '<a href=\'' + config.get('frontend-host') +
        '/change-password/' + email + '/' + activationCode +
        '\' style="background: #ed1e79; padding: 10px 15px; color:#ffffff; text-decoration: none; border-radius: 25px;">Reset Password</a>';

    let bodyHtml = eTemplate.sendForgotPassword.HTML_MSG.replace('[EMAIL]',
        email).replace('[NAME]', name).replace('[ACTIVATE_LINK]', activationLink).replace(
        '[ACTIVATE_URL]', actUrl)
        .replace('[HEAD_LOGO]', HEAD_LOGO);

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Forgot password link sent to %s through MailGun Api', email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendPasswordResetCompletedMailGun = function (email, name) {
    const methodName = '[SendPasswordResetCompleted]';

    let subject = eTemplate.sendPasswordResetCompleted.SUBJECT;
    let loginUrl = config.get('frontend-host') + '/login';
    let loginLink = '<a href=\'' + loginUrl +'\' style="background: #ed1e79; padding: 10px 15px; color:#ffffff; text-decoration: none; border-radius: 25px;">Login</a>';

    let bodyHtml = eTemplate.sendPasswordResetCompleted.HTML_MSG.replace(
        '[EMAIL]', email).replace('[NAME]', name).replace('[LOGIN_URL]', loginLink);

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Password reset mail sent to %s through MailGun Api', email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendDepositTKNMailGun = function (email, tknAmount, amountEvt, coinType, coinAmount, tknParAmount, tknBonusAmount, amountDeposited) {
    const methodName = '[SendDepositTKN]';

    let subject = eTemplate.sendDepositTKN.SUBJECT;

    let bodyHtml = eTemplate.sendDepositTKN.HTML_MSG
        .replace('[EMAIL]', email)
        .replace('[EVM_TOKEN_AMOUNT_TOTAL]', tknAmount)
        .replace('[EVT_TOKEN_AMOUNT]', amountEvt)
        .replace('[COIN_TYPE]', coinType)
        .replace('[COIN_AMOUNT]', coinAmount)
        .replace('[EVM_TOKEN_PAR_AMOUNT]', tknParAmount)
        .replace('[EVM_TOKEN_BONUS_AMOUNT]', tknBonusAmount)
        .replace('[EVM_AMOUNT_DEPOSITED]', amountDeposited)
        .replace('[HEAD_LOGO]', HEAD_LOGO);

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Deposit successful mail sent to %s through MailGun Api', email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendAddressGeneratedMailGun = function (email, tknAmount, amountEvt, coinType, coinAmount, coinAddress, tknParAmount, bonusTkns) {
    const methodName = '[SendAddressGenerated]';
    const coinType1 = coinType;
    const tknAmount1 = tknAmount;

    let subject = eTemplate.sendAddressGenerated.SUBJECT;

    let bodyHtml = eTemplate.sendAddressGenerated.HTML_MSG
        .replace('[EMAIL]', email)
        .replace('[EVM_TOKEN_AMOUNT]', tknAmount)
        .replace('[EVT_TOKEN_AMOUNT]', amountEvt)
        .replace('[COIN_TYPE]', coinType)
        .replace('[COIN_AMOUNT]', coinAmount)
        .replace('[COIN_ADDRESS]', coinAddress)
        .replace('[EVM_TOKEN_PAR_AMOUNT]', tknParAmount)
        .replace('[EVM_TOKEN_AMOUNT1]', tknAmount1)
        .replace('[COIN_TYPE1]', coinType1)
        .replace('[BONUS_TKNS]', bonusTkns)
        .replace('[HEAD_LOGO]', HEAD_LOGO);

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Address Generated mail sent to %s through MailGun Api', email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendReferralEmailMailGun = function (user, amount) {
    const methodName = '[sendReferralEmail]';
    let subject = eTemplate.sendReferralNote.SUBJECT;

    let bodyHtml = eTemplate.sendReferralNote.HTML_MSG.replace(
        '[EMAIL]',
        user.email).replace('[AMOUNT]', amount);
    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name'));
    bodyHtml += EMAIL_LOGO;
    bodyHtml += eTemplate.DISCLAIMER;

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: user.email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: user.email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Referral amount mail sent to %s through MailGun Api', user.email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendSubscriptionMailGun = function (email) {
    const methodName = '[SendEmail]';

    let subject = eTemplate.sendSubscription.SUBJECT;

    let bodyHtml = eTemplate.sendSubscription.HTML_MSG;

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Subscription mail sent to %s through MailGun Api', email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendWithdrawalBlockedMailGun = function (user) {
    const methodName = '[sendWithdrawalBlockedMail]';

    let subject = eTemplate.sendWithdrawalBlocked.SUBJECT;

    let bodyHtml = eTemplate.sendWithdrawalBlocked.HTML_MSG.replace(
        '[EMAIL]',
        user.email).replace('[LINK]', config.get('frontend-host'));
    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name'));
    bodyHtml += EMAIL_LOGO;
    bodyHtml += eTemplate.DISCLAIMER;

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: user.email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: user.email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Withdrawal blocked mail sent to %s through MailGun Api', user.email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendWithdrawalActivationMailGun = function (user) {
    const methodName = '[sendWithdrawalActivationMail]';

    let subject = eTemplate.sendWithdrawalActivate.SUBJECT;

    let bodyHtml = eTemplate.sendWithdrawalActivate.HTML_MSG.replace(
        '[EMAIL]',
        user.email).replace('[LINK]', config.get('frontend-host'));
    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name'));
    bodyHtml += EMAIL_LOGO;
    bodyHtml += eTemplate.DISCLAIMER;

    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: user.email,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: user.email,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Withdrawal activated mail sent to %s through MailGun Api', user.email));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};


module.exports.sendSpecialReferralInvestMailGun = function (advisorEmail, userEmail, investCoinAmnt, refAmountCRYPTO, cryptoCurrencyType) {
    const methodName = '[sendSpecialReferralInvestMail]';
    const currencyName = cryptoCurrencyType;

    let subject = eTemplate.sendSpecialReferralInvest.SUBJECT;

    let bodyHtml = eTemplate.sendSpecialReferralInvest.HTML_MSG
        .replace('[ADVISOR_EMAIL]', advisorEmail)
        .replace('[USER_EMAIL]', userEmail)
        .replace('[INVEST_COIN_AMOUNT]', investCoinAmnt)
        .replace('[REF_AMOUNT]', refAmountCRYPTO)
        .replace('[COIN_TYPE]', cryptoCurrencyType)
        .replace('[COIN_TYPE_CURRENCY]', currencyName)
        .replace('[HEAD_LOGO]', HEAD_LOGO);


    return new Promise((resolve, reject) => {

        const mail = mailcomposer({
            from: config.get('mail-from'),
            to: advisorEmail,
            subject: subject,
            html: bodyHtml,
        });

        return new Promise((resolve, reject) => {
            mail.build((err, message) => {
                if (err) {
                    reject(err);
                }

                resolve(message);
            });
        }).then((message) => {
            const sendMessage = {
                to: advisorEmail,
                message: message.toString('ascii'),
            };

            mailgun.messages().sendMime(sendMessage).then((result) => {
                console.log(loggerName, methodName, result);
                console.log(loggerName, methodName,
                    util.format('Special referral investment mail sent to %s through MailGun Api', advisorEmail));
                resolve(result);
            }).catch((err) => {
                console.error(loggerName, methodName, err);
                reject(err);
            });
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

