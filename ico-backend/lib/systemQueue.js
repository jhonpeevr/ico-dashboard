const Promise = require('bluebird');
const Queue = require('promise-queue');

Queue.configure(Promise);
const statisticsQueue = new Queue(1, Infinity);

module.exports = statisticsQueue;
