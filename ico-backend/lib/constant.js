configsDisplayName={
    tknName:'Token Name',
    totalCashReceivedUSD:'Total Cash Received USD',
    totalPublicTokensSold:'Total Public Tokens Sold',
    totalPrivateTokensSold:'Total Private Token Sold',
    totalTokens:'Total Tokens',
    depositEnabled:'Deposit Enabled',
    withdrawalEnabled:'Withdrawal Enabled',
    availableTokens:'Available Tokens',
    soldTokens:'Sold Tokens',
    timerMessage:'Timer Message',
    ethCap:'Eth Cap',
    tknInEth:'Token In Eth',
    endDate:'End Date',
    endDateITOStage2:'End Date ITO Stage2',
    endDateITO:'End Date ITO',
    tkn2USD:'Token To USD',
    minInvest:'Minimum Invest',
    normalBonusPercentage:'Normal Bonus Percentage',
    isIcoEnd:'Is Ico End',
    isIcoPaused:'Is Ico Paused',
    normalBonus:'Normal Bonus',
    stageBonus:'Stage Bonus',
    autoEthGenerateBitgo:'Auto Eth Generate Bitgo',
    ethAddressExpirationTime:'Eth Address Expiration Time',
    addressSyncCronJob:'Address Sync Cron Job'
}

module.exports = configsDisplayName;