'use strict';

const Promise = require('bluebird');
const util = require('util');
const config = require('config');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.get('sendgrid_api_key'));

const eTemplate = require('./emailTemplate.json');

const loggerName = '[Mail]';

// Email Template Design
const EMAIL_LOGO = '<p><img width=\'200\' height=\'80\' src=\'' +
    config.get('frontend-host') +
    '/assets/img/genesisx-logo-email.png\' alt=\'logo\'> </p>';

const HEAD_LOGO = '<img width=\'320\' height=\'80\' src=\'' +
    config.get('frontend-host') +
    '/assets/img/genesisx-logo-email.png\' alt=\'logo\'>';

const FOOT_LOGO = '<img width=\'350\' height=\'80\' src=\'' +
    config.get('frontend-host') +
    '/assets/img/genesisx-logo-email.png\' alt=\'logo\'>';

module.exports.sendActivationCode = function(email, activationCode) {
  const methodName = '[SendActivationCode]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendActivationCode.SUBJECT.replace('[COMPANY_NAME]',
        config.get('company_name'));

    // let activationURL=config.get('frontend-host') + '/activate-email/' +
    // email + '/' + activationCode;

    let activationURL = '<span><a href=\'' + config.get('frontend-host') +
        '/activate-email/' + email + '/' + activationCode +
        '\' style="color:#1352c8;">clicking here</a></span>';

    let bodyHtml = eTemplate.sendActivationCode.HTML_MSG.replace(
        '[ACTIVATE_LINK]', activationURL).
        replace('[HEAD_LOGO]', HEAD_LOGO);

    //  let bodyText = eTemplate.sendActivationCode.PLAIN_MSG.replace('[ACTIVATE_LINK]', activationURL);

    const sendMessage = {
      from: config.get('mail-from'),
      to: email,
      subject: subject,
      //  text: bodyText,
      html: bodyHtml,
    };

    sgMail.send(sendMessage).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendActivationCodeAgain = function(email, activationCode) {
    const methodName = '[SendActivationCode]';

    return new Promise((resolve, reject) => {
        let subject = eTemplate.sendActivationCode.SUBJECT.replace('[COMPANY_NAME]',
            config.get('company_name'));

        // let activationURL=config.get('frontend-host') + '/activate-email/' +
        // email + '/' + activationCode;

        let activationURL = '<span><a href=\'' + config.get('frontend-host') +
            '/activate-email/' + email + '/' + activationCode +
            '\' style="color:#1352c8;">clicking here</a></span>';

        let bodyHtml = eTemplate.sendActivationCode.HTML_MSG.replace(
            '[ACTIVATE_LINK]', activationURL).
        replace('[HEAD_LOGO]', HEAD_LOGO);


        const sendMessage = {
            from: config.get('mail-from'),
            to: email,
            subject: subject,
            //  text: bodyText,
            html: bodyHtml,
        };

        sgMail.send(sendMessage).then((result) => {
            console.log(loggerName, methodName,
                util.format('Message successfully submitted to %s through send grid api', email));
            resolve(result);
        }).catch((err) => {
            console.error(loggerName, methodName, err);
            reject(err);
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendForgotPassword = function(email, activationCode) {
  const methodName = '[SendForgotPassword]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendForgotPassword.SUBJECT;

    let activationLink = '<p><a href=\'' + config.get('frontend-host') +
        '/change-password/' + email + '/' + activationCode +
        '\' style="padding: 10px 12px; ' +
        'background-color:#34388f;color: #FFFFFF;text-decoration: none;' +
        'border-radius: 5px;">Reset</a></p>';

    let bodyHtml = eTemplate.sendForgotPassword.HTML_MSG.replace('[EMAIL]',
        email).replace('[ACTIVATE_LINK]', activationLink)
        .replace('[HEAD_LOGO]', HEAD_LOGO);

    activationLink = config.get('frontend-host') + '/change-password/' + email +
        '/' + activationCode;

    let bodyText = eTemplate.sendForgotPassword.PLAIN_MSG.replace('[EMAIL]',
        email).replace('[ACTIVATE_LINK]', activationLink);

    const sendMessage = {
      from: config.get('mail-from'),
      to: email,
      subject: subject,
      text: bodyText,
      html: bodyHtml,
    };
    sgMail.send(sendMessage).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendPasswordResetCompleted = function(email) {
  const methodName = '[SendPasswordResetCompleted]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendPasswordResetCompleted.SUBJECT;

    let bodyHtml = eTemplate.sendPasswordResetCompleted.HTML_MSG.replace(
        '[EMAIL]', email);

    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name')).replace('[HEAD_LOGO]', HEAD_LOGO);

    let bodyText = eTemplate.sendPasswordResetCompleted.PLAIN_MSG.replace(
        '[EMAIL]', email);

    const sendMessage = {
      from: config.get('mail-from'),
      to: email,
      subject: subject,
      text: bodyText,
      html: bodyHtml,
    };

    sgMail.send(sendMessage).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendDepositTKN = function(email, tknAmount, amountEvt, coinType, coinAmount, tknParAmount, tknBonusAmount, amountDeposited) {
  const methodName = '[SendDepositTKN]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendDepositTKN.SUBJECT;

    let bodyHtml = eTemplate.sendDepositTKN.HTML_MSG
                  .replace('[EMAIL]', email)
                  .replace('[EVM_TOKEN_AMOUNT_TOTAL]', tknAmount)
                  .replace('[EVT_TOKEN_AMOUNT]', amountEvt)
                  .replace('[COIN_TYPE]', coinType)
                  .replace('[COIN_AMOUNT]', coinAmount)
                  .replace('[EVM_TOKEN_PAR_AMOUNT]', tknParAmount)
                  .replace('[EVM_TOKEN_BONUS_AMOUNT]', tknBonusAmount)
                  .replace('[EVM_AMOUNT_DEPOSITED]', amountDeposited)
                  .replace('[HEAD_LOGO]', HEAD_LOGO);

    let bodyText = eTemplate.sendDepositTKN.PLAIN_MSG.replace('[EMAIL]', email).
        replace('[TOKEN_AMOUNT]', tknAmount);

    const data = {
      from: config.get('mail-from'),
      to: email,
      subject: subject,
      text: bodyText,
      html: bodyHtml,
    };

    sgMail.send(data).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendAddressGenerated = function(email, tknAmount, amountEvt, coinType, coinAmount, coinAddress, tknParAmount, bonusTkns) {
    const methodName = '[SendAddressGenerated]';
    const coinType1 = coinType;
    const tknAmount1 = tknAmount;

    return new Promise((resolve, reject) => {
        let subject = eTemplate.sendAddressGenerated.SUBJECT;

        let bodyHtml = eTemplate.sendAddressGenerated.HTML_MSG
            .replace('[EMAIL]', email)
            .replace('[EVM_TOKEN_AMOUNT]', tknAmount)
            .replace('[EVT_TOKEN_AMOUNT]', amountEvt)
            .replace('[COIN_TYPE]', coinType)
            .replace('[COIN_AMOUNT]', coinAmount)
            .replace('[COIN_ADDRESS]', coinAddress)
            .replace('[EVM_TOKEN_PAR_AMOUNT]', tknParAmount)
            .replace('[EVM_TOKEN_AMOUNT1]', tknAmount1)
            .replace('[COIN_TYPE1]', coinType1)
            .replace('[BONUS_TKNS]', bonusTkns)
            .replace('[HEAD_LOGO]', HEAD_LOGO);

/*        let bodyText = eTemplate.sendAddressGenerated.PLAIN_MSG
            .replace('[EMAIL]', email)
            .replace('[TOKEN_AMOUNT]', tknAmount);*/

        const data = {
            from: config.get('mail-from'),
            to: email,
            subject: subject,
        //    text: bodyText,
            html: bodyHtml,
        };

        sgMail.send(data).then((result) => {
            console.log(loggerName, methodName,
                util.format('Message successfully submitted to %s', email));
            resolve(result);
        }).catch((err) => {
            console.error(loggerName, methodName, err);
            reject(err);
        });
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
};

module.exports.sendSubscription = (email) => {
  const methodName = '[SendEmail]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendSubscription.SUBJECT;

    let bodyHtml = eTemplate.sendSubscription.HTML_MSG;

    const data = {
      from: config.get('mail-from'),
      to: email,
      subject: subject,
      text: bodyHtml,
      html: bodyHtml,
    };

    sgMail.send(data).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendWithdrawalBlockedMail = (user) => {
  const methodName = '[SendEmail]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendWithdrawalBlocked.SUBJECT;

    let bodyHtml = eTemplate.sendWithdrawalBlocked.HTML_MSG.replace(
        '[EMAIL]',
        user.email).replace('[LINK]', config.get('frontend-host'));
    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name'));
    bodyHtml += EMAIL_LOGO;
    bodyHtml += eTemplate.DISCLAIMER;
    let bodyText = eTemplate.sendWithdrawalBlocked.PLAIN_MSG.replace(
        '[EMAIL]',
        user.email).replace('[LINK]', config.get('frontend-host'));

    const data = {
      from: config.get('mail-from'),
      to: user.email,
      subject: subject,
      text: bodyText,
      html: bodyHtml,
    };

    sgMail.send(data).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', user.email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendWithdrawalActivationMail = (user) => {
  const methodName = '[SendEmail]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendWithdrawalActivate.SUBJECT;

    let bodyHtml = eTemplate.sendWithdrawalActivate.HTML_MSG.replace(
        '[EMAIL]',
        user.email).replace('[LINK]', config.get('frontend-host'));
    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name'));
    bodyHtml += EMAIL_LOGO;
    bodyHtml += eTemplate.DISCLAIMER;
    let bodyText = eTemplate.sendWithdrawalActivate.PLAIN_MSG.replace(
        '[EMAIL]',
        user.email).replace('[LINK]', config.get('frontend-host'));

    const data = {
      from: config.get('mail-from'),
      to: user.email,
      subject: subject,
      text: bodyText,
      html: bodyHtml,
    };

    sgMail.send(data).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', user.email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};

module.exports.sendReferralEmail = (user, amount) => {
  const methodName = '[sendReferralEmail]';

  return new Promise((resolve, reject) => {
    let subject = eTemplate.sendReferralNote.SUBJECT;

    let bodyHtml = eTemplate.sendReferralNote.HTML_MSG.replace(
        '[EMAIL]',
        user.email).replace('[AMOUNT]', amount);
    bodyHtml += eTemplate.FOOTER.replace('[COMPANY_NAME]',
        config.get('company_name'));
    bodyHtml += EMAIL_LOGO;
    bodyHtml += eTemplate.DISCLAIMER;
    let bodyText = eTemplate.sendReferralNote.PLAIN_MSG.replace(
        '[EMAIL]',
        user.email).replace('[AMOUNT]', amount);

    const data = {
      from: config.get('mail-from'),
      to: user.email,
      subject: subject,
      text: bodyText,
      html: bodyHtml,
    };

    sgMail.send(data).then((result) => {
      console.log(loggerName, methodName,
          util.format('Message successfully submitted to %s', user.email));
      resolve(result);
    }).catch((err) => {
      console.error(loggerName, methodName, err);
      reject(err);
    });
  }).catch((err) => {
    console.error(loggerName, methodName, err);
  });
};
