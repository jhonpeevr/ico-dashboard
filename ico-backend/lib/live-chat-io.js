'use strict';

const jwt = require('jsonwebtoken');
const config = require('config');
 const http = require('http');
// const https = require('https');
// const fs = require('fs');
// const privateKey = fs.readFileSync('./bin/ssl/iphost.key', 'utf8');
// const certificate = fs.readFileSync('./bin/ssl/iphost.crt', 'utf8');
// const credentials = {key: privateKey, cert: certificate};
// const server = https.createServer(credentials);
const io = require('socket.io').listen(config.get('live-chat-port'));
// server.listen(config.get('live-chat-port'), '0.0.0.0');

const LiveChatsModel = require('../models/LiveChatsModel');

const loggerName = '[LiveChatIO]';

io.use((socket, next) => {
  const token = socket.handshake.query.token;

  jwt.verify(token, config.get('secret'), function(err, decoded) {
    if (err) {
      if (err instanceof jwt.TokenExpiredError) {
        return next(new Error('EXPIRED'));
      }

      return next(new Error('FORBIDDEN'));
    } else {
      socket.decoded = decoded;
    }
  });
  return next();
});

io.on('connection', (socket) => {
  socket.on('live-chat-client', (message) => {
    io.emit('admin-live-chat', message);
    updateDB(message);
  });
  socket.on('live-chat-admin', (message) => {
    io.emit(message.sessionId, message);
    updateDB(message);
  });
});

function updateDB(message) {
  LiveChatsModel.findOneAndUpdate({sessionId: message.sessionId},
      {timestamp: message.timestamp}, {new: true}).
      exec().
      then((liveChat) => {
        const newMessage = {
          message: message.message,
          name: message.name,
          timestamp: message.timestamp,
        };
        if (!liveChat) {
          const newLiveChat = new LiveChatsModel({
            sessionId: message.sessionId,
            messages: [newMessage],
            timestamp: message.timestamp,
          });
          return newLiveChat.save();
        } else {
          liveChat.messages.push(newMessage);
          liveChat.markModified('messages');
          return liveChat.save();
        }
      }).
      catch((err) => {
        console.error(loggerName, err);
      });
}

module.exports = io;
