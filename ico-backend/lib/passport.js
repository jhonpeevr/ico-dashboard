'use strict';

const passport = require('passport');
const FacebookTokenStrategy = require('passport-facebook-token');
const GoogleTokenStrategy = require('passport-google-token');
const User = require('../lib/maindb').model('Users');
const config = require('config');

module.exports = () => {
    passport.use(new FacebookTokenStrategy({
            clientID: config.get('fb_client_id'),
            clientSecret: config.get('fb_secret'),
        },
        (accessToken, refreshToken, profile, done) => {
            User.upsertFbUser(accessToken, refreshToken, profile,
                (err, user) => {
                    return done(err, user);
                });
        }));

    passport.use(new GoogleTokenStrategy.Strategy({
            clientID: config.get('google_client_id'),
            clientSecret: config.get('google_secret'),
        },
        (accessToken, refreshToken, profile, done) => {
            User.upsertGoogleUser(
                accessToken, refreshToken, profile,
                (err, user) => {
                    return done(err, user);
                });
        }
    ));
};
