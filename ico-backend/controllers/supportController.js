'use strict';

const httpStatuses = require('http-status-codes');
const mailgun = require('../lib/mailgun');
const SupportModel = require('../lib/maindb').model('Support');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const validator = require('validator');

const loggerName = '[SupportController]';

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * @param {String} req.params.email
 */
module.exports.supportMail = (req, res) => {

    const methodName = '[Support]';

    const email = req.body.email;
    const supportIssue = req.body.supportIssue;
    const supportDesc = req.body.supportDesc;

    if (!validator.isEmail(email) || !supportDesc || !supportDesc){
        throw new InvalidInputParametersError();
    }

    const newSupportTicket = new SupportModel({
        email: email,
        supportIssue: supportIssue,
        supportDesc: supportDesc,
    });

    newSupportTicket.save().then((supportTicket) => {
        mailgun.sendSupportMail(supportTicket.email,
            '[' + supportTicket.email + '][' + supportTicket.supportIssue + ']', supportTicket.supportDesc).
        then(() => {
            console.log(loggerName, methodName, 'Support Ticket from',
                supportTicket.email);
            return res.send({msg: 'Support Mail successfully sent'});
        });
    }).
    catch((err) => {
        console.error(loggerName, err);
        return res.status(httpStatuses.BAD_REQUEST).send({err: 'BAD REQUEST'});
    });
};
