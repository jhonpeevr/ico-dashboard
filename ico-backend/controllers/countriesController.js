'use strict';

const countriesModel = require('../lib/maindb').model('Countries');

/**
 * Function returns all the countries
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 *
 */
module.exports.getAllCountries = (req, res, next) => {
  countriesModel.find({}).sort({countryName: 1}).exec().then((countries) => {
    return res.send(countries);
  }).catch(next);
};
