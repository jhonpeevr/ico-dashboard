'use strict';

const Promise = require('bluebird');
const bitgo = require('../lib/bitgo');
const config = require('config');
const httpStatus = require('http-status-codes');
const CoinAddressesModel = require('../lib/maindb').model('CoinAddresses');
const CurrenciesModel = require('../lib/maindb').model('Currencies');
const ExchangeRatesModel = require('../lib/maindb').model('ExchangeRates');
const CurrentRatesUsd = require('../lib/maindb').model('CurrentRatesUsd');
const BonusPerStageModel = require('../lib/maindb').model('BonusPerStage');
const CreateAddressModel = require('../lib/maindb').model('CreateAddress');
const emitter = require('../lib/emitter');
const UsersModel = require('../lib/maindb').model('Users');
const ConfigsModel = require('../lib/maindb').model('Configs');
const mathLib = require('../lib/mathematics');
const MaxAmountExceededError = require('../errors/MaxAmountExceededError');
const InvalidInputParametersError = require('../errors/InvalidInputParametersError');
const CoinTypeNotSupportedError = require('../errors/CoinTypeNotSupportedError');
const UserNotFoundError = require('../errors/UserNotFoundError');

const TokensNotEnoughError = require('../errors/TokensNotEnoughError');
const MinAmountNotReachedError = require('../errors/MinAmountNotReachedError');
const ExchangeRateModifiedError = require('../errors/ExchangeRateModifiedError');
const CoinAddressAlreadyGeneratedError = require('../errors/CoinAddressAlreadyGeneratedError');
const DepositDisabledError = require('../errors/DepositDisabledError');
const ICOEndedError = require('../errors/ICOEndedError');
const ICOPausedError = require('../errors/ICOPausedError');
const UnknownError = require('../errors/UnknownError');
const AddressGenerationError = require('../errors/AddressGenerationError');


const loggerName = '[CoinInvestController]';
const FinancialHistoriesModel = require('../lib/maindb').model('FinancialHistories');


/**
 * Function generates address for client to send coins
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @return {undefined}
 *
 * @param {Number} req.body.coinAmount - Expecting amount of coins
 * @param {String} req.body.coinType - Expecting coin type
 *
 * @param {String} req.decoded._id - current user id
 *
 */

module.exports.generateAddress = async (req, res, next) => {

    const methodName = '[GenerateAddress]';

    /* User inputs */
    const coinAmount = req.body.coinAmount;
    const coinType = req.body.coinType;
    const tknAmount = req.body.tknAmount;

    /* Get current user from decoded through jsonwebtoken */

    const userId = req.decoded._id;

    console.log(loggerName, methodName, coinType, coinAmount, tknAmount);

    try {
        let isIcoEnd;
        let isIcoPaused;
        let normalBonus;
        let stageBonus;

        let configs = await ConfigsModel.find();

        await configs.map((data) => {
            if (data.key === 'isIcoEnd') {
                isIcoEnd = data.valueBoolean;
            }
            if (data.key === 'isIcoPaused') {
                isIcoPaused = data.valueBoolean;
            }
            if (data.key === 'normalBonus') {
                normalBonus = data.valueBoolean;
            }
            if (data.key === 'stageBonus') {
                stageBonus = data.valueBoolean;
            }
        });

        if (isIcoEnd) {
            return next(new ICOEndedError());
            //return res.send({'success': true,'msg': 'ICO end','isICOEnd': true});
        } else if (isIcoPaused) {
            return next(new ICOPausedError());
            //return res.send({'success': true,'msg': 'ICO paused','isICOPaused': true });
        } else {

            if (!coinAmount || !coinType || config.get('coins').indexOf(coinType) === -1) { // validate all inputs
                return next(new InvalidInputParametersError());
                //return res.send({'success': true,'msg': 'Invalid input parameters' });
            }

            if (!config.get('deposit-enabled')) {  // check investment is allowed
                return next(new DepositDisabledError());
                //return res.send({'success': true,'msg': 'Deposit disabled' });
            }

            const currentUser = await UsersModel.findOne({ _id: userId });

            if (!currentUser) {
                return next(new UserNotFoundError());
                //return res.send({'success': true,'msg': 'User not found in the system' });
            }

            const currencyCOIN = await CurrenciesModel.findOne({ abbr: coinType });
            const currencyTKN = await CurrenciesModel.findOne({ abbr: 'tkn' });
            const minInvest = await ConfigsModel.findOne({ key: 'minInvest' });
            const autoEthAddData = await ConfigsModel.findOne({ key: 'autoEthGenerateBitgo' });
            let autoEthAdd;
            if (autoEthAddData) {
                autoEthAdd = autoEthAddData['valueString'] === 'true';
            }

            if (mathLib.isLessThan(tknAmount,minInvest.valueDouble)) {
                return next(new MinAmountNotReachedError());
                //return res.send({'success': true,'msg': 'Minimum amount (' + currencyTKN.minAmount + ') is not reached' });
            }
            const queryStr = { userId: userId, status: 'pending', currencyId: currencyCOIN._id };

            const coinAddresses = await CoinAddressesModel.find(queryStr);

            if (coinAddresses.length > 1) { // make sure address for coinType has not been generated in 2 hours
                return next(new CoinAddressAlreadyGeneratedError());
                //return res.send({'success': true,'err': 'You have already generated 2 addresses for the token transfer. Kindly complete those transactions first' });
            }
            // take exchange rates
            const exchangeRatesQuery = { currencyIdFirst: currencyCOIN._id, currencyIdSecond: currencyTKN._id };

            const exchangeRate = await ExchangeRatesModel.findOne(exchangeRatesQuery).lean();
            let tokenExchangeRate;
            let tokenBonus;

            const soldTokensData = await ConfigsModel.findOne({ key: 'soldTokens' }).lean();
            const normalBonusData = await ConfigsModel.findOne({ key: 'normalBonusPercentage' }).lean();

            const soldTokens = soldTokensData.valueDouble;
            const normalBonusPercentage = normalBonusData.valueDouble;
            let bonusPercentage = 0;

            tokenExchangeRate = (exchangeRate.exchangeRate);

            if (normalBonus && (normalBonusPercentage > 0)) { // normal bonus is applicable
                // tokenBonus = ((exchangeRate.exchangeRate) * ((normalBonusPercentage) / 100));
                // tokenExchangeRate = (exchangeRate.exchangeRate) + tokenBonus;
                bonusPercentage = normalBonusPercentage;

            } else if (stageBonus) { // token stage bonus is applicable
                const stageBonusData = await BonusPerStageModel.findOne({ $and: [{ minTokens: { $lte: soldTokens } }, { maxTokens: { $gte: soldTokens } }] });
                if (stageBonusData) {
                    // tokenBonus = ((exchangeRate.exchangeRate) * ((stageBonusData.percentage) / 100));
                    // tokenExchangeRate = (exchangeRate.exchangeRate) + tokenBonus;
                    bonusPercentage = stageBonusData.percentage;
                } 
            }

            const afterBonusTokens =parseFloat(mathLib.valueOf( mathLib.safeAdd(tknAmount, mathLib.safeDivide(mathLib.safeMultiply(bonusPercentage, tknAmount) ,100 ))))


            const config_ = await ConfigsModel.findOne({ key: 'availableTokens' }).lean();

            if (mathLib.isLessThan(config_.valueDouble,mathLib.safeMultiply(coinAmount,tokenExchangeRate))) {
                return next(new TokensNotEnoughError());
                //return res.send({'success': true,'err': 'Token amount is not enough'});
            }


            if (mathLib.isGreaterThan(mathLib.safeSubtract(mathLib.safeMultiply(coinAmount,tokenExchangeRate),tknAmount),5)) { // difference between last time send and current is greater than 5
                return next(new ExchangeRateModifiedError());
                //return res.send({'success': true,'msg': 'Exchange rate has been updated' });
            }

            if (coinType === 'teth' || coinType === 'eth') {
                const wallet = await bitgo.coin(coinType).wallets().get({ 'id': config.get('bitgo-' + coinType + '-wallet-id') });  // gets eth wallet

                if (autoEthAdd) {
                    // to check if config has auto TETH/ETH address generation allowed for every time add generation request comes
                    try {
                        await wallet.createAddress();
                    }catch(err) {
                        console.log("error generating new address")
                    } 
                }
                // const addresses = await wallet.addresses({'limit': 1000});
                const addresses = await CreateAddressModel.findOne({ $and: [{ addressStatus: 'notused' }, { coinName: coinType }] });

                if (!addresses) {
                    return next(new AddressGenerationError());
                }
                const address = addresses.coinAddress;
                const coinAddress = new CoinAddressesModel();

                console.log(loggerName, methodName, address);
                coinAddress.address = address;
                coinAddress.coinAmount = coinAmount;
                coinAddress.userId = currentUser._id;
                coinAddress.currencyId = currencyCOIN._id;
                coinAddress.tknAmount = tknAmount;
                coinAddress.currencyAbbr = currencyCOIN.abbr;
                coinAddress.exchangeRateValue = tokenExchangeRate;
                coinAddress.bonusPercentage = bonusPercentage;
                coinAddress.afterBonusTokens = afterBonusTokens;
                const newAddress = await coinAddress.save();
                return res.send(newAddress);

            } else {


                let userWalletLimit = config.get('bitgo-' + coinType + '-wallet-id').length;
                let randomNumber = Math.floor(Math.random() * userWalletLimit) + 1;
                let wallet, address;
                try {
                    wallet = await bitgo.coin(coinType).wallets().get({ 'id': config.get('bitgo-' + coinType + '-wallet-id')[randomNumber - 1] }); // Coin Type is bitcoin or test bitcoin
                    address = await wallet.createAddress();
                } catch (err) {
                    return next(new AddressGenerationError());
                }

                const coinAddress = new CoinAddressesModel();

                coinAddress.address = address.address
                coinAddress.coinAmount = coinAmount;
                coinAddress.userId = currentUser._id;
                coinAddress.currencyId = currencyCOIN._id;
                coinAddress.tknAmount = tknAmount;
                coinAddress.currencyAbbr = currencyCOIN.abbr;
                coinAddress.exchangeRateValue = tokenExchangeRate;
                coinAddress.bonusPercentage = bonusPercentage;
                coinAddress.afterBonusTokens = afterBonusTokens;

                const savedAddress = await coinAddress.save();
                return res.send(savedAddress);
            }
        }
    } catch (err) {
        return next(new UnknownError());
    }
};
/**
 * Function returns pending addresses that has
 * been generated in two hours for current user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.coinType - when not specified, returns all types;
 * @param {String} req.decoded._id - current user id
 *
 */
module.exports.generatedAddresses = (req, res, next) => {
    const methodName = '[GeneratedAddresses]';
    const coinType = req.params.coinType;

    if (!coinType) {
        return next(new InvalidInputParametersError());
    }

    if (coinType !== 'all' && config.get('coins').indexOf(coinType) === -1) {
        return next(new CoinTypeNotSupportedError());
    }

    if (coinType === 'all') {
        const queryStr = {
            userId: req.decoded._id,
            //status: 'pending',
            // timestamp: {
            //   $gte: new Date(Date.now() -
            //     (config.get('active_address_waiting_time') * 60000)),
            // },
        };

        CoinAddressesModel.find(queryStr).populate('currencyId').sort('-timestamp').exec().then((addresses) => {
            return res.send(addresses);
        }).catch(next);
    } else {
        CurrenciesModel.findOne({ abbr: coinType }).exec().then((currencyCOIN) => {
            if (!currencyCOIN) {
                console.error(loggerName, methodName, coinType, 'not supported');
                return next(new CoinTypeNotSupportedError());
            }
            const queryStr = {
                userId: req.decoded._id,
                currencyId: currencyCOIN._id,
                // status: 'pending',
                // timestamp: {
                //   $gte: new Date(Date.now() -
                //     (config.get('active_address_waiting_time') * 60000)),
                // },
            };

            return CoinAddressesModel.find(queryStr).sort('-timestamp').populate('currencyId').exec().then((addresses) => {
                return res.send(addresses);
            });
        }).catch((err) => {
            console.error(loggerName, methodName, err.stackTrace);
            console.error(loggerName, methodName, err);
        });
    }
};

/**
 * Function returns generated address by addressId
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.addressId - address generated id
 */
module.exports.getGeneratedAddress = (req, res, next) => {
    CoinAddressesModel.findById(req.params.addressId).populate('userId').populate('currencyId').exec().then((accounts) => {
        return res.send(accounts);
    }).catch(next);
};

/**
 * Function returns all generated addresses that has been generated
 * Protected by admin route
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @param {Number} req.params.perPage
 * @param {String} req.query.query
 *
 * @return {undefined}
 *
 */
module.exports.listGeneratedAddresses = (req, res, next) => {
    let query;
    if (req.query.query && req.query.query !== '') {
        let searchData = req.query.query;
        query = {
            '$or': [
                { 'address': new RegExp(searchData, 'i') },
                { 'currencyAbbr': new RegExp(searchData, 'i') },
            ],
        };
    } else {
        query = {};
    }
    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);
    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        CoinAddressesModel.find(query).skip(skip).limit(perPage).populate('currencyId').populate('userId').exec().then((btcAddresses) => {
            return res.send(btcAddresses);
        }).catch(next);
    } else {
        CoinAddressesModel.find(query).populate('currencyId').populate('userId').exec().then((btcAddresses) => {
            return res.send(btcAddresses);
        }).catch(next);
    }
};


/**
 * Function returns all generated addresses that has been generated by a particular user
 * Protected by admin route
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @param {Number} req.params.perPage
 * @param {String} req.query.query
 *
 * @return {undefined}
 *
 */
module.exports.listGeneratedUserAddresses = (req, res, next) => {
    const userId = req.params.userId;
    const query = { userId: userId };

    /*let query;
    const userId = parseInt(req.params.userId);
    if (req.query.query && req.query.query !== '') {
        let searchData = req.query.query;
        query = {
            '$and': [
                {userId: userId},
                {'$or': [
                    {'address': new RegExp(searchData, 'i')},
                    {'currencyAbbr': new RegExp(searchData, 'i')},
                    ],
                },
            ],
        };
    } else {
        query = {userId: userId};
    }*/

    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);
    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        CoinAddressesModel.find(query).skip(skip).limit(perPage).populate('currencyId').populate('userId').exec().then((btcAddresses) => {
            CoinAddressesModel.count({ userId: userId }).exec().then((countAddresses) => {
                return res.send({
                    count: countAddresses,
                    addresses: btcAddresses,
                });
            }).catch(next);
        }).catch(next);
    } else {
        CoinAddressesModel.find(query).populate('currencyId').populate('userId').exec().then((btcAddresses) => {
            CoinAddressesModel.count({ userId: userId }).exec().then((countAddresses) => {
                return res.send({
                    count: countAddresses,
                    addresses: btcAddresses,
                });
            }).catch(next);
        }).catch(next);
    }
};

module.exports.statistics = (req, res, next) => {
    const totalBtcEntered = new Promise((resolve) => {
        return CoinAddressesModel.aggregate([
            { '$match': { 'currencyAbbr': 'btc' } },
            { $group: { _id: null, total: { $sum: '$coinAmount' } } }]).exec().then((sum) => {
                resolve({ totalBtcEntered: sum[0] ? sum[0].total : 0 });
            });
    });
    const totalBtcReceived = new Promise((resolve) => {
        return CoinAddressesModel.aggregate([
            { '$match': { 'currencyAbbr': 'btc' } },
            { $group: { _id: null, total: { $sum: '$coinResult' } } }]).exec().then((sum) => {
                resolve({ totalBtcReceived: sum[0] ? sum[0].total : 0 });
            });
    });
    const totalLtcEntered = new Promise((resolve) => {
        return CoinAddressesModel.aggregate([
            { '$match': { 'currencyAbbr': 'ltc' } },
            { $group: { _id: null, total: { $sum: '$coinAmount' } } }]).exec().then((sum) => {
                resolve({ totalLtcEntered: sum[0] ? sum[0].total : 0 });
            });
    });
    const totalLtcReceived = new Promise((resolve) => {
        return CoinAddressesModel.aggregate([
            { '$match': { 'currencyAbbr': 'ltc' } },
            { $group: { _id: null, total: { $sum: '$coinResult' } } }]).exec().then((sum) => {
                resolve({ totalLtcReceived: sum[0] ? sum[0].total : 0 });
            });
    });
    const totalEthEntered = new Promise((resolve) => {
        return CoinAddressesModel.aggregate([
            { '$match': { 'currencyAbbr': 'eth' } },
            { $group: { _id: null, total: { $sum: '$coinAmount' } } }]).exec().then((sum) => {
                resolve({ totalEthEntered: sum[0] ? sum[0].total : 0 });
            });
    });
    const totalEthReceived = new Promise((resolve) => {
        return CoinAddressesModel.aggregate([
            { '$match': { 'currencyAbbr': 'eth' } },
            { $group: { _id: null, total: { $sum: '$coinResult' } } }]).exec().then((sum) => {
                resolve({ totalEthReceived: sum[0] ? sum[0].total : 0 });
            });
    });
    const totalGeneratedAddresses = new Promise((resolve) => {
        return CoinAddressesModel.count({}).exec().then((nbr) => {
            resolve({ totalGeneratedAddresses: nbr });
        });
    });
    const totalPendingAddresses = new Promise((resolve) => {
        return CoinAddressesModel.count({ status: 'pending' }).exec().then((nbr) => {
            resolve({ totalPendingAddresses: nbr });
        });
    });
    const totalProcessedAddresses = new Promise((resolve) => {
        return CoinAddressesModel.count({ status: 'processed' }).exec().then((nbr) => {
            resolve({ totalProcessedAddresses: nbr });
        });
    });
    Promise.all([
        totalBtcEntered,
        totalBtcReceived,
        totalLtcEntered,
        totalLtcReceived,
        totalEthEntered,
        totalEthReceived,
        totalGeneratedAddresses,
        totalPendingAddresses,
        totalProcessedAddresses]).then((data) => {
            let statistics = {};
            for (let stat of data) {
                statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
            }
            return res.send(statistics);
        }).catch(next);
};

/**
 * Function assign tokens to user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @return {undefined}
 *
 * @param {Number} req.body.tokenAmount - Expecting amount of coins
 * @param {String} req.body.id - Expecting coin type
 *
 * @param {String} req.decoded._id - current user id
 *
 */
module.exports.assignTokens = (req, res, next) => {
    const tokens = req.body.tokens;
    const bonusAmount = req.body.bounsAmount;
    const id = req.body.id;

    if (!tokens) {
        throw new InvalidInputParametersError();
    }
    getConfigsMap().then((configMap) => {
        if (mathLib.isGreaterThan(tokens,configMap.availableTokens.value)) {
            throw new MaxAmountExceededError();
        }
        let availableTokens = parseFloat(mathLib.valueOf(mathLib.safeSubtract(configMap.availableTokens,tokens)));
        let soldTokens = parseFloat(mathLib.valueOf(mathLib.safeAdd(configMap.soldTokens.value,tokens)));
        UsersModel.findById(id).exec()
            .then((user) => {
                if (!user) {
                    return next(new UserNotFoundError());
                }
                return CurrenciesModel.findOne({ abbr: 'tkn' }).then((currencyTKN) => {
                    const depositCOIN = new FinancialHistoriesModel({
                        userId: user._id,
                        amount: tokens,
                        exchangeRateValue: 1,
                        currencyAbbr: currencyTKN.abbr,
                        currencyName: currencyTKN.name,
                        currencyId: currencyTKN._id,
                        type: 'admin_deposit',
                    });
                    return depositCOIN.save().then(() => {
                        const investCOIN = new FinancialHistoriesModel({
                            userId: user._id,
                            amount: tokens,
                            exchangeRateValue: 1,
                            currencyAbbr: currencyTKN.abbr,
                            currencyName: currencyTKN.name,
                            currencyId: currencyTKN._id,
                            type: 'invest',
                        });
                        return investCOIN.save().then(() => {
                            //  return systemQueue.add(() => {
                            Promise.all([
                                ConfigsModel.findOneAndUpdate({ key: 'availableTokens' }, { valueDouble: availableTokens }, { new: true }),
                                ConfigsModel.findOneAndUpdate({ key: 'soldTokens' }, { valueDouble: soldTokens }, { new: true })]).then(() => {
                                    user.balanceTKN =parseFloat(mathLib.valueOf(mathLib.safeAdd(user.balanceTKN,tokens)));
                                    user.markModified('balanceTKN');
                                    user.save().then((user) => {
                                        return calculateBonus(user, bonusAmount, currencyTKN).then((user) => {
                                            return user;
                                        });
                                    }).then((user) => {
                                        emitter.emit('user-token-updated', user);
                                        emitter.emit('balance-changed', user);
                                        emitter.emit('fh-invest-reload', user._id);
                                        return res.send({
                                            'success': true,
                                            'msg': 'Token assigned successfully',
                                        });
                                    }).catch(next);
                                }).catch(next);
                        }).catch(next);
                        //  });
                    }).catch(next);
                }).catch(next);
            }).catch(next);
    }).catch(next);
};

const getConfigsMap = () => {
    return ConfigsModel.find({}).exec().then((configs) => {
        return convertToMap(configs);
    });
};

const convertToMap = (configs) => {
    const map = {};
    for (const c of configs) {
        if (c.valueString) {
            map[c.key] = c.valueString;
        } else if (c.valueDate) {
            map[c.key] = c.valueDate;
        } else if (c.valueDouble || c.valueDouble === 0) {
            map[c.key] = c.valueDouble;
        } else {
            throw new Error('No config type');
        }
    }
    return map;
};

module.exports.convertToMap = convertToMap;
module.exports.getConfigsMap = getConfigsMap;

module.exports.userAddressCount = (req, res, next) => {
    const id = req.params.id;
    CoinAddressesModel.count({ userId: id }).exec().then((addresscount) => {
        return res.send({ count: addresscount });
    }).catch(next);
};


/**
 * Function to calculate and transfer user bonus
 *
 * @param {Object} user
 * @param {Object} bonusAmount
 * @param {Object} currencyTKN
 *
 *
 */

const calculateBonus = (user, bonusAmount, currencyTKN) => {
    return new Promise((resolve, reject) => {
        let token = bonusAmount;
        const bonusHistory = new FinancialHistoriesModel(
            {
                userId: user._id,
                amount: token,
                exchangeRateValue: 1,
                currencyId: currencyTKN._id,
                currencyAbbr: currencyTKN.abbr,
                currencyName: currencyTKN.name,
                type: 'bonus',
            });
        bonusHistory.save().then(() => {
            UsersModel.findOneAndUpdate({ _id: user._id },
                {
                    balanceTKN: parseFloat(mathLib.valueOf(mathLib.safeAdd(user.balanceTKN,token))),
                }).then(() => {
                    resolve(user);
                });

        }).catch((err) => reject(err));
    });
};