'use strict';

const httpStatuses = require('http-status-codes');
const mailgun = require('../lib/mailgun');
const SubscriptionsModel = require('../lib/maindb').model('Subscriptions');

const loggerName = '[SubscriptionController]';

/**
 *
 * @param {Object} req
 * @param {Object} res
 *
 * @param {String} req.params.email
 */
module.exports.subscribe = (req, res) => {
  const methodName = '[Subscribe]';
  const email = req.params.email;

  SubscriptionsModel.findOne({email: email}).exec().
      then((subscription) => {
        if (subscription) {
          return res.status(httpStatuses.BAD_REQUEST).send({
            err: 'Email address already has been registered',
          });
        }

        return new SubscriptionsModel({email: email}).save().
            then(() => {
              return mailgun.sendSubscriptionMailGun(email).
                  then((result) => {
                    console.log(loggerName, methodName, result);
                    return res.send({msg: 'Successfully subscribed'});
                  });
            });
      }).
      catch((err) => {
        console.error(loggerName, err);
        return res.status(httpStatuses.BAD_REQUEST).send({err: 'BAD REQUEST'});
      });
};

module.exports.getAllSubscriptions = (req, res) => {
  SubscriptionsModel.find({}).exec().
      then((subscriptions) => {
        return res.send(subscriptions);
      }).catch((err) => {
    console.error(loggerName, err);
    return res.status(httpStatuses.BAD_REQUEST).send({err: 'BAD REQUEST'});
  });
};
