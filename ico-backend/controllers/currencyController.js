'use strict';

const CurrenciesModel = require('../lib/maindb').model('Currencies');

/**
 * Function returns all the currencies
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 */
module.exports.getCurrencies = (req, res, next) => {
    CurrenciesModel.find({})
        .exec()
        .then((currencies) => {
            return res.send(currencies);
        })
        .catch(next);
};

