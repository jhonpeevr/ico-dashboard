'use strict';

const bonusModel = require('../lib/maindb').model('Bonus');

module.exports.bonusEVTData = (req, res, next) => {

    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);

    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        bonusModel.find({}).skip(skip).limit(perPage).exec().then((bonusData) => {
            return res.send(bonusData);
        }).catch(next);
    } else {
        bonusModel.find({}).exec().then((bonusData) => {
            return res.send(bonusData);
        }).catch(next);
    }
};

module.exports.bonusEVTStats = (req, res, next) => {
    const now = new Date();
    bonusModel.findOne({
        $and: [
            {startDate: {$lte: now}},
            {endDate: {$gte: now}}],
    }).exec().then((bonusStats) => {
        return res.send(bonusStats);
    }).catch(next);

};

