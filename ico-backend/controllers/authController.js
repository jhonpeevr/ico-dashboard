'use strict';

const config = require('config');
const UsersModel = require('../lib/maindb').model('Users');
const UserInfoModel = require('../lib/maindb').model('UserInfo');
const CaptchaModel = require('../lib/maindb').model('CaptchaSessions');
const jwt = require('jsonwebtoken');
const sha512 = require('js-sha512').sha512;
const httpStatus = require('http-status-codes');
const speakeasy = require('speakeasy');
const validator = require('validator');
const randomstring = require('randomstring');
const request = require('request');
const mailgun = require('../lib/mailgun');
const sendgrid = require('../lib/mail');

/* Error Classes */
const UserNotFoundError = require('../errors/UserNotFoundError');
const AccountBlockedError = require('../errors/AccountBlockedError');
const PasswordMatchError = require('../errors/PasswordMatchError');
const PasswordModificationNotAllowedError = require('../errors/PasswordModificationNotAllowedError');
const CaptchaError = require('../errors/CaptchaError');
const InvalidTwoFactorAuthError = require(
    '../errors/InvalidTwoFactorAuthError');
const EmailOrPasswordIncorrectError = require(
    '../errors/EmailOrPasswordIncorrectError');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const EmailAddressAlreadyExistsError = require(
    '../errors/EmailAddressAlreadyExistsError');
const EmailAlreadyActivatedError = require(
    '../errors/EmailAlreadyActivatedError');
const ActivationCodeExpired = require(
    '../errors/ActivationCodeExpired');

const loggerName = '[AuthController]';
const requestPromise = require('request-promise');

const googleCaptchaSecretKey = config.get('googleCaptchaSecretKey');

const sha256 = require('js-sha256');
const io = require('../lib/io');
const emitter = require('../lib/emitter');

/**
 * Function signes up user and creates accounts
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.email
 * @param {String} req.body.password
 * @param {String} req.body.ci
 * @param {String} req.body.text
 *
 */
module.exports.signup = (req, res, next) => {
    const methodName = '[SignUp]';

    const fullName = req.body.fullName;
    const email = req.body.email;
    const password = req.body.password;
    const googleCaptchaCheckCode = req.body.googleCaptchaCheckCode;
    const emailActivationCode = req.body.emailActivationCode;

    if (!email || !password || !validator.isEmail(email) || !fullName) {
        throw new InvalidInputParametersError();
    }

    /*  if (config.get('captcha-enabled') && (!captchaHash || !captchaInput)) {
        throw new CaptchaError(email);
      }*/

    if (!googleCaptchaCheckCode) {
        throw new CaptchaError(email);
    }

    console.log(loggerName, methodName, email);

    // req.connection.remoteAddress will provide IP address of connected user
    const verificationUrl = 'https://www.google.com/recaptcha/api/siteverify?secret=' + googleCaptchaSecretKey + '&response=' + googleCaptchaCheckCode;
    console.log(verificationUrl)

    requestPromise.post(verificationUrl, (error, response, body) => {
        body = JSON.parse(body);
        if (body.success === 'undefined' && body.success !== 'true') {
            throw new CaptchaError(email);
        }

        //  CaptchaModel.findOne({sessionId: captchaHash}).exec().then((captcha) => {
        //    if (config.get('captcha-enabled') && (!captcha || !captcha.value
        //           || captcha.value !== captchaInput)) {
        //     throw new CaptchaError(email);
        //   }

        return googleCaptchaCheckCode;
    }).then(() => {
        if (req.body.referralCode) {
            return UsersModel.findOne({referralCode: req.body.referralCode}).exec().then((user) => {
                if (!user) {
                    return "";
                } else {
                    return user.id;
                }

            });
        }
        return;
    }).then((referral) => {
        return UsersModel.findOne({email: email}).exec().then((user) => {
            if (user) {
                throw new EmailAddressAlreadyExistsError();
            }

            let userObj = {
                fullName: fullName,
                email: email,
                password: sha512(password),
                isUserNew: true,
                activationCode: emailActivationCode,
                referralCode: randomstring.generate({length: 64})
            };

            console.log("@@@@@@@@"+referral)
            if (referral) {
                userObj.referBy = referral;
                io.emit('referralJoin', {referralCode:req.body.referralCode,message:fullName.toUpperCase()+" Joined with your referral"});
            }

            console.log(userObj)
            const newUser = new UsersModel(userObj);

            return newUser.save().then((user) => {
                mailgun.sendActivationCodeMailGun(user.email, user.fullName, user.activationCode).then(() => {
                    console.log(loggerName, methodName, 'Send activation code to',
                        user.email);
                    // console.log('[Mail Chimp]', user.email);
                    // const ApiKey = '7e0629cede4860951b1a6d75da898aed-us17';
                    // const listId = '7064fd3e23';
                    // const url = 'https://us17.api.mailchimp.com/3.0/lists/' + listId + '/members/';
                    // let options = {
                    //     url: url,
                    //     headers: {
                    //         'Content-Type': 'application/json',
                    //         'Authorization': 'Basic ' +
                    //         new Buffer('user:' + ApiKey).toString(
                    //             'base64'),
                    //     },
                    //     json: {
                    //         'email_address': user.email, 'status': 'subscribed',
                    //         'merge_fields': {
                    //             'FNAME': user.firstName,
                    //             'LNAME': user.lastName,
                    //         },
                    //     },
                    // };
                    // request.post(options, (error, response, body) => {
                    //     return res.send({msg: 'Successfully registered'});
                    // });
                    if (referral) {
                        emitter.emit('referralJoinReload', user);
                        emitter.emit('user-count-changed');
                    }
                    return res.send({msg: 'Successfully registered'});
                });
            });
        });
    }).catch(next);
};

/**
 * Function activates user's email address
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {{Object}} req.params
 * @param {String} req.params.email
 * @param {String} req.param.activationCode
 */
module.exports.activateEmail = (req, res, next) => {
    const methodName = '[ActivateEmail]';

    const email = req.params.email;
    const activationCode = req.params.activationCode;

    if (!email || !activationCode || !validator.isEmail(email)) {
        throw new InvalidInputParametersError();
    }

    console.log(loggerName, methodName, email, activationCode);

    const updateQuery = {
        active: true
    };
    UsersModel.findOne({email: email, active: true}).exec().then((user) => {
        if (user) {
            throw new EmailAlreadyActivatedError();
        }

        return UsersModel.findOneAndUpdate({email: email, activationCode: activationCode},
            updateQuery, {new: true}).exec().then((user) => {
            if (!user) {
                throw new UserNotFoundError();
            }
            return res.send({msg: 'User activated'});
        });
    }).catch(next);

};


/**
 * Function resends user activation mail
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {{Object}} req.params
 * @param {String} req.params.email
 * @param {String} req.param.activationCode
 */
module.exports.activateEmailResend = (req, res, next) => {
    const methodName = '[ActivateEmailResend]';

    const emailResend = req.body.emailResend;
    const activationCodeResend = req.body.activationCodeResend;

    // console.log('emailResend ', emailResend);
    // console.log('activationCodeResend ', activationCodeResend);

    if (!emailResend || !activationCodeResend || !validator.isEmail(emailResend)) {
        throw new InvalidInputParametersError();
    }

    console.log(loggerName, methodName, emailResend, activationCodeResend);

    UsersModel.findOne({emailResend: emailResend, active: true}).exec().then((user) => {
        if (user) {
            throw new EmailAlreadyActivatedError();
        }
        return sendgrid.sendActivationCodeAgain(emailResend, activationCodeResend).then((user) => { // change this to send grid
            if (!user) {
                throw new UserNotFoundError();
            }
            return res.send({msg: 'Activation Mail sent'});
        });
    }).catch(next);

};


/**
 * Function is used to reset password
 * Receives email address, generates code, activates
 * user on receiving activation
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.email
 * @param {String} req.body.ci - captcha input
 * @param {String} req.body.text - captcha session hash
 *
 */
module.exports.forgotPassword = (req, res, next) => {
    const methodName = '[ForgotPassword]';

    const email = req.body.email;

    /* Captcha protection */
    // const captchaInput = req.body.ci;
    // const captchaHash = req.body.text;

    const googleCaptchaResponseForgotPass = req.body.googleCaptchaResponseForgotPass;

    if (!email || !validator.isEmail(email)) {
        throw new InvalidInputParametersError();
    }

    /*
      if (config.get('captcha-enabled') && (!captchaHash || !captchaInput)) {
        throw new CaptchaError(email);
      }
    */

    if (!googleCaptchaResponseForgotPass) {
        throw new CaptchaError(email);
    }

    console.log(loggerName, methodName, email);

    const verificationUrl = 'https://www.google.com/recaptcha/api/siteverify?secret=' + googleCaptchaSecretKey + '&response=' + googleCaptchaResponseForgotPass;

    /*
      CaptchaModel.findOne({sessionId: {$eq: captchaHash}}).
          exec().
          then((captcha) => {
            if (config.get('captcha-enabled') && (!captcha || !captcha.value
                    || captcha.value !== captchaInput)) {
              throw new CaptchaError(email);
            }
              return captcha;
    */
    requestPromise.post(verificationUrl, (error, response, body) => {
        body = JSON.parse(body);
        if (body.success === 'undefined' && body.success !== 'true') {
            throw new CaptchaError(email);
        }
        return googleCaptchaResponseForgotPass;
    }).then(() => {
        return UsersModel.findOne({email: email, provider: 'local'}).exec().then((user) => {
            if (!user) {
                throw new UserNotFoundError();
            }

            return user;
        }).then((user) => {
            return mailgun.sendForgotPasswordMailGun(user.email, user.fullName, user.activationCode).then(() => {
                return res.send(
                    {
                        msg: 'Password reset instructions ' +
                        'has been sent to your email address',
                    });
            });
        });
    }).catch(next);
};

/**
 * Function is used for forgot-password
 * Receives email address, generates code, activates user
 * on receiving activation
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.activationCode
 * @param {String} req.body.email
 * @param {String} req.body.password
 * @param {String} req.body.ci - captcha input
 * @param {String} req.body.text - captcha session hash
 *
 */
    // generate random string.
let randomString = (len) => {
    let charSet = null;
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let randomString = '';
    for (let i = 0; i < len; i++) {
        let randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
};

module.exports.changePassword = (req, res, next) => {
    const activationCode = req.body.activationCode;
    const email = req.body.email;
    const password = req.body.password;
    const googleCaptchaResponseChangePass = req.body.googleCaptchaResponseChangePass;

    /* Captcha protection */
    /*const captchaInput = req.body.ci;
    const captchaHash = req.body.text;*/

    if (!email || !validator.isEmail(email) || !password) {
        throw new InvalidInputParametersError();
    }

    if (!googleCaptchaResponseChangePass) {
        throw new CaptchaError(email);
    }

    /*if (config.get('captcha-enabled') && !captchaHash || !captchaInput) {
        throw new CaptchaError(email);
    }*/

    const updateQuery = {
        activationCode: randomString(128),
        password: sha512(password),
    };

    /*CaptchaModel.findOne({sessionId: {$eq: captchaHash}}).exec().then((captcha) => {
        if (config.get('captcha-enabled') && (!captcha || !captcha.value
                || captcha.value !== captchaInput)) {
            throw new CaptchaError(email);
        }

        return captcha;
    }).then(() => {*/
    UsersModel.findOneAndUpdate(
        {email: email, activationCode: activationCode}, updateQuery,
        {new: true}).exec().then((user) => {
        if (!user) {
            throw new ActivationCodeExpired();
        }

        return user;
    }).then((user) => {
        return mailgun.sendPasswordResetCompletedMailGun(user.email, user.fullName).then(() => {
            return res.send({msg: 'Password has been succesfully changed'});
        });
    }).catch(next);
    // }).catch(next);
};

/**
 * Function authorizes user with 2FA if enabled
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.email
 * @param {String} req.body.password
 *
 */
module.exports.auth = (req, res, next) => {
    // inputs
    const email = req.body.email.toLowerCase();
    let password = req.body.password;
    const code = req.body.code;

    const googleCaptchaResponseLogin = req.body.googleCaptchaResponseLogin;

    // req.connection.remoteAddress will provide IP address of connected user
    const verificationUrl = 'https://www.google.com/recaptcha/api/siteverify?secret=' + googleCaptchaSecretKey + '&response=' + googleCaptchaResponseLogin;
    const role = req.body.role ? req.body.role : 'user';

    // email && password have to be provided
    if (!email || !password) {
        throw new InvalidInputParametersError();
    }

    if (!googleCaptchaResponseLogin) {
        throw new CaptchaError(email);
    }

    // captcha disabled for development & test purposes only
    /*  if (config.get('captcha-enabled') && (!captchaHash || !captchaInput)) {
        throw new CaptchaError(email);
      }*/

    /*  CaptchaModel.findOne({sessionId: {$eq: captchaHash}}).
          exec().
          then((captcha) => {
            if (config.get('captcha-enabled') && (!captcha
                    || !captcha.value || captcha.value !== captchaInput)) {
              throw new CaptchaError(email);
            }*/

    requestPromise.post(verificationUrl, (error, response, body) => {
        body = JSON.parse(body);
        if (body.success === 'undefined' && body.success !== 'true') {
            throw new CaptchaError(email);
        }
        return googleCaptchaResponseLogin;
    }).then(() => {
        password = sha512(password);

        return UsersModel.findOne(
            {email: email, password: password, active: true, role: role}).exec().then((user) => {
            if (!user) {
                return UsersModel.findOne({email: email}).exec().then((user) => {
                    if (!user) {
                        throw new UserNotFoundError();
                    } else {
                        const updateQuery = {};
                        updateQuery.authCount = (user.authCount + 1);

                        if (user.authCount >
                            config.get('auth-max-fail-count')) {
                            updateQuery.active = false;
                        }

                        return UsersModel.findOneAndUpdate({email: email},
                            updateQuery).exec().then((user) => {
                            if (user.active) {
                                throw new EmailOrPasswordIncorrectError(
                                    user.authCount > 3 ?
                                        '. Please reset your password' :
                                        '');
                            } else {
                                throw new AccountBlockedError(email);
                            }
                        });
                    }
                });
            } else {
                // if (user.twoFaEnabled) {
                //     const verified = speakeasy.totp.verify({
                //         secret: user.twoFaCode,
                //         encoding: 'base32',
                //         token: code,
                //     });
                //
                //     if (!verified) {
                //         throw new InvalidTwoFactorAuthError(email);
                //     }
                // }
                /* if user is newly signed up */
                let isNew = user.isUserNew;
                if (user.isUserNew) {
                    user.isUserNew = false;
                    user.save();
                }

                const token = jwt.sign({
                    _id: user._id,
                    email: user.email,
                    role: user.role,
                }, config.get('secret'), {
                    expiresIn: config.get('jwt_expiretime'),
                });
                const shaLinkHash = (sha256(user.email + 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D'));
                const userKycUploadLink = 'https://kyc.genesisx.io/upload/?id=GenesisX_' + shaLinkHash;

                return res.send({
                    token: token,
                    email: user.email,
                    hash: sha512(String(user._id)),
                    fullName: user.fullName,
                    balanceTKN: user.balanceTKN,
                    referralTKN: user.referralTKN,
                    purchasedTKN: user.purchasedTKN,
                    kycStatus: user.kycStatus,
                    expiresIn: config.get('jwt_expiretime'),
                    ethAddress: user.ethAddress,
                    referralCode: user.referralCode,
                    userKycUploadLink: userKycUploadLink,
                    isNew: isNew,
                    twoFaEnabled: user.twoFaEnabled,
                    isMailVerification: user.isMailVerification,
                });
            }
        });
    }).catch(next);
};

module.exports.getBalanceTKN = (req, res) => {
    const userId = req.decoded._id;

    UsersModel.findById(userId).select('balanceTKN').exec().then((user) => {
        if (!user) {
            return res.status(httpStatus.FORBIDDEN).send({err: 'Not authorized'});
        }
        return res.send({
            balanceTKN: user.balanceTKN,
        });
    });
};
/**
 * Function updates withdraw type of user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.withdrawType
 *
 */
module.exports.updateWithdrawalType = (req, res, next) => {
    const userId = req.decoded._id;
    const withdrawType = req.body.withdrawType;

    if (!withdrawType) {
        throw new InvalidInputParametersError();
    }

    UsersModel.findById(userId).exec().then((user) => {
        if (!user) {
            throw new UserNotFoundError();
        }

        user.withdrawType = withdrawType;
        user.markModified('withdrawType');
        return user.save();
    }).then(() => {
        return res.send({msg: 'Successfully updated'});
    }).catch(next);
};

/**
 * Function loin with Facebook
 * Verify auth and send JWT token
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.authToken
 *
 */

module.exports.fbAuth = (req, res, next) => {
    // console.log(req.user)
    // if (!req.user) {
    //     throw new UserNotFoundError();
    // }


    const user = req.user;
    const token = jwt.sign({
        _id: user._id,
        email: user.email,
        role: user.role,
    }, config.get('secret'), {
        expiresIn: config.get('jwt_expiretime'),
    });
    /* check if user is new */
    if (user.isUserNew) {
        user.isUserNew = false;
    }
    let isNew = user.isUserNew;
    let jwtResponse = {
        token: token,
        email: user.email,
        hash: sha512(String(user._id)),
        fullName: user.fullName,
        balanceTKN: user.balanceTKN,
        referralTKN: user.referralTKN,
        purchasedTKN: user.purchasedTKN,
        kycStatus: user.kycStatus,
        expiresIn: config.get('jwt_expiretime'),
        ethAddress: user.ethAddress,
        referralCode: user.referralCode,
        isNew: isNew,
        twoFaEnabled: user.twoFaEnabled,
    };
    if (!user.referBy && req.body.referralCode) {
        UsersModel.findOne({referralCode: req.body.referralCode}).then((referByUser) => {
            UsersModel.findOneAndUpdate({_id: user._id}, {
                referBy: referByUser._id,
                isUserNew: user.isUserNew,
            }).then(() => {
                res.send(jwtResponse);
            });
        });
    } else {
        if (isNew) {
            UsersModel.findOneAndUpdate({_id: user._id}, {
                isUserNew: user.isUserNew,
            }).then(() => {
                res.send(jwtResponse);
            }).catch(next);
        } else {
            res.send(jwtResponse);
        }
    }
};

/**
 * Function loin with google
 * Verify auth and send JWT token
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.authToken
 *
 */

module.exports.googleAuth = (req, res, next) => {
    if (!req.user) {
        throw new UserNotFoundError();
    }
    const user = req.user;
    const token = jwt.sign({
        _id: user._id,
        email: user.email,
        role: user.role,
    }, config.get('secret'), {
        expiresIn: config.get('jwt_expiretime'),
    });
    let isNew = user.isUserNew;

    let jwtResponse = {
        token: token,
        email: user.email,
        hash: sha512(String(user._id)),
        fullName: user.fullName,
        balanceTKN: user.balanceTKN,
        referralTKN: user.referralTKN,
        purchasedTKN: user.purchasedTKN,
        kycStatus: user.kycStatus,
        expiresIn: config.get('jwt_expiretime'),
        ethAddress: user.ethAddress,
        referralCode: user.referralCode,
        isNew: isNew,
        twoFaEnabled: user.twoFaEnabled,
        // userType: user.userType,
        // userKycUploadLink: userKycUploadLink,
    };
    /* check if user is new */
    if (user.isUserNew) {
        user.isUserNew = false;
    }

    if (!user.referBy && req.body.referralCode) {
        UsersModel.findOne({referralCode: req.body.referralCode}).then((referByUser) => {
            if (!referByUser) {
                throw new UserNotFoundError();
            }
            UsersModel.findOneAndUpdate({_id: user._id}, {
                referBy: referByUser._id,
                isUserNew: user.isUserNew,
            }).then(() => {
                res.send(jwtResponse);
            }).catch(next);
        }).catch(next);
    } else {
        if (isNew) {
            UsersModel.findOneAndUpdate({_id: user._id}, {
                isUserNew: user.isUserNew,
            }).then(() => {
                res.send(jwtResponse);
            }).catch(next);
        } else {
            res.send(jwtResponse);
        }
    }
};


/**
 * Function to get User's Referrals
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.decoded._id - current user
 *
 */

module.exports.getReferrals = (req, res, next) => {
    const userId = req.decoded._id;

    if (!userId) {
        throw new InvalidInputParametersError();
    }
    UsersModel.find({referBy: req.decoded._id}).select('-password -providerData').exec().then((users) => {
        return res.send(users);
    }).catch(next);
};

/**
 * Function to validate TwoFa
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.decoded._id - current user
 *
 */

module.exports.validateTwoFa = (req, res, next) => {
    const userId = req.decoded._id;
    const code = req.body.code;

    if (!code || !userId) {
        throw new InvalidInputParametersError();
    }
     UsersModel.findById(userId).lean().then((user) => {
        if (user.twoFaEnabled) {
            const verified = speakeasy.totp.verify({
                secret: user.twoFaCode,
                encoding: 'base32',
                token: code,
            });

            if (!verified) {
                throw new InvalidTwoFactorAuthError();
            }
            res.send(verified);
        }
    }).catch(next);
};


/**
 * Function is used for modify-password
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.oldPassword
 * @param {String} req.body.newPassword
 *
 */

module.exports.modifyPassword = (req, res, next) => {

    if(req.body.hasOwnProperty('code') && req.body.hasOwnProperty('twoFactorEnabled')) {
        const code = req.body.code;
        const twoFactorEnabled = req.body.twoFactorEnabled;
        const userId = req.decoded._id;
        const oldPassword = req.body.data.oldPassword;
        const newPassword = req.body.data.newPassword;

        if (code == '' || !validator.isNumeric(code) || !twoFactorEnabled) {
            throw new InvalidInputParametersError();
        }

        if (!oldPassword || !newPassword) {
            throw new InvalidInputParametersError();
        }

        UsersModel.findOne({_id: userId}).exec().then((user) => {
            if (!user) {
                throw new UserNotFoundError();
            }

            const verified = speakeasy.totp.verify({
                secret: user.twoFaCode,
                encoding: 'base32',
                token: code,
            });

            if (!verified) {
                throw new InvalidTwoFactorAuthError();
            }

            if (user.provider !== 'local') {
                throw new PasswordModificationNotAllowedError();
            }
            if (user.password !== sha512(oldPassword)) {
                throw new PasswordMatchError();
            }
            user.password = sha512(newPassword);
            return user.save().then(() => {
                return res.send({msg: 'Password has been succesfully changed'});
            });
        }).catch(next);
    } else {
        const userId = req.decoded._id;
        const oldPassword = req.body.oldPassword;
        const newPassword = req.body.newPassword;

        if (!oldPassword || !newPassword) {
            throw new InvalidInputParametersError();
        }

        UsersModel.findById(userId).exec().then((user) => {
            if (!user) {
                throw new UserNotFoundError();
            }
            if (user.provider !== 'local') {
                throw new PasswordModificationNotAllowedError();
            }
            if (user.password !== sha512(oldPassword)) {
                throw new PasswordMatchError();
            }
            user.password = sha512(newPassword);
            return user.save().then(() => {
                return res.send({msg: 'Password has been succesfully changed'});
            });
        }).catch(next);
    }
};

/**
 * Function Activate and Deactivate email verification
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.body.email
 * @param {String} req.body.isMailVerification
 *
 */
module.exports.activateDeactivateEmailVerification = (req, res, next) => {
    const userId = req.decoded._id;
    const email = req.body.email;
    const isMailVerification = req.body.isMailVerification;

    if (!email) {
        throw new InvalidInputParametersError();
    }

    UsersModel.findById(userId).exec().then((user) => {
        if (!user) {
            throw new UserNotFoundError();
        }

        user.isMailVerification = isMailVerification;
        return user.save();
    }).then((user) => {
        return res.send({isMailVerification: user.isMailVerification});
    }).catch(next);
};


/**
 * Function store user info(ip, location, browser etc) in db
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {Object}
 *
 * @param {Object} req.body
 *
 */

module.exports.saveUserInfo = (req, res, next) => {

    const userId = req.decoded._id;
    const city = req.body.userIP.city;
    const countryCode = req.body.userIP.countryCode;
    const countryName = req.body.userIP.country;
    const ipAddress = req.body.userIP.query;
    const latitude = req.body.userIP.lat;
    const longitude = req.body.userIP.lon;
    const browser = req.body.deviceInfo.browser;

    const userInfo = new UserInfoModel({
        userId: userId,
        city: city,
        countryCode: countryCode,
        countryName: countryName,
        ipAddress: ipAddress,
        latitude: latitude,
        longitude: longitude,
        browser: browser,
    });

    userInfo.save().then((userinfo) => {
        if (!userinfo) {
            res.send('err');
        }
        // emitter.emit('all-feeds-reload', userId);
        return res.send(userinfo);
    }).catch(next);
};
