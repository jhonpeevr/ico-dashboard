'use strict';

const FeedModel = require('../lib/maindb').model('Feeds');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const emitter = require('../lib/emitter');

/**
 * Function to save feeds for the user
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.saveFeed = (req, res, next) => {

    const userId = req.decoded._id;
    const subject = req.body.subject;
    const description = req.body.description;
    const visibility = req.body.visibility;

    if (!subject || !description || !visibility) {
        throw new InvalidInputParametersError();
    }

    const feed = new FeedModel({
        userId: userId,
        subject: subject,
        description: description,
        visibility: visibility,
    });

    feed.save().then((feeds) => {
        if (!feeds) {
            res.send('err');
        }
        emitter.emit('all-feeds-reload', userId);
        return res.send(feeds);
    }).catch(next);
};

/**
 * Function returns latest 50 feeds for allUser
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.getFeedsByID = (req, res, next) => {

    const userId = req.decoded._id;
    const query = {
        $or: [
            {userId: userId},
            {visibility: 'public'},
        ],
    };
    FeedModel.find(query).exec().then((feeds) => {
        return res.send(feeds);
    }).catch(next);
};
