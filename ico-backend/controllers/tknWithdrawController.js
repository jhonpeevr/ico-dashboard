'use strict';
/* Global Libraries */
const config = require('config');
/* Project Libraries */

const emitter = require('../lib/emitter');
const VestingDateError = require('../errors/VestingDateError');

/* Web3 configuration */
const Web3 = require('web3');

const web3 = new Web3(
    new Web3.providers.HttpProvider(config.get('eth-network-address')));

const cjson = require('../truffle/abi');

const Contract = new web3.eth.Contract(
    cjson, config.get('eth-chaincode-address'));

/* DB Models */
const FinancialHistoriesModel =
    require('../lib/maindb').model('FinancialHistories');
const CurrenciesModel = require('../lib/maindb').model('Currencies');
const UsersModel = require('../lib/maindb').model('Users');

/* Catchable errors */
const EthUnlockError = require('../errors/EthUnlockError');
const InvalidInputParameters = require('../errors/InvalidInputParametersError');
const BalanceNotEnoughError = require('../errors/BalanceNotEnoughError');
const UserNotFoundError = require('../errors/UserNotFoundError');
const WithdrawTokenError = require('../errors/WithdrawTokenError');
const mathLib = require('../lib/mathematics');
const loggerName = '[TknWithdrawController]';

/**
 * Function sends tokens to user address
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {Number} req.body.amountTKN - amount of tokens to withdraw
 * @param {String} req.decoded._id - current userId
 *
 */
module.exports.sendTx = (req, res, next) => {
  const userId = req.decoded._id;
  const tokenWithdraw = req.body.tokenWithdraw;
  const ethAddress = req.body.ethAddress;
  if (!config.get('withdraw-enabled')) {
    throw new WithdrawTokenError();
  }
  if (!tokenWithdraw || !ethAddress){
      throw new InvalidInputParameters();
  }
  UsersModel.findOneAndUpdate({_id: userId}, {ethAddress: ethAddress}).exec().
      then((user) => {
        if (!user) {
          throw new UserNotFoundError();
        }
        if (!user.withdrawalStatus) {
          throw new WithdrawTokenError();
        }

        return withdrawTokens(user._id, ethAddress, tokenWithdraw).
            then((transaction) => {
              return res.send({msg: 'transaction proceed..'});
            });
      }).
      catch(next);
};

/**
 * Function sends tokens to user address
 * @param {String} userId
 * @param {String} userAddress
 * @param {Number} amountTKN
 *
 * @return {Object}
 *
 */
const withdrawTokens = (userId, userAddress, amountTKN) => {
  const methodName = '[WithdrawTokens]: ';

  // check for empty input values
  if (!userAddress || !amountTKN) {
    throw new InvalidInputParameters();
  }

  if (Date.now() < config.get('vesting-end-date')) {
    throw new VestingDateError();
  }

  console.log(loggerName, methodName, 'transfer', userAddress, amountTKN);

  return CurrenciesModel.findOne({abbr: 'tkn'}).exec().then((currencyTKN) => {
    return UsersModel.findById(userId).
        exec().
        then((user) => {
          if (!user) {
            throw new UserNotFoundError();
          }

          if (mathLib.isLessThan(user.balanceTKN,amountTKN)) {
            throw new BalanceNotEnoughError();
          }
          return new Promise((resolve, reject) => {
              web3.eth.personal.unlockAccount(
                  config.get('eth-wallet-address'),
                  config.get('eth-wallet-password')).
              then((unlocked) => {
                  if (!unlocked) {
                      throw new EthUnlockError();
                  }
                  console.log(loggerName, methodName, userAddress,
                      amountTKN);

                  return Contract.methods.transfer(userAddress,
                      (amountTKN * 1000000)).
                  send({from: config.get('eth-wallet-address')}).
                  once('transactionHash', function(hash) {
                      console.log(loggerName, methodName,
                          'ethereum transaction: ', hash);
                  }).on('error', function(error) {
                      console.error(loggerName, methodName, error);
                      reject(error);
                  }).
                  then((transaction) => {
                      user.balanceTKN =parseFloat(mathLib.valueOf( mathLib.safeSubtract(user.balanceTKN,amountTKN)));
                      user.markModified('balanceTKN');

                      return user.save().then((user) => {
                          const financialHistoryWithdraw = new
                          FinancialHistoriesModel({
                              userId: userId,
                              amount: amountTKN,
                              currencyId: currencyTKN._id,
                              currencyAbbr: currencyTKN.abbr,
                              currencyName: currencyTKN.name,
                              type: 'withdraw',
                              exchangeRateValue: 1.0,
                          });

                          return financialHistoryWithdraw.save().
                          then((financialHistoryWithdraw) => {
                              console.log(loggerName, methodName,
                                  'successfully completed with tx',
                                  transaction);
                              emitter.emit('balance-changed', user);
                              emitter.emit('config-changed');
                              emitter.emit('fh-tkn-withdraw',
                                  financialHistoryWithdraw);
                              emitter.emit('fh-withdraw-reload', userId);
                              resolve(transaction);
                          });
                      });
                  });
              });
          });
        });
  });
};

module.exports.withdrawTokens = withdrawTokens;
