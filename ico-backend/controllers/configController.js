'use strict';

const ConfigsModel = require('../lib/maindb').model('Configs');
const configsDisplayName=require('../lib/constant')

/**
 * Function gets all configs as a map
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.key - key value in configs
 *
 */
module.exports.getConfigs = (req, res, next) => {
  getConfigsMap().then((configMap) => {
    return res.send(configMap);
  }).catch(next);
};

/**
 * Function gets all configs as an array
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.key - key value in configs
 *
 */
module.exports.getConfigsArray = (req, res, next) => {
  ConfigsModel.find({}).
      exec().
      then((configs) => {
        console.log(configs);
        return configs;
      });
};

const getConfigsMap = () => {
  return ConfigsModel.find({}).
      exec().
      then((configs) => {
        return convertToMap(configs);
      });
};

const convertToMap = (configs) => {
  const map = {};
  for (const c of configs) {
    if (c.valueString) {
      map[c.key] = c.valueString;
    } else if (c.valueDate) {
      map[c.key] = c.valueDate;
    } else if (c.valueDouble || c.valueDouble === 0) {
        map[c.key] = c.valueDouble;
    } else if (c.valueBoolean || c.valueBoolean === false || c.valueBoolean === true) {
        map[c.key] = c.valueBoolean;
    } else {
      throw new Error('No config type');
    }
  }
  return map;
};

module.exports.convertToMap = convertToMap;
module.exports.getConfigsMap = getConfigsMap;

/**
 * Function gets all configs with key and values
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.key - key value in configs
 *
 */

module.exports.getAllConfigs = (req, res, next) => {
  ConfigsModel.find().
      exec().
      then((configs) => {
        const keyValueConfig = convertToKeyValue(configs);
        return res.send(keyValueConfig);
      }).catch(next);
};


const convertToSpace = (key) => {
  return configsDisplayName[key];
  }

const convertToKeyValue = (configs) => {
  const keyValue = [];
  for (const c of configs) {
    let defaultVal = {_id: c._id, key: c.key, editable: c.editable,displayName:''};
    defaultVal.displayName = convertToSpace( c.key);
    if (c.valueString) {
      defaultVal.type = 'valueString';
      defaultVal.value = c.valueString;
    } else if (c.valueDate) {
      defaultVal.type = 'valueDate';
      defaultVal.value = (c.valueDate).toISOString().substring(0, 10);
    } else if (c.valueDouble || c.valueDouble === 0) {
        defaultVal.type = 'valueDouble';
        defaultVal.value = c.valueDouble;
    } else if (c.valueBoolean || c.valueBoolean === false || c.valueBoolean === true) {
        defaultVal.type = 'valueBoolean';
        defaultVal.value = c.valueBoolean;
    } else {
      throw new Error('No config type');
    }
    keyValue.push(defaultVal);
  }
  return keyValue;
};
/**
 * Update config value
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.key - key value in configs
 *
 */

module.exports.convertToKeyValue = convertToKeyValue;

module.exports.editDynamicConfig = (req, res, next) => {
  const configKey = req.body.key;
  const value = req.body.value;
  const type = req.body.type;
  let conditions = {};
  conditions[type] = value;
  ConfigsModel.findOneAndUpdate({key: configKey}, conditions, {new: true}).
      exec().
      then((configs) => {
        const confKeyVal = convertToKeyValue([configs]);
        res.send(confKeyVal[0]);
      }).
      catch(next);
};
