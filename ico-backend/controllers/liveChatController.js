'use strict';

const io = require('../lib/live-chat-io');
const LiveChatsModel = require('../lib/maindb').model('LiveChats');
io.on('live-chat', (message) => {
  console.log('live-chat', message);
});

module.exports.reply = (req, res) => {
  const name = req.body.name;
  const message = req.body.message;
  const sessionId = req.body.sessionId;
  io.emit(sessionId, {
    name: name,
    message: message,
    sessionId: sessionId,
    timestamp: Date.now(),
  });

  res.send('Send');
};

module.exports.listActive = (req, res, next) => {
  LiveChatsModel.find({}).
      exec().
      then((liveChats) => {
        if (!liveChats || liveChats.length === 0) {
          return res.send([]);
        }
        return res.send(liveChats);
      }).catch(next);
};

module.exports.getChat = (req, res, next) => {
  const sessionId = req.params.sessionId;
  LiveChatsModel.findOne({'sessionId': sessionId}).
      exec().
      then((liveChat) => {
        if (!liveChat) {
          return res.send([]);
        }
        return res.send(liveChat.messages);
      }).catch(next);
};
