'use strict';

const usersModel = require('../lib/maindb').model('Users');
const UserInfoModel = require('../lib/maindb').model('UserInfo');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const mailAgent = require('../lib/mailgun');
const request = require('request');
const AmlModel = require('../lib/maindb').model('Aml');
const Promise = require('bluebird');
const speakeasy = require('speakeasy');
const validator = require('validator');
const InvalidTwoFactorAuthError = require(
    '../errors/InvalidTwoFactorAuthError');

/**
 * Function returns getUsdInvests of all users
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.listAll = (req, res, next) => {
    let query;
    if (req.query.query && req.query.query !== '') {
        let searchData = req.query.query;
        query = {
            '$or': [
                {'email': new RegExp(searchData, 'i')},
                {'role': new RegExp(searchData, 'i')},
            ],
        };
    } else {
        query = {};
    }
    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);
    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        usersModel.find(query).skip(skip).limit(perPage).populate('amlRecord').populate('referBy').exec().then((users) => {
            return res.send(users);
        }).catch(next);
    } else {
        usersModel.find(query).exec().then((users) => {
            return res.send(users);
        }).catch(next);
    }
};


/**
 * Function returns getUsdInvests of all users
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.getUserByEmail = (req, res, next) => {
    const email = req.params.email;
    usersModel.find({email: email}).exec().then((user) => {
        return res.send(user);
    }).catch(next);
};

/**
 * Function returns user info
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.getUserByID = (req, res, next) => {
    const id = req.params.id;
    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);

    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        UserInfoModel.find({userId: id}).skip(skip).limit(perPage).exec().then((userinfo) => {
            return res.send(userinfo);
        }).catch(next);
    } else {
        UserInfoModel.find({userId: id}).exec().then((userinfo) => {
            return res.send(userinfo);
        }).catch(next);
    }
};

module.exports.getUserInfoCount = (req, res, next) => {
    const id = req.params.id;
    UserInfoModel.count({userId: id}).exec().then((infocount) => {
        return res.send({count: infocount});
    }).catch(next);
};

/**
 * Function updates user information
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.userId
 * @param {String} req.decoded._id   - current user
 *
 *
 */
module.exports.update = (req, res, next) => {
    const userId = req.body.userId;

    if (!userId) {
        throw new InvalidInputParametersError();
    }

    usersModel.findOneAndUpdate({_id: userId}, req.body, {new: true}).exec().then((user) => {
        return res.send(user);
    }).catch(next);
};

/**
 * Function blocks the user access
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.userId - user
 * @param {String} req.decoded._id - current admin
 *
 */
module.exports.block = (req, res, next) => {
    const userId = req.body.userId;

    if (!userId) {
        throw new InvalidInputParametersError();
    }

    if (req.body.hasOwnProperty('isUserStatus')) {
        const status = req.body.isUserStatus;
        usersModel.findOneAndUpdate({_id: userId}, {active: status},
            {new: true}).exec().then((user) => {
            return res.send(user);
        }).catch(next);
    } else {
        usersModel.findOneAndUpdate({_id: userId}, {active: false},
            {new: true}).exec().then((user) => {
            return res.send(user);
        }).catch(next);
    }
};

/**
 * Function activate the user access
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.userId - user
 * @param {String} req.decoded._id - current admin
 *
 */
module.exports.activate = (req, res, next) => {
    const userId = req.body.userId;

    if (!userId) {
        throw new InvalidInputParametersError();
    }

    usersModel.findOneAndUpdate({_id: userId}, {active: true},
        {new: true}).exec().then((user) => {
        return res.send(user);
    }).catch(next);
};

/**
 * Function to get statistics of users
 *
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 *  @param {String} req.decoded._id - current admin
 *
 */
module.exports.statistics = (req, res, next) => {

    const totalEVMBalance = new Promise((resolve) => {
        return usersModel.aggregate(
            {$group: {_id: null, total: {$sum: '$balanceTKN'}}}).exec().then((nbr) => {
            resolve({totalEVMBalance: nbr[0].total});
        });
    });

    const totalKycSubmitted = new Promise((resolve, reject) => {
        return usersModel.count({kycStatus: 'SUBMITTED'}).exec().then((nbr) => {
            resolve({totalKycSubmitted: nbr});
        });
    });

    const totalKycConfirmed = new Promise((resolve, reject) => {
        return usersModel.count({kycStatus: 'SUCCESS'}).exec().then((nbr) => {
            resolve({totalKycConfirmed: nbr});
        });
    });

    const totalUsers = new Promise((resolve, reject) => {
        return usersModel.count({}).exec().then((nbr) => {
            resolve({totalUsers: nbr});
        });
    });
    const totalRoleAdmin = new Promise((resolve, reject) => {
        return usersModel.count({role: 'admin'}).exec().then((nbr) => {
            resolve({totalRoleAdmin: nbr});
        });
    });
    const totalRoleUser = new Promise((resolve, reject) => {
        return usersModel.count({role: 'user'}).exec().then((nbr) => {
            resolve({totalRoleUser: nbr});
        });
    });
    const totalActiveUser = new Promise((resolve, reject) => {
        return usersModel.count({active: true}).exec().then((nbr) => {
            resolve({totalActiveUser: nbr});
        });
    });
    const totalBlockUser = new Promise((resolve, reject) => {
        return usersModel.count({active: false}).exec().then((nbr) => {
            resolve({totalBlockUser: nbr});
        });
    });
    const totalTodayRegisteredUser = new Promise((resolve, reject) => {
        return usersModel.count({timestamp: {'$gte': new Date().toISOString().substr(0, 10)}}).exec().then((nbr) => {
            resolve({totalTodayRegisteredUser: nbr});
        });
    });
    const totalInvestedUser = new Promise((resolve, reject) => {
        return usersModel.count({balanceTKN: {'$gt': 0}}).exec().then((nbr) => {
            resolve({totalInvestedUser: nbr});
        });
    });
    Promise.all([
        totalEVMBalance,
        totalKycSubmitted,
        totalKycConfirmed,
        totalUsers,
        totalRoleAdmin,
        totalRoleUser,
        totalActiveUser,
        totalBlockUser,
        totalTodayRegisteredUser,
        totalInvestedUser]).then((data) => {
        let statistics = {};
        for (let stat of data) {
            statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
        }
        return res.send(statistics);
    });
};

/**
 * Function activate the user access
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.userId - user
 * @param {String} req.decoded._id - current admin
 *
 */
module.exports.withdrawalBlock = (req, res, next) => {
    const methodName = '[withdrawalBlock]';
    const userId = req.body.userId;
    if (!userId) {
        throw new InvalidInputParametersError();
    }
    usersModel.findOneAndUpdate({_id: userId}, {withdrawalStatus: false},
        {new: true}).exec().then((user) => {
        mailAgent.sendWithdrawalBlockedMailGun(user).then(() => {
            console.log(methodName, 'Sent activation email',
                user.email);
            return res.send(user);
        });
    }).catch(next);
};

/**
 * Function activate the user access
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.userId - user
 * @param {String} req.decoded._id - current admin
 *
 */
module.exports.withdrawalActivate = (req, res, next) => {
    const methodName = '[withdrawalActivate]';
    const userId = req.body.userId;
    if (!userId) {
        throw new InvalidInputParametersError();
    }
    usersModel.findOneAndUpdate({_id: userId}, {withdrawalStatus: true},
        {new: true}).exec().then((user) => {
        mailAgent.sendWithdrawalActivationMailGun(user).then(() => {
            console.log(methodName, 'Sent activation email',
                user.email);
            return res.send(user);
        });
    }).catch(next);
};

/**
 * Function save user ethereum address
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.address - user address
 * @param {String} req.decoded._id - user
 *
 */
module.exports.saveAddress = (req, res, next) => {

    const methodName = '[saveAddress]';

    const userId = req.decoded._id;
    const address = req.body.address;

    if (!address) {
        throw new InvalidInputParametersError();
    }

    if (req.body.hasOwnProperty('code') && req.body.hasOwnProperty('twoFactorEnabled')) {
        const code = req.body.code;
        const twoFactorEnabled = req.body.twoFactorEnabled;

        if (code == '' || !validator.isNumeric(code) || !twoFactorEnabled) {
            throw new InvalidInputParametersError();
        }

        usersModel.findOne({_id: userId}).exec().then((user) => {
            if (!user) {
                throw new UserNotFoundError();
            }

            const verified = speakeasy.totp.verify({
                secret: user.twoFaCode,
                encoding: 'base32',
                token: code,
            });

            if (!verified) {
                throw new InvalidTwoFactorAuthError();
            }
            usersModel.findOneAndUpdate({_id: userId}, {ethAddress: address},
                {new: true}).exec().then((user) => {
                return res.send({msg: 'address stored successfully'});
            }).catch(next);
        }).catch(next);
    } else {
        usersModel.findOneAndUpdate({_id: userId}, {ethAddress: address},
            {new: true}).exec().then((user) => {
            return res.send({msg: 'address stored successfully'});
        }).catch(next);
    }
};


/**
 * Function save user ethereum address
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.address - user address
 * @param {String} req.decoded._id - user
 *
 */
module.exports.countUsers = (req, res, next) => {

    usersModel.count().then((users) => {
        return res.send({
            count: users
        })
    })
};

/**
 * Function to verify AML from coinfirm.io
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.body.userId - user
 * @param {String} req.decoded._id - current admin
 *
 */
module.exports.processAml = (req, res, next) => {
    const methodName = '[processAml]';
    usersModel.find({amlConfirmed: false}).exec().then((users) => {
        const address = '1AuhjbLkuhbAfyCJopfUEbdGaktABdTavJ';
        const promises = [];
        users.forEach((user) => {
            promises.push(coinFirm(user, address));
        });
        Promise.all(promises).then((data) => {
            usersModel.find().populate('amlRecord').exec().then((users) => {
                return res.send(users);
            });
        }).catch((err) => { // error handling});.
            console.log(err);
        });
    }).catch(next);
};

const coinFirm = (user, address) => {
    // production Url
    // http://api.coinfirm.io/v2/reports/aml/standard/1AuhjbLkuhbAfyCJopfUEbdGaktABdTavJ
    return new Promise((resolve, rejected) => {
        request(
            'https://private-anon-09feba0df8-aml.apiary-mock.com/v2/reports/aml/standard/' +
            address,
            function (error, response, body) {
                body = JSON.parse(body);

                const amlRecord = new AmlModel({
                    userId: user._id,
                    recommendation: body.recommendation,
                    balance: body.balance,
                    whitelist: body.whitelist,
                    cscore: body.cscore,
                    usd_balance: body.usd_balance,
                });
                amlRecord.save().then((result) => {
                    usersModel.findOneAndUpdate({_id: user._id},
                        {amlConfirmed: true, amlRecord: result._id}).exec().then(() => {
                        console.log('Status:', response.statusCode);
                        console.log('Headers:', JSON.stringify(response.headers));
                        console.log('Response:', body);
                        resolve(body);
                    });
                }).catch((err) => resolve(err));
            });
    });
};

