'use strict';
const request = require('request');
const config = require('config');
const exchangeRateUsdToEthApiUrl = 'https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=ETH';
const exchangeRateEthToBtcApiUrl = 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=BTC';
const exchangeRateBtcToUsdApiUrl = 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD';
const exchangeRateEthToUsdApiUrl = 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD';
const exchangeRateLtcToUsdApiUrl = 'https://min-api.cryptocompare.com/data/price?fsym=LTC&tsyms=USD';
const ExchangeRatesModel = require('../lib/maindb').model('ExchangeRates');
const CurrentRatesUsdModel = require('../models/CurrentRatesUsdModel');
const ConfigModel = require('../lib/maindb').model('Configs');
const BonusModel = require('../lib/maindb').model('Bonus');
const BonusPerStageModel = require('../lib/maindb').model('BonusPerStage');
const CurrentRatesUsd = require('../lib/maindb').model('CurrentRatesUsd');
const emitter = require('../lib/emitter');
const socket = require('socket.io-client')(
    'https://streamer.cryptocompare.com/');
const subscription = ['5~CCCAGG~USD~ETH', '5~CCCAGG~ETH~BTC', '5~CCCAGG~BTC~USD', '5~CCCAGG~ETH~USD'];
socket.emit('SubAdd', {subs: subscription});
const Promise = require('bluebird');
let globalUsdInEth = 0;

/**
 * Function returns currency exchange rates
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 */
module.exports.getExchangeRates = (req, res, next) => {
    ExchangeRatesModel.find({}).select('-_id -__v').exec().then((exchangeRates) => {
        return res.send(exchangeRates);
    }).catch(next);
};


module.exports.getUsdRates = (req, res, next) => {
    CurrentRatesUsdModel.find({}).select('-_id').exec().then((exchangeRates) => {
        return res.send(exchangeRates);
    }).catch(next);
};


/*
* Cron to update exchange Rate
* @param {Object} req Express request object
* @param {Object} res Express response object
* @param {Function} next- Express next middleware function
*
* @return {ok}
*
* */
module.exports.updateExchangeRate = (req, res, next) => {
    Promise.all([
        new Promise((resolve, reject) => {
            request(exchangeRateUsdToEthApiUrl, function (error, response) {
                if (error) {
                    reject(error);
                }
                resolve(JSON.parse(response.body).ETH);
            });
        }),
        getTKNinUSD(),
        new Promise((resolve, reject) => {
            request(exchangeRateEthToBtcApiUrl, function (error, response) {
                if (error) {
                    reject(error);
                }
                resolve(JSON.parse(response.body).BTC);
            });
        }),
        new Promise((resolve, reject) => {
            request(exchangeRateBtcToUsdApiUrl, function (error, response) {
                if (error) {
                    reject(error);
                }
                resolve(JSON.parse(response.body).USD);
            });
        }),
        new Promise((resolve, reject) => {
            request(exchangeRateEthToUsdApiUrl, function (error, response) {
                if (error) {
                    reject(error);
                }
                resolve(JSON.parse(response.body).USD);
            });
        }),
        new Promise((resolve, reject) => {
            request(exchangeRateLtcToUsdApiUrl, function (error, response) {
                if (error) {
                    reject(error);
                }
                resolve(JSON.parse(response.body).USD);
            });
        }),
    ]).then((results) => {
        const usdInEth = parseFloat(results[0]);
        const tknInUsd = parseFloat(results[1]);
        const ethInBtc = parseFloat(results[2]);
        const btcInUsd = parseFloat(results[3]);
        const ethInUsd = parseFloat(results[4]);
        const ltcInUsd = parseFloat(results[5]);
        globalUsdInEth = usdInEth;
        const tknInEth = usdInEth * tknInUsd;
        const tknToBtcValue = (1 / ethInBtc) * (1 / tknInEth);
        const tknToLtcValue = (ltcInUsd / tknInUsd);
        Promise.all([
            updateTknInEth(tknInEth),
            updateTknInBtc(tknToBtcValue),
            updateTknInLtc(tknToLtcValue),
            updateBtcInUsd(btcInUsd),
            updateEthInUsd(ethInUsd),
            updateLtcInUsd(ltcInUsd),
        ]).then(() => {
            emitter.emit('exchange-rate-changed');
            if (res) {
                res.send('ok');
            }
        });
    }).catch((err) => {
        if (next) {
            next(err);
        }
    });
};

const getTKNinUSD = async () => {

    try {
        const stageBonusData = await ConfigModel.findOne({key: 'stageBonus'}).lean();
        const stageBonus = stageBonusData.valueBoolean;

        const tknPriceData = await ConfigModel.findOne({key: 'tkn2USD'}).lean();
        const tknPrice = tknPriceData.valueDouble;

        if (stageBonus) {
            const soldTokensData = await ConfigModel.findOne({key: 'soldTokens'}).lean();
            const soldTokens = soldTokensData.valueDouble;
            const bonusPerStageData =  await BonusPerStageModel.findOne({$and: [{minTokens: { $lte: soldTokens } },{maxTokens: { $gte: soldTokens }}]});
            if(bonusPerStageData){
                return bonusPerStageData.priceInUSD;
            }else{
                return tknPrice;
            }
        }else {
            return tknPrice;
        }
    } catch (error) {
        console.log('error ', error)
    }
};


this.updateExchangeRate(null, null, null);

const updateTknInEth = (tknInEth) => {
    return new Promise((resolve, reject) => {
        Promise.all([
            ConfigModel.findOneAndUpdate({
                key: 'tknInEth',
            }, {valueDouble: tknInEth}).exec(),
            ExchangeRatesModel.findOneAndUpdate({
                currencyIdFirstAbbr: config.get('coins')[1],
                currencyIdSecondAbbr: 'tkn',
                type: 'invest',
            }, {exchangeRate: ((1 / tknInEth).toFixed(18))}).exec(),
            ExchangeRatesModel.findOneAndUpdate({
                currencyIdFirstAbbr: 'tkn',
                currencyIdSecondAbbr: config.get('coins')[1],
                type: 'withdraw',
            }, {exchangeRate: (tknInEth.toFixed(18))}).exec(),
        ]).then((result) => {
            resolve();
        }).catch(reject);
    });
};

const updateTknInBtc = (tknToBtcValue) => {
    return new Promise((resolve, reject) => {
        Promise.all([
            ExchangeRatesModel.findOneAndUpdate({
                currencyIdFirstAbbr: 'tkn',
                currencyIdSecondAbbr: config.get('coins')[0],
                type: 'withdraw',
            }, {exchangeRate: ((1 / tknToBtcValue).toFixed(8))}).exec(),
            ExchangeRatesModel.findOneAndUpdate({
                currencyIdFirstAbbr: config.get('coins')[0],
                currencyIdSecondAbbr: 'tkn',
                type: 'invest',
            }, {exchangeRate: (tknToBtcValue.toFixed(8))}).exec(),
        ]).then(resolve).catch(reject);
    });
};

const updateTknInLtc = (tknToLtcValue) => {
    return new Promise((resolve, reject) => {
        Promise.all([
            ExchangeRatesModel.findOneAndUpdate({
                currencyIdFirstAbbr: 'tkn',
                currencyIdSecondAbbr: config.get('coins')[2],
                type: 'withdraw',
            }, {exchangeRate: ((1 / tknToLtcValue).toFixed(8))}).exec(),
            ExchangeRatesModel.findOneAndUpdate({
                currencyIdFirstAbbr: config.get('coins')[2],
                currencyIdSecondAbbr: 'tkn',
                type: 'invest',
            }, {exchangeRate: (tknToLtcValue.toFixed(8))}).exec(),
        ]).then(resolve).catch(reject);
    });
};

const updateBtcInUsd = (price) => {
    return new Promise((resolve, reject) => {
        return CurrentRatesUsd.findOneAndUpdate(
            {currencyId: 'btc'}, {valueInUSD: price}).exec().then((result) => {
            //  console.log(result);
            return resolve(result);
        }).catch((err) => {
            console.error(err);
            return reject(err);
        });
    });
};

const updateEthInUsd = (price) => {
    return new Promise((resolve, reject) => {
        return CurrentRatesUsd.findOneAndUpdate(
            {currencyId: 'eth'}, {valueInUSD: price}).exec().then((result) => {
            //  console.log(result);
            return resolve(result);
        }).catch((err) => {
            console.error(err);
            return reject(err);
        });
    });
};


const updateLtcInUsd = (price) => {
    return new Promise((resolve, reject) => {
        return CurrentRatesUsd.findOneAndUpdate(
            {currencyId: 'ltc'}, {valueInUSD: price}).exec().then((result) => {
            return resolve(result);
        }).catch((err) => {
            console.error(err);
            return reject(err);
        });
    });
};


socket.on('m', (message) => {
    const sub = message.substring(0, 16);
    if (subscription.indexOf(sub) > 0 && message.charAt(17) !== '4') {
        const price = parseFloat(message.substring(19, message.indexOf('~', 19)));
        switch (sub) {
            case subscription[0]:
                //   console.log('sub0: ', sub, price);
                globalUsdInEth = price;
                getTKNinUSD().then((tknInUsd) => {
                    //  console.log('price: ', price);
                    //  console.log('tknInUsd: ', tknInUsd);
                    //find ltc in usd
                    CurrentRatesUsd.findOne({
                        'currencyId': 'ltc'
                    }).lean().exec().then((ltcPrice) => {
                        const tknInEth = price * tknInUsd;
                        const tknToLtcValue = (ltcPrice / tknInUsd);

                        Promise.all([
                            updateTknInEth(tknInEth),
                            updateTknInLtc(tknToLtcValue),
                        ]).then(() => {
                            emitter.emit('exchange-rate-changed');
                        }).catch((err) => {
                            console.error(err);
                        });

                    }).catch((err) => {
                        console.error(err)
                    })


                }).catch((err) => {
                    console.error(err);
                });
                break;
            case subscription[1]:
                //    console.log('sub1: ', sub, price);
                getTKNinUSD().then((tknInUsd) => {
                    const tknInEth = globalUsdInEth * tknInUsd;
                    const tknToBtcValue = (1 / price) * (1 / tknInEth);
                    updateTknInBtc(tknToBtcValue).then(() => {
                        //  console.log('updateEthInUsd', valueChange);
                        emitter.emit('exchange-rate-changed');
                    });
                }).catch((err) => {
                    console.error(err);
                });
                break;

            case subscription[2]:
                //console.log('subscription 2 ', sub, price);
                updateBtcInUsd(price).then((ratesUpdated) => {
                    // console.log('Updates ', ratesUpdated);
                }).catch((err) => {
                    console.error(err);
                });
                break;

            case subscription[3]:
                //console.log('subscription 3 ', sub, price);
                request(exchangeRateLtcToUsdApiUrl, function (error, response) {
                    if (error) {
                        console.error(error);
                    }
                    const ltcPrice = JSON.parse(response.body).USD;


                    Promise.all([
                        updateEthInUsd(price),
                        updateLtcInUsd(ltcPrice),
                    ]).then(() => {
                        //    console.log('Updates ', ratesUpdated);

                        //This has been placed here because sockets don't send information to subscription[0]
                        getTKNinUSD().then((tknInUsd) => {
                                const tknInEth = (1 / price) * tknInUsd;
                                const tknToLtcValue = (ltcPrice / tknInUsd);

                                Promise.all([
                                    updateTknInEth(tknInEth),
                                    updateTknInLtc(tknToLtcValue),
                                ]).then(() => {
                                    emitter.emit('exchange-rate-changed');
                                }).catch((err) => {
                                    console.error(err);
                                });

                        }).catch((err) => {
                            console.error(err);
                        });



                    }).catch((err) => {
                        console.error(err);
                    });
                });

                break;
        }
    }
});

module.exports.getTknInUSD = async (req, res) => {
    try {
        const tknInUSD = await getTKNinUSD();
        res.send({tknInUSD});
    } catch (error) {
        console.log('error ', error);
    }

};
