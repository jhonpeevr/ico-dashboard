'use strict';

const UsersModel = require('../models/UsersModel');
const FinancialHistoryModel = require('../lib/maindb').
    model('FinancialHistories');
const CurrenciesModel = require('../lib/maindb').model('Currencies');
const NotificationsModel = require('../lib/maindb').model('Notifications');
const configController = require('../controllers/configController');
const io = require('../lib/io');
const sha512 = require('js-sha512').sha512.sha512;

let counter = 1;

module.exports.simulateFinancialHistories = (req, res, next) => {
  UsersModel.find({}).
      exec().
      then((users) => {
        CurrenciesModel.findOne({abbr: 'tkn'}).
            exec().
            then((currencyTKN) => {
              for (const user of users) {
                FinancialHistoryModel.find({userId: user._id}).
                    exec().
                    then((financialHistories) => {
                      for (let i = 0; i < (counter); i++) {
                        const f = new FinancialHistoryModel();
                        f.userId = user._id;
                        f.currencyId = currencyTKN._id;
                        f.currencyAbbr = currencyTKN.abbr;
                        f.currencyName = currencyTKN.name;
                        f.type = 'deposit';
                        f.amount = Math.random() * 10;
                        f.exchangeRateValue = Math.random() * 100;
                        financialHistories.push(f);
                      }
                      io.emit('fh-invest-reload-' + sha512(String(user._id)),
                          financialHistories);
                    });
              }
              counter++;

              if (counter > 12) {
                counter = 0;
              }
            });
        res.send(users);
      }).
      catch(next);
};

module.exports.simulateBalanceChange = (req, res) => {
  UsersModel.find({}).
      exec().
      then((users) => {
        for (const user of users) {
          io.emit('balance-changed-' + sha512(String(user._id)), {
            balanceTKN: (user.balanceTKN + Math.random() *
                (Math.random() * 10)),
          });
        }

        return users;
      }).
      then(() => {
        return res.send('SUBMITTED');
      });
};

module.exports.simulateConfigUpdate = (req, res) => {
  configController.getConfigsMap().then((configMap) => {
    configMap.availableTokens = configMap.availableTokens - Math.random() * 10;
    configMap.soldTokens = configMap.soldTokens + Math.random() * 10;
    io.emit('config-changed', configMap);
    return res.send(configMap);
  });
};

module.exports.simulateNotifications = (req, res, next) => {
  UsersModel.find({}).exec().then((users) => {
    for (const user of users) {
      const notifications = [];
      const n = new NotificationsModel({
        userId: user._id,
        message: 'DEPOSIT 123123 BTC',
      });

      notifications.push(n);
      notifications.push(n);
      notifications.push(n);

      io.emit('notifications-' + sha512(String(user._id)), notifications);
    }

    return res.send('ok');
  }).catch(next);
}
;
