'use strict';

const CaptchaModel = require('../lib/maindb').model('CaptchaSessions');
const config = require('config');
const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const httpStatus = require('http-status-codes');
const loggerName = '[captchaController]';

/**
 * Function generated captcha, registers on DB
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {Connection} req.connection
 */
module.exports.captcha = (req, res, next) => {
  const ip = req.connection.remoteAddress;

  const session = sha512(ip + config.get('secret') + Date.now());

  const captcha = svgCaptcha.create();

  const cm = new CaptchaModel({
    sessionId: session,
    value: captcha.text,
    ip: ip,
  });

  cm.save().then((cm) => {
    captcha.text = cm.sessionId;
    res.set('Content-Type', 'image/svg+xml');
    return res.send(captcha);
  }).catch(next);
};

module.exports.resetCaptcha = (req, res, next) => {
  const methodName = '[CheckCaptcha]';
  const captchaHash = req.body.text;

  if (!captchaHash) {
    const errMsg = 'Invalid input parameters';
    console.error(loggerName, methodName, errMsg);
    return res.status(httpStatus.BAD_REQUEST).send(errMsg);
  }
  CaptchaModel.remove({sessionId: {$eq: captchaHash}}).
      exec().
      then((captchaModel) => {
        if (!captchaModel) {
          return res.status(httpStatus.BAD_REQUEST).
              send({err: 'Invalid captcha parameters'});
        }
        this.captcha(req, res, next);
      }).catch(next);
};
