/* eslint-disable max-len */
'use strict';

const config = require('config');
const httpStatus = require('http-status-codes');
const Promise = require('bluebird');
const bitgo = require('../lib/bitgo');
const emitter = require('../lib/emitter');
const systemQueue = require('../lib/systemQueue');
const mathLib = require('../lib/mathematics');
const ne = require('node-each');
const mailAgent = require('../lib/mailgun');

const tknWithdrawController = require('../controllers/tknWithdrawController');
const CoinAddressesModel = require('../lib/maindb').model('CoinAddresses');
const CoinTransferHooksModel =
    require('../lib/maindb').model('CoinTransferHooks');
const CoinTransfersModel = require('../lib/maindb').model('CoinTransfers');
const FinancialHistoriesModel =
    require('../lib/maindb').model('FinancialHistories');
const ExchangeRatesModel = require('../lib/maindb').model('ExchangeRates');
const CurrenciesModel = require('../lib/maindb').model('Currencies');
const UsersModel = require('../lib/maindb').model('Users');
const ConfigsModel = require('../lib/maindb').model('Configs');
const BonusModel = require('../lib/maindb').model('Bonus');
const CurrentRatesUsd = require('../lib/maindb').model('CurrentRatesUsd');
const AdvisorsModel = require('../lib/maindb').model('Advisors');

const ConfigNotFoundError = require('../errors/ConfigNotFoundError');
const TransferNotFoundError = require('../errors/TransferNotFoundError');
const EntryNotFoundError = require('../errors/EntryNotFoundError');
const GeneratedAddressNotFoundError = require(
    '../errors/GeneratedAddressNotFoundError');
const ExchangeRateNotFoundError = require(
    '../errors/ExchangeRateNotFoundError');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const AlreadyProcessedHookError =
    require('../errors/AlreadyProcessedHookError');
const AlreadyInProcessHookError =
    require('../errors/AlreadyInProcessHookError');
const CoinTypeNotSupportedError =
    require('../errors/CoinTypeNotSupportedError');
const CoinNotFoundError = require('../errors/CoinNotFoundError');
const WalletNotFoundError = require('../errors/WalletNotFoundError');
const CoinTransferHookUpdateError =
    require('../errors/CoinTransferHookUpdateError');
const CoinTransferUpdateError = require('../errors/CoinTransferUpdateError');
const CoinTransferHookSaveError =
    require('../errors/CoinTransferHookSaveError');
const CoinTransferSaveError = require('../errors/CoinTransferSaveError');
const ExistingCoinTransferHookInvalidStatusError =
    require('../errors/ExistingCoinTransferHookInvalidStatusError');

const loggerName = '[COINHookController]';

/**
 * Function performs transfer operation.
 * Receives transfer hook, gets transaction from COIN network
 * Has protection against double spending
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @return {undefined}
 *
 * @param {Object} req.connection - Connection object
 * @param {String} req.connection.remoteAddress - ip address
 *
 * @param {Object} req.body - body of the tx
 */

// module.exports.processTransfer = async (req, res, next) => {
//     const methodName = '[ProcessTransfer]';
//     const transferBody = req.body;

//     const ip = req.connection.remoteAddress;

//     console.log(loggerName, methodName, ip, JSON.stringify(transferBody));

//     let isIcoEnd;
//     try {
//         let configs = await ConfigsModel.find();
//         await configs.map((data) => {
//             if (data.key === 'isIcoEnd') {
//                 isIcoEnd = data.valueBoolean;
//             }
//         });

//         if (!isIcoEnd) {
//             /* body must contain every parameter */
//             if (!transferBody || !transferBody.hash || !transferBody.type ||
//                 !transferBody.coin) {
//                 console.error(loggerName, methodName, 'Inputs are invalid');
//                 console.error(loggerName, methodName, transferBody);
//                 console.error(loggerName, methodName, transferBody.hash);
//                 console.error(loggerName, methodName, transferBody.type);
//                 console.error(loggerName, methodName, transferBody.coin);
//                 return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//             }

//             if ((!transferBody.wallet || !transferBody.transfer)) {
//                 console.error(loggerName, methodName, 'Inputs are invalid');
//                 console.error(loggerName, methodName, transferBody);
//                 console.error(loggerName, methodName, transferBody.wallet);
//                 console.error(loggerName, methodName, transferBody.transfer);
//                 return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//             }

//             /* check for coin supporting */
//             if (config.get('coins').indexOf(transferBody.coin) === -1) {
//                 return res.sendStatus(httpStatus.OK);
//             }

//             const coinTransferHook = new CoinTransferHooksModel({
//                 type: transferBody.type,
//                 wallet: transferBody.wallet,
//                 hash: transferBody.hash,
//                 transfer: transferBody.transfer,
//                 coin: transferBody.coin,
//                 // under process
//                 status: 'processing',
//             });

//             let wallet = await bitgo.coin(coinTransferHook.coin).wallets().get({ id: coinTransferHook.wallet });

//             let transaction = await wallet.getTransaction({ txHash: coinTransferHook.hash });

//             let transactionEntries = transaction.entries;

//             for (let transactionData of transactionEntries) {
//                 console.log(transactionData.address)
//                 let userOfAddress = await CoinAddressesModel.findOne({ 'address': transactionData.address });
//                 console.log(userOfAddress);
//                 if (userOfAddress) {
//                     let transfer = new CoinTransferHooksModel({
//                         type: transferBody.type,
//                         wallet: transferBody.wallet,
//                         hash: transferBody.hash,
//                         transfer: transferBody.transfer,
//                         coin: transferBody.coin,
//                         status: 'processing',// under process
//                         coinAddressId: userOfAddress._id
//                     });
//                     //check min investement in tokens and coins

//                     let existingCoinTransferHook = await CoinTransferHooksModel.findOne({
//                         transfer: coinTransferHook.transfer,
//                         coinAddressId: userOfAddress._id
//                     });

//                     if (existingCoinTransferHook) {
//                         console.log(loggerName, methodName, 'transfer already registered');
//                         if (existingCoinTransferHook.status === 'processed') {
//                             console.log("existingCoinTransferHook status is'processed'")
//                             //return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//                         }
//                         if (existingCoinTransferHook.status === 'processing') {
//                             console.log("existingCoinTransferHook status is'processing'")
//                             //return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//                         }
//                         if (existingCoinTransferHook.status === 'insert-error' ||
//                             existingCoinTransferHook.status === 'update-error') {
//                             console.log(loggerName, methodName, 'recovering transfer', existingCoinTransferHook);

//                             try {
//                                 let updatedCoinTransfer = await CoinTransfersModel.findOneAndUpdate({ transfer: existingCoinTransferHook.transfer }, transfer, { new: true });
//                                 if (updatedCoinTransfer) {
//                                     await CoinTransferHooksModel.findOneAndUpdate({
//                                         _id: existingCoinTransferHook._id
//                                     }, { status: 'processed' }, { new: true });
//                                 }
//                             } catch (error) {
//                                 existingCoinTransferHook.status = 'update-error';
//                                 await existingCoinTransferHook.save();
//                             }
//                         }

//                     }
//                     else {
//                         let [currencyCOIN, currencyTKN] = await Promise.all([
//                             CurrenciesModel.findOne({ abbr: coinTransferHook.coin }),
//                             CurrenciesModel.findOne({ abbr: 'tkn' }),
//                         ]);

//                         if (!currencyCOIN || !currencyTKN) {
//                             console.log("currencyCOIN or currencyTKN not found")
//                             return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//                         }

//                         let savedCoinTransferHook = await transfer.save();

//                         let coinTransfer = await new CoinTransfersModel(transaction).save();
//                         if (!coinTransfer) {
//                             console.log("coin transafer not saved")
//                             return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//                         }
//                         let myAddress = await CoinAddressesModel.findOne({ address: transactionData.address })

//                         if (myAddress) {

//                             const updateQuery = {
//                                 coinResult: mathLib.safeAdd(myAddress.coinResult,mathLib.safeMultiply(transactionData.value,config.get(savedCoinTransferHook.coin + '-double'))),
//                                 status: 'processed',
//                             };

//                             let coinAddress = await CoinAddressesModel.findOneAndUpdate({ _id: myAddress._id }, updateQuery, { new: true });
//                             if (coinAddress) {
//                                 // TKN amount
//                                 const amountTKN = mathLib.safeMultiply(coinAddress.coinResul,coinAddress.exchangeRateValue);

//                                 let financialHistoryObj={
//                                     userId: coinAddress.userId,
//                                     amount: coinAddress.coinResult,
//                                     exchangeRateValue: coinAddress.exchangeRateValue,
//                                     currencyAbbr: currencyCOIN.abbr,
//                                     currencyName: currencyCOIN.name,
//                                     currencyId: currencyCOIN._id,
//                                     type: 'deposit',
//                                     coinAddressId: coinAddress._id
//                                 }
//                                 const depositCOIN = new FinancialHistoriesModel({financialHistoryObj});
//                                 await depositCOIN.save();

//                                 financialHistoryObj.type= 'invest';
//                                 const investCOIN = new FinancialHistoriesModel({financialHistoryObj});
//                                 await investCOIN.save();

//                                 financialHistoryObj.type= 'deposit';
//                                 financialHistoryObj.currencyId= currencyTKN._id;
//                                 financialHistoryObj.currencyAbbr=currencyTKN.abbr;
//                                 financialHistoryObj.currencyName=currencyTKN.name;

//                                 const depositTKNObj = new FinancialHistoriesModel({financialHistoryObj});
//                                 let depositTKN = await depositTKNObj.save();

//                                 console.log(loggerName, methodName, 'depositTKN saved');
//                                 emitter.emit('fh-tkn-deposit' + depositTKN.type, depositTKN);
//                                 console.log(loggerName, methodName, 'account received');

//                                 let user = await systemQueue.add(async () => {

//                                     // to update available tokens config
//                                     let availableTokensConfig = await ConfigsModel.findOne({ key: 'availableTokens' })
//                                     if (!availableTokensConfig) {
//                                         return false;
//                                     }
//                                     availableTokensConfig.valueDouble =mathLib.safeSubtract(availableTokensConfig.valueDouble, depositTKN.amount);
//                                     availableTokensConfig.markModified('valueDouble');
//                                     await availableTokensConfig.save();

//                                     // to update sold tokens config
//                                     let soldTokensConfig = await ConfigsModel.findOne({ key: 'soldTokens' });
//                                     if (!soldTokensConfig) {
//                                         return false;
//                                     }
//                                     soldTokensConfig.valueDouble = mathLib.safeAdd(soldTokensConfig.valueDouble ,depositTKN.amount);
//                                     soldTokensConfig.markModified('valueDouble');
//                                     await soldTokensConfig.save();

//                                     //to update user balance
//                                     let user = await UsersModel.findById(coinAddress.userId);
//                                     await UsersModel.findOneAndUpdate(
//                                         { _id: coinAddress.userId },
//                                         {
//                                             balanceTKN: mathLib.safeAdd(user.balanceTKN ,depositTKN.amount),
//                                             purchasedTKN: mathLib.safeAdd(user.purchasedTKN ,depositTKN.amount),
//                                         }, { new: true });

//                                     user = await UsersModel.findById({ _id: coinAddress.userId });

//                                     const cryptoCurrencyType = coinAddress.currencyAbbr;
//                                     const investCoinAmnt = coinAddress.coinResult;
//                                     const coinCurrencyId = coinAddress.currencyId;

//                                     // Referral income calculation
//                                     if (config.get('referral-enabled') && user.referBy) {
//                                         user = await manageReferral(user, depositTKN.amount,
//                                             coinAddress, currencyTKN, cryptoCurrencyType, investCoinAmnt, coinCurrencyId);
//                                     }

//                                     // if (config.get('bonus-percentage') > 0) {
//                                     //     user = await calculateBonus(user, depositTKN, coinAddress, currencyTKN);
//                                     //     console.log(loggerName, methodName, 'bonus saved');
//                                     // }

//                                     //save coin transfer hook
//                                     savedCoinTransferHook.status = 'processed';
//                                     savedCoinTransferHook.markModified('status');
//                                     let updatedCoinTransferHook = await savedCoinTransferHook.save();
//                                     return user;
//                                 });

//                                 if (user) {

//                                     let userObj = await UsersModel.findById({ _id: user._id });
//                                     console.log(loggerName, methodName, 'user balance update', userObj);
//                                     emitter.emit('balance-changed', userObj);
//                                     emitter.emit('address-balance-changed', userObj, coinAddress);
//                                     emitter.emit('config-changed');
//                                     emitter.emit('fh-invest-reload', userObj._id);
//                                 }
//                                 else {
//                                     console.log("WEBHOOK ERRRRORRRRR")
//                                     return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//                                 }

//                             }//closing of   if (coinAddress) { 
//                             else {
//                                 console.log("WEBHOOK ERRRRORRRRR")
//                                 //return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: 'err' });
//                             }
//                         }//closing of   if (myAddress) { 

//                     }//closing of else {

//                 }//closing of if (userOfAddress )
//                 else {
//                     console.log("ADDRESS NOT FOUND IN OUR DATABASE")
//                 }
//             };

//             //closing of for loop
//             console.log("WEBHOOK SUCCCESSSSSSS")
//             return res.sendStatus(httpStatus.OK);
//         }
//         else {
//             return res.sendStatus(httpStatus.OK);
//         }
//     } catch (error) {
//         console.log("WEBHOOK ERRRRORRRRR")
//         return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ err: error });
//     }

// }



module.exports.processTransfer = async (req, res) => {
    const methodName = '[ProcessTransfer]';
    const transferBody = req.body;

    const ip = req.connection.remoteAddress;

    console.log(loggerName, methodName, ip, JSON.stringify(transferBody));

    let isIcoEnd;
    let bonusPercentage=0;
    try{
        let configs = await ConfigsModel.find();
        await configs.map((data) => {
            if(data.key === 'isIcoEnd'){
                isIcoEnd = data.valueBoolean;
            }
            if(data.key==='normalBonusPercentage'){
                bonusPercentage=data.valueDouble;
            }
        });
    }catch (err) {
        return res.status(httpStatus.BAD_REQUEST).send({err: err});
    }

    if(!isIcoEnd){
        /* body must contain every parameter */
        if (!transferBody || !transferBody.hash || !transferBody.type ||
            !transferBody.coin) {
            console.error(loggerName, methodName, 'Inputs are invalid');
            console.error(loggerName, methodName, transferBody);
            console.error(loggerName, methodName, transferBody.hash);
            console.error(loggerName, methodName,transferBody.type);
            console.error(loggerName, methodName, transferBody.coin);
            throw new InvalidInputParametersError();
        }

        if ((!transferBody.wallet || !transferBody.transfer)) {
            console.error(loggerName, methodName, 'Inputs are invalid');
            console.error(loggerName, methodName, transferBody);
            console.error(loggerName, methodName, transferBody.wallet);
            console.error(loggerName, methodName, transferBody.transfer);
            throw new InvalidInputParametersError();
        }

        /* check for coin supporting */
        if (config.get('coins').indexOf(transferBody.coin) === -1) {
            throw new CoinTypeNotSupportedError(transferBody.coin);
        }

        const coinTransferHook = new CoinTransferHooksModel({
            type: transferBody.type,
            wallet: transferBody.wallet,
            hash: transferBody.hash,
            transfer: transferBody.transfer,
            coin: transferBody.coin,
            // under process
            status: 'processing',
        });
        
        /*if (coinTransferHook.coin === 'teth' || coinTransferHook.coin === 'eth') {
            let walletId = config.get('bitgo-' + coinTransferHook.coin + '-wallet-id');
        } else {
            let userWalletLimit = config.get('bitgo-' + coinTransferHook.coin + '-wallet-id').length;
            let randomNumber = Math.floor(Math.random() * userWalletLimit) + 1;
            let walletId = config.get('bitgo-' + coinTransferHook.coin + '-wallet-id')[randomNumber - 1];
        }*/

        bitgo.coin(coinTransferHook.coin).wallets().get({
            id: transferBody.wallet,
        }).then((wallet) => {


            wallet.getTransaction({txHash: coinTransferHook.hash})
                .then(function (transaction) {

                    return Promise.each(transaction.entries, (entry) => {
                        return new Promise(function (resolve, reject) {

                            return CoinAddressesModel.findOne({
                                'address': entry.address
                            }).exec().then((userOfAddress) => {

                                if (!userOfAddress) {
                                    return resolve();
                                } else {

                                    let transfer = new CoinTransferHooksModel({
                                        type: transferBody.type,
                                        wallet: transferBody.wallet,
                                        hash: transferBody.hash,
                                        transfer: transferBody.transfer,
                                        coin: transferBody.coin,
                                        // under process
                                        status: 'processing',
                                        coinAddressId: userOfAddress._id
                                    });

                                    CoinTransferHooksModel.findOne({
                                        transfer: coinTransferHook.transfer,
                                        coinAddressId: userOfAddress._id
                                    }).exec().then((existingCoinTransferHook) => {


                                        if (existingCoinTransferHook) {

                                            console.log(loggerName, methodName, 'transfer already registered');

                                            if (existingCoinTransferHook.status === 'processed') {
                                                return resolve();
                                            }

                                            if (existingCoinTransferHook.status === 'processing') {
                                                return reject();
                                            }

                                            if (existingCoinTransferHook.status === 'insert-error' ||
                                                existingCoinTransferHook.status === 'update-error') {
                                                console.log(loggerName, methodName, 'recovering transfer', existingCoinTransferHook);
                                            } else {
                                                console.error(loggerName, methodName, 'existing coin transfer' + 'hook has status', existingCoinTransferHook.status);
                                                return reject();
                                            }

                                            CoinTransfersModel.findOneAndUpdate({
                                                id: existingCoinTransferHook.transfer
                                            }, transfer, {new: true})
                                                .exec()
                                                .then((updatedCoinTransfer) => {

                                                    CoinTransferHooksModel.findOneAndUpdate({
                                                        _id: existingCoinTransferHook._id
                                                    }, {status: 'processed'}, {new: true})
                                                        .exec().then(() => {

                                                        return resolve();

                                                    }).catch((err) => {

                                                        existingCoinTransferHook.status = 'update-error';
                                                        existingCoinTransferHook.markModified('status');
                                                        existingCoinTransferHook.save().then(() => {
                                                            return resolve()
                                                        }).catch((err) => {
                                                            return reject();
                                                        })
                                                    })
                                                }).catch((err) => {
                                                existingCoinTransferHook.status = 'update-error';
                                                existingCoinTransferHook.markModified('status');
                                                existingCoinTransferHook.save().then(() => {
                                                    return resolve()
                                                }).catch((err) => {
                                                    return reject();
                                                })
                                            })


                                        } else {


                                            return Promise.all([
                                                CurrenciesModel.findOne({abbr: coinTransferHook.coin}),
                                                CurrenciesModel.findOne({abbr: 'tkn'}),
                                            ]).spread((currencyCOIN, currencyTKN) => {

                                                if (!currencyCOIN) {
                                                    return reject();
                                                }

                                                if (!currencyTKN) {
                                                    return reject();
                                                }

                                                console.log(loggerName, methodName, 'got currencies');


                                                // save the hook as the first step
                                                return transfer.save().then((savedCoinTransferHook) => {

                                                    if (!savedCoinTransferHook) {
                                                        return reject();
                                                    }
                                                    console.log(loggerName, methodName, 'savedCoinTransferHook saved', savedCoinTransferHook.coin, savedCoinTransferHook);


                                                    return new CoinTransfersModel(transaction).save().then((coinTransfer) => {


                                                        if (!coinTransfer) {
                                                            return reject();
                                                        }


                                                        return CoinAddressesModel.findOne({
                                                            address: entry.address
                                                        }).exec().then((myAddress) => {

                                                            if (!myAddress) {
                                                                return resolve()
                                                            } else {

                                                                const updateQuery = {
                                                                    coinResult: myAddress.coinResult + (entry.value * config.get(savedCoinTransferHook.coin + '-double')),
                                                                    status: 'processed',
                                                                };

                                                            return CoinAddressesModel.findOneAndUpdate(
                                                                {_id: myAddress._id},
                                                                updateQuery,
                                                                {new: true}).exec().then((coinAddress) => {
                                                                if (!coinAddress) {
                                                                    return reject();
                                                                }
                                                                console.log(loggerName, methodName, 'updated coinAddress received', JSON.stringify(coinAddress));
                                                                    const depositCOIN = new FinancialHistoriesModel({
                                                                        userId: coinAddress.userId,
                                                                        amount: coinAddress.coinResult,
                                                                        exchangeRateValue: coinAddress.exchangeRateValue,
                                                                        currencyAbbr: currencyCOIN.abbr,
                                                                        currencyName: currencyCOIN.name,
                                                                        currencyId: currencyCOIN._id,
                                                                        type: 'deposit',
                                                                        coinAddressId: coinAddress._id
                                                                    });

                                                                        return depositCOIN.save().then(() => {
                                                                            console.log(loggerName, methodName, 'depositCOIN saved');

                                                                            const investCOIN = new FinancialHistoriesModel({
                                                                                userId: coinAddress.userId,
                                                                                amount: coinAddress.coinResult,
                                                                                exchangeRateValue: coinAddress.exchangeRateValue,
                                                                                currencyId: currencyCOIN._id,
                                                                                currencyAbbr: currencyCOIN.abbr,
                                                                                currencyName: currencyCOIN.name,
                                                                                type: 'invest',
                                                                                coinAddressId: coinAddress._id
                                                                            });

                                                                            return investCOIN.save().then(() => {
                                                                                console.log(loggerName, methodName, 'investCOIN saved');

                                                                                // TKN amount
                                                                                const amountTKN = (coinAddress.coinResult * coinAddress.exchangeRateValue);

                                                                                const depositTKN = new FinancialHistoriesModel({
                                                                                    userId: coinAddress.userId,
                                                                                    amount: amountTKN,
                                                                                    exchangeRateValue: coinAddress.exchangeRateValue,
                                                                                    currencyId: currencyTKN._id,
                                                                                    currencyAbbr: currencyTKN.abbr,
                                                                                    currencyName: currencyTKN.name,
                                                                                    type: 'deposit',
                                                                                    coinAddressId: coinAddress._id
                                                                                });

                                                                                return depositTKN.save().then((depositTKN) => {
                                                                                    console.log(loggerName, methodName, 'depositTKN saved');
                                                                                    emitter.emit('fh-tkn-deposit' + depositTKN.type, depositTKN);
                                                                                    console.log(loggerName, methodName, 'account received');


                                                                                    return systemQueue.add(() => {
                                                                                        return ConfigsModel.findOne(
                                                                                            {key: 'availableTokens'}).exec().then((availableTokensConfig) => {
                                                                                            if (!availableTokensConfig) {
                                                                                                return reject();
                                                                                            }

                                                                                            availableTokensConfig.valueDouble -= depositTKN.amount;

                                                                                            availableTokensConfig.markModified('valueDouble');
                                                                                            return availableTokensConfig.save();

                                                                                        }).then(() => {
                                                                                            return ConfigsModel.findOne({key: 'soldTokens'}).exec();
                                                                                        }).then((soldTokensConfig) => {
                                                                                            if (!soldTokensConfig) {
                                                                                                return reject();
                                                                                            }
                                                                                            soldTokensConfig.valueDouble += depositTKN.amount;

                                                                                            soldTokensConfig.markModified('valueDouble');
                                                                                            return soldTokensConfig.save();
                                                                                        }).then(() => {
                                                                                            return UsersModel.findById(coinAddress.userId).exec();
                                                                                        }).then((user) => {
                                                                                            return UsersModel.findOneAndUpdate(
                                                                                                {_id: coinAddress.userId},
                                                                                                {
                                                                                                    balanceTKN: (user.balanceTKN + depositTKN.amount),
                                                                                                    purchasedTKN: (user.purchasedTKN + depositTKN.amount),
                                                                                                }, {new: true});
                                                                                        }).then(() => {
                                                                                            return UsersModel.findById({_id: coinAddress.userId})
                                                                                                .exec().then((user) => {
                                                                                                    return user;
                                                                                                });
                                                                                        }).then((user) => {

                                                                                            const cryptoCurrencyType = coinAddress.currencyAbbr;
                                                                                            const investCoinAmnt = coinAddress.coinResult;
                                                                                            const coinCurrencyId = coinAddress.currencyId;

                                                                                            // Referral income calculation
                                                                                            if (config.get('referral-enabled') && user.referBy) {

                                                                                                return manageReferral(user, depositTKN.amount,
                                                                                                    coinAddress, currencyTKN, cryptoCurrencyType, investCoinAmnt, coinCurrencyId).then((user) => {
                                                                                                    return user;
                                                                                                }).catch((err) => {
                                                                                                    return reject(err)
                                                                                                });
                                                                                            } else {
                                                                                                return user;
                                                                                            }
                                                                                        }).then((user) => {
                                                                                            if (bonusPercentage > 0) {
                                                                                                return calculateBonus(user, depositTKN,
                                                                                                    coinAddress, currencyTKN).then((user) => {
                                                                                                    console.log(loggerName, methodName,
                                                                                                        'bonus saved');
                                                                                                    return user;
                                                                                                });
                                                                                            } else {
                                                                                                return user;
                                                                                            }
                                                                                        }).then((user) => {
                                                                                            savedCoinTransferHook.status = 'processed';
                                                                                            savedCoinTransferHook.markModified('status');
                                                                                            return savedCoinTransferHook.save().then((updatedCoinTransferHook) => {
                                                                                                if (!updatedCoinTransferHook) {
                                                                                                    throw new CoinTransferHookUpdateError();
                                                                                                }
                                                                                                return user;
                                                                                            });
                                                                                        });

                                                                                    }).then((user) => {
                                                                                    return UsersModel.findById({_id: user._id}).then((user) => {
                                                                                        console.log(loggerName, methodName, 'user balance update', user);
                                                                                        emitter.emit('balance-changed', user);
                                                                                        emitter.emit('address-balance-changed', user, coinAddress);
                                                                                        emitter.emit('config-changed');
                                                                                        emitter.emit('fh-invest-reload', user._id);
                                                                                        // emitter.emit('transfer-mail', user.email,
                                                                                        //     user.balanceTKN.value,
                                                                                        //     depositCOIN.currencyName, depositCOIN.amount.value,
                                                                                        //     currentCryptoRates, tknBonusAmount, amountDeposited);
                                                                                        return resolve();
                                                                                    });
                                                                                })
                                                                            })
                                                                        })
                                                                    })
                                                            })

                                                            }

                                                        })


                                                    })
                                                })

                                            })


                                        }


                                    }).catch((err) => {
                                        return reject(err);
                                    })
                                }
                            })
                        })
                    }).then(() => {
                        console.log("WEBHOOK SUCCCESSSSSSS")
                        return res.sendStatus(httpStatus.OK);
                    }).catch((err) => {
                        console.log("WEBHOOK ERRRRORRRRR")
                        return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({err: err});
                    })
                }).catch((err) => {
                return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({err: err});
            })


        }).catch((err) => {
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({err: err});
        });
    }
};

module.exports.getReferral = (req, res, next) => {
    let ref = getReferralUser(req.body.referBy);
    ref.then((response) => {
        res.send(response);
    });
};

module.exports.getBonusPercentage = (req, res, next) => {
    return getBonusPercentage().then((bonus) => {
        return res.send({bonus: bonus});
    }).catch(next);
};

module.exports.getBtcInUSD = (req, res, next) => {
    return getBtcInUSD().then((valueInUSD) => {
        return res.send({valueInUSD: valueInUSD});
    }).catch(next);
};

module.exports.getEthInUSD = (req, res, next) => {
    return getEthInUSD().then((valueInUSD) => {
        return res.send({valueInUSD: valueInUSD});
    }).catch(next);
};

/**
 * Function to calculate referrer
 *
 * @param {String}  referBy
 */
const getReferralUser = (referBy) => {
    let refArray = [];
    refArray.push({
        percent: config.get('firstLevelReferralPercentage'), to: referBy,
    });
    return new Promise((resolve, reject) => {
        resolve(refArray);

    });
};
/**
 * Function to calculate and transfer user bonus
 *
 * @param {Object} user
 * @param {Object} depositTKN
 * @param {Object} coinAddress
 * @param {Object} currencyTKN
 *
 *
 */

const calculateBonus = async (user, depositTKN, coinAddress, currencyTKN) => {
    let bonusPercentage=0;
    let configs = await ConfigsModel.find();
    await configs.map((data) => {
        if(data.key==='normalBonusPercentage'){
            bonusPercentage=data.valueDouble;
        }
    });
    return new Promise((resolve, reject) => {
        let token = parseFloat(mathLib.valueOf(mathLib.safeDivide(mathLib.safeMultiply(depositTKN.amount,bonusPercentage),100)));
        const bonusHistory = new FinancialHistoriesModel(
            {
                userId: user._id,
                amount: token,
                exchangeRateValue: coinAddress.exchangeRateValue,
                currencyId: currencyTKN._id,
                currencyAbbr: currencyTKN.abbr,
                currencyName: currencyTKN.name,
                type: 'bonus',
                coinAddressId: coinAddress._id
            });
        bonusHistory.save().then(() => {
            UsersModel.findOneAndUpdate({_id: user._id},
                {
                    balanceTKN: parseFloat(mathLib.valueOf(mathLib.safeAdd(user.balanceTKN ,token))),
                    referralTKN: parseFloat(mathLib.valueOf(mathLib.safeAdd(user.referralTKN,token))),
                });
            resolve(user);
        }).catch((err) => reject(err));
    });
};

const manageReferral = (user, amountTKN, coinAddress, currencyTKN, cryptoCurrencyType, investCoinAmnt, coinCurrencyId) => {
    const methodName = '[manageReferral]';
    return new Promise((resolve, reject) => {
        return UsersModel.findById(user.referBy).exec().then((referralUser) => {
            console.log(loggerName, methodName, 'regular referral');
            return getReferralUser(user.referBy).then((refArray) => {
                ne.each(refArray, (ref, i) => {
                    let refAmount = parseFloat(mathLib.valueOf( mathLib.safeDivide(mathLib.safeMultiply(amountTKN,ref.percent),100)));
                    return sendReferralAmount(ref, refAmount, coinAddress, currencyTKN).then();
                }).then(() => {
                    console.log(loggerName, methodName, 'Referal income sent');
                    return resolve(user);
                }).catch((err) => {
                    console.error(loggerName, methodName, err);
                    reject(err);
                });
            });

        }).catch((err) => {
            console.error(loggerName, methodName, err);
            reject(err);
        });
    });
};

const sendReferralAmount = (ref, refAmount, coinAddress, currencyTKN) => {
    const methodName = '[sendReferralAmount]';
    return new Promise((resolve, reject) => {
        const referalFinHistory = new FinancialHistoriesModel({
            userId: ref.to,
            amount: refAmount,
            exchangeRateValue: coinAddress.exchangeRateValue,
            currencyId: currencyTKN._id,
            currencyAbbr: currencyTKN.abbr,
            currencyName: currencyTKN.name,
            type: 'referral_payment',
        });
        console.log(loggerName, methodName, 'referral Send to', ref);
        return referalFinHistory.save().then(() => {
            Promise.all([
                UsersModel.findOne({_id: ref.to}),
                ConfigsModel.findOne({key: 'availableTokens'}).exec(),
                ConfigsModel.findOne({key: 'soldTokens'}).exec(),
            ]).then((results) => {
                const user = results[0];
                let availableTokensConfig = results[1];
                let soldTokensConfig = results[2];
                availableTokensConfig.valueDouble = parseFloat(mathLib.valueOf(mathLib.safeSubtract(availableTokensConfig.valueDouble,refAmount)));
                availableTokensConfig.markModified('valueDouble');
                soldTokensConfig.valueDouble = parseFloat(mathLib.valueOf(mathLib.safeAdd(soldTokensConfig.valueDouble,refAmount)));
                soldTokensConfig.markModified('valueDouble');
                user.balanceTKN =parseFloat(mathLib.valueOf( mathLib.safeAdd(user.balanceTKN, refAmount)));
                const userDataModel = new UsersModel(user);
                console.log(loggerName, methodName, 'USERMODEL', userDataModel);
                Promise.all([
                    userDataModel.save(),
                    availableTokensConfig.save(),
                    soldTokensConfig.save(),
                ]).then(() => {
                    console.log(loggerName, methodName, 'user referral balance updated', user);
                });
                return user;
            }).then((user) => {
                return mailAgent.sendReferralEmailMailGun(user, refAmount).then((result) => {
                    console.log(loggerName, methodName, 'Send referral email',
                        user.email);
                    resolve(result);
                });
            });
        }).catch((err) => {
            reject(err);
        });
    });
};

const getBonusPercentage = () => {
    const now = new Date();
    return BonusModel.findOne({
        $and: [
            {startDate: {$lte: now}},
            {endDate: {$gte: now}}],
    }).exec().then((bonus) => {
        return bonus.percentage;
    });
};

const getBtcInUSD = () => {
    return CurrentRatesUsd.findOne({
        currencyId: 'btc',
    }).exec().then((btcData) => {
        return btcData.valueInUSD;
    });
};

const getEthInUSD = () => {
    return CurrentRatesUsd.findOne({
        currencyId: 'eth',
    }).exec().then((ethData) => {
        return ethData.valueInUSD;
    });
};
