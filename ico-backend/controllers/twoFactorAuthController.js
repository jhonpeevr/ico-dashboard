'use strict';

const usersModel = require('../lib/maindb').model('Users');
const httpStatus = require('http-status-codes');
const speakeasy = require('speakeasy');
const validator = require('validator');
const UserNotFoundError = require('../errors/UserNotFoundError');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const InvalidTwoFactorAuthError = require(
    '../errors/InvalidTwoFactorAuthError');

/**
 * Function checks if 2FA enabled for current user
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @param {String} req.decoded._id - current user
 *
 */
module.exports.check2FA = (req, res, next) => {
  usersModel.findOne({_id: req.decoded._id}).exec().then((user) => {
    if (!user) {
      throw new UserNotFoundError();
    } else {
      if (user.twoFaEnabled) {
        return res.status(httpStatus.ACCEPTED).
            send({msg: 'Two factor authentication is enabled'});
      } else {
        return res.status(httpStatus.OK).
            send({msg: 'Two factor authentication is disabled'});
      }
    }
  }).catch(next);
};

/**
 * Function generates 2FA code
 *
 * @param {Object} req
 * @param {Object} res
 *
 */
module.exports.generate2FA = (req, res) => {
  const secret = speakeasy.generateSecret({length: 20});

  res.send({
    url: secret.otpauth_url,
    secret: secret.base32,
  });
};

/**
 * Function enables 2FA for current user and updates generated code
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Object} next
 *
 */
module.exports.confirm2FA = (req, res, next) => {
  const secret = req.body.secret;
  const code = req.body.code;

  if (!secret || !code || !validator.isNumeric(code)) {
    throw new InvalidInputParametersError();
  }

  const verified = speakeasy.totp.verify({
    secret: secret,
    encoding: 'base32',
    token: code,
  });

  if (!verified) {
    throw new InvalidTwoFactorAuthError();
  }

  usersModel.findOneAndUpdate({_id: req.decoded._id},
      {twoFaEnabled: true, twoFaCode: secret}).
      exec().
      then((user) => {
        if (!user) {
          throw new UserNotFoundError();
        }
        return res.
            send({msg: 'Two factor authentication succesfully confirmed'});
      }).catch(next);
};

/**
 * Function disables 2FA for current user and updates generated code
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.disable2FA = (req, res, next) => {
  const code = req.body.code;

  if (!code || !validator.isNumeric(code)) {
    throw new InvalidInputParametersError();
  }

  usersModel.findOne({_id: req.decoded._id}).exec().then((user) => {
    if (!user) {
      throw new UserNotFoundError();
    }

    const verified = speakeasy.totp.verify({
      secret: user.twoFaCode,
      encoding: 'base32',
      token: code,
    });

    if (!verified) {
      throw new InvalidTwoFactorAuthError();
    }

    usersModel.findOneAndUpdate(
        {_id: user._id},
        {twoFaEnabled: false, twoFaCode: null}).
        exec().
        then(() => {
          return res.
              send({err: 'Two factor authentication succesfully disabled'});
        });
  }).catch(next);
};
