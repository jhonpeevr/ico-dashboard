'use strict';

const httpStatusCode = require('http-status-codes');
const validator = require('validator');
const userModel = require('../lib/maindb').model('Users');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const statuses = ['REJECTED', 'SUCCESS', 'SUBMITTED', 'PENDING', 'PROGRESS', 'INCOMPLETE'];
const loggerName = '[KYCEmailsController]';
const emitter = require('../lib/emitter');

/**
 * Function to generate the (KYC) user details of a user
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.decoded._id   - current user
 *
 */
module.exports.getEmails = (req, res, next) => {
  const methodName = '[getEmails]: ';
  console.log(loggerName, methodName);
  userModel.find({
    '$or': [
      {
        'kycStatus': 'PENDING',
      },
      {
        'kycStatus': null,
      },
    ],
    'active': true,
    'balanceTKN': {$gt: 0},
  }).
      select('email -_id').exec().then((users) => {
    return res.send(users);
  }).catch(next);
};

module.exports.kycSubmitted = (req, res, next) => {
  const methodName = '[kycSubmitted]: ';
  const userEmail = req.body.email;
  console.log(loggerName, methodName, userEmail);

  if (!userEmail || !validator.isEmail(userEmail)) {
    throw new InvalidInputParametersError();
  }
  userModel.findOne({email: userEmail}).exec().then((user) => {
    if (!user) {
      return res.status(httpStatusCode.BAD_REQUEST).
          send({msg: 'EMAIL NOT FOUND'});
    }
    userModel.findOneAndUpdate({email: userEmail},
        {kycSubmitted: true, kycStatus: statuses[3]}).
        exec().
        then(() => {
          emitter.emit('kyc-status-changed', userEmail);
          return res.sendStatus(httpStatusCode.OK);
        }).catch(next);
  }).catch(next);
};

module.exports.kycStatus = (req, res, next) => {
  const methodName = '[kycStatus]: ';
  const userEmail = req.body.email;
  const status = req.body.status;
  console.log(loggerName, methodName, userEmail, status);

  if (!userEmail || !status || statuses.indexOf(status) === -1 ||
      !validator.isEmail(userEmail)) {
    throw new InvalidInputParametersError();
  }
  const update = {kycStatus: status};
  if (status === statuses[1]) {
    update.kycConfirmed = true;
  }
  userModel.findOne({email: userEmail}).exec().then((user) => {
    if (!user) {
      return res.status(httpStatusCode.BAD_REQUEST).
          send({msg: 'EMAIL NOT FOUND'});
    }
    if (user.kycStatus === status) {
      return res.sendStatus(httpStatusCode.OK);
    }
    userModel.findOneAndUpdate({email: userEmail}, update).
        exec().
        then(() => {
          emitter.emit('kyc-status-changed', userEmail);
          return res.sendStatus(httpStatusCode.OK);
        }).
        catch(next);
  }).catch(next);
};
