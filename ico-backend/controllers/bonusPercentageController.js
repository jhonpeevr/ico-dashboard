'use strict';

const BonusPerStageModel = require('../lib/maindb').model('BonusPerStage');
const ConfigModel = require('../lib/maindb').model('Configs');

module.exports.bonusPercentage = async (req, res, next) => {
    console.log('bonusPercentage');
    try {
        let normalBonus;
        let stageBonus;

        let bonusPercentage = 0;

        let configs = await ConfigModel.find();

        await configs.map((data) => {
            if (data.key === 'normalBonus') {
                normalBonus = data.valueBoolean;
            }
            if (data.key === 'stageBonus') {
                stageBonus = data.valueBoolean;
            }
        });

        const soldTokensData = await ConfigModel.findOne({key: 'soldTokens'}).lean();
        const normalBonusData = await ConfigModel.findOne({key: 'normalBonusPercentage'}).lean();

        const soldTokens = soldTokensData.valueDouble;
        const normalBonusPercentage = normalBonusData.valueDouble;

        if(normalBonus && (normalBonusPercentage > 0)){ // normal bonus is applicable
            return res.send({"bonusPercentage": normalBonusPercentage});
        } else if (stageBonus){ // token stage bonus is applicable
            const stageBonusData =  await BonusPerStageModel.findOne({$and: [{minTokens: { $lte: soldTokens } },{maxTokens: { $gte: soldTokens }}]});
            if(stageBonusData){
                bonusPercentage = stageBonusData.percentage;
                return res.send({bonusPercentage});
            }else{
                return res.send({bonusPercentage});
            }
        } else { // no bonus is applicable
            return res.send({bonusPercentage});
        }
    }catch (error) {
        console.log('error ', error);
        next(error);
    }
};