'use strict';
const fs = require('fs');
const pdf = require('html-pdf');
const InvalidInputParametersError = require('../errors/InvalidInputParametersError');
const FinancialHistoriesModel = require('../lib/maindb').model('FinancialHistories');
const moment = require('moment');
/**
 * Function returns financial investments of the user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.decoded._id - current user
 *
 */
module.exports.listInvestments = (req, res, next) => {
    getInvestments(req.decoded._id).then((finHistories) => {
        return res.send(finHistories);
    }).catch(next);
};

const getInvestments = (userId) => {
    return FinancialHistoriesModel.find({
        userId: userId,
        type: ['invest', 'deposit', 'referral_payment', 'bonus'],
    }).sort('-timestamp');
};

module.exports.getInvestments = getInvestments;

/**
 * Function returns financial withdrawals of the user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.decoded._id - current user
 *
 */
module.exports.listWithdrawals = (req, res, next) => {
    getWithdrawals(req.decoded._id).then((finHistories) => {
        return res.send(finHistories);
    }).catch(next);
};

const getWithdrawals = (userId) => {
    return FinancialHistoriesModel.find({
        userId: userId,
        type: ['withdraw', 'referral_payment_canceled'],
    }).sort('-timestamp');
};

module.exports.getWithdrawals = getWithdrawals;

/**
 * Function returns all financial investments
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 */
module.exports.getAllInvestmentList = (req, res, next) => {
    let query;
    if (req.query.query && req.query.query !== '') {
        let searchData = req.query.query;
        query = {
            '$and': [
                {type: ['invest', 'deposit', 'referral_payment', 'bonus']},
                {
                    '$or': [
                        {'currencyAbbr': new RegExp(searchData, 'i')},
                        {'currencyName': new RegExp(searchData, 'i')},
                        {'type': new RegExp(searchData, 'i')},
                    ],
                }],
        };
    } else {
        query = {type: ['invest', 'deposit', 'referral_payment', 'bonus']};
    }

    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);

    if (perPage && page) {
        const skip = parseInt(perPage * page);
        FinancialHistoriesModel.find(query).skip(skip).limit(perPage).populate('userId').populate('currencyId').exec().then((finHistories) => {
            return res.send(finHistories);
        }).catch(next);
    } else {
        FinancialHistoriesModel.find(query).populate('userId').populate('currencyId').sort('-timestamp').exec().then((finHistories) => {
            return res.send(finHistories);
        }).catch(next);
    }
};

/**
 * Function returns all financial investments daily, weekly, monthly and yearly basis.
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 */
module.exports.getAllInvestmentListByRange = (req, res, next) => {
    const isDay = req.query.isDay;
    const isMonth = req.query.isMonth;
    const isYear = req.query.isYear;

    if (isDay && isDay === 'true') {
        FinancialHistoriesModel.aggregate(
            [
                {
                    '$match': {
                        'type': {
                            '$in': [
                                'invest',
                                'deposit',
                                'referral_payment',
                                'bonus',
                                'admin_deposit'],
                        },
                        'timestamp': {
                            $lte: new Date(),
                        },
                    },
                },
                {
                    '$group':
                        {
                            _id: {
                                day: {$dayOfYear: '$timestamp'},
                                year: {$year: '$timestamp'},
                            },
                            timestamp: {$first: '$timestamp'},
                            totalAmount: {$sum: '$amount'},
                            count: {$sum: 1},
                        },
                },
            ]
        ).sort('timestamp').then((data) => {
            return res.send({totalInvestmentByDay: data});
        });
    };

    if (isMonth && isMonth === 'true') {
        FinancialHistoriesModel.aggregate(
            [
                {
                    '$match': {
                        'type': {
                            '$in': [
                                'invest',
                                'deposit',
                                'referral_payment',
                                'bonus',
                                'admin_deposit'],
                        },
                        'timestamp': {
                            $lte: new Date(),
                        },
                    },
                },
                {
                    $group:
                        {
                            _id: {
                                month: {$month: '$timestamp'},
                                year: {$year: '$timestamp'},
                            },
                            timestamp: {$first: '$timestamp'},
                            totalAmount: {$sum: '$amount'},
                            count: {$sum: 1},
                        },
                },
            ]
        ).sort('timestamp').then((data) => {
            return res.send({totalInvestmentByMonth: data});
        });
    };

    if (isYear && isYear === 'true') {
        FinancialHistoriesModel.aggregate(
            [
                {
                    '$match': {
                        'type': {
                            '$in': [
                                'invest',
                                'deposit',
                                'referral_payment',
                                'bonus',
                                'admin_deposit'],
                        },
                        'timestamp': {
                            $lte: new Date(),
                        },
                    },
                },
                {
                    $group:
                        {
                            _id: {
                                year: {$year: '$timestamp'},
                            },
                            timestamp: {$first: '$timestamp'},
                            totalAmount: {$sum: '$amount'},
                            count: {$sum: 1},
                        },
                },
            ]
        ).sort('timestamp').then((data) => {
            return res.send({totalInvestmentByYear: data});
        });
    };

    if (req.query.hasOwnProperty('fromDate') && req.query.hasOwnProperty('toDate')) {
        const fromDate = req.query.fromDate;
        const toDate = req.query.toDate;

        FinancialHistoriesModel.aggregate(
            [
                {
                    '$match': {
                        'type': {
                            '$in': [
                                'invest',
                                'deposit',
                                'referral_payment',
                                'bonus',
                                'admin_deposit'],
                        },
                        'timestamp': {
                            $gte: new Date(fromDate),
                            $lte: new Date(toDate),
                        },
                    },
                },
                {
                    $group:
                        {
                            _id: {
                                week: {$week: '$timestamp'},
                                year: {$year: '$timestamp'},
                            },
                            timestamp: {$first: '$timestamp'},
                            totalAmount: {$sum: '$amount'},
                            count: {$sum: 1},
                        },
                },
            ]
        ).sort('timestamp').then((data) => {
            return res.send({totalInvestmentByRange: data});
        });
    }
};

/**
 * Function returns all financial investments of  a user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 */
module.exports.getUserInvestmentList = (req, res, next) => {

    let query;
    const userId = req.params.userId;

    query = {
        '$and': [
            {type: ['invest', 'deposit', 'referral_payment', 'bonus']},
            {userId: userId},
        ],
    };

    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);

    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        FinancialHistoriesModel.find(query).skip(skip).limit(perPage).populate('userId').populate('currencyId').exec().then((finHistories) => {
            return res.send(finHistories);
        }).catch(next);
    } else {
        FinancialHistoriesModel.find(query).populate('userId').populate('currencyId').sort('-timestamp').exec().then((finHistories) => {
            return res.send(finHistories);
        }).catch(next);
    }
};

module.exports.userInvestmentsCount = (req, res, next) => {
    const id = req.params.id;
    FinancialHistoriesModel.count({userId: id}).exec().then((fincount) => {
        return res.send({count: fincount});
    }).catch(next);
};
/**
 * Function returns all financial withdrawals
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 */
module.exports.getAllWithdrawalList = (req, res, next) => {
    let query;
    if (req.query.query && req.query.query !== '') {
        let searchData = req.query.query;
        query = {
            '$and': [
                {type: ['withdraw', 'referral_payment_canceled']},
                {
                    '$or': [
                        {'currencyAbbr': new RegExp(searchData, 'i')},
                        {'currencyName': new RegExp(searchData, 'i')},
                        {'type': new RegExp(searchData, 'i')},
                    ],
                }],
        };
    } else {
        query = {type: ['withdraw', 'referral_payment_canceled']};
    }

    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);
    if (perPage && page) {
        const skip = parseInt(perPage * page);
        FinancialHistoriesModel.find(query).skip(skip).limit(perPage).populate('userId').populate('currencyId').exec().then((finHistories) => {
            return res.send(finHistories);
        }).catch(next);
    } else {
        FinancialHistoriesModel.find(query).populate('userId').populate('currencyId').sort('-timestamp').exec().then((finHistories) => {
            return res.send(finHistories);
        }).catch(next);
    }
};

/**
 * Function returns all financial Investments Statics
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 */
module.exports.investmentStatistics = (req, res, next) => {
    const totalNoInvestments = new Promise((resolve) => {
        return FinancialHistoriesModel.count(
            {type: ['invest', 'deposit', 'referral_payment', 'bonus']}).exec().then((nbr) => {
            resolve({totalNoInvestments: nbr});
        });
    });
    const totalAmountInvestment = new Promise((resolve) => {
        return FinancialHistoriesModel.aggregate([
            {
                '$match': {
                    'type': {
                        '$in': [
                            'invest',
                            'deposit',
                            'referral_payment',
                            'bonus'],
                    },
                },
            },
            {$group: {_id: null, total: {$sum: '$amount'}}}]).exec().then((sum) => {
            resolve({totalAmountInvestment: sum[0] ? sum[0].total : 0});
        });
    });

    let totalNoInvestmentsByDate = {};

    if (req.query.hasOwnProperty('fromDate') && req.query.hasOwnProperty('toDate')) {
        const fromDate = req.query.fromDate;
        const toDate = req.query.toDate;
        totalNoInvestmentsByDate = new Promise((resolve) => {
            return FinancialHistoriesModel.aggregate([
                {
                    '$match': {
                        'type': {
                            '$in': [
                                'invest',
                                'deposit',
                                'referral_payment',
                                'bonus'],
                        },
                        'timestamp': {
                                $gte: new Date(fromDate),
                                $lte: new Date(toDate),
                            },
                    },
                },
                {$group: {_id: null, total: {$sum: '$amount'}}}]).exec().then((sum) => {
                resolve({totalAmountInvestmentByDate: sum[0] ? sum[0].total : 0});
            });
        });
    }


    Promise.all([
        totalNoInvestments,
        totalAmountInvestment,
        totalNoInvestmentsByDate,
    ]).then((data) => {
        let statistics = {};
        for (let stat of data) {
            statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
        }
        return res.send(statistics);
    }).catch(next);
};

/**
 * Function returns all financial Withdrawal Statics
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 */
module.exports.withdrawalStatistics = (req, res, next) => {
    const totalNoWithdrawal = new Promise((resolve) => {
        return FinancialHistoriesModel.count(
            {type: ['withdraw', 'referral_payment_canceled']}).exec().then((nbr) => {
            resolve({totalNoWithdrawal: nbr});
        });
    });
    const totalAmountWithdrawal = new Promise((resolve) => {
        return FinancialHistoriesModel.aggregate([
            {'$match': {'type': {'$in': ['withdraw', 'referral_payment_canceled']}}},
            {$group: {_id: null, total: {$sum: '$amount'}}}]).exec().then((sum) => {
            resolve({totalAmountWithdrawal: sum[0] ? sum[0].total : 0});
        });
    });

    Promise.all([
        totalNoWithdrawal,
        totalAmountWithdrawal,
    ]).then((data) => {
        let statistics = {};
        for (let stat of data) {
            statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
        }
        return res.send(statistics);
    }).catch(next);
};

/**
 * Get address associated finHistory
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 */
module.exports.getAddressInvest = (req, res, next) => {
    const addressId = req.params.addressId;
    const userId = req.decoded._id;

    if (!addressId || !userId) {
        throw new InvalidInputParametersError();
    }
    FinancialHistoriesModel.find({
        coinAddressId: addressId,
    }).populate('coinAddressId').exec().then((finHistory) => {
        res.send(finHistory);
    }).catch(next);
};

/**
 * Generate Pdf for particular transaction
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 */
module.exports.generateReceipt = (req, res, next) => {
    const transactionId = req.body.transactionId;
    const userId = req.decoded._id;

    if (!transactionId || !userId) {
        throw new InvalidInputParametersError();
    }
    FinancialHistoriesModel.find({coinAddressId: transactionId}).populate('userId').populate('coinAddressId').populate('currencyId').exec().then((finHistories) => {
        let pdfcontent = '<p>Transaction Receipt  </p> <table> <tr> <td>Amount</td> <td>Currency </td> <td>Type</td>  <td>Created</td> </tr>';
        for (let finHistory of finHistories) {
            pdfcontent += '<tr> <td> ' + finHistory.amount + '</td> <td> ' + finHistory.currencyName + '</td> <td> ' + finHistory.type + '</td>  <td> ' + moment(finHistory.timestamp).format('YYYY-DD-MM')
                + '</td> </tr>';
        }
        pdfcontent += '</table><br/> Address ' + finHistories[0].coinAddressId.address;
        let filename = finHistories[0]._id;
        filename = encodeURIComponent(filename) + '.pdf';
        pdf.create(pdfcontent).toFile('./receipt/' + filename, (err, response) => {
            if (err) return console.log(err);
            res.send({filename: filename});
        });
    }).catch(next);
};

