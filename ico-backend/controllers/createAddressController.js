'use strict';

const Promise = require('bluebird');
const bitgo = require('../lib/bitgo');
const config = require('config');

const CreateAddressModel = require('../lib/maindb').model('CreateAddress');
const CoinAddressesModel = require('../lib/maindb').model('CoinAddresses');

const InvalidInputParametersError = require('../errors/InvalidInputParametersError');

module.exports.createAddress = (req, res) => {

    // const coinType = req.params.coinType;
    const coinType = config.get('coins')[1];
    const addressQty = req.body.addressQty;

    if (!coinType || !addressQty) {
        throw new InvalidInputParametersError();
    }

    let counter = 0;

    bitgo.coin(coinType).wallets().get({
        'id': config.get('bitgo-' + coinType + '-wallet-id'),
    }).then((wallet) => {
        for (let i = 0; i < addressQty; i++) {
            wallet.createAddress().then((address) => {
                console.log('creating address '+address);
                //console.dir(address);
                if (counter === (addressQty - 1)) {
                    res.send({msg: 'Address generated'});
                }
                counter++;
            });
        }
    });
};

module.exports.fetchBitgoAddresses = (req, res, next) => {
    const coinType = config.get('coins')[1];
    bitgo.coin(coinType).wallets().get({
        'id': config.get('bitgo-' + coinType + '-wallet-id'),
    }).then((wallet) => {
        wallet.addresses({
            'limit': 1000,
        }).then(async (addresses) => {
            const etherAddressCount = addresses.count;
            const createdAddressCount = await CreateAddressModel.count({}).exec();
            // console.log('etherAddressCount '+etherAddressCount);
            for (let i = etherAddressCount-1; i > createdAddressCount-1; i--) {
                //for (let i = etherAddressCount-1; i >= 0; i--) {
                let ethAddress = addresses.addresses[i].address;
                CreateAddressModel.findOne({coinAddress: ethAddress}).exec().then((ethAddressFound) => {
                    if (!ethAddressFound) {
                        CoinAddressesModel.findOne({address: ethAddress}).exec().then((coinAddressFound) => {
                            if (!coinAddressFound) {
                                const ethAddressData = new CreateAddressModel({
                                    coinName: 'teth',
                                    coinAddress: ethAddress,
                                });
                                ethAddressData.save(() => {
                                    console.log('Address saved '+ethAddressData.coinAddress);
                                    return res.send({msg: 'Address synced'})
                                });
                            } else {
                                console.log('Address found in coin Address Model.'+coinAddressFound.address);
                                return res.send({msg: 'Address synced'})
                            }
                        });
                    } else {
                        console.log('Address found in create Address Model. '+ethAddressFound.coinAddress);
                        return res.send({msg: 'Address synced'})
                    }
                });
            }
        });
    }).catch(err=>{
        console.log('Bitgo Err', err);
        res.send({msg: 'Bitgo service is unavailable'})
    });
};

module.exports.getAddresses = (req, res, next) => {
    return CreateAddressModel.find({}).exec().then((addresses) => {
        return res.send(addresses);
    });
};

module.exports.createdAddressesStatistics = (req, res, next) => {
    const totalAddressesCreated = new Promise((resolve) => {
        return CreateAddressModel.count({}).exec().then((nbr) => {
            resolve({totalAddressesCreated: nbr});
        });
    });

    const totalAddressesNotUsed = new Promise((resolve) => {
        return CreateAddressModel.count({addressStatus: 'notused'}).exec().then((nbr) => {
            resolve({totalAddressesNotUsed: nbr});
        });
    });

    const totalAddressesUsed = new Promise((resolve, reject) => {
        return CreateAddressModel.count({addressStatus: 'used'}).exec().then((nbr) => {
            resolve({totalAddressesUsed: nbr});
        });
    });

    Promise.all([
        totalAddressesCreated,
        totalAddressesNotUsed,
        totalAddressesUsed]).then((data) => {
        let statistics = {};
        for (let stat of data) {
            statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
        }
        return res.send(statistics);
    });
};

