'use strict';

const config = require('config');
const UsdInvestModel = require('../lib/maindb').model('UsdInvests');
const CurrenciesModel = require('../lib/maindb').model('Currencies');
const ExchangeRatesModel = require('../lib/maindb').model('ExchangeRates');
const FinHistoryModel = require('../lib/maindb').model('FinancialHistories');
const UsersModel = require('../lib/maindb').model('Users');
const KycModel = require('../lib/maindb').model('KYCDetails');
const Promise = require('bluebird');
const emitter = require('../lib/emitter');
const MaxAmountExceededError = require('../errors/MaxAmountExceededError');
const MinAmountNotReachedError = require('../errors/MinAmountNotReachedError');
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const TransferNotFoundError = require('../errors/TransferNotFoundError');
const UsdInvestNotFoundError = require('../errors/UsdInvestNotFoundError');
const UsdInvestInvalidStatusError =
    require('../errors/UsdInvestInvalidStatusError');
const UserNotFoundError = require('../errors/UserNotFoundError');
const KYCNotConfirmedError = require('../errors/KYCNotConfirmedError');
const loggerName = '[UsdInvestController]: ';
const mathLib = require('../lib/mathematics');

/**
 * Function registers usd investment
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @param {String} req.decoded._id   - current user
 *
 * @param {Object} req.body - body of request
 *
 */
module.exports.register = (req, res, next) => {
  const methodName = '[Register]';

  // inputs
  const fullname = req.body.fullname;
  const country = req.body.country;
  const amountUsd = req.body.amountUsd;
  const paymentDetails = req.body.paymentDetails;

  if (!fullname || !country || !amountUsd || !paymentDetails ||
      typeof amountUsd !== 'number') {
    throw new InvalidInputParametersError();
  }

  console.log(loggerName, methodName, fullname, country, amountUsd,
      paymentDetails);

  const userId = req.decoded._id;

  UsersModel.findById(userId).exec().
      then((user) => {
        if (!user) {
          throw new UserNotFoundError();
        }

        return KycModel.find({userId: userId}).exec().then((kyc) => {
          if (config.get('kyc-enabled') && (!kyc ||
                  kyc.status !== 'confirmed')) {
            throw new KYCNotConfirmedError();
          }

          return Promise.all([
            CurrenciesModel.findOne({abbr: 'tkn'}),
            CurrenciesModel.findOne({abbr: 'usd'}),
          ]).spread((currencyTKN, currencyUSD) => {
            if (mathLib.isLessThan(amountUsd,currencyUSD.minAmount)) {
              throw new MinAmountNotReachedError(currencyUSD.minAmount);
            }

            if (mathLib.isGreaterThan(amountUsd,currencyUSD.maxAmount)) {
              throw new MaxAmountExceededError();
            }

            return ExchangeRatesModel.findOne(
                {
                  currencyIdFirst: currencyUSD._id,
                  currencyIdSecond: currencyTKN._id,
                }).
                exec().
                then((exchangeRate) => {
                  const usdInvest = new UsdInvestModel({
                    userId: req.decoded._id,
                    fullname: fullname,
                    country: country,
                    paymentDetails: paymentDetails,
                    amountUsd: amountUsd,
                    currencyId: currencyUSD._id,
                    exchangeRateValue: exchangeRate.exchangeRate,
                    status: 'processing',
                  });

                  return usdInvest.save();
                });
          }).then(() => {
            return UsdInvestModel.find(
                {userId: req.decoded._id, status: 'processing'}).
                populate('currencyId').
                exec().
                then((usdInvests) => {
                  return res.send(usdInvests);
                });
          });
        });
      }).catch(next);
};

/**
 * Function investmentByAdmin usd investment by Admin
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @param {String} req.decoded._id   - current user admin
 *
 * @param {Object} req.body - body of request
 *
 */
module.exports.investmentByAdmin = (req, res, next) => {
  const methodName = '[investmentByAdmin]';

  // inputs
  const amountUsd = parseFloat(req.body.amountUsd);
  const userId = req.body.userId;
  const paymentDetails = req.body.paymentDetails;

  if (!amountUsd || !userId ||
      typeof amountUsd !== 'number' || !paymentDetails) {
    throw new InvalidInputParametersError();
  }

  console.log(loggerName, methodName, amountUsd);

  UsersModel.findById(userId).exec().
      then((user) => {
        if (!user) {
          throw new UserNotFoundError();
        }

        return Promise.all([
          CurrenciesModel.findOne({abbr: 'tkn'}),
          CurrenciesModel.findOne({abbr: 'usd'}),
        ]).spread((currencyTKN, currencyUSD) => {
          if (mathLib.isLessThan(amountUsd, currencyUSD.minAmount)) {
            throw new MinAmountNotReachedError(currencyUSD.minAmount);
          }

          if (mathLib.isGreaterThan(amountUsd,currencyUSD.maxAmount)) {
            throw new MaxAmountExceededError();
          }

          return ExchangeRatesModel.findOne(
              {
                currencyIdFirst: currencyUSD._id,
                currencyIdSecond: currencyTKN._id,
              }).
              exec().
              then((exchangeRate) => {
                const usdInvest = new UsdInvestModel({
                  userId: userId,
                  amountUsd: amountUsd,
                  paymentDetails: paymentDetails,
                  currencyId: currencyUSD._id,
                  exchangeRateValue: exchangeRate.exchangeRate,
                  status: 'confirmed',
                });
                return usdInvest.save().then((usdInvest) => {
                  return usdInvest;
                });
              });
        }).then((usdInvest) => {
          const TKNVALUE = (amountUsd * usdInvest.exchangeRateValue);
          return Promise.all([
            CurrenciesModel.findOne({abbr: 'tkn'}),
            CurrenciesModel.findOne({abbr: 'usd'}),
          ]).spread((currencyTKN, currencyUSD) => {
            const finHistoryDeposit = new FinHistoryModel({
              userId: userId,
              amount: amountUsd,
              currencyId: currencyUSD._id,
              exchangeRateValue: 1,
              currencyAbbr: currencyUSD.abbr,
              currencyName: currencyUSD.name,
              type: 'deposit',
              status: 'confirmed',
            });
            console.log(loggerName, methodName, currencyUSD.abbr,
                currencyUSD.name);
            return finHistoryDeposit.save().then((finHistoryDeposit) => {
              emitter.emit('fh_deposit', finHistoryDeposit);

              return UsersModel.findOne(
                  {_id: userId}).
                  exec().
                  then((accountTKN) => {
                    if (!accountTKN) {
                      res.send('NO account found');
                    }
                    accountTKN.balanceTKN += TKNVALUE;
                    accountTKN.markModified('balanceTKN');

                    return accountTKN.save().then((account) => {
                      emitter.emit('balance_changed', account);

                      const finHistoryInvest = new FinHistoryModel({
                        userId: userId,
                        amount: amountUsd,
                        currencyId: currencyUSD._id,
                        exchangeRateValue: 1,
                        currencyAbbr: currencyUSD.abbr,
                        currencyName: currencyUSD.name,
                        type: 'invest',
                      });

                      return finHistoryInvest.save().
                          then((finHistoryInvest) => {
                            emitter.emit('fh_investment', finHistoryInvest);

                            const finHistoryTKNDeposit = new FinHistoryModel({
                              userId: userId,
                              amount: TKNVALUE,
                              currencyId: currencyTKN,
                              exchangeRateValue: usdInvest.exchangeRateValue,
                              currencyAbbr: currencyTKN.abbr,
                              currencyName: currencyTKN.name,
                              type: 'deposit',
                            });
                            return finHistoryTKNDeposit.save().
                                then((finHistoryTKNDeposit) => {
                                  emitter.emit('fh_deposit',
                                      finHistoryTKNDeposit);
                                  UsdInvestModel.find({}).
                                      populate('userId').
                                      populate('currencyId').
                                      exec().
                                      then((usdTransfers) => {
                                        emitter.emit('balance-changed', userId);
                                        emitter.emit('config-changed');
                                        emitter.emit('fh-tkn-deposit',
                                            finHistoryTKNDeposit);
                                        emitter.emit('fh-invest-reload',
                                            userId);
                                        return usdTransfers;
                                      }).then((usdTransfers) => {
                                    if (config.get('bonus-percentage') > 0) {
                                      console.log(loggerName, methodName,
                                          'user bonus update', accountTKN);
                                      return calculateBonus(accountTKN,
                                          finHistoryTKNDeposit,
                                          usdInvest, currencyTKN).
                                          then((user) => {
                                            console.log(loggerName, methodName,
                                                'bonus saved');
                                            res.send(usdTransfers);
                                          });
                                    } else {
                                      res.send(usdTransfers);
                                    }
                                  }).catch(next);
                                });
                          });
                    });
                  });
            });
          });
        });
      }).catch(next);
};

const calculateBonus = (user, depositTKN, coinAddress, currencyTKN) => {
  return new Promise((resolve, reject) => {
    let token = ((depositTKN.amount * config.get('bonus-percentage')) / 100);
    const bonusHistory = new FinHistoryModel(
        {
          userId: user._id,
          amount: token,
          exchangeRateValue: coinAddress.exchangeRateValue,
          currencyId: currencyTKN._id,
          currencyAbbr: currencyTKN.abbr,
          currencyName: currencyTKN.name,
          type: 'bonus',
        });
    bonusHistory.save().then(() => {
      UsersModel.
          findOneAndUpdate({_id: user._id},
              {
                balanceTKN: (user.balanceTKN + token),
              });
      resolve(user);
    });
  });
};

/**
 * Function returns getUsdInvests of usd investments of current user id
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @param {String} req.decoded._id   - current user
 *
 */
module.exports.getUsdInvests = (req, res, next) => {
  UsdInvestModel.find({userId: req.decoded._id, status: 'processing'}).
      populate('currencyId').
      exec().
      then((usdInvests) => {
        return res.send(usdInvests);
      }).
      catch(next);
};

/**
 * Function returns all usd investments
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @param {String} req.decoded._id   - current user
 *
 */
module.exports.getAllUsdInvests = (req, res, next) => {
  let searchData = req.query.query;
  let query;
  if (req.query.query && req.query.query !== '') {
    query = {
      '$or': [
        {'fullname': new RegExp(searchData, 'i')},
        {'country': new RegExp(searchData, 'i')},
        {'paymentDetails': new RegExp(searchData, 'i')},
      ],
    };
  } else {
    query = {};
  }
  const perPage = parseInt(req.params.perPage);
  const page = parseInt(req.params.page);
  if (perPage !== undefined && page !== undefined) {
    const skip = parseInt(perPage * page);
    UsdInvestModel.find(query).skip(skip).limit(perPage).
        populate('userId').
        populate('currencyId').
        exec().
        then((usdTransfers) => {
          return res.send(usdTransfers);
        }).
        catch(next);
  } else {
    UsdInvestModel.find(query).
        populate('userId').
        populate('currencyId').
        exec().
        then((usdTransfers) => {
          return res.send(usdTransfers);
        }).
        catch(next);
  }
};

/**
 * Function confirms USD investment
 * Protected by admin route
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.decoded._id   - current user
 * @param {Number} req.body.amountUsd
 * @param {Number} req.body.transactionId
 *
 */
module.exports.confirm = (req, res, next) => {
  const methodName = '[Confirm]';

  const usdTransferId = req.body.id;
  const amountUsd = req.body.amountUsd;
  const transactionId = req.body.transactionId;

  if (!usdTransferId || !amountUsd || !transactionId) {
    throw new InvalidInputParametersError();
  }

  console.log(loggerName, methodName, usdTransferId, amountUsd, transactionId);

  const updateQuery = {
    status: 'confirmed',
    resultUsd: amountUsd,
    transactionId: transactionId,
  };

  UsdInvestModel.findOneAndUpdate({_id: usdTransferId}, updateQuery).
      exec().
      then((usdTransfer) => {
        const TKNVALUE = (amountUsd * usdTransfer.exchangeRateValue);

        if (!usdTransfer) {
          throw new TransferNotFoundError();
        }

        return Promise.all([
          CurrenciesModel.findOne({abbr: 'tkn'}),
          CurrenciesModel.findOne({abbr: 'usd'}),
        ]).spread((currencyTKN, currencyUSD) => {
          const finHistoryDeposit = new FinHistoryModel({
            userId: usdTransfer.userId,
            amount: amountUsd,
            currencyId: currencyUSD._id,
            exchangeRateValue: 1,
            currencyAbbr: currencyUSD.abbr,
            currencyName: currencyUSD.name,
            type: 'deposit',
            status: 'confirmed',
          });
          console.log(loggerName, methodName, usdTransferId, currencyUSD.abbr,
              currencyUSD.name);
          return finHistoryDeposit.save().then((finHistoryDeposit) => {
            emitter.emit('fh_deposit', finHistoryDeposit);

            return UsersModel.findOne(
                {_id: usdTransfer.userId}).
                exec().
                then((accountTKN) => {
                  if (!accountTKN) {
                    res.send('NO account found');
                  }

                  accountTKN.balanceTKN += TKNVALUE;
                  accountTKN.markModified('balanceTKN');

                  return accountTKN.save().then((account) => {
                    emitter.emit('balance_changed', account);

                    const finHistoryInvest = new FinHistoryModel({
                      userId: usdTransfer.userId,
                      amount: amountUsd,
                      currencyId: currencyUSD._id,
                      exchangeRateValue: 1,
                      currencyAbbr: currencyUSD.abbr,
                      currencyName: currencyUSD.name,
                      type: 'invest',
                    });

                    return finHistoryInvest.save().then((finHistoryInvest) => {
                      emitter.emit('fh_investment', finHistoryInvest);

                      const finHistoryTKNDeposit = new FinHistoryModel({
                        userId: usdTransfer.userId,
                        amount: TKNVALUE,
                        currencyId: currencyTKN,
                        exchangeRateValue: usdTransfer.exchangeRateValue,
                        currencyAbbr: currencyTKN.abbr,
                        currencyName: currencyTKN.name,
                        type: 'deposit',
                      });

                      return finHistoryTKNDeposit.save().
                          then((finHistoryTKNDeposit) => {
                            emitter.emit('fh_deposit', finHistoryTKNDeposit);
                            UsdInvestModel.find({}).
                                populate('userId').
                                populate('currencyId').
                                exec().
                                then((usdTransfers) => {
                                  emitter.emit('balance-changed', userId);
                                  emitter.emit('config-changed');
                                  emitter.emit('fh-tkn-deposit',
                                      finHistoryTKNDeposit);
                                  emitter.emit('fh-invest-reload', userId);
                                  return res.send(usdTransfers);
                                }).
                                catch(next);
                          });
                    });
                  });
                });
          });
        });
      }).
      catch(next);
};

/**
 * Function cancels USD investment
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 * @param {String} req.decoded._id   - current user
 * @param {Number} req.body.usdInvestmentId
 * @param {Number} req.body.transactionId
 *
 */
module.exports.cancel = (req, res, next) => {
  const userId = req.decoded._id;
  const usdInvestmentId = req.body.usdInvestmentId;

  UsdInvestModel.findOne({userId: userId, _id: usdInvestmentId}).
      exec().
      then((usdInvest) => {
        if (!usdInvest) {
          throw new UsdInvestNotFoundError();
        }

        if (usdInvest.status !== 'processing') {
          throw new UsdInvestInvalidStatusError();
        }

        usdInvest.status = 'canceled';
        usdInvest.markModified('status');
        return usdInvest.save();
      }).
      then((usdInvest) => {
        if (!usdInvest) {
          throw new UsdInvestNotFoundError();
        }

        return UsdInvestModel.find(
            {userId: req.decoded._id, status: 'processing'}).
            exec();
      }).
      then((usdInvests) => {
        return res.send(usdInvests);
      }).
      catch(next);
};

module.exports.statistics = (req, res, next) => {
  const totalUSDInvest = new Promise((resolve) => {
    return UsdInvestModel.count({}).exec().then((nbr) => {
      resolve({totalUSDInvest: nbr});
    });
  });
  const totalProcessingInvest = new Promise((resolve) => {
    return UsdInvestModel.count({status: 'processing'}).exec().then((nbr) => {
      resolve({totalProcessingInvest: nbr});
    });
  });
  const totalConfirmedInvest = new Promise((resolve) => {
    return UsdInvestModel.count({status: 'confirmed'}).exec().then((nbr) => {
      resolve({totalConfirmedInvest: nbr});
    });
  });
  const totalCanceledInvest = new Promise((resolve) => {
    return UsdInvestModel.count({status: 'canceled'}).exec().then((nbr) => {
      resolve({totalCanceledInvest: nbr});
    });
  });
  Promise.all([
    totalUSDInvest,
    totalProcessingInvest,
    totalConfirmedInvest,
    totalCanceledInvest]).then((data) => {
    let statistics = {};
    for (let stat of data) {
      statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
    }
    return res.send(statistics);
  }).catch(next);
};

module.exports.decline = (req, res, next) => {
  const usdTransferId = req.body.usdInvestmentId;

  UsdInvestModel.findOne({_id: usdTransferId}).
      exec().
      then((usdInvest) => {
        if (!usdInvest) {
          throw new UsdInvestNotFoundError();
        }

        if (usdInvest.status !== 'processing') {
          throw new UsdInvestInvalidStatusError();
        }

        usdInvest.status = 'canceled';
        usdInvest.markModified('status');
        return usdInvest.save();
      }).
      then((usdInvest) => {
        if (!usdInvest) {
          throw new UsdInvestNotFoundError();
        }

        return res.send(usdInvest);
      }).
      catch(next);
};
