'use strict';
const config = require('config');
const bitgo = require('../lib/bitgo');
const enterprise = config.get('bitgo-enterprise-id');

/**
 * Returns list of specific coin wallets
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.params.coin
 *
 * @return {undefined}
 *
 */
module.exports.listWallets = (req, res, next) => {
  const coin = req.params.coin;

  if (!coin) {
    return res.send('Coin were not provided');
  }

  if (coin === 'tbtc' || coin === 'btc') {
    bitgo.wallets().list({}).then((data) => {
      let result = [];
      data.wallets.forEach((wallet, index) => {
        if (wallet.wallet.enterprise &&
            wallet.wallet.enterprise === enterprise) {
          result.push(wallet.wallet);
        }
      });
      return res.send(result);
    }).catch(next);
  } else {
    bitgo.coin(coin).wallets().list({}).then((wallets) => {
      return res.send(wallets);
    }).catch(next);
  }
};

/**
 * Generates wallet
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.body.coin
 * @param {String} req.body.label
 * @param {String} req.body.passphrase
 *
 * @return {undefined}
 *
 */
module.exports.generateWallet = (req, res, next) => {
  const coin = req.body.coin;
  const label = req.body.label;
  const passphrase = req.body.passphrase;

  if (!coin || !label || !passphrase) {
    return res.send('One of the required parameters were not provided');
  }

  if (coin === 'btc' || coin === 'tbtc') {
    return res.send({msg: 'method not available in BTC'});
  } else {
    bitgo.coin(coin).
        wallets().
        generateWallet(req.body).
        then((wallet) => {
          return res.send(wallet);
        }).
        catch(next);
  }
};

/**
 * Returns list of specific wallet webhooks
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.params.coin
 * @param {String} req.params.walletId
 *
 * @return {undefined}
 *
 */
module.exports.listHooks = (req, res, next) => {
  const coin = req.params.coin;
  const walletId = req.params.walletId;

  if (!coin || !walletId) {
    return res.send('Coin or Wallet were not provided');
  }

  if (coin === 'btc' || coin === 'tbtc') {
    bitgo.wallets().get({'id': walletId}).then((wallet) => {
      wallet.listWebhooks({}).then((result) => {
        return res.send(result);
      }).catch(next);
    }).catch(next);
  } else {
    bitgo.coin(coin).wallets().get({id: walletId}).then((wallet) => {
      wallet.listWebhooks().then((hooks) => {
        return res.send(hooks);
      });
    }).catch(next);
  }
};

/**
 * Adds hook to specific wallet
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.body.coin
 * @param {String} req.body.walletId
 * @param {String} req.body.type
 * @param {String} req.body.url
 * @param {Number} req.body.numOfConfirmations
 *
 * @return {undefined}
 *
 */
module.exports.addHook = (req, res, next) => {
  console.log(req.body);
  const coin = req.body.coin;
  const walletId = req.body.walletId;
  const type = req.body.type;
  const url = req.body.url;
  const numOfConfirmations = req.body.numOfConfirmations;

  if (!coin || !walletId || !type || !url || !numOfConfirmations) {
    return res.send('One of the required paramenters were not provided');
  }

  if (coin === 'btc' || coin === 'tbtc') {
    bitgo.wallets().get({'id': walletId}).then((wallet) => {
      wallet.addWebhook({url: url, type: 'transaction'}).then((result) => {
        return res.send(result);
      });
    }).catch(next);
  } else {
    bitgo.coin(coin).wallets().get({id: walletId}).then((wallet) => {
      return wallet.addWebhook({
        url: url,
        type: type,
        numOfConfirmations: numOfConfirmations,
      });
    }).then((webhook) => {
      return res.send(webhook);
    }).catch(next);
  }
};

/**
 * Removes hook from specific wallet
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @param {String} req.body.coin
 * @param {String} req.body.walletId
 * @param {String} req.body.url
 * @param {String} req.body.type
 *
 * @return {undefined}
 */
module.exports.removeHook = (req, res, next) => {
  const coin = req.body.coin;
  const walletId = req.body.walletId;
  const url = req.body.url;
  const type = req.body.type;

  if (!coin || !walletId || !url || !type) {
    return res.send('One of the required parameters were not provided');
  }
  if (coin === 'btc' || coin === 'tbtc') {
    bitgo.wallets().get({'id': walletId}, (err, wallet) => {
      wallet.removeWebhook({url: url, type: 'transaction'}).then((result) => {
        return res.send(result);
      }).catch(next);
    });
  } else {
    bitgo.coin(coin).wallets().get({id: walletId}).then((wallet) => {
      wallet.removeWebhook({url: url, type: type}).then((result) => {
        return res.send(result);
      }).catch(next);
    });
  }
};

/**
 * Returns list transfers for specific wallet
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.coin
 * @param {String} req.params.walletId
 *
 * @return {Object[]} transfers
 */
module.exports.listTransfers = (req, res, next) => {
  const coin = req.params.coin;
  const walletId = req.params.walletId;

  if (!coin || !walletId) {
    return res.send('One of the required parameters were not provided');
  }

  if (coin === 'btc' || coin === 'tbtc') {
    bitgo.wallets().get({'id': walletId}, (err, wallet) => {
      if (err) {
        throw err;
      }
      wallet.transactions({}, (err, transactions) => {
        // handle transactions
        if (err) {
          return res.send(err);
        }
        return res.send(transactions);
        //   console.log(JSON.stringify(transactions, null, 4));
      });
    });
  } else {
    bitgo.coin(coin).wallets().get({id: walletId}).then((wallet) => {
      wallet.transfers().then((transfers) => {
        return res.send(transfers);
      });
    }).catch(next);
  }
};

/**
 * Returns list of specific coin wallets
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.coin
 * @param {String} req.params.walletId
 * @param {String} req.parans.transferId
 *
 */
module.exports.getTransfer = (req, res, next) => {
  const coin = req.params.coin;
  const walletId = req.params.walletId;
  const transferId = req.params.transferId;

  if (!coin || !walletId || !transferId) {
    return res.send('One of the required parameters were not provided');
  }
  if (coin === 'btc' || coin === 'tbtc') {
    bitgo.wallets().get({'id': walletId}, (err, wallet) => {
      if (err) {
        throw err;
      }
      wallet.transactions({id: transferId}, (err, transactions) => {
        // handle transactions
        if (err) {
          return res.send(err);
        }
        return res.send(transactions);
        //   console.log(JSON.stringify(transactions, null, 4));
      });
    });
  } else {
    bitgo.coin(coin).wallets().get({id: walletId}).then((wallet) => {
      return wallet.transfers().get({id: transferId});
    }).then((transfer) => {
      return res.send(transfer);
    }).catch(next);
  }
};
