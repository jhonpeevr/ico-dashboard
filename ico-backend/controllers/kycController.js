'use strict';

const KYCModel = require('../lib/maindb').model('KYCDetails');
const config = require('config');
const httpStatus = require('http-status-codes');
const Promise = require('bluebird');
const fs = Promise.promisifyAll(require('fs'));
const InvalidInputParametersError = require(
    '../errors/InvalidInputParametersError');
const InvalidTwoFactorAuthError = require(
    '../errors/InvalidTwoFactorAuthError');
const KYCNotFoundError = require('../errors/KYCNotFoundError');
const KYCNotEnabled = require('../errors/KYCNotEnabled');
const KYCAlreadyExistError = require('../errors/KYCAlreadyExistError');
const usersModel = require('../lib/maindb').model('Users');

const loggerName = '[KYCController]';
const AWS = require('aws-sdk');
AWS.config.update({
  accessKeyId: config.get('aws_access_key_id'),
  secretAccessKey: config.get('aws_secret_key'),
});
const mv = require('mv');

const s3bucket = new AWS.S3({params: {Bucket: config.get('s3_bucket')}});
const speakeasy = require('speakeasy');
const validator = require('validator');


/**
 * Function to save the user details given by the user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {Object} req.connection
 *
 * @param {String} req.decoded._id   - current user
 * @param {String} req.body.kyc.firstName
 * @param {String} req.body.kyc.lastName
 * @param {String} req.body.kyc.address
 * @param {String} req.body.kyc.state
 * @param {String} req.body.kyc.country
 *
 * @param {Array} req.files
 * @param {Object} req.files.passportImage
 * @param {Object} req.files.utilityImage
 * @param {Object} req.body.kyc
 *
 */

let passportfilepath = '';
let renameAndStorepassport = (files, email) => {
    passportfilepath = '';
    let folderName = email.split('@').join('_') + '_' + new Date().toISOString().replace('T', '').substr(0, 10);
    let dir = './KYCDetails/' + folderName + '/';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    let imgName = 'passportImage';
    mv(files, dir + imgName + '.' + files.split('.').pop(), function (err) {
        passportfilepath = dir + imgName + '.' + files.split('.').pop();
        if (err) console.log('ERROR: ' + err);
    });
};

let otherfilepath = '';
let renameAndStoreOther = (files, email) => {
    otherfilepath = '';
    let folderName = email.split('@').join('_') + '_' + new Date().toISOString().replace('T', '').substr(0, 10);
    let dir = './KYCDetails/' + folderName + '/';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    let imgName = 'otherImage';
    mv(files, dir + imgName + '.' + files.split('.').pop(), function (err) {
        otherfilepath = dir + imgName + '.' + files.split('.').pop();
        if (err) console.log('ERROR: ' + err);
    });
};

module.exports.enterKYC = (req, res, next) => {
    if (!config.get('kyc-enabled')) {
        throw new KYCNotEnabled();
    }
    const methodName = '[EnterKYC]';

    const email = req.body.email;
    const passportImage = req.files.passportImage;
    const utilityImage = req.files.utilityImage;
    const kyc = JSON.parse(req.body.kyc);
    const userId = req.decoded._id;
    const name = kyc.name;
    const contactno = kyc.contactno;
    const address = kyc.address;
    const dateofBirth = kyc.dateOfBirth;
    const countryId = kyc.country;
    const passportNumber = kyc.passportNumber ? kyc.passportNumber : '';

    const imagesTypes = ['image/jpeg', 'image/png'];
    const limitSize = 10485760;

    console.log(loggerName, methodName, name, contactno, address, dateofBirth);

    if (!userId || !name || !contactno || !address || !countryId ||
        !dateofBirth) {
        throw new InvalidInputParametersError();
    }

    if (passportImage) {
        if (imagesTypes.indexOf(passportImage.type) === -1 ||
            passportImage.size <= 0 || passportImage.size > limitSize) {
            throw new InvalidInputParametersError();
        }
    }
    if (utilityImage) {
        if (imagesTypes.indexOf(utilityImage.type) === -1 ||
            utilityImage.size <= 0 || utilityImage.size > limitSize) {
            throw new InvalidInputParametersError();
        }
    }

    // check 2FA, if enable check and process.
    if (req.body.hasOwnProperty('code') && req.body.hasOwnProperty('twoFactorEnabled')) {
        const code = req.body.code;
        const twoFactorEnabled = req.body.twoFactorEnabled;

        if (code == '' || !validator.isNumeric(code) || !twoFactorEnabled) {
            throw new InvalidInputParametersError();
        }

        usersModel.findOne({_id: req.decoded._id}).exec().then((user) => {
            if (!user) {
                throw new UserNotFoundError();
            }

            const verified = speakeasy.totp.verify({
                secret: user.twoFaCode,
                encoding: 'base32',
                token: code,
            });

            if (!verified) {
                throw new InvalidTwoFactorAuthError();
            }

            const insertKYC = new Promise((resolve, reject) => {
                return KYCModel.findOne({userId: userId}).exec().then((user) => {
                    if (user) {
                        throw new KYCAlreadyExistError();
                    } else {
                        const kyc = new KYCModel({
                            userId: userId,
                            name: name,
                            contactno: contactno,
                            address: address,
                            countryId: countryId,
                            dateOfBirth: dateofBirth,
                            status: 'pending',
                            passportNumber: passportNumber,
                        });
                        return kyc.save().then((kyc) => {
                            if (!kyc) {
                                reject(kyc);
                            }
                            resolve();
                        }).catch(next);
                    }
                }).catch(next);
            }).catch(next);

            let passportPromise = null;
            let utilityPromise = null;

            if (passportImage) {
                renameAndStorepassport(passportImage.path, email);
                passportPromise = new Promise((resolve) => {
                    setTimeout(() => {
                        return KYCModel.findOneAndUpdate({userId: userId},
                            {
                                passportImage: passportfilepath,
                            }).exec().then(resolve());
                    }, 200);
                }).catch(next);
            }
            if (utilityImage) {
                renameAndStoreOther(utilityImage.path, email);
                utilityPromise = new Promise((resolve) => {
                    setTimeout(() => {
                        return KYCModel.findOneAndUpdate({userId: userId},
                            {
                                utilityImage: otherfilepath,
                            }).exec().then(resolve());
                    }, 200);
                }).catch(next);
            }

            Promise.all([insertKYC, passportPromise, utilityPromise]).then((data) => {
                if (data[0] === undefined) {
                    return res.send({msg: 'success'});
                } else {
                    return res.status(httpStatus.BAD_REQUEST).send({msg: 'Problem with KYC insert'});
                }
            }).catch(next);
        }).catch(next);
    } else {
        const insertKYC = new Promise((resolve, reject) => {
            return KYCModel.findOne({userId: userId}).exec().then((user) => {
                if (user) {
                    throw new KYCAlreadyExistError();
                } else {
                    const kyc = new KYCModel({
                        userId: userId,
                        name: name,
                        contactno: contactno,
                        address: address,
                        countryId: countryId,
                        dateOfBirth: dateofBirth,
                        status: 'pending',
                        passportNumber: passportNumber,
                    });
                    return kyc.save().then((kyc) => {
                        if (!kyc) {
                            reject(kyc);
                        }
                        resolve();
                    }).catch(next);
                }
            }).catch(next);
        }).catch(next);

        let passportPromise = null;
        let utilityPromise = null;

        if (passportImage) {
            renameAndStorepassport(passportImage.path, email);
            passportPromise = new Promise((resolve) => {
                setTimeout(() => {
                    return KYCModel.findOneAndUpdate({userId: userId},
                        {
                            passportImage: passportfilepath,
                        }).exec().then(resolve());
                }, 200);
            }).catch(next);
        }
        if (utilityImage) {
            renameAndStoreOther(utilityImage.path, email);
            utilityPromise = new Promise((resolve) => {
                setTimeout(() => {
                    return KYCModel.findOneAndUpdate({userId: userId},
                        {
                            utilityImage: otherfilepath,
                        }).exec().then(resolve());
                }, 200);
            }).catch(next);
        }

        Promise.all([insertKYC, passportPromise, utilityPromise]).then((data) => {
            if (data[0] === undefined) {
                return res.send({msg: 'success'});
            } else {
                return res.status(httpStatus.BAD_REQUEST).send({msg: 'Problem with KYC insert'});
            }
        }).catch(next);
    }
};

/**
 * Function to confirm the (KYC) user details given by the user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {Object} req.connection
 * @param {String} req.params.userId
 * @param {String} req.decoded._id   - current user
 *
 */
module.exports.confirmKYC = (req, res, next) => {
  if (!config.get('kyc-enabled')) {
    throw new KYCNotEnabled();
  }
  const methodName = '[ConfirmKYC]';
  const userId = req.params.userId;

  console.log(loggerName, methodName, req.decoded._id, userId);

  KYCModel.findOneAndUpdate({userId: userId}, {status: 'confirmed'},
      {new: true}).
      populate('countryId').
      exec().
      then((kyc) => {
        if (!kyc) {
          throw new KYCNotFoundError();
        } else {
          return res.send(kyc);
        }
      }).
      catch(next);
};

/**
 * Function to decline the (KYC) user details given by the user
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {Object} req.connection
 * @param {String} req.params.userId
 * @param {String} req.decoded._id - current user
 *
 */
module.exports.declineKYC = (req, res, next) => {
  if (!config.get('kyc-enabled')) {
    throw new KYCNotEnabled();
  }
  const userId = req.params.userId;

  KYCModel.findOneAndUpdate({userId: userId}, {status: 'declined'},
      {new: true}).
      populate('countryId').
      exec().
      then((kyc) => {
        if (!kyc) {
          throw new KYCNotFoundError();
        } else {
          return res.send(kyc);
        }
      }).
      catch(next);
};

/**
 * Function to generate all the user details having KYC details
 * Protected by admin route
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 */
module.exports.getAllKYCs = (req, res, next) => {
  if (!config.get('kyc-enabled')) {
    throw new KYCNotEnabled();
  }
  let query;
  if (req.query.query && req.query.query !== '') {
    let searchData = req.query.query;
    query = {
      '$or': [
        {'firstName': new RegExp(searchData, 'i')},
        {'lastName': new RegExp(searchData, 'i')},
        {'address': new RegExp(searchData, 'i')},
        {'state': new RegExp(searchData, 'i')},
        {'status': new RegExp(searchData, 'i')},
      ],
    };
  } else {
    query = {};
  }

  const perPage = parseInt(req.params.perPage);
  const page = parseInt(req.params.page);
  if (perPage !== undefined && page !== undefined) {
    const skip = parseInt(perPage * page);
    KYCModel.find(query).skip(skip).limit(perPage).
        populate('countryId').
        exec().
        then((users) => {
          return res.send(users);
        }).
        catch(next);
  } else {
    KYCModel.find(query).populate('countryId').exec().then((kycList) => {
      return res.send(kycList);
    }).catch(next);
  }
};

/**
 * Function to generate the (KYC) user details of a user
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.decoded._id   - current user
 *
 */
module.exports.getKYC = (req, res, next) => {
  if (!config.get('kyc-enabled')) {
    throw new KYCNotEnabled();
  }
  KYCModel.findOne({userId: req.decoded._id}).
      populate('countryId').
      then((kyc) => {
        if (!kyc) {
          return res.send({});
        }
        return res.send(kyc);
      }).
      catch(next);
};

/**
 * Function returns passport and utility image of a user
 * Protected by admin route
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @return {undefined}
 *
 * @param {String} req.params.userId
 *
 */
module.exports.getImages = (req, res, next) => {
  if (!config.get('kyc-enabled')) {
    throw new KYCNotEnabled();
  }
  const userId = req.params.userId;

  if (!userId) {
    return res.status(httpStatus.BAD_REQUEST).
        send({err: 'Invalid input parameters'});
  }

  KYCModel.findOne({userId: userId}).
      exec().
      then((images) => {
        return res.send(images);
      }).
      catch(next);
};

/**
 * Function returns true if KYC is enabled else false
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @return {undefined}
 *
 */
module.exports.getKYCEnabled = (req, res) => {
  return res.send(config.get('kyc-enabled'));
};

module.exports.statistics = (req, res, next) => {
  const totalKYC = new Promise((resolve, reject) => {
    return KYCModel.count({}).exec().then((nbr) => {
      resolve({totalKYC: nbr});
    });
  });
  const totalConfirmedKYC = new Promise((resolve, reject) => {
    return KYCModel.count({status: 'confirmed'}).exec().then((nbr) => {
      resolve({totalConfirmedKYC: nbr});
    });
  });
  const totalDeclinedKYC = new Promise((resolve, reject) => {
    return KYCModel.count({status: 'declined'}).exec().then((nbr) => {
      resolve({totalDeclinedKYC: nbr});
    });
  });
  const totalPendingKYC = new Promise((resolve, reject) => {
    return KYCModel.count({status: 'pending'}).exec().then((nbr) => {
      resolve({totalPendingKYC: nbr});
    });
  });
  Promise.all([
    totalKYC,
    totalConfirmedKYC,
    totalDeclinedKYC,
    totalPendingKYC]).then((data) => {
    let statistics = {};
    for (let stat of data) {
      statistics[Object.keys(stat)[0]] = stat[Object.keys(stat)[0]];
    }
    return res.send(statistics);
  }).catch(next);
};
