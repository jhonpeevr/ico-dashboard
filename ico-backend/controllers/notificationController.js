'use strict';

const notificationsModel = require('../lib/maindb').model('Notifications');
const userModel = require('../models/UsersModel');
const MessageUsersNotFound = require('../errors/MessageUsersNotFound');
const emitter = require('../lib/emitter');

/**
 * Function returns latest 5 notifications for currentUser
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.getNotifications = (req, res, next) => {
  const query = {
    $or: [{isForAll: true},{userId: req.decoded._id}]
  };

  const fields = {
    'message': true,
    'subject': true,
    'timestamp': true,
    'seen': true};
  const options = {
    sort: {timestamp: -1},
    limit: 5000,
  };

  notificationsModel.find(query, fields, options).
      exec().
      then((notifications) => {
        return res.send(notifications);
      }).
      catch(next);
};

/**
 * Function returns all notifications for the user
 * Protected by admin route
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.getAllNotifications = (req, res, next) => {

    const query = {
        $or: [{isForAll: true},{userId: req.decoded._id}]
    };

    notificationsModel.find(query).
      sort('-timestamp').
      exec().
      then((notifications) => {
        return res.send(notifications);
      }).
      catch(next);
};

/**
 * Function returns all notifications for all users by admin
 * Protected by admin route
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.getAllNotificationByAdmin = (req, res, next) => {
    let query;
    if (req.query.query && req.query.query !== '' && req.query.query != null && req.query.query != 'null') {
        let searchData = req.query.query;
        query = {
            '$or': [
                {'subject': new RegExp(searchData, 'i')},
                {'message': new RegExp(searchData, 'i')},
            ],
        };
    } else {
        query = {};
    }
    const perPage = parseInt(req.params.perPage);
    const page = parseInt(req.params.page);
    if (perPage !== undefined && page !== undefined) {
        const skip = parseInt(perPage * page);
        notificationsModel.find(query).sort('-timestamp').skip(skip).limit(perPage).populate('userId').exec().then((notifications) => {
            return res.send(notifications);
        }).catch(next);
    } else {
        notificationsModel.find(query).sort('-timestamp').populate('userId').exec().then((notifications) => {
            return res.send(notifications);
        }).catch(next);
    }
};

/**
 * Function send all notifications for the user/all
 * Protected by admin route
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @param {Boolean} req.body.isForAll
 * @param {String} req.body.subject
 * @param {String} req.body.message
 * @param {Array} req.body.emailList
 *
 */
module.exports.sendNotifications = (req, res, next) => {
    const isForAll = req.body.sendToAll;
    const subject = req.body.subject;
    const message = req.body.message;
    const emailList = req.body.emailList;

    if (isForAll) {
        // send to all
        const notification = new notificationsModel({
            isForAll: isForAll,
            subject: subject,
            message: message,
        });

        notification.save().then((savedNotification) => {
            emitter.emit('notification-for-all', savedNotification);
            return res.send({success: true});
        });
    } else {
        if (emailList.length === 0) {
            throw new MessageUsersNotFound();
        } else {
            // loop and store
            emailList.forEach((email) => {
                // check if present
                userModel.find({email: email.trim()}).select('_id').exec().then((userId) => {
                    if (userId) {
                        userId = userId[0]._id;
                        let notification = new notificationsModel({
                            subject: subject,
                            message: message,
                            userId: userId,
                        });
                        notification.save().then((savedNotification) => {
                            emitter.emit('notification-for-user', savedNotification);
                        });
                    }
                });
            });
            // need not to be sync with the each, return and free admin
            return res.send({success: true});
        }
    }
};

/**
 * Function returns all notifications for the user
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 *
 */
module.exports.updateSeenNotification = (req, res, next) => {
  notificationsModel.update(
      {userId: req.decoded._id},
      {seen: true},
      {multi: true}).
      then(() => {
          return notificationsModel.find({userId: req.decoded._id})
              .exec().then((notifications) => {
              return res.send(notifications);
          });
      }).
      catch(next);
};

module.exports.countNotification = (req, res, next) => {
    notificationsModel.count().then((count) => {
        return res.send({
            count: count,
        });
    });
};
