const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

const kycEmailsController = require('../controllers/kycEmailsController');

router.get('/getNonComplianceUsersDetail', kycEmailsController.getEmails);
router.post('/sendKYCAcknowledgement', kycEmailsController.kycSubmitted);
router.post('/submitKYCStatus', kycEmailsController.kycStatus);

module.exports = router;
