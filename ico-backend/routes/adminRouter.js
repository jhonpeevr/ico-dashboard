const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const userController = require('../controllers/userController');
const finHistoryController = require('../controllers/finHistoryController');
const kycController = require('../controllers/kycController');
const configController = require('../controllers/configController');
const coinInvestController = require('../controllers/coinInvestController');
const usdInvestController = require('../controllers/usdInvestController');
const bonusEVTController = require('../controllers/bonusEVTController');
const advisorsController = require('../controllers/advisorsController');
const notificationController = require('../controllers/notificationController');

router.get('/bonusEVT/:perPage?/:page?', bonusEVTController.bonusEVTData);
router.get('/bonusEVTStats/', bonusEVTController.bonusEVTStats);

router.get('/user/count', userController.countUsers);
router.get('/advisors/', advisorsController.advisorsReferralData);
router.get('/advisorsNumbers/', advisorsController.advisorsReferralUsers);

router.post('/user/update', userController.update);
router.post('/user/block', userController.block);
router.post('/user/activate', userController.activate);
router.get('/user/by-email/:email', userController.getUserByEmail);
router.get('/user/user-info/count/:id', userController.getUserInfoCount);
router.get('/user/user-info/:id/:perPage?/:page?', userController.getUserByID);
router.get('/user/:perPage?/:page?', userController.listAll);
router.get('/finhistory/investments/getallinvestment',
    finHistoryController.getAllInvestmentListByRange);
router.get('/finhistory/investments/:perPage?/:page?',
    finHistoryController.getAllInvestmentList);
router.get('/finhistory/userinvestments/count/:id',
    finHistoryController.userInvestmentsCount);
router.get('/finhistory/userinvestments/:userId/:perPage?/:page?',
    finHistoryController.getUserInvestmentList);

router.get('/finhistory/withdrawals/:perPage?/:page?',
    finHistoryController.getAllWithdrawalList);

router.get('/coin/generated-addresses/:perPage?/:page?',
    coinInvestController.listGeneratedAddresses);

router.get('/coin/generated-user-addresses/count/:id',
    coinInvestController.userAddressCount);

router.get('/coin/generated-user-addresses/:userId/:perPage?/:page?',
    coinInvestController.listGeneratedUserAddresses);

router.get('/kyc/images/:userId', kycController.getImages);
router.get('/kyc/:perPage?/:page?', kycController.getAllKYCs);
router.post('/kyc/confirm/:userId', kycController.confirmKYC);
router.post('/kyc/decline/:userId', kycController.declineKYC);

router.post('/usd-invest/confirm', usdInvestController.confirm);
router.post('/usd-invest/decline', usdInvestController.decline);
router.get('/usd-invest/:perPage?/:page?',
    usdInvestController.getAllUsdInvests);


router.get('/notification/count', notificationController.countNotification);
router.post('/send', notificationController.sendNotifications);
router.get('/notification/:perPage?/:page?', notificationController.getAllNotificationByAdmin);

router.get('/audit', (req, res) => {
  res.send([]);
});
router.get('/configs', configController.getAllConfigs);
// router.get('/config-map', configController.getConfigsArray);
router.post('/configs', configController.editDynamicConfig);
router.post('/user/withdrawal-block', userController.withdrawalBlock);
router.post('/user/withdrawal-activate', userController.withdrawalActivate);
// AML SYNCING with coinFirm
router.get('/proceed-aml', userController.processAml);
// Manual Investment by Admin on behalf of user
router.post('/add-investment', usdInvestController.investmentByAdmin);
router.post('/assign-token', coinInvestController.assignTokens);
module.exports = router;
