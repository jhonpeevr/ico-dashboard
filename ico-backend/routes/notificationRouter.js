const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationController');

router.get('/', notificationController.getNotifications);
router.post('/update-seen', notificationController.updateSeenNotification);
router.get('/all', notificationController.getAllNotifications);

module.exports = router;
