const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const exchangeRateController = require('../controllers/exchangeRateController');

router.get('/', exchangeRateController.getExchangeRates);
router.get('/getRates', exchangeRateController.getUsdRates);
router.get('/update', exchangeRateController.updateExchangeRate);
router.get('/tknInUSD', exchangeRateController.getTknInUSD);

module.exports = router;
