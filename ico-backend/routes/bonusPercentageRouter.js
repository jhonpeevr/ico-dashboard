const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const bonusPercentageController = require('../controllers/bonusPercentageController');

router.get('/bonusPercentage', bonusPercentageController.bonusPercentage);

module.exports = router;