const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const btcHookController = require('../controllers/coinHookController');

router.post('/transfer', btcHookController.processTransfer);
router.post('/getReferral', btcHookController.getReferral);
router.get('/bonus', btcHookController.getBonusPercentage);
module.exports = router;
