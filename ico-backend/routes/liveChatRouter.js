const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const liveChatController = require('../controllers/liveChatController');

router.post('/reply', liveChatController.reply);
router.get('/list-active', liveChatController.listActive);
router.get('/chats/:sessionId', liveChatController.getChat);

module.exports = router;
