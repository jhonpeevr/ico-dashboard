const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const configController = require('../controllers/configController');

router.get('/', configController.getConfigs);
router.get('/array', configController.getConfigsArray);

module.exports = router;
