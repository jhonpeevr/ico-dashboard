const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const createAddressController =
    require('../controllers/createAddressController');

// Create number of addresses using admin panel by giving addressQty
router.post('/', createAddressController.createAddress);

// Sync addresses in local database after address mining is complete
router.post('/fetchBitgoAddresses', createAddressController.fetchBitgoAddresses);

// Create number of addresses using admin panel by giving addressQty
router.get('/getAddresses', createAddressController.getAddresses);

// Get total addresses created , total addresses unused and  total addresses used
router.get('/addressesStatistics', createAddressController.createdAddressesStatistics);
// router.get('/update', exchangeRateController.updateExchangeRate);

module.exports = router;
