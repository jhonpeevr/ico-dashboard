const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const finHistoryController = require('../controllers/finHistoryController');

router.get('/investments', finHistoryController.listInvestments);
router.get('/withdrawals', finHistoryController.listWithdrawals);
router.post('/generate-receipt', finHistoryController.generateReceipt);
router.get('/address-invest/:addressId', finHistoryController.getAddressInvest);

module.exports = router;
