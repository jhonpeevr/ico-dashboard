const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const usdInvestController = require('../controllers/usdInvestController');

router.get('/', usdInvestController.getUsdInvests);
router.post('/register', usdInvestController.register);
router.post('/cancel', usdInvestController.cancel);

module.exports = router;
