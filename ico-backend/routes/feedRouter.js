const express = require('express');
const router = express.Router();
const feedController = require('../controllers/feedController');

router.get('/', feedController.getFeedsByID);
router.post('/', feedController.saveFeed);

module.exports = router;
