/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();
const kycController = require('../controllers/kycController');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();

router.post('/', multipartMiddleware, kycController.enterKYC);
router.post('/confirm/:userId', kycController.confirmKYC);
router.post('/decline/:userId', kycController.declineKYC);

router.get('/', kycController.getKYC);
router.get('/enabled', kycController.getKYCEnabled);

module.exports = router;
