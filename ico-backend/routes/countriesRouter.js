/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();
const countriesController = require('../controllers/countriesController');

router.get('/', countriesController.getAllCountries);

module.exports = router;
