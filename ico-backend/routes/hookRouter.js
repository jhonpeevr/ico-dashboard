const config = require('config');
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

if (String(config.get('ENV')) === 'DEVELOPMENT') {
  const hookController = require('../controllers/hookController');

  router.get('/list-wallets/:coin', hookController.listWallets);
  router.get('/list-hooks/:coin/:walletId', hookController.listHooks);

  router.get('/list-transfers/:coin/:walletId',
      hookController.listTransfers);

  router.get('/transfer/:coin/:walletId/:transferId',
      hookController.getTransfer);

  router.post('/generate-wallet', hookController.generateWallet);

  router.post('/add-hook', hookController.addHook);
  router.post('/remove-hook', hookController.removeHook);
}

module.exports = router;
