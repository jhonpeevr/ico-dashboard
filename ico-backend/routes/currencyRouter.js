const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const currencyController = require('../controllers/currencyController');

router.get('/all', currencyController.getCurrencies);

module.exports = router;
