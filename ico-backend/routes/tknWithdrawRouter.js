const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const ethWithdrawController = require('../controllers/tknWithdrawController');

router.post('/send-tx', ethWithdrawController.sendTx);

module.exports = router;
