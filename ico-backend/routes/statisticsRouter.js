const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const userController = require('../controllers/userController');
const kycController = require('../controllers/kycController');
const coinInvestController = require('../controllers/coinInvestController');
const usdInvestController = require('../controllers/usdInvestController');
const finHistoryController = require('../controllers/finHistoryController');
const createAddressController = require('../controllers/createAddressController');
// const configController = require('../controllers/configController');

router.get('/user', userController.statistics);
router.get('/kyc', kycController.statistics);
router.get('/coin', coinInvestController.statistics);
router.get('/usd-invest', usdInvestController.statistics);
router.get('/investments', finHistoryController.investmentStatistics);
router.get('/withdrawal', finHistoryController.withdrawalStatistics);
router.get('/createdAddresses', createAddressController.createdAddressesStatistics);

module.exports = router;
