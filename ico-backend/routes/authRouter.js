const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const authController = require('../controllers/authController');
// const passport = require('../lib/passport');
const passport = require('passport');
router.post('/', authController.auth);
router.post('/sign-up', authController.signup);
router.post('/activate/:email/:activationCode', authController.activateEmail);
router.post('/activateEmailResend', authController.activateEmailResend);
router.post('/change-password', authController.changePassword);
router.post('/forgot-password', authController.forgotPassword);
router.post('/modify-password', authController.modifyPassword);
router.post('/update-withdraw-type', authController.updateWithdrawalType);
router.get('/balance', authController.getBalanceTKN);
router.get('/my-referrals', authController.getReferrals);
router.post('/validate-twofa', authController.validateTwoFa);
router.post('/email-verifivation', authController.activateDeactivateEmailVerification);
router.post('/user-info', authController.saveUserInfo);


router.route('/facebook')
    .post(passport.authenticate('facebook-token', {session: false}), authController.fbAuth);
router.route('/google')
    .post(passport.authenticate('google-token', {session: false}), authController.googleAuth);

module.exports = router;
