const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const subscriptionController = require('../controllers/subscriptionController');

router.post('/subscribe/:email', subscriptionController.subscribe);
// router.get('/config-map', configController.getConfigsArray);
router.get('/', subscriptionController.getAllSubscriptions);

module.exports = router;
