const config = require('config');
const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();

if (String(config.get('ENV')) === 'DEVELOPMENT') {
  const eventSimulatorController =
      require('../controllers/eventSimulatorController');

  router.post('/finhistory',
      eventSimulatorController.simulateFinancialHistories);

  router.post('/balance-changed',
      eventSimulatorController.simulateBalanceChange);

  router.post('/config-changed',
      eventSimulatorController.simulateConfigUpdate);

  router.post('/notifications',
      eventSimulatorController.simulateNotifications);
}

module.exports = router;

