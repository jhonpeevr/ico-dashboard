const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const captchaController = require('../controllers/captchaController');

router.get('/', captchaController.captcha);
router.post('/reset', captchaController.resetCaptcha);

module.exports = router;
