const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const coinInvestController = require('../controllers/coinInvestController');
const userController = require('../controllers/userController');

router.post('/generate-address', coinInvestController.generateAddress);

router.post('/save-withdrawal-address',
    userController.saveAddress);

router.get('/generated-addresses/:coinType',
    coinInvestController.generatedAddresses);

router.get('/generated-address/:addressId',
    coinInvestController.getGeneratedAddress);

module.exports = router;
