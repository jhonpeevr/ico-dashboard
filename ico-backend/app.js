'use strict';
/* Main dependencies */
const express = require('express');
const bodyParser = require('body-parser');
const httpStatus = require('http-status-codes');

/* Register all libs */
require('./lib/maindb');
require('./lib/io');
require('./lib/live-chat-io');

/* Register all models */
require('./models/CoinAddressesModel');
require('./models/CoinTransferHooksModel');
require('./models/CoinTransfersModel');
require('./models/ConfigsModel');
require('./models/CurrenciesModel');
require('./models/ExchangeRatesModel');
require('./models/FinancialHistoriesModel');
require('./models/NotificationsModel');
require('./models/UsersModel');
require('./models/UsdInvestsModel');
require('./models/KycModel');
require('./models/CountriesModel');
require('./models/CaptchaModel');
require('./models/LiveChatsModel');
require('./models/SubscriptionsModel');
require('./models/AmlModel');
require('./models/BonusModel');
require('./models/BonusPerStageModel');
require('./models/SupportModel');
require('./models/AdvisorsModel');
require('./models/CurrentRatesUsdModel');
require('./models/FeedModel');
require('./models/UserInfoModel');
require('./models/CreateAddressModel');

const ConfigsModel = require('./lib/maindb').model('Configs');
(async function ()  {
    let addressSyncCronJob = await ConfigsModel.findOne({key: 'addressSyncCronJob'}).exec();
    if(addressSyncCronJob['valueString'] === 'Enabled'){
        require('./cronjobs/etherAddressSyncBitgo');
    }
}())


require('./listeners/ioListener');
require('./cronjobs/expireAddressAddToPool');

/* Register all routes */
const authRouter = require('./routes/authRouter');
const btcRouter = require('./routes/coinRouter');
const btcHookRouter = require('./routes/coinHookRouter');
const configRouter = require('./routes/configRouter');
const currencyRouter = require('./routes/currencyRouter');
const tknWithdrawRouter = require('./routes/tknWithdrawRouter');
const exchangeRateRouter = require('./routes/exchangeRateRouter');
const finHistoryRouter = require('./routes/finHistoryRouter');
const notificationRouter = require('./routes/notificationRouter');
const feedRouter = require('./routes/feedRouter');
const usdInvestRouter = require('./routes/usdInvestRouter');
const adminRouter = require('./routes/adminRouter');
const hookRouter = require('./routes/hookRouter');
const captchaRouter = require('./routes/captchaRouter');
const twoFactorAuthRouter = require('./routes/twoFactorAuthRouter');
const kycRouter = require('./routes/kycRouter');
const countriesRouter = require('./routes/countriesRouter');
const liveChatRouter = require('./routes/liveChatRouter');
const simulatorRouter = require('./routes/simulatorRouter');
const statisticsRouter = require('./routes/statisticsRouter');
const subscriptionRouter = require('./routes/subscriptionRouter');
const supportRouter = require('./routes/supportRouter');
const kycEmails = require('./routes/kycEmailsRouter');
const bonusPercentageRouter = require('./routes/bonusPercentageRouter');
const createAddress = require('./routes/createAddressRouter');

const AbstractError = require('./errors/AbstractError');

const app = express();

// app.use(morgan);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Enable Cross Origin Resource Sharing
app.all('/*', function (req, res, next) {
    // CORS headers
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.header('Access-Control-Allow-Headers', 'Content-Type,X-Access-Token');
        return res.sendStatus(httpStatus.OK).end();
    } else {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    }
});

/* Coinfirm kyc */
app.all('/kyc/*', [require('./guards/kycGuard')]);
app.use('/kyc', kycEmails);

/* Express Router configrations */
const API = '/api/v1/';
app.all(API + '*', [require('./guards/tokenGuard')]);

app.use(API + 'admin', adminRouter);
app.use(API + 'auth', authRouter);
app.use(API + 'coin', btcRouter);
app.use(API + 'coin/hook', btcHookRouter);
app.use(API + 'tkn-withdraw', tknWithdrawRouter);
app.use(API + 'hook', hookRouter);
app.use(API + 'config', configRouter);
app.use(API + 'currency', currencyRouter);
app.use(API + 'exchange-rate', exchangeRateRouter);
app.use(API + 'finhistory', finHistoryRouter);
app.use(API + 'notification', notificationRouter);
app.use(API + 'feed', feedRouter);
app.use(API + 'usd-invest', usdInvestRouter);
app.use(API + 'captcha', captchaRouter);
app.use(API + '2fa', twoFactorAuthRouter);
app.use(API + 'kyc', kycRouter);
app.use(API + 'countries', countriesRouter);
app.use(API + 'live-chat', liveChatRouter);
app.use(API + 'simulate', simulatorRouter);
app.use(API + 'statistics', statisticsRouter);
app.use(API + 'subscription', subscriptionRouter);
app.use(API + 'support', supportRouter);
app.use(API + 'bonus', bonusPercentageRouter);
app.use(API + 'createAddress', createAddress);

app.use(express.static('receipt'));
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    return res.status(httpStatus.NOT_FOUND).send({err: 'Not found'});
});

const passportConfig = require('./lib/passport');

// setup configuration for facebook login
passportConfig();


// error handler
app.use(function (err, req, res, next) {

    console.log("ERRRRORRRR")

    if (err instanceof AbstractError) {
        const status = err.status;
        console.error('[CaughtError]', '[' + err.constructor.name + ']',
            err.message,
            err.status);
        return res.status(status).send({err: err.message});
    } else {
        console.error('[Handler]', err);
        const status = err.status ? err.status : 500;
        return res.status(status).send({err: err.message});
    }
});

module.exports = app;
