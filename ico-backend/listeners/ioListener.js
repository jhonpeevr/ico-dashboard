'use strict';

const io = require('../lib/io');
const emitter = require('../lib/emitter');
const mail = require('../lib/mailgun');
const configController = require('../controllers/configController');
const finHistoryController = require('../controllers/finHistoryController');
const NotificationsModel = require('../lib/maindb').model('Notifications');
const ExchangeRatesModel = require('../lib/maindb').model('ExchangeRates');
const CurrentRatesUsdModel = require('../models/CurrentRatesUsdModel');
const UsersModel = require('../lib/maindb').model('Users');
const FeedModel = require('../lib/maindb').model('Feeds');

const loggerName = '[IOListener]';
const sha512 = require('js-sha512').sha512;
/*
* TypeOfData is financialHistoriesModel
* */
emitter.on('fh-invest-reload', (userId) => {
    const methodName = '[FHInvestReload]';
    console.log(loggerName, methodName, userId, 'executed');

    const socketChannel = 'fh-invest-reload-' + sha512(String(userId));

    finHistoryController.getInvestments(userId).then((finHistories) => {
        return io.emit(socketChannel, finHistories);
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
});

emitter.on('fh-withdraw-reload', (userId) => {
    const methodName = '[FHWithdrawReload]';
    console.log(loggerName, methodName, userId, 'executed');

    const socketChannel = 'fh-withdraw-reload-' + sha512(userId);

    finHistoryController.getWithdrawals(userId).then((finHistories) => {
        io.emit(socketChannel, finHistories);
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
});

emitter.on('fh-tkn-deposit', (fh) => {
    const methodName = '[FHDeposit]';
    console.log(loggerName, methodName, JSON.stringify(fh));

    const notification = new NotificationsModel({
        userId: fh.userId,
        message: 'DEPOSIT',
        notificationComment: '+ ' + fh.amount + ' ' + fh.currencyAbbr,
    });

    notification.save().then(() => {
        return NotificationsModel.find({userId: fh.userId}).exec().then((notifications) => {
            io.emit('notifications-' + sha512(String(fh.userId)), notifications);
        });
    });
});

emitter.on('fh-tkn-withdraw', (fh) => {
    const methodName = '[FHWithdraw]';
    console.log(loggerName, methodName, JSON.stringify(fh));

    const notification = new NotificationsModel({
        userId: fh.userId,
        message: 'WITHDRAW',
        notificationComment: '- ' + fh.amount + ' ' + fh.currencyAbbr,
    });

    notification.save().then(() => {
        return NotificationsModel.find({userId: fh.userId}).exec().then((notifications) => {
            io.emit('notifications-' + sha512(String(fh.userId)), notifications);
        });
    });
});

emitter.on('balance-changed', (user) => {
    const methodName = '[BalanceChanged]';
    const id = sha512(String(user._id));
    console.log(loggerName, methodName, id, user.balanceTKN);
    io.emit('balance-changed-' + id, {
        balanceTKN: user.balanceTKN,
        referralTKN: user.referralTKN,
        purchasedTKN: user.purchasedTKN
    });
});

emitter.on('address-balance-changed', (user, coinAddress) => {
    const methodName = '[BalanceChanged]';
    const id = sha512(String(user._id));
    io.emit('address-balance-changed-' + id, {
        balance: coinAddress.coinResult,
        address: coinAddress.address
    });
});

emitter.on('config-changed', () => {
    const methodName = '[Config]';
    console.log(loggerName, methodName);

    configController.getConfigsMap().then((configsMap) => {
        io.emit('config-changed', configsMap);
    }).catch((err) => {
        console.error(loggerName, methodName, err);
    });
});

emitter.on('transfer-mail', (email, amountTknTotal, amountEVT, coinType,
                             coinAmount, tknParAmount, tknBonusAmount, amountDeposited) => {
    const methodName = '[TransferMail]';
    /*    console.log('loggerName: ', loggerName);
        console.log('methodName: ', methodName);
        console.log('email: ', email);
        console.log('amount: ', amount);
        console.log('amountEVT: ', amountEVT);
        console.log('coinType: ', coinType);
        console.log('coinAmount: ', coinAmount);
        console.log('tknParAmount: ', tknParAmount);
        console.log('tknBonusAmount: ', tknBonusAmount);*/
    console.log(loggerName, methodName, email, amountTknTotal, amountEVT, coinType,
        coinAmount, tknParAmount, tknBonusAmount, amountDeposited);
    mail.sendDepositTKNMailGun(email, amountTknTotal, amountEVT, coinType, coinAmount, tknParAmount, tknBonusAmount, amountDeposited);
});

// This emitter sends the mail when a user generates an address
emitter.on('address-generated', (email, amount, amountEVT, coinType,
                                 coinAmount, coinAddress, tknParAmount, bonusTkns) => {
    const methodName = '[AddressGeneratedMail]';
    /*    console.log('loggerName: ', loggerName);
        console.log('methodName: ', methodName);
        console.log('email: ', email);
        console.log('amount: ', amount);
        console.log('amountEVT: ', amountEVT);
        console.log('coinType: ', coinType);
        console.log('coinAmount: ', coinAmount);
        console.log('coinAddress: ', coinAddress);
        console.log('tknParAmount: ', tknParAmount);*/
    console.log(loggerName, methodName, email, amount, amountEVT, coinType,
        coinAmount, coinAddress, tknParAmount, bonusTkns);
    mail.sendAddressGeneratedMailGun(email, amount, amountEVT, coinType, coinAmount, coinAddress, tknParAmount, bonusTkns);
});

// this emitter updates the exchange rates in real time on the dashboard footer

emitter.on('exchange-rate-changed', () => {
    // console.log('btcToEvm-changed');
    ExchangeRatesModel.find({}).select('-_id -__v').exec().then((exchangeRates) => {
        io.emit('exchange-rates-updated', exchangeRates);
    });

    CurrentRatesUsdModel.find({}).select('-_id').exec().then((exchangeUsds) => {
        io.emit('exchange-usd-updated', exchangeUsds);
    });
});


emitter.on('user-count-changed', () => {
    // console.log('btcToEvm-changed');
    UsersModel.count().then((userCount) => {
        io.emit('user-count-changed', userCount);
    })
});


// this emitter is for KYC notifications

emitter.on('kyc-status-changed', (userEmail) => {
    UsersModel.findOne({email: userEmail}).exec().then((user) => {
        const notification = new NotificationsModel({
            userId: user._id,
            message: 'Your KYC status is ' + user.kycStatus,
        });
        notification.save();
        io.emit('kyc-status-updated-' + sha512(String(user._id)),
            user.kycStatus);
    });
});

// this emitter sends the email to Special Advisors on referral's investments

emitter.on('special-referral-invest', (advisorEmail, userEmail, investCoinAmnt, refAmountCRYPTO, cryptoCurrencyType) => {
    const methodName = '[SpecialReferralInvestments]';
    console.log(loggerName, methodName, advisorEmail, userEmail, investCoinAmnt, refAmountCRYPTO, cryptoCurrencyType);
    mail.sendSpecialReferralInvestMailGun(advisorEmail, userEmail, investCoinAmnt, refAmountCRYPTO, cryptoCurrencyType);
});

emitter.on('all-feeds-reload', (userid) => {
    const methodName = '[FeedsReload]';
    console.log(loggerName, methodName);

    const userId = userid;
    const query = {
        $or: [
            {userId: userId},
            {visibility: 'public'},
        ],
    };
    FeedModel.find(query).exec().then((feeds) => {
        return io.emit('all-feeds-reload', feeds);
    });
});

emitter.on('referralJoinReload', (data) => {
    const methodName = '[referralReload]';
    console.log(loggerName, methodName);

    UsersModel.find({referBy: data.referBy}).select('-password -providerData').exec().then((users) => {
        io.emit('referralJoinReload', users);
    });
});


emitter.on('notification-for-all', (data) => {
    io.emit('notification-for-all', data);
});


emitter.on('notification-for-user', (data) => {
    io.emit('notification-for-user-' + sha512(String(data.userId)), data);
    io.emit('notification-for-user-admin', data);
});

emitter.on('user-token-updated', (user) => {
    io.emit('user-token-updated', user);
});
