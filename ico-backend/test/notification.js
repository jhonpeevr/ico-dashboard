'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const CaptchaModel = require('../models/CaptchaModel');
const process = require('process');

const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const config = require('config');
/**
 * Set up mocha with `chai`.
 *
 * routes -notificationRouter
 * @param {Object} options
 * @api private
 */
describe('Notification testing started', () => {
  let token = null;
  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    CaptchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1] +
        new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new CaptchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      token = res.body.token;
      done();
    });
  });

  /**
   * Api Name: getNotifications
   * URL:/api/v1/notification
   *
   */

  describe('List Notifications api', () => {
    it('Should getUsdInvests user\'s Notification ', (done) => {
      request(app).
          get('/api/v1/notification').
          set('Accept', 'application/json').
          set('Content-Type', 'application/json').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            response.body.forEach(function(element) {
              expect(element).to.be.an('object');
              expect(element).to.have.property('title');
              expect(element).to.have.property('notificationComment');
            });
          }).
          end(done);
    });
  });

  /**
   * Api Name: getAllNotifications
   * URL:/api/v1/notification/all
   *
   */

  describe('List all Notifications api', () => {
    it('Should getUsdInvests user\'s all Notification ', (done) => {
      request(app).
          get('/api/v1/notification/all').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            response.body.forEach(function(element) {
              expect(element).to.be.an('object');
              expect(element).to.have.property('title');
              expect(element).to.have.property('notificationComment');
            });
          }).
          end(done);
    });
  });
});
