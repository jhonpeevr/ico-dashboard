'use strict';

/**
 * Module dependencies.
 * 
 */

const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');
/**
 * Set up mocha with `chai`.
 *
 * routes -adminRouter
 * @param {Object} options
 * @api public
 */

describe('Admin testing started', () => {
    let token = null;
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    /**
     * Api Name:update
     * URL:/api/v1/admin/user/update
     * 
     */

    describe('User Update Api', () => {
        it('Should return error without user id', (done) => {
            let userObject = {

            };
            request(app)
                .post('/api/v1/admin/user/update')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .send(userObject)
                .expect(400)
                .end(done);
        });
        it('Should update user details', (done) => {
            let userObject = {
                userId: userData._id,
            };
            request(app)
                .post('/api/v1/admin/user/update')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .send(userObject)
                .expect(200)
                .expect(function(response) {
                    expect(response.body).to.be.an('object');
                    expect(response.body).to.have.property('email');
                    expect(response.body).to.have.property('password');
                })
                .end(done);
        });
    });

    /**
     * Api Name:block
     * URL:/api/v1/admin/user/block
     * 
     */


    describe('User Block Api', () => {
        it('Should return error without user id', (done) => {
                let userObject = {

                };
                request(app)
                    .post('/api/v1/admin/user/block')
                    .set('Content-Type', 'application/json')
                    .set('X-Access-Token', token)
                    .send(userObject)
                    .expect(400)
                    .end(done);
            });
            // it('Should block to user', (done) => {
            //     var userObject = {
            //         userId: '5994c0350d2b7c16e1fd3e1c'
            //     }
            //     request(app)
            //         .post('/api/v1/admin/user/block')
            //         .set('Content-Type', 'application/json')
            //         .set('X-Access-Token', token)
            //         .send(userObject)
            //         .expect(200)
            //         .expect(function(response) {
            //             expect(response.body).not.to.be.empty;
            //             expect(response.body).to.have.property('email');
            //             expect(response.body).to.have.property('password');
            //         })
            //         .end(done);
            // })
    });

    /**
     * Api Name:listAll
     * URL:/api/v1/admin/user
     * 
     */

    describe('List All user Api', () => {
        it('Should return getUsdInvests of users', (done) => {
            request(app)
                .get('/api/v1/admin/user')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect((response) => {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('email');
                        expect(element).to.have.property('password');
                    }, this);
                })
                .end(done);
        });
    });

    /**
     * Api Name:listAll
     * URL:/api/v1/admin/account/all
     * 
     */

    describe('List All account Api', () => {
        it('Should return getUsdInvests of accounts', (done) => {
            request(app)
                .get('/api/v1/admin/account/all')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect((response) => {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('userId');
                        expect(element).to.have.property('currencyId');
                    }, this);
                })
                .end(done);
        });
    });

    /**
     * Api Name:listGeneratedAddresses
     * URL:/api/v1/admin/btc/generated-addresses/all
     * 
     */
    describe('Bitcoin List Generated address Api', () => {
            it('Should return getUsdInvests of generated address', (done) => {
                request(app)
                    .get('/api/v1/admin/btc/generated-addresses/all')
                    .set('Content-Type', 'application/json')
                    .set('X-Access-Token', token)
                    .expect(200)
                    .expect((response) => {
                        response.body.forEach(function(element) {
                            expect(element).to.be.an('object');
                            expect(element).to.have.property('exchangeRateValue');
                            expect(element).to.have.property('currencyId');
                            expect(element).to.have.property('amountBTC');
                            expect(element).to.have.property('resultBTC');
                            expect(element).to.have.property('found');
                            expect(element).to.have.property('status');
                        }, this);
                    })
                    .end(done);
            });
        });
        /**
         * Api Name:listGeneratedAddresses
         * URL:/eth/generated-addresses/all
         * 
         */
    describe('Eth List Generated address Api', () => {
        it('Should return getUsdInvests of generated address', (done) => {
            request(app)
                .get('/api/v1/admin/eth/generated-addresses/all')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)

            .expect((response) => {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('exchangeRateValue');
                        expect(element).to.have.property('currencyId');
                        expect(element).to.have.property('amountBTC');
                        expect(element).to.have.property('resultBTC');
                        expect(element).to.have.property('found');
                        expect(element).to.have.property('status');
                    }, this);
                })
                .end(done);
        });
    });

    /**
     * Api Name:getAllKYCs
     * URL:/kyc/all
     * 
     */

    describe('Get All Kyc List Api', () => {

        // if (false) {
        //     it('Should return all getUsdInvests of kyc', (done) => {
        //
        //         request(app)
        //             .get('/api/v1/admin/kyc/all')
        //             .set('Content-Type', 'application/json')
        //             .set('X-Access-Token', token)
        //             .expect(200)
        //             // .expect((response) => {
        //
        //         //     // response.body.forEach(function(element) {
        //         //     //     expect(element).not.to.be.empty;
        //         //     //     expect(element).to.be.an('object');
        //         //     //     expect(element).to.have.property('exchangeRateValue');
        //         //     //     expect(element).to.have.property('currencyId');
        //         //     //     expect(element).to.have.property('amountBTC');
        //         //     //     expect(element).to.have.property('resultBTC');
        //         //     //     expect(element).to.have.property('found');
        //         //     //     expect(element).to.have.property('status');
        //
        //         //     // }, this);
        //
        //         // })
        //         .end(done);
        //
        //     });
        // }
    });

    /**
     * Api Name:getPassportImage
     * URL:/api/v1/admin/kyc/passportImage/:userid
     * 
     */
    describe('Get Kyc Passport Image Api', () => {

        // if (false) {
        //     it('Should return user\'s passport image', (done) => {
        //
        //         request(app)
        //             .get('/api/v1/admin/kyc/passportImage/' + userData._id)
        //             .set('Content-Type', 'application/json')
        //             .set('X-Access-Token', token)
        //             .expect(200)
        //             .expect((response) => {
        //                 expect(response.body).to.be.an('object');
        //
        //             })
        //             .end(done);
        //
        //     });
        // }
        // if (false) {
        //     it('Should return error if KYC is disabled or not found', (done) => {
        //         request(app)
        //             .get('/api/v1/admin/kyc/passportImage/' + userData._id)
        //             .set('Content-Type', 'application/json')
        //             .set('X-Access-Token', token)
        //             .expect(400)
        //             .end(done);
        //     });
        // }


    });

    /**
     * Api Name:getUtilityImage
     * URL:/api/v1/admin/kyc/utilityImage/:userid
     * 
     */

    describe('Get Kyc Utility Image Api', () => {
        // if (false) {
        //     it('Should return kyc utility image', (done) => {
        //
        //         request(app)
        //             .get('/api/v1/admin/kyc/utilityImage/' + userData._id)
        //             .set('Content-Type', 'application/json')
        //             .set('X-Access-Token', token)
        //             .expect(200)
        //             .expect((response) => {
        //                 expect(response.body).to.be.an('object');
        //             })
        //             .end(done);
        //
        //     });
        //
        // }
        // if (false) {
        //     it('Should return error if KYC is disabled or not found', (done) => {
        //         request(app)
        //             .get('/api/v1/admin/kyc/passportImage/' + userData._id)
        //             .set('Content-Type', 'application/json')
        //             .set('X-Access-Token', token)
        //             .expect(400)
        //             .end(done);
        //     });
        // }

    });

    /**
     * Api Name:confirm
     * URL:/api/v1/admin/usd-invest/confirm
     * 
     */

    describe('USD Investment Confirmation Api', () => {
        // if (false) {
        //
        //     it('Should return error if Usd Transfer data not found', (done) => {
        //         let transferData = {
        //             id: userData._id,
        //             amountUsd: 20
        //         }
        //         request(app)
        //             .post('/api/v1/admin/usd-invest/confirm')
        //             .set('Content-Type', 'application/json')
        //             .set('X-Access-Token', token)
        //             .send(transferData)
        //             .expect(400)
        //             .end(done);
        //
        //     });
        // }

    });

    /**
     * Api Name:listAll
     * URL:/api/v1/admin/usd-invest/all
     * 
     */

    describe('GET all USD Investment List', () => {
        it('Should return error if Usd Transfer data not found', (done) => {
            request(app)
                .get('/api/v1/admin/usd-invest/all')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect((response) => {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('userId');
                        expect(element).to.have.property('fullname');
                        expect(element).to.have.property('country');
                        expect(element).to.have.property('paymentDetails');
                        expect(element).to.have.property('amountUsd');
                        expect(element).to.have.property('currencyId');
                        expect(element).to.have.property('exchangeRateValue');
                    }, this);
                })
                .end(done);
        });
    });

    /**
     * Api Name:getAllAudits
     * URL:/api/v1/admin/audit
     * 
     */

    describe('GET All Audit List Api', () => {
        it('Should return getUsdInvests of all audit', (done) => {
            request(app)
                .get('/api/v1/admin/audit')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect((response) => {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('userId');
                        expect(element).to.have.property('action');
                        expect(element).to.have.property('data');
                    }, this);
                })
                .end(done);
        });
    });


    /**
     * Api Name:getAllDynamicConfigs
     * URL:/api/v1/admin/configs
     * 
     */

    describe('GET All config List Api', () => {
        it('Should return getUsdInvests of all configs', (done) => {
            request(app)
                .get('/api/v1/admin/configs')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect((response) => {
                    expect(response.body).to.be.an('object');
                    expect(response.body).to.have.property('key');
                    expect(response.body).to.have.property('value');
                })
                .end(done);
        });
    });

    /**
     * Api Name:editConfigs
     * URL:/api/v1/admin/configs
     * 
     */

    describe('edit config List Api', () => {
        let updateData = {
            key: 'test',
            value: 'test',

        };
        it('Should return 200 after update the data', (done) => {
            request(app)
                .post('/api/v1/admin/configs')
                .set('X-Access-Token', token)
                .send(updateData)
                .expect(200)
                .end(done);
        });
    });
});
