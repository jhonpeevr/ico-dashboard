'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;
const CaptchaModel = require('../models/CaptchaModel');
const process = require('process');
const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -usdInvestRouter
 * @param {Object} options
 * @api private
 */

describe('USD Investment testing started', () => {
  let token = null;
  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    CaptchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1]
        + new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new CaptchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      token = res.body.token; // Get Token from Response /api/vi/auth
      done();
    });
  });

  /**
   * Api Name: getUsdInvests
   * URL:/api/v1/usd-invest
   *
   */

  describe('List api', () => {
    it('Should getUsdInvests user\'s investment in USD ', (done) => {
      request(app).
          get('/api/v1/usd-invest').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            response.body.forEach(function(element) {
              expect(element).to.be.an('object');
              expect(element).to.have.property('userId');
              expect(element).to.have.property('fullname');
              expect(element).to.have.property('country');
              expect(element).to.have.property('paymentDetails');
              expect(element).to.have.property('amountUsd');
              expect(element).to.have.property('currencyId');
              expect(element).to.have.property('exchangeRateValue');
              expect(element).to.have.property('status');
              expect(element).to.have.property('resultUsd');
            });
          }).
          end(done);
    });
  });

  /**
   * Api Name: register
   * URL:/api/v1/usd-invest/register
   *
   */


  describe('Register api', () => {
    it('Should check and throw error ' +
        'for invalid details to registers usd investment', (done) => {
      let incompleteInvestmentData = {
        fullname: 'Test',
        country: 'INDIA',
        amountUsd: 20,
      };

      request(app).
          post('/api/v1/usd-invest/register').
          send(incompleteInvestmentData).
          set('X-Access-Token', token).
          expect(400).
          end(done);
    });

    if (false) {
      it('Should check and register ' +
          'usd investment with all details', (done) => {
        let investmentData = {
          fullname: 'Test',
          country: 'In',
          amountUsd: 10,
          paymentDetails: 'test payment details',
        };

        request(app).
            post('/api/v1/usd-invest/register').
            send(investmentData).
            set('X-Access-Token', token).
            expect(200).
            expect(function(response) {
              expect(response.body).to.be.an('object');
              expect(response.body).to.have.property('userId');
              expect(response.body).to.have.property('fullname');
              expect(response.body).to.have.property('country');
              expect(response.body).to.have.property('paymentDetails');
              expect(response.body).to.have.property('amountUsd');
              expect(response.body).to.have.property('currencyId');
              expect(response.body).to.have.property('exchangeRateValue');
              expect(response.body).to.have.property('confirmed');
              expect(response.body).to.have.property('resultUsd');
            }).
            end(done);
      });
    }
  });
});
