'use strict';

/**
 * Module dependencies.
 * 
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');
/**

/**
 * Set up mocha with `chai`.
 *
 * routes -authRouter
 * @param {Object} options
 * @api public
 */

describe('Auth', () => {
    let token = null;
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    // before(function(done) {
    //     console.log(userObject);

    // });

    /**
     * Api Name:auth
     * URL:/api/v1/auth
     * 
     */


    describe('Authentication API', function() {
        // if (false) {
        //
        //     it('Should check valid credentials', function(done) {
        //         let newUserObject = {
        //             email: "dummy@ico.com",
        //             password: "123456"
        //         };
        //         request(app)
        //             .post('/api/v1/auth')
        //             .set('Accept', 'application/json')
        //             .set('Content-Type', 'application/json')
        //             .expect(404)
        //             .send(newUserObject)
        //             .end(done);
        //     });
        // }

        it('Should success if credential is valid', function(done) {
            request(app)
                .post('/api/v1/auth')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .send(userObject)
                .expect(200)
                .expect('Content-Type', /json/)
                .expect(function(response) {
                    expect(response.body).to.be.an('object');
                })
                .end(done);
        });
    });

    /**
     * Api Name:signup
     * URL:/api/v1/auth/sign-up
     * 
     */

    describe('SingUp API', function() {
        // let newUserObject = {
        //     email: "user@gmail.com",
        //     password: "123456"
        // };
        // if (false) {
        //     it('Should save new user credentials', function(done) {
        //         request(app)
        //             .post('/api/v1/auth/sign-up')
        //             .set('Accept', 'application/json')
        //             .set('Content-Type', 'application/json')
        //             .send(newUserObject)
        //             .expect(200)
        //             .expect(function(response) {
        //                 expect(response.text).to.equal('OK');
        //             })
        //             .end(done);
        //     });
        // }

        it('should return error trying to save duplicate username', function(done) {
            request(app)
                .post('/api/v1/auth/sign-up')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .send(userObject)
                .expect(400)
                .end(done);
        });
    });

    // /**
    //  * Api Name:forgotPassword
    //  * URL:/api/v1/auth/forgot-password
    //  * 
    //  */

    // describe('forgot-password API', function() {
    //     var newUserObject = {
    //         email: "dummyemail@gmail.com"
    //     }


    //     it('should return 404 email not found', function(done) {
    //         request(app)
    //             .post('/api/v1/auth/forgot-password')
    //             .send(userObject)
    //             .expect(404)
    //             .end(done);
    //     });
    // });
    // 


    /**
     * Api Name:activateEmail
     * URL:/api/v1/auth/activate/:email/:activationCode
     * 
     */

    // describe('activate-email API', function() {
    //     var newUserObject = {
    //         email: "dummyemail@gmail.com"
    //     }


    //     it('should return 404 email not found', function(done) {
    //         request(app)
    //             .post('/api/v1/auth/activate/:email/:activationCode')
    //             .send(userObject)
    //             .expect(404)
    //             .end(done);
    //     });
    // });
});
