'use strict';

/**
 * Module dependencies.
 *
 */
const config = require('config');
// const bitgo = require('../lib/bitgo');
const chai = require('chai');
const spies = require('chai-spies');

console.log(__dirname);

chai.use(spies);

// const request = require('supertest');
//
// const chai = require('chai');

// const captchaModel = require('../models/captchaModel');
// const process = require('process'),
//     sha512 = require('js-sha512').sha512,
//     svgCaptcha = require('svg-captcha'),
//     config = require('config');
//


describe('bulk transaction testing started,', () => {
    if (config.get('ENV')==='DEVELOPMENT') {
        it('send BTC in wallet', (done) => {
            let params = {
                recipients: [
                    {
                        amount: 0.00001,
                        address: '2Mt4DQ8oFfWNoVADLkdTAeD1zNAAtgP9fXW',
                    },
                ],
                walletPassphrase: config.get('eth-wallet-password'),
            };

            // bitgo.coin(coin)
            //     .wallets()
            //     .getUsdInvests({})
            //     .then((wallets) => {
            //         return res.send(wallets);
            //     })
            //     .catch(next);


            bitgo.coin('tbtc').wallets().get({id: '2MuuBCekwWfXReLuY2BH7zdcAPo9ApzAJJe'})
                .then((wallet) => {
                console.log(wallet);
                    wallet.send(params)
                        .then(function(transaction) {
                            // print transaction details
                            console.dir(transaction);

                            done();
                        });
                });
        });
    }
});

