'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const CaptchaModel = require('../models/CaptchaModel');
const process = require('process');
const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -twoFactorAuth
 * @param {Object} options
 * @api private
 */


describe('Two Factor Authentication testing started,', () => {
  let token = null;
  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    CaptchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1] +
        new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new CaptchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      token = res.body.token; // Get Token from Response /api/vi/auth
      done();
    });
  });

  /**
   *API Name: generate2FA
   * URL: 2fa/generate
   */
  describe('generate2FA api', () => {
    it('Should return Two Factor details ', (done) => {
      request(app).
          post('/api/v1/2fa/generate').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('url');
            expect(response.body).to.have.property('secret');
          }).
          end(done);
    });
  });
});
