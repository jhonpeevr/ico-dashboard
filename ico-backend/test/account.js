'use strict';
/**
 * Module dependencies.
 * 
 */

const accountsModel = require('../models/AccountsModel');
const request = require('supertest');
const app = require('../app');
const chai = require('chai');

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');

const spies = require('chai-spies');

chai.use(spies);
const expect = chai.expect;
const sinon = require('sinon');

/**
 * Set up mocha with `chai`.
 *
 * routes -accountRouter
 * @param {Object} options
 * @api public
 */

describe('Account testing started', function() {
    let token = null;
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(function() {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then(function(response) {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });


    after(function(done) {
        done();
    });

    /**
     * Api Name:getUsdInvests
     * URL:/api/v1/account
     * 
     */

    // // Test will pass if we fail to get a todo
    // it("should return error", function(done) {
    //     var TodoMock = sinon.mock(Todo);
    //     var expectedResult = { status: false, error: "Something went wrong" };
    //     TodoMock.expects('find').yields(expectedResult, null);
    //     Todo.find(function(err, result) {
    //         TodoMock.verify();
    //         TodoMock.restore();
    //         expect(err.status).to.not.be.true;
    //         done();
    //     });
    // });


    describe('List api', () => {
        it('should return all getUsdInvests', function(done) {
            let accountMock = sinon.mock(accountsModel);
            let expectedResult = [];
            accountMock.expects('find').yields(null, expectedResult);

            accountsModel.find(function(err, result) {
                console.log(result);
                accountMock.verify();
                accountMock.restore();
                expect(200);
                done();
            });
        });

        // it("should return all getUsdInvests", function(done) {


        //     // var scope = nock('http://localhost:3000/api/v1')
        //     //     .post('/account', {
        //     //         'X-Access-Token': token
        //     //     })
        //     //     .reply(200, {
        //     //         ok: true
        //     //     });

        //     var scope = nock('http://localhost:3000/api/v1', {
        //             reqheaders: {
        //                 'X-Access-Token': 'token'
        //             }
        //         })
        //         .get('/account')
        //         .reply(200, { results: [] });

        //     console.log("==============================>", scope)


        // });

        // it('Should getUsdInvests user\'s account ', (done) => {
        //     request(app)
        //         .get('/api/v1/account')
        //         .set('Accept', 'application/json')
        //         .set('Content-Type', 'application/json')
        //         .set('X-Access-Token', token)
        //         .expect(200)
        //         .expect(function(response) {
        //             console.log(response.body)
        //             response.body.forEach(function(element) {
        //                 expect(element).not.to.be.empty;
        //                 expect(element).to.be.an('object');
        //                 expect(element.currencyId).to.be.an('object');
        //                 expect(element).to.have.property('userId');
        //                 expect(element).to.have.property('active');
        //                 expect(element).to.have.property('balance');
        //             }, this);
        //         })
        //         .end(done);

        // })
    });
});
