'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -exchangeRateRouter
 * @param {Object} options
 * @api private
 */


describe('Exchange Rate testing started', () => {
    let token = null;

    /* GET Auth token */
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    /* GET Auth token */
    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    after(function(done) {
        done();
    });

    describe('List api', () => {
        it('Should getUsdInvests all exchange rate ', (done) => {
            request(app)
                .get('/api/v1/exchange-rate')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect(function(response) {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('currencyIdFirst');
                        expect(element).to.have.property('currencyIdSecond');
                        expect(element).to.have.property('type');
                        expect(element).to.have.property('exchangeRate');
                    }, this);
                })
                .end(done);
        });
    });
});
