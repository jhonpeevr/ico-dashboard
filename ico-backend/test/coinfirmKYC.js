const jwt = require('jsonwebtoken');
const request = require('request');
const now = new Date();
const secret = new Date(now.getFullYear(), now.getMonth(), now.getDate(),
    now.getHours(), now.getMinutes()).getTime().toString();
const token = jwt.sign({iat: 111111}, secret);

console.log(token);
console.log(secret);

const options = {
  // url: 'http://localhost:3003/kyc/submitKYCStatus',
  url: 'https://api.genesisx.io/kyc/getNonComplianceUsersDetail',
  method: 'GET',
  headers: {
    'x-auth-token': token,
    'content-type': 'application/x-www-form-urlencoded',
  },
  // body: 'email=user@ico.com',
  // body: 'email=achraf@genesisx.com',
  /* form: {
    email: 'user@ico.com',
    status: 'SUBMITTED',
  },*/
};
request(options, function(error, response, body) {
  console.error(error);
  console.log(body);
  console.log(body.length);
});
/*

request.post('http://localhost:3003/kyc/sendKYCAcknowledgement', {
  form: {
    email: 'user@ico.com'
  }
})
*/
