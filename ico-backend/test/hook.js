'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const CaptchaModel = require('../models/CaptchaModel');
const process = require('process');
const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -hookRouter
 * @param {Object} options
 * @api private
 */


describe('Hook testing started', () => {
  let token = null;
  let wallet = null;
  let transfer = null;
  let coin = 'tbtc';
  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    CaptchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1] +
        new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new CaptchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      let userData = res.body;
      token = res.body.token; // Get Token from Response /api/vi/auth
      done();
    });
  });

  /**
   * Api Name:getUsdInvests-wallets
   * URL:/api/v1/hook/getUsdInvests-wallets/:coin
   *
   */
  describe('listWallets api', () => {
    it('return wallet data as per given coin', (done) => {
      request(app).
          get('/api/v1/hook/getUsdInvests-wallets/' + coin).
          set('Accept', 'application/json').
          set('Content-Type', 'application/json').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            wallet = response.body.wallets[0]._wallet.id;
            response.body.wallets.forEach(function(element) {
              expect(element).to.be.an('object');
              expect(element).to.have.property('bitgo');
              expect(element).to.have.property('_wallet');
              expect(element._wallet).
                  to.
                  have.
                  any.
                  keys('users', 'coin', 'tags', 'keys', 'balance');
            });
          }).
          end(done);
    });
  });

  /**
   * Api Name:getUsdInvests-wallets
   * URL:/getUsdInvests-hooks/:coin/:walletId
   *
   */

  describe('listhooks api', () => {
    it('return hooks data with coin and wallet id', (done) => {
      request(app).
          get('/api/v1/hook/getUsdInvests-hooks/' + coin + '/' + wallet).
          set('Accept', 'application/json').
          set('Content-Type', 'application/json').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
           let type = response.body.webhooks[0].type;
           let url = response.body.webhooks[0].url;
            response.body.webhooks.forEach(function(element) {
              expect(element).to.be.an('object');
              expect(element).to.have.property('id');
              expect(element).to.have.property('walletId');
              expect(element).to.have.property('type');
              expect(element).to.have.property('coin');
              expect(element).to.have.property('url');
            });
          }).
          end(done);
    });
  });

  /**
   * Api Name:getUsdInvests-wallets
   * URL:/getUsdInvests-transfers/:coin/:walletId
   *
   */


  describe('listTransfers api', () => {
    it('return all getUsdInvests of transfer data with coin and wallet id',
        (done) => {
          request(app).
              get('/api/v1/hook/getUsdInvests-transfers/' + coin + '/' +
                  wallet).
              set('Accept', 'application/json').
              set('Content-Type', 'application/json').
              set('X-Access-Token', token).
              expect(200).
              expect(function(response) {
                transfer = response.body.transfers[0].id;
                response.body.transfers.forEach(function(element) {
                  expect(element).to.be.an('object');
                  expect(element).to.have.property('txid');
                  expect(element).to.have.property('value');
                  expect(element).to.have.property('state');
                  expect(element).to.have.property('history');
                  expect(element).to.have.property('entries');
                });
              }).
              end(done);
        });
  });
  /**
   * Api Name:getTransfer
   * URL:/transfer/:coin/:walletId/:transferId
   *
   */


  describe('getTransfers api', () => {
    it('return a transfer data with coin, wallet ID and transfer ID',
        (done) => {
          request(app).
              get('/api/v1/hook/transfer/' + coin + '/' + wallet + '/' +
                  transfer).
              set('Accept', 'application/json').
              set('Content-Type', 'application/json').
              set('X-Access-Token', token).
              expect(200)
              // .expect(function(response) {
              //     console.log(response.body)
              // })
              .end(done);
        });
  });

  /**
   * Api Name:generate-wallet
   * URL:/transfer/:coin/:walletId/:transferId
   *
   */


  describe('generate-wallet api', () => {
    it('generate wallet with valid data', (done) => {
      let walletData = {
        coin: coin,
        label: 'my-btc-wallet',
        passphrase: 'secret',
      };

      request(app).
          post('/api/v1/hook/generate-wallet').
          set('X-Access-Token', token).
          send(walletData).
          expect(200).
          expect(function(response) {
            expect(response.body.wallet).to.be.an('object');
            expect(response.body.wallet).to.have.property('bitgo');
            expect(response.body.wallet).to.have.property('_wallet');
            // expect(response.body.wallet).to.have.property('userKeychain');
            // expect(response.body.wallet).to.have.property('backupKeychain');
            // expect(response.body.wallet).to.have.property('bitgoKeychain');
          }).
          end(done);
    });
  });

  /**
   * Api Name:add-hook
   * URL:/hook/add-hook
   *
   */

  // describe('add-hook api', () => {

  //     it('add hook with valid data', (done) => {

  //         var hookData = {
  //             coin: coin,
  //             walletId: wallet,
  //             type: type,
  //             url: url,
  //             numOfConfirmations: 1
  //         };

  //         request(app)
  //             .post('/api/v1/hook/add-hook')
  //             .set('X-Access-Token', token)
  //             .send(hookData)
  //             .expect(200)
  //             .expect(function(response) {
  //                 console.log(response.body)

  //             })
  //             .end(done);

  //     })

  // });

  // /**
  //  * Api Name:remove hook
  //  * URL:/remove-hook
  //  *
  //  */

  // describe('remove-hook api', () => {

  //     it('remove hook with valid data', (done) => {

  //         var hookData = {
  //             coin: coin,
  //             walletId: wallet,
  //             type: type,
  //             url: url
  //         };

  //         request(app)
  //             .post('/api/v1/hook/remove-hook')
  //             .set('X-Access-Token', token)
  //             .send(hookData)
  //             .expect(200)
  //             .expect(function(response) {
  //                 console.log(response.body)

  //             })
  //             .end(done);

  //     })

  // });
});
