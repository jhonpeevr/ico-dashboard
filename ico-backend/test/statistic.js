'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;
const CaptchaModel = require('../models/CaptchaModel');
const process = require('process');

const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -statisticsRouter
 * @param {Object} options
 * @api private
 */

describe('Statistics testing started', () => {
  let token = null;
  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    CaptchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1] +
        new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new CaptchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      token = res.body.token;
      done();
    });
  });

  /**
   * Api Name: statistics
   * URL:/api/v1/statistics/user
   *
   */

  describe('Get User Statistics', () => {
    it('Should get user statistics ', (done) => {
      request(app).
          get('/api/v1/statistics/user').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('totalUsers');
            expect(response.body).to.have.property('totalRoleAdmin');
            expect(response.body).to.have.property('totalRoleUser');
            expect(response.body).to.have.property('totalActiveUser');
            expect(response.body).to.have.property('totalBlockUser');
          }).
          end(done);
    });
  });
  /**
   * Api Name: statistics
   * URL:/api/v1/statistics/kyc
   *
   */

  describe('Get kyc Statistics ', () => {
    it('Should get kyc statistics ', (done) => {
      request(app).
          get('/api/v1/statistics/kyc').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('totalKYC');
            expect(response.body).to.have.property('totalConfirmedKYC');
            expect(response.body).to.have.property('totalDeclinedKYC');
            expect(response.body).to.have.property('totalPendingKYC');
          }).
          end(done);
    });
  });
  /**
   * Api Name: statistics
   * URL:/api/v1/statistics/coin
   *
   */

  describe('Get Coin Statistics', () => {
    it('Should get coin statistics ', (done) => {
      request(app).
          get('/api/v1/statistics/coin').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('totalGeneratedAddresses');
            expect(response.body).to.have.property('totalPendingAddresses');
            expect(response.body).to.have.property('totalProcessedAddresses');
          }).
          end(done);
    });
  });
  /**
   * Api Name: statistics
   * URL:/api/v1/statistics/usd-invest
   *
   */

  describe('Get USD Investment Statistics', () => {
    it('Should get usd-investment statistics ', (done) => {
      request(app).
          get('/api/v1/statistics/usd-invest').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('totalUSDInvest');
            expect(response.body).to.have.property('totalProcessingInvest');
            expect(response.body).to.have.property('totalConfirmedInvest');
            expect(response.body).to.have.property('totalCanceledInvest');

          }).
          end(done);
    });
  });
  /**
   * Api Name: statistics
   * URL:/api/v1/statistics/investments
   *
   */

  describe('Get Investment Statistics', () => {
    it('Should get investment statistics ', (done) => {
      request(app).
          get('/api/v1/statistics/investments').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('totalNoInvestments');
            expect(response.body).to.have.property('totalAmountInvestment');
          }).
          end(done);
    });
  });
  /**
   * Api Name: statistics
   * URL:/api/v1/statistics/withdrawal
   *
   */

  describe('Get Withdrawal Statistics', () => {
    it('Should get withdrawal statistics ', (done) => {
      request(app).
          get('/api/v1/statistics/withdrawal').
          set('X-Access-Token', token).
          expect(200).
          expect(function(response) {
            expect(response.body).to.be.an('object');
            expect(response.body).to.have.property('totalAmountWithdrawal');
            expect(response.body).to.have.property('totalNoWithdrawal');
          }).
          end(done);
    });
  });
});
