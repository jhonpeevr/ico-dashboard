'use strict';

/**
 * Module dependencies.
 *
 */

const request = require('supertest');
const app = require('../app');
const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');
/**
 * Set up mocha with `chai`.
 *
 * routes -ethHookRouter
 * @param {Object} options
 * @api private
 */

describe('ethHook testing started,', () => {
    let token = null,
        userData = null,
        wallet = null,
        transfer = null,
        hash = null,
        coin = 'teth';

    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    before(function(done) {
        request(app)
            .get('/api/v1/hook/getUsdInvests-wallets/' + coin)
            .set('X-Access-Token', token)
            .end(function(err, res) {
                wallet = res.body.wallets[0]._wallet.id;
                done();
            });
    });

    before(function(done) {
        request(app)
            .get('/api/v1/hook/getUsdInvests-transfers/' + coin + '/' + wallet)
            .set('X-Access-Token', token)
            .end(function(err, response) {
                transfer = response.body.transfers[0].id;
                hash = response.body.transfers[0].txid;
                done();
            });
    });

    /**
     * Api Name:processTransfer
     * URL:/eth/hook/transfer
     *
     */
    describe('Transfer Api', () => {
        it('should return 200 if trasfer already exist or new created', (done) => {
            let transferData = {
                type: 'transfer',
                wallet: wallet,
                hash: hash,
                transfer: transfer,
                coin: coin,
            };
            request(app)
                .post('/api/v1/eth/hook/transfer')
                .set('X-Access-Token', token)
                .send(transferData)
                .expect(200)
                .end(done);
        });
    });
});
