'use strict';

/**
 * Module dependencies.
 * 
 */
const request = require('supertest');
const app = require('../app');
const chai = require('chai');

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');

const spies = require('chai-spies');
chai.use(spies);


/**
 * Set up mocha with `chai`.
 *
 * routes -btcRouter
 * @param {Object} options
 * @api private
 */

describe('btc testing started', () => {
    let token = null;
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    /**
     * Api Name:generateAddress
     * URL:/btc/generate-address
     * 
     */
    describe('Generate Address Api', () => {
        it('should return error with incomplete post data', (done) => {
            let postData = {};
            request(app)
                .post('/api/v1/btc/generate-address')
                .set('X-Access-Token', token)
                .send(postData)
                .expect(400)
                .end(done);
        });

        // if (false) {
        //     it('should check minimum and maximum btc', (done) => {
        //         let postData = {
        //             amountBTC: 1
        //         };
        //
        //         request(app)
        //             .post('/api/v1/btc/generate-address')
        //             .set('X-Access-Token', token)
        //             .send(postData)
        //             .expect(400)
        //             .expect(function(response) {
        //                 console.log(response.text);
        //             })
        //             .end(done);
        //     });
        //
        //
        // }
        // if (false) {
        //
        //     it('should generate address with given btc amount', (done) => {
        //         var postData = {
        //             amountBTC: 1
        //         }
        //
        //         request(app)
        //             .post('/api/v1/btc/generate-address')
        //             .set('X-Access-Token', token)
        //             .send(postData)
        //             .expect(200)
        //             .expect(function(response) {
        //                 expect(response.body).not.to.be.empty;
        //                 expect(response.body).to.be.an('object');
        //                 expect(response.body).to.have.property('exchangeRateValue');
        //                 expect(response.body).to.have.property('currencyId');
        //                 expect(response.body).to.have.property('address');
        //                 expect(response.body).to.have.property('status');
        //                 expect(response.body).to.have.property('userId');
        //             })
        //             .end(done);
        //     });
        // }
    });

    /**
     * Api Name:generatedAddresses
     * URL:/btc/generated-addresses
     * 
     */
    describe('Get Generated Address Api', () => {
        // if (false) {
        //     it('should return generated address for the user', (done) => {
        //         request(app)
        //             .get('/api/v1/btc/generated-addresses')
        //             .set('X-Access-Token', token)
        //             .expect(200)
        //             .expect(function(response) {
        //                 address = response.body[0]._id;
        //                 // response.body.forEach(function(element) {
        //                 //     expect(element).not.to.be.empty;
        //                 //     expect(element).to.be.an('object');
        //                 //     expect(element).to.have.property('exchangeRateValue');
        //                 //     expect(element).to.have.property('currencyId');
        //                 //     expect(element).to.have.property('address');
        //                 //     expect(element).to.have.property('status');
        //                 //     expect(element).to.have.property('userId');
        //                 //     expect(element).to.have.property('found');
        //                 // }, this);
        //             })
        //             .end(done);
        //     });
        // }

    });

    /**
     * Api Name:getGeneratedAddress
     * URL:/btc/generated-addresses:addressId
     * 
     */
    describe('Get Generated Address Api for address', () => {
        // if (false) {
        //     it('should return generated address for the user\'s adreess', (done) => {
        //         request(app)
        //             .get('/api/v1/btc/generated-address/' + address)
        //             .set('X-Access-Token', token)
        //             .expect(200)
        //             .expect(function(response) {
        //                 expect(response.body).to.be.an('object');
        //                 expect(response.body).to.have.property('exchangeRateValue');
        //                 expect(response.body).to.have.property('currencyId');
        //                 expect(response.body).to.have.property('address');
        //                 expect(response.body).to.have.property('status');
        //                 expect(response.body).to.have.property('userId');
        //                 expect(response.body).to.have.property('found');
        //             })
        //             .end(done);
        //     });
        // }

    });
});
