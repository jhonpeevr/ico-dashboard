'use strict';


/**
 * Module dependencies.
 *
 */

const request = require('supertest');
const app = require('../app');
const expect = require('chai').expect;

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -configRouter
 * @param {Object} options
 * @api private
 */


describe('Currency Test Started', () => {
    let token = null;
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });


    describe('List All Api', () => {
        it('Should getUsdInvests all currency ', (done) => {
            request(app)
                .get('/api/v1/currency/all')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect(function(response) {
                    response.body.forEach(function(element) {
                        expect(element).to.be.an('object');
                        expect(element).to.have.property('abbr');
                        expect(element).to.have.property('name');
                        expect(element).to.have.property('type');
                        expect(element).to.have.property('minAmount');
                        expect(element).to.have.property('active');
                    }, this);
                })
                .end(done);
        });
    });
});
