'use strict';

/**
 * Module dependencies.
 *
 */

const request = require('supertest');
const app = require('../app');
const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -ethWithdrawRouter
 * @param {Object} options
 * @api private
 */

describe('ethWithdraw testing started', () => {
  let token = null,
      userData = null;

  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    captchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1] +
        new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new captchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      userData = res.body;
      token = res.body.token; // Get Token from Response /api/vi/auth
      done();
    });
  });
  /**
   * Api Name:sendTx
   * URL:/eth-withdraw/send-tx
   *
   */
  describe('SendTx Api', () => {
    it('should return error with incomplete post data', (done) => {
      let postData = {
        address: 'IMPZ, Dubai, UAE',
      };

      request(app).
          post('/api/v1/eth-withdraw/send-tx').
          set('X-Access-Token', token).
          send(postData).
          expect(400).
          end(done);
    });

  });
});
