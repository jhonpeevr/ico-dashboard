'use strict';


/**
 * Module dependencies.
 * 
 */

const request = require('supertest');
const app = require('../app');
const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');


/**
 * Set up mocha with `chai`.
 *
 * routes -finHistoryRouter
 * @param {Object} options
 * @api private
 */


describe('Find History testing started', () => {
    let token = null;

    /* GET Auth token */
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    /* GET Auth token */
    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    after(function(done) {
        done();
    });

    /**
     * Api Name:getUsdInvests
     * URL:/api/v1/finhistory
     * 
     */


    describe('Find History List api', () => {
        it('Should get getUsdInvests of all history', (done) => {
            request(app)
                .get('/api/v1/finhistory')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect(function(response) {

                })
                .end(done);
        });
    });

    /**
     * Api Name:investments
     * URL:/api/v1/finhistory/investments
     * 
     */


    describe('Find Investment History List api', () => {
        it('Should get getUsdInvests of all investments', (done) => {
            request(app)
                .get('/api/v1/finhistory/investments')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect(function(response) {
                    // console.log(response.body)
                })
                .end(done);
        });
    });

    /**
     * Api Name:withdrawals
     * URL:/api/v1/finhistory/withdrawals
     * 
     */


    describe('Find withdrawals History List api', () => {
        it('Should get getUsdInvests of all withdrawals', (done) => {
            request(app)
                .get('/api/v1/finhistory/withdrawals')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect(function(response) {
                    // console.log(response.body)
                })
                .end(done);
        });
    });

    /**
     * Api Name:all
     * URL:/api/v1/finhistory/all
     * 
     */

    describe('Find all History List api', () => {
        it('Should get getUsdInvests of all type of history', (done) => {
            request(app)
                .get('/api/v1/finhistory/all')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(200)
                .expect(function(response) {
                    // console.log(response.body)
                })
                .end(done);
        });
    });
});
