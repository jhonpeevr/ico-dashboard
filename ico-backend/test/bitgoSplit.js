const BitGoJS = require('bitgo');

const bitgo = new BitGoJS.BitGo({
  env: 'test',
  accessToken: 'v2xc14a1aa8c3dc94f28c12978070cb753e42a8506c0afff224aa926b4d2739895d',
});

/*
bitgo.coin('tbtc').wallets().get({id: '2MyJRER43p7pZTU1iAVLRpDULMsds125Rkj'}).
    then((wallet) => {
      let params = {
        recipients: [
          {
            amount: '100000000',
            address: '2NED2sighAoEQMsZC7i6MKd9avrQhMbdgYf',
          },
        ],
        walletPassphrase: 'TBTC-17-09-1992-tbtc',
      };
      console.log(params);
      wallet.sendMany(params).then((transaction) => {
        // print transaction details
        console.dir(transaction);
      }).catch(console.error);
    }).catch(console.error);
    */

bitgo.wallets().
    get({'id': '2MyJRER43p7pZTU1iAVLRpDULMsds125Rkj'}).
    then((wallet) => {
      let params = {
        recipients: [
          {
            amount: 0.1 * 1e8,
            address: '2NED2sighAoEQMsZC7i6MKd9avrQhMbdgYf',
          },
        ],
        walletPassphrase: 'TBTC-17-09-1992-tbtc',
      };
      console.log(params);
      wallet.sendMany(params).then((transaction) => {
        // print transaction details
        console.dir(transaction);
      }).catch(console.error);
    }).catch(console.error);
