'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const chai = require('chai');

const spies = require('chai-spies');
chai.use(spies);

/**
 * Set up mocha with `chai`.
 *
 * routes -captchaRouter
 * @param {Object} options
 * @api public
 */

describe('Captcha testing started', () => {
  before(function(done) {
    done();
  });

  after(function(done) {
    done();
  });

  /**
   * Api Name:captcha
   * URL:/api/v1/captcha
   *
   */

  describe('create captcha api', () => {
    it('Should return captcha code and hash ', (done) => {
      request(app).
          get('/api/v1/captcha').
          set('Accept', 'application/json').
          set('Content-Type', 'application/json').
          expect(200).
          end(done);
    });
  });

  /**
   * Api Name:checkCaptcha
   * URL:/api/v1/captcha/valid
   *
   */
  describe('checkCaptcha api', () => {
    let capthaData = {
      captchaHash: '#####',
      captchaInput: 'CI',
    };
    it('Should return invalid captcha parameters ', (done) => {
      request(app).
          post('/api/v1/captcha/valid').
          send(capthaData).
          expect(400).
          end(done);
    });
  });
});
