'use strict';

/**
 * Module dependencies.
 *
 */
const request = require('supertest');
const app = require('../app');
const CaptchaModel = require('../models/CaptchaModel');
const process = require('process');
const sha512 = require('js-sha512').sha512;
const svgCaptcha = require('svg-captcha');
const config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -kycRouter
 * @param {Object} options
 * @api private
 */


describe('KYC testing started, ', () => {
  let token = null;
  let userData = null;
  let filename = 'test.png';
  let userObject = {
    email: config.get('test-credential-email'),
    password: config.get('test-credential-password'),
  };
  const ip = '127.0.0.1';

  /* GET Auth token */
  before(function(done) {
    const end = new Date();
    end.setHours(-2);
    CaptchaModel.remove({'timestamp': {$lt: end}}).exec().then(() => {
    });
    done();
  });

  before(function(done) {
    let session = sha512(process.hrtime() + ip + process.hrtime()[1] +
        new Date().getTime());
    const captcha = svgCaptcha.create();
    const cm = new CaptchaModel({
      sessionId: session,
      value: captcha.text,
      ip: ip,
    });
    cm.save().then((response) => {
      userObject.ci = response.value;
      userObject.text = response.sessionId;
      done();
    });
  });

  before(function(done) {
    request(app).post('/api/v1/auth').send(userObject).end(function(err, res) {
      userData = res.body;
      token = res.body.token; // Get Token from Response /api/vi/auth
      done();
    });
  });

  after(function(done) {
    done();
  });

  /**
   * Api Name:enterkyc
   * URL:/api/v1/kyc
   *
   */


  describe('Enterkyc api', () => {
    // let kycData = {
    //     firstName: "FirstName",
    //     lastName: "LastName",
    //     address: "IMPZ",
    //     state: "Dubai",
    //     country: "599fd3e9cb1fb0219ad83ebe"
    // };

    // if (false) {

    // it('Should be updatd/enter kyc ', (done) => {
    //         request(app)
    //             .post('/api/v1/kyc')
    //             .set('Content-Type', 'application/json')
    //             .set('X-Access-Token', token)
    //             .send(kycData)
    //             .expect(200)
    //             .end(done);
    //     })
    // }
    if (!config.get('kyc-enabled')) {
      it('Should be checked if kyc is disabled ', (done) => {
        request(app).
            post('/api/v1/kyc').
            set('Content-Type', 'application/json').
            set('X-Access-Token', token).
            expect(400).
            end(done);
      });
    }
  });

  /**
   * Api Name:confirmKYC
   * URL:/api/v1/confirm/:userId
   *
   */


  describe('Confirm Kyc api', () => {
    // if (false) {
    //     it('Should be confirmed kyc', (done) => {
    //         request(app)
    //             .post('/api/v1/kyc/confirm')
    //             .set('Content-Type', 'application/json')
    //             .set('X-Access-Token', token)
    //             .expect(200)
    //             .end(done);
    //     })
    // }
    if (!config.get('kyc-enabled')) {
      it('Should be checked if kyc is disabled ', (done) => {
        request(app).
            post('/api/v1/kyc/confirm/' + userData._id).
            set('Content-Type', 'application/json').
            set('X-Access-Token', token).
            expect(400).
            end(done);
      });
    }
  });

  /**
   * Api Name:declineKYC
   * URL:/api/v1/decline/:userId
   *
   */

  // describe('decline Kyc api', () => {

  //     if (false) {
  //         it('Should be confirmed kyc', (done) => {
  //             request(app)
  //                 .post('/api/v1/kyc/decline')
  //                 .set('Content-Type', 'application/json')
  //                 .set('X-Access-Token', token)
  //                 .expect(200)
  //                 .end(done);
  //         })
  //     }
  //     if (false) {
  //         it('Should be checked if kyc is disabled ', (done) => {
  //             request(app)
  //                 .post('/api/v1/kyc/decline/' + userData._id)
  //                 .set('Content-Type', 'application/json')
  //                 .set('X-Access-Token', token)
  //                 .expect(400)
  //                 .end(done);
  //         })

  //     }

  // });

  /**
   * Api Name:getKYC
   * URL:/api/v1/kyc
   *
   */


  describe('get Kyc api', () => {
    it('Should return kyc data for the user', (done) => {
      request(app).
          get('/api/v1/kyc').
          set('Content-Type', 'application/json').
          set('X-Access-Token', token).
          expect(200).
          expect((response) => {
            userData = response.body;
          }).
          end(done);
    });

    if (!config.get('kyc-enabled')) {
      it('Should be checked if kyc is disabled ', (done) => {
        request(app).
            get('/api/v1/kyc').
            set('Content-Type', 'application/json').
            set('X-Access-Token', token).
            expect(400).
            end(done);
      });
    }
  });

  /**
   * Api Name:uploadPassport
   * URL:/api/v1/kyc/uploadPassport
   *
   */


  describe('upload Passport Kyc api', () => {
    if (!config.get('kyc-enabled')) {
      it('Should be checked if kyc is disabled ', (done) => {
        request(app).
            post('/api/v1/kyc/uploadPassport').
            set('Content-Type', 'application/json').
            set('X-Access-Token', token).
            expect(400).
            end(done);
      });
    }
    if (userData && userData.passport) {
      it('Should return kyc data for the user', (done) => {
        request(app).
            post('/api/v1/kyc/uploadPassport').
            set('X-Access-Token', token).
            attach('image', __dirname + '/' + filename).
            expect(200).
            end(done);
      });
    }
  });

  /**
   * Api Name:uploadUtility
   * URL:/api/v1/kyc/uploadUtility
   *
   */

  describe('upload utility Kyc api', () => {
    if (userData && userData.utility) {
      it('Should return kyc data for the user', (done) => {
        request(app).
            post('/api/v1/kyc/uploadUtility').
            set('Content-Type', 'application/json').
            set('X-Access-Token', token).
            attach('image', __dirname + '/' + filename).
            expect(200).
            end(done);
      });
    }
    if (!config.get('kyc-enabled')) {
      it('Should be checked if kyc is disabled ', (done) => {
        request(app).
            post('/api/v1/kyc/uploadUtility').
            set('Content-Type', 'application/json').
            set('X-Access-Token', token).
            expect(400).
            end(done);
      });
    }
  });
});
