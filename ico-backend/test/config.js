'use strict';

/**
 * Module dependencies.
 *
 */

const request = require('supertest');
const app = require('../app');
const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');

/**
 * Set up mocha with `chai`.
 *
 * routes -configRouter
 * @param {Object} options
 * @api public
 */

describe('Config Test Started', () => {
    let token = null;
    let userData = null;
    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    /**
     * Api Name:config
     * URL:/config/:key
     * 
     */

    describe('Get Config Api', () => {
        it('Should return error if config key not exist ', (done) => {
            request(app)
                .get('/api/v1/config/')
                .set('Accept', 'application/json')
                .set('Content-Type', 'application/json')
                .set('X-Access-Token', token)
                .expect(404)
                .end(done);
        });
    });

    /**
     * Api Name:getDynamicConfig
     * URL:/config/dynamic/:key
     * 
     */

    describe('Get Dynamic Config Api', () => {
        it('Should return error if config key not exist ', (done) => {
            request(app)
                .get('/api/v1/config/dynamic/')
                .set('X-Access-Token', token)
                .expect(404)
                .end(done);
        });
    });
});
