'use strict';

/**
 * Module dependencies.
 *
 */

const request = require('supertest');
const app = require('../app');
const chai = require('chai');

const captchaModel = require('../models/CaptchaModel');
const process = require('process'),
    sha512 = require('js-sha512').sha512,
    svgCaptcha = require('svg-captcha'),
    config = require('config');


const spies = require('chai-spies');

chai.use(spies);

/**
 * Set up mocha with `chai`.
 *
 * routes -btcHookRouter
 * @param {Object} options
 * @api private
 */

describe('btcHook testing started,', () => {
    let token = null,
        userData = null,
        wallet = null,
        transfer = null,
        hash = null,
        coin = 'tbtc';

    let userObject = {
        email: config.get('test-credential-email'),
        password: config.get('test-credential-password'),
    };
    const ip = '127.0.0.1';

    before(function(done) {
        const end = new Date();
        end.setHours(-2);
        captchaModel.remove({'timestamp': {$lt: end}}).exec()
            .then(() => {});
        done();
    });

    before(function(done) {
        let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
        const captcha = svgCaptcha.create();
        const cm = new captchaModel({
            sessionId: session,
            value: captcha.text,
            ip: ip,
        });
        cm.save()
            .then((response) => {
                userObject.ci = response.value;
                userObject.text = response.sessionId;
                done();
            });
    });

    before(function(done) {
        request(app)
            .post('/api/v1/auth')
            .send(userObject)
            .end(function(err, res) {
                userData = res.body;
                token = res.body.token; // Get Token from Response /api/vi/auth
                done();
            });
    });

    before(function(done) {
        request(app)
            .get('/api/v1/hook/getUsdInvests-wallets/' + coin)
            .set('X-Access-Token', token)
            .end(function(err, res) {
                wallet = res.body.wallets[0]._wallet.id;
                done();
            });
    });

    before(function(done) {
        request(app)
            .get('/api/v1/hook/getUsdInvests-transfers/' + coin + '/' + wallet)
            .set('X-Access-Token', token)
            .end(function(err, response) {
                transfer = response.body.transfers[0].id;
                hash = response.body.transfers[0].txid;
                done();
            });
    });


    /**
     * Api Name:processTransfer
     * URL:/btc/hook/transfer
     * 
     */
    describe('Transfer Api', () => {
        /* sinon stubs */
        // it('responds to processTransfer', function processTransfer(done) {

        //     const rootControllerStub = sinon.stub(btcHookController, "processTransfer",
        //         function(req, res, next) {
        //             res.status(200).json({ stubbed: 'data' });
        //         });
        //     request(app)
        //         .post('/api/v1/btc/hook/transfer')
        //         .set('X-Access-Token', token)
        //         .expect(200)
        //         .expect({ stubbed: 'data' })
        //         .end(done);
        // });


        /* Sinon Mocking */
        // var btcAddressesModelMock = sinon.mock(btcAddressesModel);

        // // btcAddressesModelMock.expects("create").once().withArgs("foo", hash("1234"));

        // btcTransferHooksModel.findOne({ $or: [{ transfer: '598fcb0e9aa9d2f40bb40912294a24e3' }] })
        //     .exec()
        //     .then(existingBtcTransferHook => {

        //         console.log(existingBtcTransferHook);

        //     });

        // btcAddressesModelMock.verify();
        // btcAddressesModelMock.restore();
        // var testCase = buster.testCase("Form controller", {
        //     setUp: function () {
        //         this.form = document.createElement("form");
        //         this.form.innerHTML = "<fieldset>" +
        //             "<input type='text' name='item' id='item'>" +
        //             "</fieldset>";

        //         this.input = this.form.getElementsByTagName("input")[0];
        //         this.backend = { add: sinon.spy() };
        //         this.controller = todoList.formController.create(this.form, this.backend);
        //         this.callback = sinon.spy();
        //         this.controller.on('item', this.callback);
        //     },

        //     "adding items": {
        //         setUp: function () {
        //             this.input.value = "It puts the lotion in the basket";
        //         },

        //         "successfully": {
        //             "should emit onItem on success": function () {
        //                 var item = { text: "It puts the lotion in the basket" };
        //                 sinon.stub(this.backend, "add").yields(item);

        //                 this.controller.addItem();

        //                 sinon.assert.calledOnce(this.callback);
        //                 sinon.assert.calledWith(this.callback, item);
        //             },

        //             "should clear form on success": function () {
        //                 this.input.value = "It puts the lotion in the basket";
        //                 this.backend.add = sinon.stub().yields({});

        //                 this.controller.addItem();

        //                 buster.assert.equals("", this.input.value);
        //             }
        //         },

        //         "unsuccessfully": {
        //             "should render error on failure": function () {
        //                 sinon.stub(this.backend, "add").yields(null);

        //                 this.controller.addItem();
        //                 var err = this.form.firstChild;

        //                 buster.assert.match(err, {
        //                     tagName: "p",
        //                     className: "error",
        //                     innerHTML: "An error prevented the item from being saved"
        //                 });
        //             }
        //         }
        //     }
        // });


        // var assert = require('assert');

        // it('calls original function processtransfer with right this and args', function() {
        //     var callback = sinon.spy();
        //     var proxy = once(callback);
        //     var obj = {};

        //     proxy.call(obj, 1, 2, 3);

        //     assert(callback.calledOn(obj));
        //     assert(callback.calledWith(1, 2, 3));
        // });

        // var server;

        // before(function() { server = sinon.fakeServer.create(); });
        // after(function() { server.restore(); });

        // it("calls callback with deserialized data", function() {
        //     var callback = sinon.spy();
        //     var transferData = {


        //     };
        //     processTransfer(transferData, callback);

        //     // This is part of the FakeXMLHttpRequest API
        //     server.requests[0].respond(
        //         200, { "Content-Type": "application/json" },
        //         JSON.stringify([{ id: 1, text: "Provide examples", done: true }])
        //     );

        //     assert(callback.calledOnce);
        // });


        it('should return 200 if trasfer already exist or new created', (done) => {
            // var transferData = {
            //     type: 'transfer',
            //     wallet: wallet,
            //     hash: hash,
            //     transfer: transfer,
            //     coin: coin

            // };

            let transferData = {
                type: 'transfer',
                wallet: '598ec811774b2bda0946b5aa003f381b',
                hash: '41cf49f5c3a47691a0b1f2a6ff17790d775cd0d20ae6e5b91b45d0183cf9703f',
                transfer: '598fcb0e9aa9d2f40bb40912294a24e3',
                coin: 'tbtc',
            };

            /* Sinon Mocking */
            // var btcAddressesModelMock = sinon.mock(btcAddressesModel);

            // // btcAddressesModelMock.expects("create").once().withArgs("foo", hash("1234"));

            // btcTransferHooksModel.findOne({ $or: [{ transfer: '598fcb0e9aa9d2f40bb40912294a24e3' }] })
            //     .exec()
            //     .then(existingBtcTransferHook => {

            //         console.log(existingBtcTransferHook);

            //     });

            // btcAddressesModelMock.verify();
            // btcAddressesModelMock.restore();


            // var transferData = {
            //     hash: "41cf49f5c3a47691a0b1f2a6ff17790d775cd0d20ae6e5b91b45d0183cf9703f",
            //     transfer: "598fcb0e9aa9d2f40bb40912294a24e3",
            //     coin: "tbtc",
            //     type: "transfer",
            //     wallet: "598ec811774b2bda0946b5aa003f381b"
            // }

            // let stub = sinon.createStubInstance(btcHookController);
            // console.log(stub);


            // var cb = sinon.spy();
            // btcHookController.processTransfer(transferData, cb);

            // //expect(cb).to.have.been.calledWith("hello foo");
            // expect(cb).to.have.been.called();


            // var rpStub = sinon.stub(rp, 'post'); // mock rp.post() calls

            //     // calls to rp.post() return a promise that will resolve to an object
            //     rpStub.returns(Promise.resolve({
            //         fakedResponse: 'will return exactly this'
            //     }));

            request(app)
                .post('/api/v1/btc/hook/transfer')
                .set('X-Access-Token', token)
                .send(transferData)
                .expect(200)
                .end(done);
        });
    });
});
