// 'use strict';


// const ethInvestController = require('../controllers/ethInvestController');

// const request = require('supertest');
// const app = require('../app');
// const expect = require('chai').expect;

// const captchaModel = require('../models/captchaModel');
// const process = require('process'),
//     sha512 = require('js-sha512').sha512,
//     svgCaptcha = require('svg-captcha'),
//     config = require('config');

// /**
//  * Set up mocha with `chai`.
//  *
//  * routes -ethRouter
//  * @param {Object} options
//  * @api private
//  */

// describe('eth testing started', () => {

//     var token = null;
//     var userData = null;
//     var ci = null;
//     var captchaHash = null;
//     var userObject = {
//         email: config.get('test-credential-email'),
//         password: config.get('test-credential-password')
//     }
//     const ip = '127.0.0.1';

//     before(function(done) {

//         const end = new Date();
//         end.setHours(-2);
//         captchaModel.remove({ 'timestamp': { $lt: end } }).exec()
//             .then(() => {})
//         done();

//     });

//     before(function(done) {
//         let session = sha512(process.hrtime() + ip + process.hrtime()[1] + new Date().getTime());
//         const captcha = svgCaptcha.create();
//         const cm = new captchaModel({
//             sessionId: session,
//             value: captcha.text,
//             ip: ip
//         });
//         cm.save()
//             .then((response) => {
//                 userObject.ci = response.value;
//                 userObject.text = response.sessionId;
//                 done();
//             })

//     });

//     before(function(done) {
//         request(app)
//             .post('/api/v1/auth')
//             .send(userObject)
//             .end(function(err, res) {
//                 userData = res.body;
//                 token = res.body.token; // Get Token from Response /api/vi/auth
//                 done();
//             });
//     })

//     /**
//      * Api Name:generateAddress
//      * URL:/eth/generate-address
//      * 
//      */
//     describe('Generate Address Api', () => {
//         if (false) {

//             it('should return error with incomplete post data', (done) => {

//                 var postData = {}

//                 request(app)
//                     .post('/api/v1/eth/generate-address')
//                     .set('X-Access-Token', token)
//                     .send(postData)
//                     .expect(400)
//                     .end(done);
//             });

//         }

//         if (false) {
//             it('should check minimum and maximum eth', (done) => {
//                 var postData = {
//                     amountETH: 3
//                 }

//                 request(app)
//                     .post('/api/v1/eth/generate-address')
//                     .set('X-Access-Token', token)
//                     .send(postData)
//                     .expect(400)
//                     .end(done);
//             });


//         }
//         if (false) {

//             it('should generate address with given eth amount', (done) => {
//                 var postData = {
//                     amountETH: 1
//                 }

//                 request(app)
//                     .post('/api/v1/eth/generate-address')
//                     .set('X-Access-Token', token)
//                     .send(postData)
//                     .expect(200)
//                     .expect(function(response) {
//                         expect(response.body).not.to.be.empty;
//                         expect(response.body).to.be.an('object');
//                         expect(response.body).to.have.property('exchangeRateValue');
//                         expect(response.body).to.have.property('currencyId');
//                         expect(response.body).to.have.property('address');
//                         expect(response.body).to.have.property('status');
//                         expect(response.body).to.have.property('userId');
//                     })
//                     .end(done);
//             });
//         }


//     });

//     /**
//      * Api Name:generatedAddresses
//      * URL:/eth/generated-addresses
//      * 
//      */
//     describe('Get Generated Address Api', () => {

//         it('should return generated address for the user', (done) => {
//             request(app)
//                 .get('/api/v1/eth/generated-addresses')
//                 .set('X-Access-Token', token)
//                 .expect(200)
//                 .expect(function(response) {
//                     address = response.body[0]._id;

//                     response.body.forEach(function(element) {
//                         expect(element).not.to.be.empty;
//                         expect(element).to.be.an('object');
//                         expect(element).to.have.property('exchangeRateValue');
//                         expect(element).to.have.property('currencyId');
//                         expect(element).to.have.property('address');
//                         expect(element).to.have.property('status');
//                         expect(element).to.have.property('userId');
//                         expect(element).to.have.property('found');
//                     }, this);

//                 })
//                 .end(done);
//         });

//     });

//     /**
//      * Api Name:getGeneratedAddress
//      * URL:/eth/generated-addresses/:addressId
//      * 
//      */
//     describe('Get Generated Address Api for address', () => {

//         it('should return generated address for the user\'s adreess', (done) => {
//             request(app)
//                 .get('/api/v1/eth/generated-address/' + address)
//                 .set('X-Access-Token', token)
//                 .expect(200)
//                 .expect(function(response) {
//                     expect(response.body).not.to.be.empty;
//                     expect(response.body).to.be.an('object');
//                     expect(response.body).to.have.property('exchangeRateValue');
//                     expect(response.body).to.have.property('currencyId');
//                     expect(response.body).to.have.property('address');
//                     expect(response.body).to.have.property('status');
//                     expect(response.body).to.have.property('userId');
//                     expect(response.body).to.have.property('found');
//                 })
//                 .end(done);
//         });

//     });

// });
