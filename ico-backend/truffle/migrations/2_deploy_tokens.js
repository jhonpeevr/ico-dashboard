const SafeMathLib = artifacts.require('./SafeMath.sol');
const GENX = artifacts.require('./GENX.sol');
const EVT = artifacts.require('./EVT.sol');

module.exports = (deployer) => {
  deployer.deploy(SafeMathLib);
  deployer.link(SafeMathLib, GENX);
  deployer.link(SafeMathLib, EVT);
  deployer.deploy(GENX);
  deployer.deploy(EVT);
};
