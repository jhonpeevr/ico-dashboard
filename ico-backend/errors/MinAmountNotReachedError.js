const AbstractError = require('./AbstractError');
/**
 * Class representing error
 * when entered amount is less than minimum allowed amount
 */
module.exports = class MinAmountNotReachedError extends AbstractError {
  /** Create an instance */
  constructor(arg) {
    /** Calling parent constructor of base Error class. */
    super('Minimum amount (' + arg + ') is not reached');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 400;
  }
};
