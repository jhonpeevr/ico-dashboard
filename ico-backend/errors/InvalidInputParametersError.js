const AbstractError = require('./AbstractError');
/**
 * Universal Class representing error
 * when input parameters for service is not valid.
 * Has to be used everywhere
 */
module.exports = class InvalidInputParametersError extends AbstractError {
  /** Create an instance */
  constructor() {
    /** Calling parent constructor of base Error class. */
    super('Invalid input parameters');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 400;
  }
};
