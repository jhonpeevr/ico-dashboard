/**
 * Parent Abstract error
 */
module.exports = class AbstractError extends Error {
  /**
   * Install message on error
   * @param {String} message - error message
   *
   */
  constructor(message) {
    /** Calling parent constructor of base Error class. */
    super(message);

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * & route user to proper page, respond immediately
     */
    this.status = 0; // BAD_REQUEST
  }
};
