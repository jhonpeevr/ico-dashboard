const AbstractError = require('./AbstractError');
/**
 * Class representing error
 * when user account is blocked
 */
module.exports = class AddressGenerationError extends AbstractError {
  /** Create an instance */
  constructor() {
    /** Calling parent constructor of base Error class. */
    super('An error occurred while generating address. Please try again later');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 400;
  }
};
