const AbstractError = require('./AbstractError');
/**
 * Class representing error
 * when user is trying to get tokens before vesting date
 */
module.exports = class WithdrawTokenError extends AbstractError {
  /** Create an instance */
  constructor() {
    /** Calling parent constructor of base Error class. */
    super('Token Withdraw is disable');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 400;
  }
};
