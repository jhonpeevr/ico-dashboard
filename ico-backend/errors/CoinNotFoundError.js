const AbstractError = require('./AbstractError');
/**
 * Class representing error
 * when coin type is not supported for operations
 */
module.exports = class CoinTypeNotSupportedError extends AbstractError {
  /**
   * Create an instance
   *
   * @param coinType - not found coin type
   * */
  constructor(coinType) {
    /** Calling parent constructor of base Error class. */
    super('Coin ' + coinType + ' type is not supported');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 400;
  }
};
