const AbstractError = require('./AbstractError');
/**
 * Class representing error
 * when user not found
 */
module.exports = class ActivationCodeExpired extends AbstractError {
  /** Create an instance */
  constructor() {
    /** Calling parent constructor of base Error class. */
    super('Activation code has expired or has already been used!');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 401;
  }
};
