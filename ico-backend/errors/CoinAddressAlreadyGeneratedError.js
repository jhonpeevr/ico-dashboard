const AbstractError = require('./AbstractError');
/**
 * Class representing error
 * when coin address already exists
 */
module.exports = class CoinAddressAlreadyGeneratedError extends AbstractError {
  /** Create an instance */
  constructor() {
    /** Calling parent constructor of base Error class. */
    super('You have already generated 2 addresses for the token transfer. Kindly complete those transactions first');

    /** Saving class name in the property of our custom error as a shortcut. */
    this.name = this.constructor.name;

    /** Default status for the error
     * That's how we can determine system errors
     * When system error happens, log it
     * route user to proper page, respond immediately
     *
     * 400 - BAD_REQUEST
     */
    this.status = 400;
  }
};
