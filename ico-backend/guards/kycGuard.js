const jwt = require('jsonwebtoken');
const httpStatus = require('http-status-codes');

const loggerName = '[KYCGuard]: ';

const allowedList = [
    '/kyc/getNonComplianceUsersDetail',
    '/kyc/sendKYCAcknowledgement',
    '/kyc/submitKYCStatus',
];

const urlChecker = (url) => {
  for (const allowedUrl of allowedList) {
    if (url === allowedUrl) {
      return true;
    }
  }
};
module.exports = function(req, res, next) {
  const token = req.headers['x-auth-token'];

  if (urlChecker(req.originalUrl)) {
    if (token) {
      const now = new Date();
      const secret = new Date(now.getFullYear(), now.getMonth(), now.getDate(),
          now.getHours(), now.getMinutes()).getTime().toString();
      jwt.verify(token, secret, function(err, decoded) {
        if (err) {
          console.error(loggerName, err.name);
          console.error(loggerName, err.message);
          if (err instanceof jwt.TokenExpiredError) {
            return res.status(httpStatus.FORBIDDEN).
                send({err: 'Session expired'});
          }

          console.error(loggerName, err.message);
          return res.status(httpStatus.BAD_REQUEST).send({err: err});
        } else {
          req.decoded = decoded;
          req.token = token;
          return next();
        }
      });
    } else {
      return res.status(httpStatus.FORBIDDEN).send({err: 'Forbidden'});
    }
  } else {
    return res.status(httpStatus.FORBIDDEN).send({err: 'Forbidden'});
  }
};
