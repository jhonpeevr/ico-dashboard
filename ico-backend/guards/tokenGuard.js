const config = require('config');
const jwt = require('jsonwebtoken');

const httpStatus = require('http-status-codes');

const loggerName = '[TokenGuard]: ';

const allowedList = [
  '/api/v1/auth',
  '/api/v1/auth/facebook',
  '/api/v1/auth/google',
  '/api/v1/auth/sign-up',
  '/api/v1/auth/forgot-password',
  '/api/v1/auth/check-activation-code',
  '/api/v1/coin/hook/getReferral',
  '/api/v1/coin/hook/transfer', // todo: protect by ip tables
  '/api/v1/captcha',
  '/api/v1/captcha/valid',
  '/api/v1/captcha/reset',
  '/api/v1/countries',
  '/api/v1/auth/mailchimp',
  '/api/v1/exchange-rate/update',
  '/api/v1/support'
];

const dynamicAllowedList = [
  '/api/v1/auth/activate',
  '/api/v1/auth/change-password',
  '/api/v1/subscription',
];

const urlChecker = (url) => {
  for (const allowedUrl of dynamicAllowedList) {
    if (url.indexOf(allowedUrl) > -1) {
      return true;
    }
  }

  for (const allowedUrl of allowedList) {
    if (url === allowedUrl) {
      return true;
    }
  }
};

module.exports = function(req, res, next) {
  const token = req.headers['x-access-token'];

  if (urlChecker(req.originalUrl)) {
    next();
  } else {
    if (token) {
      jwt.verify(token, config.get('secret'), function(err, decoded) {
        if (err) {
          console.error(loggerName, err.name);
          console.error(loggerName, err.message);
          if (err instanceof jwt.TokenExpiredError) {
            return res.status(httpStatus.FORBIDDEN).
                send({err: 'Session expired'});
          }

          console.error(loggerName, err.message);
          return res.status(httpStatus.BAD_REQUEST).send({err: err});
        } else {
          req.decoded = decoded;
          req.token = token;

          if (req.originalUrl.indexOf('admin') >= 0 &&
              decoded.role !== 'admin') {
            console.error(loggerName, req.originalUrl, 'not an admin');
            return res.status(403).send({err: 'Forbidden'});
          }
          return next();
        }
      });
    } else {
      return res.status(httpStatus.FORBIDDEN).send({err: 'Forbidden'});
    }
  }
};
