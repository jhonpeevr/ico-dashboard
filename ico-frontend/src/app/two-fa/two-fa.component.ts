import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../_service/auth.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {Router} from '@angular/router';

@Component({
  selector: 'app-two-fa',
  templateUrl: './two-fa.component.html',
  styleUrls: ['./two-fa.component.css']
})
export class TwoFaComponent implements OnInit {

  constructor(public formBuilder: FormBuilder,
              private authService: AuthService,
              private router: Router) {
  }

  form: FormGroup;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  pageError: string;

  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  ngOnInit() {

    this.form = this.formBuilder.group({
      twoFactorCode: this.formBuilder.control(null, [Validators.minLength(6), Validators.maxLength(6)])
    });
    this.pageError="";

  }

  submit() {
    if (this.form.valid) {
      this.authService.validateTwoFa(this.form.value.twoFactorCode).subscribe((message) => {
        localStorage.setItem('twofaCheck', 'notRequired');
        this.router.navigate(['/account']);
      }, (err) => {
        this.pageError = err.error.err;
        this.pageStatus = 'START';
        if (this.pageError !== '') {
          setTimeout(() => {
            this.errorSwal.show();
          }, 500);
        }
      });
    }
  }

}
