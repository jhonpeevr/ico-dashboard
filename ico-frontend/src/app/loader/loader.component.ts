import { Component, OnInit, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Feed} from '../_models/feed';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {element} from 'protractor';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  startLoader() {
    document.getElementById('loaderImg').classList.add('loader');
  }
  stopLoader() {
    document.getElementById('loaderImg').classList.remove('loader');
  }
}
