import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {NgxSmartModalModule, NgxSmartModalService} from 'ngx-smart-modal';
// import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
// import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {LiveChatComponent} from './live-chat/live-chat.component';
import {ActiveEmailComponent} from './active-email/active-email.component';
import {HomeComponent} from './dashboard/home/home.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {UnavailableComponent} from './unavailable/unavailable.component';
import {AuthGuard} from './_guards/auth.guard';
import {AccountComponent} from './dashboard/account/account.component';
import {NavigationComponent} from './dashboard/navigation/navigation.component';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {UserService} from './_service/user.service';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgxQRCodeModule} from 'ngx-qrcode2';
import {MyDatePickerModule} from 'mydatepicker';
import {KYCService} from './_service/kyc.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
// import { ChartModule } from 'angular2-highcharts';
import {AmChartsModule} from '@amcharts/amcharts3-angular';
import {SocialLoginModule, AuthServiceConfig} from 'angularx-social-login';
import {FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';

import {
  RECAPTCHA_SETTINGS,
  RecaptchaLoaderService,
  RecaptchaModule,
  RecaptchaSettings
} from 'ng-recaptcha/index';
import {environment} from '../environments/environment';
import {ReferralComponent} from './dashboard/referral/referral.component';
import {TransactionComponent} from './dashboard/transaction/transaction.component';
import {HeaderComponent} from './dashboard/header/header.component';
import {AuthService} from './_service/auth.service';
import {ConfigService} from './_service/config.service';
import {TokenService} from './_service/token.service';
import {ProfileComponent} from './dashboard/profile/profile.component';
import {SupportComponent} from './dashboard/support/support.component';
/*copy to clipboard module */
import {ClipboardModule} from 'ngx-clipboard';
/* Share module*/
import {ShareModule} from '@ngx-share/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {FinHistoryService} from './_service/fin-history.service';
/* Toaster Module */
import {ToastrModule} from 'ngx-toastr';
import {SupportMailService} from './_service/support.service';
import {TwoFaComponent} from './two-fa/two-fa.component';
import {TwoFactorAuthService} from './_service/two-factor-auth.service';
import {ReferralService} from './_service/referral.service';
import {ServiceWorkerModule} from '@angular/service-worker';
import {CoinService} from './_service/coin.service';
import {ExchangeRateService} from './_service/exchange-rate.service';
import {CountriesService} from './_service/countries.service';
import {NotificationService} from './_service/notification.service';
import {SocketIoModule, SocketIoConfig, Socket} from 'ng-socket-io';
import {NgProgressModule} from '@ngx-progressbar/core';
import {NgProgressHttpModule} from '@ngx-progressbar/http';
import {NgProgressRouterModule} from '@ngx-progressbar/router';
import {NotificationPipe} from './_pipe/notification.pipe';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {PaymentComponent} from './dashboard/payment/payment.component';
import {IcoTimerComponent} from './dashboard/ico-timer/ico-timer.component';
import {GenerateAddressComponent} from './dashboard/generate-address/generate-address.component';
import {CompletedTransactionComponent} from './dashboard/completed-transaction/completed-transaction.component';
import { NotificationComponent } from './dashboard/notification/notification.component';
import { LoaderComponent } from './loader/loader.component';
import { MathematicsService } from './_service/mathematics.service';


const configSocket: SocketIoConfig = {url: 'http://localhost:8988', options: {}};


let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.googleAppId)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.facebookAppId)
  },
]);

export function provideConfig() {
  return config;
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const globalSettings: RecaptchaSettings = {siteKey: environment.googleCaptchaSiteKey};

const AppRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'app',
    component: AppComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', redirectTo: '/home', pathMatch: 'full'},
      {path: 'dashboard', component: DashboardComponent, pathMatch: 'full'},
      {path: '**', redirectTo: '/notfound', pathMatch: 'full'}
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'unavailable', component: UnavailableComponent},
  {path: 'notfound', component: NotFoundComponent},
  {path: 'account', component: AccountComponent, canActivate: [AuthGuard]},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'payment/:coinType', component: PaymentComponent, canActivate: [AuthGuard]},
  {path: 'generated-address', component: GenerateAddressComponent, canActivate: [AuthGuard]},
  {path: 'referral', component: ReferralComponent, canActivate: [AuthGuard]},
  {path: 'notification', component: NotificationComponent, canActivate: [AuthGuard]},
  {path: 'transaction', component: TransactionComponent, canActivate: [AuthGuard]},
  {path: 'completed-transaction/:transactionId', component: CompletedTransactionComponent, canActivate: [AuthGuard]},
  {path: 'support', component: SupportComponent, canActivate: [AuthGuard]},
  {path: 'two-fa', component: TwoFaComponent},
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: 'activate-email/:email/:activationCode', component: ActiveEmailComponent},
  {path: 'change-password/:email/:activationCode', component: ChangePasswordComponent},
  {path: 'privacy-policy', component: PrivacyPolicyComponent},
];

// let config = new AuthServiceConfig([
//   {
//     id: GoogleLoginProvider.PROVIDER_ID,
//     provider: new GoogleLoginProvider('Google-OAuth-Client-Id')
//   },
//   {
//     id: FacebookLoginProvider.PROVIDER_ID,
//     provider: new FacebookLoginProvider('Facebook-App-Id')
//   }
// ]);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    LiveChatComponent,
    ActiveEmailComponent,
    HomeComponent,
    NotFoundComponent,
    UnavailableComponent,
    DashboardComponent,
    AccountComponent,
    NavigationComponent,
    ReferralComponent,
    TransactionComponent,
    HeaderComponent,
    ProfileComponent,
    SupportComponent,
    TwoFaComponent,
    NotificationPipe,
    PrivacyPolicyComponent,
    PaymentComponent,
    IcoTimerComponent,
    GenerateAddressComponent,
    CompletedTransactionComponent,
    NotificationComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    RecaptchaModule.forRoot(),
    NgxPaginationModule,
    ClipboardModule,
    RouterModule.forRoot(AppRoutes),
    NgxSmartModalModule.forRoot(),
    HttpClientModule,
    HttpModule,
    AmChartsModule,
    ShareModule.forRoot(),
    FormsModule,
    MyDatePickerModule,
    ReactiveFormsModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
    }),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    NgxQRCodeModule,
    environment.production ? ServiceWorkerModule.register('ngsw-worker.js') : [],
    SocketIoModule.forRoot(configSocket),
    NgProgressModule.forRoot(),
    NgProgressHttpModule,
    NgProgressRouterModule
  ],
  providers: [
    AuthGuard,
    NgxSmartModalService,
    UserService,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: globalSettings,
    },
    AuthService,
    ConfigService,
    TokenService,
    FinHistoryService,
    SupportMailService,
    TwoFactorAuthService,
    CoinService,
    ExchangeRateService,
    ReferralService,
    KYCService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    CountriesService,
    NotificationService,
    MathematicsService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
