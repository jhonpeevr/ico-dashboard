import {Component, OnInit, ViewChild} from '@angular/core';

import {UserService} from '../_service/user.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {AuthService as AuthServiceSocial, SocialUser} from 'angularx-social-login';
import {FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider} from 'angularx-social-login';
import {AuthService as AuthServiceInternal} from '../_service/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TokenService} from '../_service/token.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {AuthService} from 'angularx-social-login/src/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  googleCaptchaResponseLogin: string;
  googleCaptchaCheck: boolean;
  pageError: string;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  private user: SocialUser;
  private loggedIn: boolean;
  form: FormGroup;
  public referralCode: string = null;
  signupSubmit: boolean = true;
  emailActivationCode: string;

  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(private userService: UserService,
              private router: Router,
              private authServiceSocial: AuthServiceSocial,
              private authServiceInternal: AuthServiceInternal,
              private tokenService: TokenService,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,) {
  }

  ngOnInit() {

    if (localStorage.getItem('_u')) {
      AuthServiceInternal.logout();
      this.signOut();
    }
    if (localStorage.getItem('terms') !== 'true') {
      this.router.navigate(['/app/dashboard']);
    }

    if (localStorage.getItem('_u')) {
      this.router.navigate(['/account']);
    } else {
      this.authServiceSocial.authState.subscribe((user) => {
        if (user) {
          this.signOut();
          this.authServiceInternal.signUpSocial(user, this.referralCode)
            .subscribe((userData) => {
                localStorage.setItem('_u', JSON.stringify(userData));
                localStorage.setItem('token', userData['token']);
                // localStorage.setItem('kycStatus', userData['kycStatus']);
                localStorage.setItem('setup', 'init');
                this.pageStatus = 'COMPLETED';
                if (userData['twoFaEnabled']) {
                  localStorage.setItem('twofaCheck', 'required');
                  this.router.navigate(['/two-fa']);
                } else {
                  if (userData['isNew']) {
                    this.router.navigate(['/account']);
                  } else {
                    this.router.navigate(['/home']);
                  }
                  localStorage.removeItem('referralCode');
                }
              },
              (err) => {
                this.pageError = err.error.err;
                this.pageStatus = 'START';

                if (this.pageError !== '') {
                  setTimeout(() => {
                    this.errorSwal.show();
                  }, 500);
                }

              });
        }

      });
    }

    this.form = this.formBuilder.group({
      email: this.formBuilder.control(null, [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]),
      password: this.formBuilder.control(null, [Validators.required, Validators.minLength(6)]),
      //  captcha: this.formBuilder.control(null, [Validators.required, Validators.maxLength(4), Validators.minLength(4)]),
      twoFactorCode: this.formBuilder.control(null, [Validators.minLength(6), Validators.maxLength(6)])
    });

    // subscribing query string
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['rhash']) {
        localStorage.setItem('referralCode', params['rhash']);
        this.form.patchValue({rhash: params['rhash']});
      }
    });

    this.referralCode = localStorage.getItem('referralCode');

    this.emailActivationCode = this.randomString(128);

    // this.loadScript();
  }

  /*public loadScript() {
    const APP_ID = 'wsxa0rpl';
    const node = document.createElement('script');
    node.type = 'text/javascript';
    node.text = 'window.intercomSettings={app_id:"' + APP_ID + '"}, (function(){function t(){var t=a.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://widget.intercom.io/widget/wsxa0rpl";var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,n=e.Intercom;if("function"==typeof n)n("reattach_activator"),n("update",intercomSettings);else{var a=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Intercom=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}})();';
    document.getElementsByTagName('body')[0].appendChild(node);
  }*/

  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }


  resolvedLogin(captchaResponse: string) {
    this.googleCaptchaResponseLogin = captchaResponse;
    this.googleCaptchaCheck = true;
  }


  login() {
    this.pageStatus = 'PROCESSING';

    if (this.form.valid) {
      this.authServiceInternal.login({
        email: this.form.controls['email'].value,
        password: this.form.controls['password'].value,
        code: 'twoFactorCode',
         googleCaptchaResponseLogin: this.googleCaptchaResponseLogin
      })
        .subscribe(
          (userData) => {
            localStorage.setItem('_u', JSON.stringify(userData));
            localStorage.setItem('token', userData['token']);
            //localStorage.setItem('kycStatus', userData['kycStatus']);
            localStorage.setItem('setup', 'init');
            localStorage.removeItem('referralCode');
            this.pageStatus = 'START';
            if (userData['twoFaEnabled']) {
              localStorage.setItem('twofaCheck', 'required');
              this.router.navigate(['/two-fa']);
            } else {
              if (userData['isNew']) {
                this.router.navigate(['/account']);
              } else {
                this.router.navigate(['/home']);
              }
            }

          },
          (err) => {
            this.tokenService.processError('[loginError]', err);
            this.pageError = err.error.err;
            this.pageStatus = 'START';

            if (this.pageError !== '') {
              setTimeout(() => {
                this.errorSwal.show();
              }, 500);
            }
          }
        );
    } else {
      this.pageError = 'Form is invalid';
    }
  }

  randomString(len, charSet = null) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
      var randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  }

}
