import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {CoinService} from '../../_service/coin.service';
import {ToastrService} from 'ngx-toastr';
import {TokenService} from '../../_service/token.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-generate-address',
  templateUrl: './generate-address.component.html',
  styleUrls: ['./generate-address.component.css']
})
export class GenerateAddressComponent implements OnInit {
  generatedAddressText: string;
  isAddressGenerated = false;
  generatedAddressAmount: string;
  coinResult: string
  trId: string
  currency: string;
  coinAddress: any;
  tkn2USD: number;
  tkn2Tbtc: number;
  tkn2Teth: number;
  tkn2Tltc: number;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(private router: Router,
              private coinService: CoinService,
              private toastr: ToastrService,
              private tokenService: TokenService) {


    this.tokenService.socket.on('address-balance-changed-' + TokenService.hash(), (data) => {
      if(data.address == this.generatedAddressText){
        this.coinResult = data.balance.toFixed(8)
      }
    });
  }

  ngOnInit() {

    this.coinAddress = this.coinService.getAddressStore();

    if(this.coinAddress){
      this.isAddressGenerated = true;
      this.generatedAddressText = this.coinAddress.address;
      this.generatedAddressAmount = this.coinAddress.coinAmount;
      this.coinResult = this.coinAddress.coinResult.toFixed(8);
      this.currency = this.coinAddress.currencyAbbr.slice( 1 );
      this.trId = this.coinAddress._id;
    }else{
      this.router.navigate(['/home']);
    }

  }

  textCopied() {
    this.toastr.success('Successfully Copied!', 'Address');
  }


  showReceipt() {
    if(parseFloat(this.coinResult) > 0.00000001) {
      this.router.navigate(['/completed-transaction/' + this.trId]);
    }else{
      //show message
      setTimeout(() => {
        this.errorSwal.show();
      }, 200);
    }
  }

}
