import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FinHistory} from '../../_models/fin-history';
import {FinHistoryService} from '../../_service/fin-history.service';
import {environment} from '../../../environments/environment';
import {TokenService} from '../../_service/token.service';

@Component({
  selector: 'app-completed-transaction',
  templateUrl: './completed-transaction.component.html',
  styleUrls: ['./completed-transaction.component.css']
})
export class CompletedTransactionComponent implements OnInit {

  transactionId: string;
  tkn2USD: number;
  tkn2Tbtc: number;
  tkn2Teth: number;
  tkn2Tltc: number;
  pageError: string;
  p1: number;
  finHistory: FinHistory[];

  constructor(private route: ActivatedRoute, private finHistoryService: FinHistoryService, private  router: Router, private tokenService: TokenService) {
    this.transactionId = this.route.snapshot.paramMap.get('transactionId');

  }

  ngOnInit() {
    this.finHistoryService.getAddressFinHistory(this.transactionId).subscribe((finHistory) => {
      this.finHistory = finHistory;
    }, err => {
      this.tokenService.processError('[GetAddressFinHistory]', err);
    });
  }

  downloadReceipt(transactionId: string) {
    this.finHistoryService.downloadReceipt(transactionId).subscribe((data: any) => {
      this.router.ngOnDestroy();
      window.location.href = environment.backendSiteURl + '/' + data.filename;
    }, err => {
      console.error(err);
    });
  }

}
