import {Component, OnInit} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {ConfigService} from '../../_service/config.service';
import {TokenService} from '../../_service/token.service';
import {NotificationService} from '../../_service/notification.service';
import {SocketIoConfig, Socket} from 'ng-socket-io';
import {Notification} from '../../_models/notification';
import {Observable} from 'rxjs/Observable';
import {Config} from '../../_models/config';
import {environment} from '../../../environments/environment';
import {FormGroup} from '@angular/forms';
import {FormBuilder, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../_service/user.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  host: {
    '(window:scroll)': 'updateHeader($event)'
  }
})


export class HeaderComponent implements OnInit {
  isScrolled = false;
  currPos: Number = 0;
  startPos: Number = 0;
  changePos: Number = 100;
  public socket: any = null;
  constructor(public configsService: ConfigService,
              public tokenService: TokenService,
              public ngxSmartModalService: NgxSmartModalService,
              public notificationService: NotificationService,
              private  userService: UserService,
              private router: Router
  ) {

  }
  fullName: string;
  balance: number;
  tknName: string;
  seenNotificationCountStatus: boolean;




  ngOnInit() {

    this.configsService.getConfig()
      .subscribe((config) => {
          if (config) {
            this.tknName = config.tknName;
          }
        }, (err) => {
          this.tokenService.processError('[GetConfig]', err);
        }
      );

    if (localStorage.getItem('_u')) {
      const userData = JSON.parse(localStorage.getItem('_u'));
      this.fullName = userData.fullName;
      this.balance = userData.balanceTKN;
    }


    this.userService.getUserBalances().subscribe((userData) => {
      if (userData) {
        this.balance = userData.balanceTKN;
      }
    });


    // this.socket.on("referralJoin",(data)=>{
    //   if(data) {
    //     const lst = this._u();
    //     console.log(lst)
    //     if(lst && lst.hasOwnProperty('hash'))
    //       if(lst.referralCode === data.referralCode)
    //         this.toastr.success(data.message,'Referral Joined')
    //   }
    // })

  }

  logout() {
    localStorage.removeItem('_u');
    localStorage.removeItem('token');
    localStorage.removeItem('setup');
  }


  _u() {
    if (localStorage.getItem('_u')) {
      return JSON.parse(localStorage.getItem('_u'));
    }

    return null;
  }


  seenNotification() {
    this.notificationService.updateNotification();
    this.seenNotificationCountStatus = true;
  }

  updateHeader(evt) {
    this.currPos = (window.pageYOffset || evt.target.scrollTop) - (evt.target.clientTop || 0);
    if(this.currPos >= this.changePos ) {
      this.isScrolled = true;
    } else {
      this.isScrolled = false;
    }
  }

  gotoKYCPage() {
    if (this.router.url === '/profile') {
      this.ngxSmartModalService.getModal('myModal').close();
    } else {
      this.router.navigate(['/profile']);
    }
  }

}
