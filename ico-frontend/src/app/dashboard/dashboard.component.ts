import {Component, OnInit} from '@angular/core';
import {UserService} from '../_service/user.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',

})
export class DashboardComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  acceptTermsAndCondition() {
    //this.userService.setTermsStatus();
    this.router.navigate(['/login']);
  }
}
