import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {TokenService} from '../../_service/token.service';
import {ConfigService} from '../../_service/config.service';
import {environment} from '../../../environments/environment';
import {AmChartsService, AmChart} from '@amcharts/amcharts3-angular';
import {ToastrService} from 'ngx-toastr';
import {ExchangeRateService} from '../../_service/exchange-rate.service';
import {CoinService} from '../../_service/coin.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public options: any;
  private chart2: AmChart;
  private timer: any;
  availableTokens: number;
  soldPercentage: number;
  tokenName: string;
  referralLink: string;
  referralTKN: string;
  purchasedTKN: string;
  balanceTKN: string;
  generatedAddressText: string;
  isAddressGenerated: boolean = false;
  generatedAddressAmount: string;
  clickedButton: string = 'BTC';
  tkn2USD: number;
  tkn2Tbtc: number;
  tkn2Teth: number;
  tkn2Tltc: number;
  tokenAmount: number;
  coins = 0;
  tkn = 0;
  isICOEnd: boolean = false;
  isICOPaused: boolean = false;
  IcoMSG: string = '';

  @ViewChild('ICOMessage') private ICOMessage: SwalComponent;


  constructor(public configsService: ConfigService,
              private tokenService: TokenService,
              private AmCharts: AmChartsService,
              private toastr: ToastrService,
              private router: Router,
              private exchangeRateService: ExchangeRateService,
              private coinService: CoinService) {
  }

  ngOnInit() {
    this.options = this.makeOptions(this.makeRandomDataProvider());
    // Create chartdiv2
    this.chart2 = this.AmCharts.makeChart('chartdiv', this.makeOptions(this.makeRandomDataProvider()));

    this.timer = setInterval(() => {
      // Update chartdiv1
      this.options = this.makeOptions(this.makeRandomDataProvider());
    }, 3000);
    this.configsService.getConfig()
      .subscribe((config) => {
          if (config) {
            this.isICOEnd = config.isIcoEnd;
            this.isICOPaused = config.isIcoPaused;
           // this.tkn2USD = (config['tkn2USD'] || 1).toFixed(2); // #1 till config is not updated
            this.tokenEconomics(config.availableTokens, config.soldTokens, config.tknName);
          }
        }, (err) => {
          this.tokenService.processError('[GetConfig]', err);
        }
      );

    if (localStorage.getItem('_u')) {
      const userData = JSON.parse(localStorage.getItem('_u'));
      this.referralLink = environment.SITE_URL + '/sign-up?rhash=' + userData.referralCode;

      // update balances on screen
      // TODO: Balances should also be immediatedly changed during db updates using sockets
      this.referralTKN = userData.referralTKN.toFixed(8);
      this.purchasedTKN = userData.purchasedTKN.toFixed(8);
      this.balanceTKN = userData.balanceTKN.toFixed(8);
    }
    // this.loadScript();

    this.exchangeRateService.getExchangeRates().subscribe((exchangeRate) => {
      if (exchangeRate) {
        for (const exchange of exchangeRate) {
          if (exchange.currencyIdFirstAbbr === 'tkn' && exchange.currencyIdSecondAbbr === 'tbtc') {
            this.tkn2Tbtc = exchange.exchangeRate;
          }
          if (exchange.currencyIdFirstAbbr === 'tkn' && exchange.currencyIdSecondAbbr === 'teth') {
            this.tkn2Teth = exchange.exchangeRate;
          }
          if (exchange.currencyIdFirstAbbr === 'tkn' && exchange.currencyIdSecondAbbr === 'tltc') {
            this.tkn2Tltc = exchange.exchangeRate;
          }
        }
      }
      // unset generated address step
      if (localStorage.getItem('generatedAddress')) {
        localStorage.removeItem('generatedAddress');
      }
    });

    this.exchangeRateService.getTknInUSD().subscribe((tknInUSD) => {
      if (tknInUSD) {
        this.tkn2USD = tknInUSD.tknInUSD;
      }
    });

  }

  /*public loadScript() {
    const userData = JSON.parse(localStorage.getItem('_u'));
    const APP_ID = 'wsxa0rpl';
    const current_user_email = userData.email;
    const current_user_name = userData.fullName;

    const node = document.createElement('script');
    node.type = 'text/javascript';
    node.text = 'window.intercomSettings={app_id:"' + APP_ID + '", name:"' + current_user_name + '", email:"' + current_user_email + '"}, (function(){function t(){var t=a.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://widget.intercom.io/widget/wsxa0rpl";var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,n=e.Intercom;if("function"==typeof n)n("reattach_activator"),n("update",intercomSettings);else{var a=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Intercom=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}})();';
    document.getElementsByTagName('body')[0].appendChild(node);
  }*/

  tokenEconomics(totalSupply: number, totalSold: number, tokenName: string) {
    this.soldPercentage = parseInt(String(((totalSold / totalSupply) * 100)));
    this.availableTokens = totalSupply;
    this.tokenName = tokenName;
  }


  makeRandomDataProvider() {
    const dataProvider = [];

    // Generate random data
    for (let year = 1950; year <= 2005; ++year) {
      dataProvider.push({
        year: '' + year,
        value: Math.floor(Math.random() * 100) - 50
      });
    }

    return dataProvider;
  }

  makeOptions(dataProvider) {
    return {
      'type': 'serial',
      'theme': 'light',
      'dataProvider': [{
        'count': 'JAN 2018 - FEB 2018',
        'count-value': 0.0000025
      }, {
        'count': 'JAN 2018 - FEB 2018',
        'count-value': 0.00000195
      }, {
        'count': 'JAN 2018 - FEB 2018',
        'count-value': 0.0000015
      }, {
        'count': 'JAN 2018 - FEB 2018',
        'count-value': 0.0000013
      }, {
        'count': 'JAN 2018 - FEB 2018',
        'count-value': 0.0000012
      }],
      'valueAxes': [{
        'gridColor': '#ed1e79',
        'gridAlpha': 0.3,
        'dashLength': 0
      }],
      'gridAboveGraphs': true,
      'startDuration': 1,
      'graphs': [{
        'balloonText': '[[category]]: <b>[[value]]</b>',
        'fillAlphas': 0.4,
        'fillColor': '#93278f',
        'lineAlpha': 0.2,
        'type': 'column',
        'valueField': 'count-value'
      }],
      'chartCursor': {
        'categoryBalloonEnabled': false,
        'cursorAlpha': 0,
        'zoomable': false
      },
      'categoryField': 'count',
      'categoryAxis': {
        'gridPosition': 'start',
        'gridAlpha': 0,
        'tickPosition': 'start',
        'tickLength': 20
      },
      'export': {
        'enabled': false
      }
    };
  }

  textCopied() {
    this.toastr.success('Successfully Copied!', 'Referral Link!');
  }

  moveOnAddressPage(state) {
    if (this.isICOEnd) {
      this.IcoMSG = 'ICO has been ended';
      setTimeout(() => {
        this.ICOMessage.show();
      }, 100);
    } else if (this.isICOPaused) {
      this.IcoMSG = 'ICO has been paused for a while';
      setTimeout(() => {
        this.ICOMessage.show();
      }, 100);
    } else {
      this.router.navigate([state]);
    }
  }

}
