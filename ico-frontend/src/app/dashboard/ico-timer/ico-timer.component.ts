import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../../_service/config.service';
import {TokenService} from '../../_service/token.service';

@Component({
  selector: 'app-ico-timer',
  templateUrl: './ico-timer.component.html',
  styleUrls: ['./ico-timer.component.css']
})
export class IcoTimerComponent implements OnInit {
  HOURS: string;
  MINUTES: string;
  SECONDS: string;
  DAYS: string;
  HOURS_2: string;
  MINUTES_2: string;
  SECONDS_2: string;
  DAYS_2: string;
  endDateInterval: any;
  timerMessage: string;

  constructor(public configsService: ConfigService,
              private tokenService: TokenService,) {

  }

  ngOnInit() {

    this.configsService.getConfig()
      .subscribe((config) => {
          if (config) {
            this.timerMessage = config['timerMessage'] || "TIME";
            this.installDayLeft(new Date(config.endDate));
          }
        }, (err) => {
          this.tokenService.processError('[GetConfig]', err);
        }
      );
  }

  installDayLeft(endDate: Date) {
    const diff = endDate.getTime() - Date.now();
    let days = Math.floor(diff / (60 * 60 * 24 * 1000));
    let hours, minutes, seconds;
    if (days >= 0) {
      hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
      minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
      seconds = Math.floor(diff / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));
    } else {
      days = 0;
      hours = 0;
      minutes = 0;
      seconds = 0;
    }

    let r_days, r_days_2, r_hours, r_hours_2, r_minutes, r_minutes_2, r_seconds, r_seconds_2: string;

    if (seconds < 10) {
      r_seconds_2 = '' + seconds;
      r_seconds = '0';
    } else {
      r_seconds = '' + Math.floor(seconds / 10);
      r_seconds_2 = '' + seconds % 10;
    }

    if (hours < 10) {
      r_hours_2 = '' + hours;
      r_hours = '0';
    } else {
      r_hours = '' + Math.floor(hours / 10);
      r_hours_2 = '' + hours % 10;
    }
    if (minutes < 10) {
      r_minutes_2 = '' + minutes;
      r_minutes = '0';
    } else {
      r_minutes = '' + Math.floor(minutes / 10);
      r_minutes_2 = '' + minutes % 10;
    }
    if (days < 10) {
      r_days_2 = '' + days;
      r_days = '0';
    } else {
      r_days = '' + Math.floor(days / 10);
      r_days_2 = days % 10;
    }

    if (days < 0) {
      r_days = '0';
      r_days_2 = '0';
    }

    if ((seconds === 0) && (minutes === 0) && (hours === 0) && (days === 0)) {
      //  console.log('PREITOTIME All zero');
      this.DAYS = '0';
      this.HOURS = '0';
      this.MINUTES = '0';
      this.SECONDS = '0';
      this.DAYS_2 = '0';
      this.HOURS_2 = '0';
      this.MINUTES_2 = '0';
      this.SECONDS_2 = '0';
    } else {
      //  console.log('PREITOTIME Not zero');
      this.DAYS = r_days;
      this.HOURS = r_hours;
      this.MINUTES = r_minutes;
      this.SECONDS = r_seconds;
      this.DAYS_2 = r_days_2;
      this.HOURS_2 = r_hours_2;
      this.MINUTES_2 = r_minutes_2;
      this.SECONDS_2 = r_seconds_2;
    }


    clearInterval(this.endDateInterval);
    this.endDateInterval = setInterval(() => {
      this.installDayLeft(endDate);
    }, 1000);
  }
}
