import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../environments/environment';
import {ReferralService} from '../../_service/referral.service';


@Component({
  selector: 'app-referral',
  templateUrl: './referral.component.html',
  styleUrls: ['./referral.component.css']
})

export class ReferralComponent implements OnInit {
  constructor(private toastr: ToastrService, private referralService: ReferralService) {
  }

  p1 = 1;
  p2 = 1;
  itemsPerPage: number;
  referralLink: string;
  bannerSourceCode: string;
  bHeight = 90;
  bWidth = 728;
  bannerSource = '/assets/images/ads-banner-01.jpg';
  referralList = [];

  ngOnInit() {
    if (localStorage.getItem('_u')) {
      const userData = JSON.parse(localStorage.getItem('_u'));
      this.referralLink = environment.SITE_URL + '/sign-up?rhash=' + userData.referralCode;
    }
    this.createBannerSorceCode();
    this.referralService.getList().subscribe((refList) => {
      if (refList) {
        this.referralList = refList.sort(function (a, b) {
          return +new Date(b.timestamp) - +new Date(a.timestamp);
        });
      }
    });
     }

  selectBannerSize(val) {
    switch (val) {

      case '1':
        this.bHeight = 90;
        this.bWidth = 728;
        this.bannerSource = '/assets/images/ads-banner-01.jpg';
        break;
      case '2':
        this.bHeight = 300;
        this.bWidth = 400;
        this.bannerSource = '/assets/images/ads-banner-01.jpg';
        break;
      case '3':
        this.bHeight = 600;
        this.bWidth = 1200;
        this.bannerSource = '/assets/images/ads-banner-01.jpg';
        break;
      case '4':
        this.bHeight = 400;
        this.bWidth = 400;
        this.bannerSource = '/assets/images/ads-banner-01.jpg';
        break;
      default:
        this.bHeight = 90;
        this.bWidth = 970;
        this.bannerSource = '/assets/images/ads-banner-01.png';
    }
  }

  textCopied() {
    this.toastr.success('Successfully Copied!', 'Referral Link!');
  }

  getSourceCode() {
    this.createBannerSorceCode();
    this.toastr.success('Successfully Copied!', 'Source Code!');
  }

  createBannerSorceCode() {
    this.bannerSourceCode = '<a href="' + this.referralLink + '" target="_blank">' + '<img src="' + environment.SITE_URL + this.bannerSource + '" height="' + this.bHeight + '" width="' + this.bWidth + '"> </a>';

  }
}
