import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TokenService } from '../../_service/token.service';
import { SupportMailService } from '../../_service/support.service';
import { SwalComponent } from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {


  supportTicketForm: FormGroup;
  supportMailStatus: string; // START, PROCESSING, COMPLETED
  supportMessage: string;

  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(private formBuilder: FormBuilder, private tokenService: TokenService, private supportMailService: SupportMailService) { }

  ngOnInit() {

    this.supportTicketForm = this.formBuilder.group({
      supportIssue: this.formBuilder.control(null, [Validators.required]),
      supportDesc: this.formBuilder.control(null, [Validators.required]),
    });

  }

  submitSupportMail() {
    if (this.supportTicketForm.valid) {
      this.supportMailStatus = 'PROCESSING';
      this.supportMailService.sendSupportMail(this.tokenService.email(), this.supportTicketForm.value)
        .subscribe((data) => {
          this.supportMailStatus = 'COMPLETED';
          this.supportTicketForm.reset();
          this.clearMsg();
          this.successSwal.show()
        }, (err) => {
          this.supportMailStatus = 'COMPLETED';
          this.supportMessage = err.msg;
          this.errorSwal.show();
          this.clearMsg();
        });
    }else{
      this.supportMessage = "Please fill all the details";
      setTimeout(() => {
        this.errorSwal.show();
      }, 300);
    }
  }

  clearMsg() {
    setTimeout(() => {
      this.supportMailStatus = "START";
    }, 3000);
  }

}
