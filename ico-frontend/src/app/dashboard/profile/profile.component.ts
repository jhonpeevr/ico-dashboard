import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {AuthService} from '../../_service/auth.service';
import {UserService} from '../../_service/user.service';
import {TwoFactorAuthService} from '../../_service/two-factor-auth.service';
import {TokenService} from '../../_service/token.service';
import {CoinService} from '../../_service/coin.service';
import {IMyDpOptions} from 'mydatepicker';
import {Observable} from 'rxjs/Observable';
import {Country} from '../../_models/country';
import {CountriesService} from '../../_service/countries.service';
import {KYCService} from '../../_service/kyc.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'mm/dd/yyyy',
    yearSelector: true,
    disableSince: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()}
  };

  countries: Observable<Country[]>;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  pageError: string;
  name: string;
  email: string;
  ethereumAddress: string;
  passwordForm: FormGroup;
  kycForm: FormGroup;

  passwordFormStatus: string; // START, PROCESSING, COMPLETED
  passwordFormMessage: string;
  KYCErrorMessage: string;
  disableError: any;

  @ViewChild('passportImage') passportImage;
  @ViewChild('utilityImage') utilityImage;
  passportCheck = false;
  utilityCheck = false;
  passportValid = false;
  utilityValid = false;
  status: string = null;
  sizeLimit = false;
  size = 10485760;
  imageType = false;
  imagesTypes = ['image/jpeg', 'image/png'];
  kycSubmitted = false;
  enterKYC = false;
  kycPageStatus = 'START'; // START, PROCESSING, COMPLETED*/
  error: string = null;

  loading = true;

  //2FA
  qrcode: any;
  twoFactorEnabled: any;
  twofaBtnDisabled = false;
  twofaError: string;

  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('ethSwal') private ethSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;
  @ViewChild('errorKYCSwal') private errorKYCSwal: SwalComponent;
  @ViewChild('successKYCSwal') private successKYCSwal: SwalComponent;
  @ViewChild('twoFASwal') private twoFASwal: SwalComponent;
  @ViewChild('enableSucces2FASwal') private enableSucces2FASwal: SwalComponent;
  @ViewChild('enableError2FASwal') private enableError2FASwal: SwalComponent;
  @ViewChild('disableSucces2FASwal') private disableSucces2FASwal: SwalComponent;
  @ViewChild('disableError2FASwal') private disableError2FASwal: SwalComponent;


  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private twoFactorAuthService: TwoFactorAuthService,
              private authService: AuthService,
              private tokenService: TokenService,
              private coinService: CoinService,
              private countriesService: CountriesService,
              private kycService: KYCService) {
  }

  ngOnInit() {
    var file = document.getElementById('files');
    file.addEventListener('change', function() { document.getElementById('files').classList.add('classToBeAdded'); });
    var file = document.getElementById('files1');
    file.addEventListener('change', function() { document.getElementById('files1').classList.add('classToBeAdded'); });
    if (localStorage.getItem('_u')) {
      const userData = JSON.parse(localStorage.getItem('_u'));
      this.name = userData.fullName;
      this.email = userData.email;
      this.ethereumAddress = userData.ethAddress ? userData.ethAddress : '';
    }

    this.passwordForm = this.formBuilder.group({
      oldPassword: this.formBuilder.control(null, [Validators.required]),
      newPassword: this.formBuilder.control(null, [Validators.required]),
    });

    this.twoFactorAuthService.check2FA().subscribe(
      (response) => {
        this.twoFactorEnabled = response.msg !== 'Two factor authentication is disabled';
        this.generate2FA();
      },
      (err) => {
        this.tokenService.processError('check2FA', err);
      }
    );
    this.kycForm = this.formBuilder.group({
      name: this.formBuilder.control(null, [Validators.required, Validators.pattern(/^[a-zA-Z ]*$/)]),
      address: this.formBuilder.control(null, Validators.required),
      contactno: this.formBuilder.control(null, [Validators.required, Validators.pattern(/^[0-9]*$/)]),
      country: this.formBuilder.control(null, [Validators.required]),
      dateOfBirth: this.formBuilder.control(null, Validators.required),
      passportNumber: this.formBuilder.control(null, Validators.required)
    });
    this.countries = this.countriesService.getAllCountries();
    setTimeout(() => {
      this.getKYC();
    }, 300);
  }


  submitNewPassword() {
    if (!this.twoFactorEnabled) {
      if (this.passwordForm.valid) {
        this.passwordFormStatus = 'PROCESSING';
        this.userService.modifyPassword(this.passwordForm.value)
          .subscribe((data) => {
            this.passwordFormStatus = 'COMPLETED';
            this.passwordForm.reset();
            this.clearMsg();
            this.successSwal.show();
          }, (err) => {
            this.passwordFormStatus = 'COMPLETED';
            this.passwordFormMessage = err.error.err;
            setTimeout(() => {
              this.errorSwal.show();
            }, 300);
            this.clearMsg();
          });
      } else {
        this.passwordFormMessage = 'Please both the sections';
        setTimeout(() => {
          this.errorSwal.show();
        }, 300);
      }
    } else {
      document.getElementById('id01').style.display = 'block';
    }
  }

  modifyPasswordwith2FA(code) {
      if (this.passwordForm.valid) {
        document.getElementById('id01').style.display = 'none';
        (document.getElementById('resetPassword') as HTMLInputElement).value = '';
        this.passwordFormStatus = 'PROCESSING';
        this.userService.modifyPasswordwith2FA(this.passwordForm.value, code, this.twoFactorEnabled)
          .subscribe((data) => {
            (document.getElementById('resetPassword') as HTMLInputElement).value = '';
            this.passwordFormStatus = 'COMPLETED';
            this.passwordForm.reset();
            this.clearMsg();
            this.successSwal.show();
          }, (err) => {
            (document.getElementById('resetPassword') as HTMLInputElement).value = '';
            this.passwordFormStatus = 'COMPLETED';
            this.passwordFormMessage = err.error.err;
            setTimeout(() => {
              this.errorSwal.show();
            }, 300);
            this.clearMsg();
          });
      } else {
        this.passwordFormMessage = 'Please both the sections';
        (document.getElementById('resetPassword') as HTMLInputElement).value = '';
        setTimeout(() => {
          this.errorSwal.show();
        }, 300);
      }
  }


  clearMsg() {
    setTimeout(() => {
      this.passwordFormStatus = 'START';
    }, 3000);
  }

  generate2FA() {
    this.twofaError = null;
    if (!this.twoFactorEnabled) {
      this.twoFactorAuthService.generate2FA().subscribe(
        (qrcode) => {
          this.qrcode = qrcode;
        }, (err) => {
          this.tokenService.processError('[Get2FA]', err);
        }
      );
    }
  }

  confirm2FA(code: string): Boolean {
    this.twofaBtnDisabled = true;
    this.twoFactorAuthService.confirm2FA(this.qrcode.secret, code).subscribe(
      () => {
        this.twoFactorEnabled = true;
        this.twofaBtnDisabled = false;
        this.enableSucces2FASwal.show();
        this.generate2FA();
      }, (err) => {
        this.twofaError = err.error.err;
        this.twofaBtnDisabled = false;
        this.enableError2FASwal.show();
      });

    return false;
  }

  disable2FA(code: string): Boolean {
    this.twofaBtnDisabled = true;
    this.twoFactorAuthService.disable2FA(code).subscribe(
      () => {
        this.twoFactorEnabled = false;
        this.twofaBtnDisabled = false;
        this.disableSucces2FASwal.show();
      }, (err) => {
        this.twofaError = err.error.err;
        this.disableError = err.error.err;
        this.twofaBtnDisabled = false;
        this.disableError2FASwal.show();
      }
    );
    return false;
  }

  saveAddress() {
    if (!this.twoFactorEnabled) {
      if (this.ethereumAddress.length == 42) {
        this.coinService.saveAddressForWithdrawal(this.ethereumAddress)
          .subscribe((data) => {
            //update token
            let userData = JSON.parse(localStorage.getItem('_u'));
            userData.ethAddress = this.ethereumAddress;
            localStorage.setItem('_u', JSON.stringify(userData));
            //show message
            this.ethSwal.show();
          }, (error) => {
            this.passwordFormMessage = error.err;
            setTimeout(() => {
              this.errorSwal.show();
            }, 300);
            this.tokenService.processError('[SAVEAddress]', error);
          });
      } else {
        this.passwordFormMessage = 'Please enter valid ethereum address';
        setTimeout(() => {
          this.errorSwal.show();
        }, 300);
      }
    } else {
      document.getElementById('walletPopup').style.display = 'block';
    }
  }

  saveAddresswith2FA(code) {
    document.getElementById('walletPopup').style.display = 'none';
    (document.getElementById('walletAddress') as HTMLInputElement).value = '';
      if (this.ethereumAddress.length == 42) {
        this.coinService.saveAddressForWithdrawalwith2FA(this.ethereumAddress, code, this.twoFactorEnabled)
          .subscribe((data) => {
            //update token
            let userData = JSON.parse(localStorage.getItem('_u'));
            userData.ethAddress = this.ethereumAddress;
            localStorage.setItem('_u', JSON.stringify(userData));
            //show message
            this.ethSwal.show();
          }, (error) => {
            (document.getElementById('walletAddress') as HTMLInputElement).value = '';
            this.passwordFormMessage = error.error.err;
            setTimeout(() => {
              this.errorSwal.show();
            }, 300);
            this.tokenService.processError('[SAVEAddress]', error);
          });
      } else {
        this.passwordFormMessage = 'Please enter valid ethereum address';
        (document.getElementById('walletAddress') as HTMLInputElement).value = '';
        setTimeout(() => {
          this.errorSwal.show();
        }, 300);
      }
  }

  submit() {
    if (!this.twoFactorEnabled) {
      this.kycPageStatus = 'PROCESSING';
      this.kycForm.patchValue({
        dateOfBirth: this.kycForm.value.dateOfBirth.formatted
      });
      this.kycService.saveKYC(this.kycForm.value, this.email, this.passportImage.nativeElement.files[0], this.utilityImage.nativeElement.files[0])
        .subscribe((msg) => {
          this.successKYCSwal.show();
          this.kycService.pullKyc();
          this.kycForm.markAsUntouched();
          this.kycPageStatus = 'COMPLETED';
          this.kycSubmitted = true;
        }, (err) => {
          this.KYCErrorMessage = err.error.err;
          setTimeout(() => {
            this.errorKYCSwal.show();
          }, 300);
          this.kycPageStatus = 'START';
          this.kycForm.reset();
          this.passportImage.nativeElement.value = '';
          this.utilityImage.nativeElement.value = '';
        });
    } else {
      document.getElementById('kycPopup').style.display = 'block';
      // this.twoFASwal.show();
    }
  }

  submitwith2FA(code) {
    document.getElementById('kycPopup').style.display = 'none';
    (document.getElementById('kycData') as HTMLInputElement).value = '';
      this.kycPageStatus = 'PROCESSING';
      this.kycForm.patchValue({
        dateOfBirth: this.kycForm.value.dateOfBirth.formatted
      });
      this.kycService.saveKYCwith2FA(this.kycForm.value, this.email, code, this.twoFactorEnabled, this.passportImage.nativeElement.files[0], this.utilityImage.nativeElement.files[0])
        .subscribe((msg) => {
          (document.getElementById('kycData') as HTMLInputElement).value = '';
          this.successKYCSwal.show();
          this.kycService.pullKyc();
          this.kycForm.markAsUntouched();
          this.kycPageStatus = 'COMPLETED';
          this.kycSubmitted = true;
        }, (err) => {
          (document.getElementById('kycData') as HTMLInputElement).value = '';
          this.KYCErrorMessage = err.error.err;
          setTimeout(() => {
            this.errorKYCSwal.show();
          }, 300);
          this.kycPageStatus = 'START';
          this.kycForm.reset();
          this.passportImage.nativeElement.value = '';
          this.utilityImage.nativeElement.value = '';
        });
  }

  // getCountries() {
  //   this.authService.getAllCountriesData().subscribe((message) => {
  //
  //     console.log(  message )
  //    this.countries = message;
  //   }, (err) => {
  //     this.pageError = err.error.err;
  //     this.pageStatus = 'START';
  //   });
  // }

  validatePassport() {
    const imageSize = this.passportImage.nativeElement.files[0].size;
    const imageType = this.passportImage.nativeElement.files[0].type;
    if (imageSize > this.size) {
      this.imageType = false;
      this.sizeLimit = true;
      this.passportImage.nativeElement.value = '';
    } else if (this.imagesTypes.indexOf(imageType) === -1) {
      this.sizeLimit = false;
      this.imageType = true;
      this.passportImage.nativeElement.value = '';
    } else {
      this.sizeLimit = false;
      this.imageType = false;
      this.passportValid = true;
    }
  }

  validateUtility() {
    const imageSize = this.utilityImage.nativeElement.files[0].size;
    const imageType = this.utilityImage.nativeElement.files[0].type;
    if (imageSize > this.size) {
      this.imageType = false;
      this.sizeLimit = true;
      this.utilityImage.nativeElement.value = '';
    } else if (this.imagesTypes.indexOf(imageType) === -1) {
      this.sizeLimit = false;
      this.imageType = true;
      this.utilityImage.nativeElement.value = '';
    } else {
      this.sizeLimit = false;
      this.imageType = false;
      this.utilityValid = true;
    }
  }

  getKYC() {
    this.kycService.getKYC().subscribe((kyc) => {
      this.status = kyc.status;
      kyc.passportImage ? kyc.passport = true : kyc.passport = false;
      kyc.utilityImage ? kyc.utility = true : kyc.utility = false;
      const dob = new Date(kyc.dateOfBirth);
      switch (kyc.status) {
        case 'pending':
        case 'confirmed':
          this.enterKYC = true;
          this.kycForm.patchValue({
            name: kyc.name,
            address: kyc.address,
            country: kyc.countryId._id,
            contactno: kyc.contactno,
            dateOfBirth: {
              date: {
                year: dob.getFullYear(),
                month: dob.getMonth() + 1,
                day: dob.getDate()
              }
            },
            passportNumber: kyc.passportNumber
          });
          this.kycForm.disable();
          this.passportCheck = kyc.passport;
          this.utilityCheck = kyc.utility;
          break;
        case 'declined':
          this.enterKYC = false;
          this.kycForm.patchValue({
            name: kyc.name,
            address: kyc.address,
            country: kyc.countryId._id,
            contactno: kyc.contactno,
            dateOfBirth: {
              year: dob.getFullYear(),
              month: dob.getMonth() + 1,
              day: dob.getDate()
            },
            passportNumber: kyc.passportNumber
          });
          this.passportCheck = false;
          this.utilityCheck = false;
          break;
        default:
          this.enterKYC = false;
      }
    }, (err) => {
      this.error = err;
      this.loading = false;
    });
  }
}
