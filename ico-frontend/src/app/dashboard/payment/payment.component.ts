import {Component, OnInit, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ExchangeRateService} from '../../_service/exchange-rate.service';
import {ConfigService} from '../../_service/config.service';
import {TokenService} from '../../_service/token.service';
import {CoinService} from '../../_service/coin.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {LoaderComponent} from '../../loader/loader.component';
import { MathematicsService } from '../../_service/mathematics.service';

@Component({
  providers: [LoaderComponent ],
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  coinType: string;
  coinBonus: number;
  tkn2Tbtc: number;
  tkn2Teth: number;
  tkn2Tltc: number;
  tokenAmount = 0;
  minInvest = 1;
  PAGE = 'START';
  coins = 0;
  tkn = 0;
  tokenName: string;
  afterBonusAmount: number = 0;
  afterBonusCoins: number;
  public pageError: string;
  allowedCurrency = ['BTC', 'ETH', 'LTC'];
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(private router: Router,
              private activeRouter: ActivatedRoute,
              private exchangeRateService: ExchangeRateService,
              private tokenService: TokenService,
              public configsService: ConfigService,
              private coinService: CoinService,
              private loaderComponent: LoaderComponent,
              private maths: MathematicsService) {

    this.coinType = this.activeRouter.snapshot.paramMap.get('coinType');
    if (this.allowedCurrency.indexOf(this.coinType) === -1) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.exchangeRateService.getExchangeRates().subscribe((exchangeRate) => {
      if (exchangeRate) {
        for (const exchange of exchangeRate) {
          if (exchange.currencyIdFirstAbbr === 'tkn' && exchange.currencyIdSecondAbbr === 'tbtc') {
            this.tkn2Tbtc = exchange.exchangeRate;
          }
          if (exchange.currencyIdFirstAbbr === 'tkn' && exchange.currencyIdSecondAbbr === 'teth') {
            this.tkn2Teth = exchange.exchangeRate;
          }
          if (exchange.currencyIdFirstAbbr === 'tkn' && exchange.currencyIdSecondAbbr === 'tltc') {
            this.tkn2Tltc = exchange.exchangeRate;
          }
        }
      }

    });
    this.configsService.getConfig()
      .subscribe((config) => {
          if (config) {
            this.tokenName = config.tknName;
            this.minInvest = config['minInvest'] || 1;
          }
        }, (err) => {
          this.tokenService.processError('[GetConfig]', err);
        }
      );

    this.exchangeRateService.getBonusPercentage().subscribe((bonusPercent) => {
      if (bonusPercent) {
        this.coinBonus = bonusPercent.bonusPercentage;
      }
    });
  }


  calculateCoins() {
    if (this.coinType === 'BTC') {
      if (this.coinBonus > 0) {
        this.afterBonusAmount = this.maths.toFixed(this.maths.safeAdd(this.maths.safeDivide(this.coins,this.tkn2Tbtc),(this.maths.safeMultiply(this.maths.safeDivide(this.coins,this.tkn2Tbtc),this.maths.safeDivide(this.coinBonus, 100)))),8);
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins,this.tkn2Tbtc),8);
      } else {
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Tbtc),8);
        this.afterBonusAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Tbtc),8);
      }
    } else if (this.coinType === 'ETH') {
      if (this.coinBonus > 0) {
        this.afterBonusAmount = this.maths.toFixed(this.maths.safeAdd(this.maths.safeDivide(this.coins,this.tkn2Teth),(this.maths.safeMultiply(this.maths.safeDivide(this.coins,this.tkn2Teth),this.maths.safeDivide(this.coinBonus, 100)))),8);
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Teth),8);
      } else {
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Teth),8);
        this.afterBonusAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Teth),8);
      }
    } else if (this.coinType === 'LTC') {
      if (this.coinBonus > 0) {
        this.afterBonusAmount = this.maths.toFixed(this.maths.safeAdd(this.maths.safeDivide(this.coins,this.tkn2Tltc),(this.maths.safeMultiply(this.maths.safeDivide(this.coins,this.tkn2Tltc),this.maths.safeDivide(this.coinBonus, 100)))),8);
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Tltc),8);
      } else {
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Tltc),8);
        this.afterBonusAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Tltc),8);
      }
    }
    this.tkn = this.afterBonusAmount;
   
  }

  calculateTokens() {

    this.afterBonusAmount = this.tkn;
    if (this.coinType === 'BTC') {
        this.coins = this.maths.toFixed(this.maths.safeDivide(this.maths.safeMultiply(this.maths.safeMultiply(this.tkn,this.tkn2Tbtc),100),this.maths.safeAdd(100,this.coinBonus)),8);
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins,this.tkn2Tbtc),8);
    } else if (this.coinType === 'ETH') {
        this.coins = this.maths.toFixed(this.maths.safeDivide(this.maths.safeMultiply(this.maths.safeMultiply(this.tkn,this.tkn2Teth),100),this.maths.safeAdd(100,this.coinBonus)),8);
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Teth),8);
    } else if (this.coinType === 'LTC') {
        this.coins = this.maths.toFixed(this.maths.safeDivide(this.maths.safeMultiply(this.maths.safeMultiply(this.tkn,this.tkn2Tltc),100),this.maths.safeAdd(100,this.coinBonus)),8);
        this.tokenAmount = this.maths.toFixed(this.maths.safeDivide(this.coins , this.tkn2Tltc),8);
    }
  }

  generateAddress(): void {
    this.loaderComponent.startLoader();
    // this.pageStatus = this.CRYPTO_PROCESSING;
    if (this.tokenAmount < this.minInvest) {
      this.loaderComponent.stopLoader();
      this.pageError = 'Minimum investment of ' + this.minInvest + ' is required';
      if (this.pageError !== '') {
        setTimeout(() => {
          this.errorSwal.show();
        }, 200);
      }
    } else {
      this.PAGE = 'PROCESSING';
      this.coinService.generateNewAddress(this.coins, 't' + this.coinType.toLowerCase(), this.tokenAmount)
        .subscribe((coinAddresses) => {
          this.coinService.updateCoinAddressObservable();
          this.loaderComponent.stopLoader();
          this.PAGE = 'START';
          this.coinService.setAddressStore(coinAddresses);
          this.router.navigate(['/generated-address']);
        }, (error) => {
          this.loaderComponent.stopLoader();
          this.pageError = error.error.err;
          this.tokenService.processError('[GenerateAddress]', error);
          if (this.pageError !== '') {
            setTimeout(() => {
              this.errorSwal.show();
            }, 200);
          }
        });
    }
  }
}
