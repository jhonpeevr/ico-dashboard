import {Component, OnInit, ViewChild} from '@angular/core';
import {FinHistoryService} from '../../_service/fin-history.service';
import {ConfigService} from '../../_service/config.service';
import {TokenService} from '../../_service/token.service';
import {CoinService} from '../../_service/coin.service';
import {Router} from '@angular/router';
import {UserService} from '../../_service/user.service';
import {environment} from '../../../environments/environment';
import * as moment from 'moment';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
})
export class TransactionComponent implements OnInit {
  transaction: any[] = [];
  withdraw: any[] = [];
  duration: any;

  p1 = 1;
  p2 = 1;
  addressExpirationTime: number;
  referralTKN: string;
  balanceTKN: string;
  tknName: string;
  withdrawalStatus: string;
  isAddressExpire: any = [];

  HOURS: any = [];
  MINUTES: any = [];
  SECONDS: any = [];
  DAYS: any = [];
  HOURS_2: any = [];
  MINUTES_2: any = [];
  SECONDS_2: any = [];
  DAYS_2: any = [];
  endDateInterval: any = [];
  timerMessage: string;

  @ViewChild('ICOSwal') private ICOSwal: SwalComponent;

  constructor(public finHistoryService: FinHistoryService,
              public configService: ConfigService,
              private tokenService: TokenService,
              private coinService: CoinService,
              private router: Router,
              private userService: UserService,
  ) {
  }

  ngOnInit() {

    this.configService.getConfig()
      .subscribe((config) => {
          if (config) {
            this.withdrawalStatus = config.withdrawalEnabled;
            this.tknName = config.tknName;
            this.addressExpirationTime = config.ethAddressExpirationTime;
          }
        }, (err) => {
          this.tokenService.processError('[GetConfig]', err);
        }
      );

    this.finHistoryService.getWithdrawals()
      .subscribe((withdraw) => {
        this.withdraw = withdraw;
      }, (err) => {
        this.tokenService.processError('[GetWithdrawals]', err);
      });




    if (localStorage.getItem('_u')) {
      const userData = JSON.parse(localStorage.getItem('_u'));
      // update balances on screen
      // TODO: Balances should also be immediatedly changed during db updates using sockets
      this.referralTKN = userData.referralTKN.toFixed(8);
      this.balanceTKN = userData.balanceTKN.toFixed(8);
    }

    this.userService.getUserBalances().subscribe((userData) => {
      if(userData){
        this.balanceTKN = userData.balanceTKN.toFixed(8);
        this.referralTKN = userData.referralTKN.toFixed(8);
      }
    });

    // this.coinService.updateCoinAddressObservable();
    this.coinService.getCoinAddresses()
      .subscribe((coinAddresses) => {
        const todayDate = moment(new Date());
        if (coinAddresses ) {
          coinAddresses.map((data, i) => {
            this.isAddressExpire[i] = false;
            this.duration = moment.duration(todayDate.diff(data.timestamp));
            const diffInSec = this.duration.asSeconds();
            if (diffInSec >= this.addressExpirationTime) {
              this.isAddressExpire[i] = true;
            }
            this.installDayLeft(new Date(data.timestamp), i );
          });
          this.transaction = coinAddresses;
        }
      }, (err) => {
        this.tokenService.processError('[GetCoinDetails]', err);
      });

  }

  processedTransaction(transactionId: string) {
    this.router.navigate(['/completed-transaction/' + transactionId]);
  }

  pendingTransaction(transactionId: string, index: number) {
    //localStorage.setItem('generatedAddress', JSON.stringify(this.transaction[index]));
    this.coinService.setAddressStore(this.transaction[index]);
    this.router.navigate(['/generated-address']);
  }

  installDayLeft(endDate: Date, i) {

    const diff =  endDate.getTime() + (this.addressExpirationTime) - Date.now();
    let days = Math.floor(diff / (60 * 60 * 24 * 1000));
    let hours, minutes, seconds;
    if (days >= 0) {
      hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
      minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
      seconds = Math.floor(diff / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));
    } else {
      days = 0;
      hours = 0;
      minutes = 0;
      seconds = 0;
    }

    let r_days, r_days_2, r_hours, r_hours_2, r_minutes, r_minutes_2, r_seconds, r_seconds_2: string;

    if (seconds < 10) {
      r_seconds_2 = '' + seconds;
      r_seconds = '0';
    } else {
      r_seconds = '' + Math.floor(seconds / 10);
      r_seconds_2 = '' + seconds % 10;
    }

    if (hours < 10) {
      r_hours_2 = '' + hours;
      r_hours = '0';
    } else {
      r_hours = '' + Math.floor(hours / 10);
      r_hours_2 = '' + hours % 10;
    }
    if (minutes < 10) {
      r_minutes_2 = '' + minutes;
      r_minutes = '0';
    } else {
      r_minutes = '' + Math.floor(minutes / 10);
      r_minutes_2 = '' + minutes % 10;
    }
    if (days < 10) {
      r_days_2 = '' + days;
      r_days = '0';
    } else {
      r_days = '' + Math.floor(days / 10);
      r_days_2 = days % 10;
    }

    if (days < 0) {
      r_days = '0';
      r_days_2 = '0';
    }

    if ((seconds === 0) && (minutes === 0) && (hours === 0) && (days === 0)) {
      this.DAYS[i] = '0';
      this.HOURS[i] = '0';
      this.MINUTES[i] = '0';
      this.SECONDS[i] = '0';
      this.DAYS_2[i] = '0';
      this.HOURS_2[i] = '0';
      this.MINUTES_2[i] = '0';
      this.SECONDS_2[i] = '0';
      this.isAddressExpire[i] = true;
    } else {
      this.DAYS[i] = r_days;
      this.HOURS[i] = r_hours;
      this.MINUTES[i] = r_minutes;
      this.SECONDS[i] = r_seconds;
      this.DAYS_2[i] = r_days_2;
      this.HOURS_2[i] = r_hours_2;
      this.MINUTES_2[i] = r_minutes_2;
      this.SECONDS_2[i] = r_seconds_2;
    }



    clearInterval(this.endDateInterval[i]);

    this.endDateInterval[i] = setInterval(() => {
      this.installDayLeft(endDate, i);
    }, 1000);
  }

  expireMessage() {
    setTimeout(() => {
      this.ICOSwal.show();
    }, 50);
  }

}
