import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../_service/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  isMailVerification: boolean;
  isMailVerificationClass: string;
  email: string;

  constructor(private router: Router,
              private authService: AuthService) {
  }

  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  pageError: string;

  ngOnInit() {
    if (localStorage.getItem('setup') === 'done') {
      this.router.navigate(['/home']);
    }

    const userData = JSON.parse(localStorage.getItem('_u'));
    this.email = userData.email;
    this.isMailVerification = userData.isMailVerification;
    this.isMailVerificationClass = (this.isMailVerification) ? 'active' : '';

    // this.loadScript();
    // this.getUserDetails();
  }

  /* public loadScript() {
    const userData = JSON.parse(localStorage.getItem('_u'));
    const APP_ID = 'wsxa0rpl';
    const current_user_email = userData.email;
    const current_user_name = userData.fullName;
    const node = document.createElement('script');
    node.type = 'text/javascript';
    node.text = 'window.intercomSettings={app_id:"' + APP_ID + '", name:"' + current_user_name + '", email:"' + current_user_email + '"}, (function(){function t(){var t=a.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://widget.intercom.io/widget/wsxa0rpl";var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,n=e.Intercom;if("function"==typeof n)n("reattach_activator"),n("update",intercomSettings);else{var a=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Intercom=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}})();';
    document.getElementsByTagName('body')[0].appendChild(node);
  } */

  skip() {
    localStorage.setItem('setup', 'done');
    this.router.navigate(['/home']);
  }

  mailVerification(data) {
    if (this.isMailVerification) {
      this.isMailVerification = false;
      this.isMailVerificationClass = 'active';
    } else {
      this.isMailVerification = true;
      this.isMailVerificationClass = '';
    }
    this.authService.eMailVerification(this.email, this.isMailVerification).subscribe((message) => {
      const userData = JSON.parse(localStorage.getItem('_u'));
      userData.isMailVerification = message['isMailVerification'];
      localStorage.setItem('_u', JSON.stringify(userData));
    }, (err) => {
      this.pageError = err.error.err;
      this.pageStatus = 'START';
    });
  }

}
