import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../_service/notification.service';
import {TokenService} from '../../_service/token.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {

  constructor(public notificationService: NotificationService, private tokenService: TokenService) {
  }

  seenNotificationCountStatus: boolean;
  feeds: any[] = [];
  error: string = null;
  p1 = 1;
  p2 = 1;
  itemsPerPage: number;
  notifications: any[] = [];

  ngOnInit() {
    this.notificationService.getAllFeeds()
      .subscribe((feed) => {
        if (feed) {
          this.feeds = feed.sort(function (a, b) {
            return +new Date(b.timestamp) - +new Date(a.timestamp);
          });
        }

      }, (err) => {
        this.tokenService.processError('[GetAddressFinHistory]', err);
        this.error = err;

      });

    this.notificationService.getNotifications()
      .subscribe((notifications) => {
        if (notifications) {
          this.notifications = notifications;
        }

      }, (err) => {
        this.tokenService.processError('[GetAddressFinHistory]', err);
        this.error = err;
      });
  }

  seenNotification() {
    this.notificationService.updateNotification();
    this.seenNotificationCountStatus = true;
  }
}
