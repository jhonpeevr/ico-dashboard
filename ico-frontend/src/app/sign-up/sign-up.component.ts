import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService as AuthServiceInternal} from '../_service/auth.service';
import {UserService} from '../_service/user.service';
import {Router} from '@angular/router';
import {AuthService as AuthServiceSocial, FacebookLoginProvider, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {ActivatedRoute, Params} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TokenService} from '../_service/token.service';
import {Observable} from 'rxjs/Observable';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})


export class SignUpComponent implements OnInit {

  googleCaptchaResponseLogin: string;
  googleCaptchaResponseSignup: string;
  googleCaptchaCheck: boolean;
  pageError: string;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  private user: SocialUser;
  private loggedIn: boolean;
  signupForm: FormGroup;
  public referralCode: string = null;
  signupSubmit: boolean = true;
  emailActivationCode: string;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;
  @ViewChild('acknowledgeSwal') private acknowledgeSwal: SwalComponent;

  constructor(private userService: UserService,
              private router: Router,
              private authServiceSocial: AuthServiceSocial,
              private authServiceInternal: AuthServiceInternal,
              private tokenService: TokenService,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,) {
  }

  ngOnInit() {

    if (localStorage.getItem('_u')) {
      this.router.navigate(['/account']);
    } else {
      this.authServiceSocial.authState.subscribe((user) => {
        if (user) {
          this.signOut();
          this.authServiceInternal.signUpSocial(user, this.referralCode)
            .subscribe((userData) => {
                localStorage.setItem('_u', JSON.stringify(userData));
                localStorage.setItem('token', userData['token']);
                localStorage.setItem('kycStatus', userData['kycStatus']);
                localStorage.setItem('setup', 'init');
                localStorage.removeItem('referralCode');
                this.pageStatus = 'COMPLETED';

                if(userData['isNew']){
                  this.router.navigate(['/account']);
                }else{
                  this.router.navigate(['/home']);
                }

              },
              (err) => {

                this.pageError = err.error.err;
                this.pageStatus = 'START';
                if (this.pageError != '') {
                  setTimeout(() => {
                    this.errorSwal.show();
                  }, 500);
                }
              });
        }

      });
    }


    this.signupForm = this.formBuilder.group({
      fullName: this.formBuilder.control(null,
        [Validators.required]),
      email: this.formBuilder.control(null,
        [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]),
      password: this.formBuilder.control(null,
        [Validators.required, Validators.minLength(6)]),
      confirmPassword: this.formBuilder.control(null,
        [Validators.required, Validators.minLength(6)]),
      terms: this.formBuilder.control(null),
      rhash: this.formBuilder.control(null)
    });

    // subscribing query string
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params['rhash']) {
        localStorage.setItem('referralCode', params['rhash']);
        this.signupForm.patchValue({rhash: params['rhash']});
      }
    });

    this.referralCode = localStorage.getItem('referralCode');

    this.emailActivationCode = this.randomString(128);

    // this.loadScript();
  }

  /*public loadScript() {
    const APP_ID = 'wsxa0rpl';
    const node = document.createElement('script');
    node.type = 'text/javascript';
    node.text = 'window.intercomSettings={app_id:"' + APP_ID + '"}, (function(){function t(){var t=a.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://widget.intercom.io/widget/wsxa0rpl";var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,n=e.Intercom;if("function"==typeof n)n("reattach_activator"),n("update",intercomSettings);else{var a=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Intercom=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}})();';
    document.getElementsByTagName('body')[0].appendChild(node);
  }*/

  submit() {

    if (this.signupForm.value.terms === 'yes') {
      if (this.signupForm.valid) {
        this.pageStatus = 'PROCESSING';
        this.authServiceInternal.signUp(this.signupForm.value, this.googleCaptchaResponseSignup, this.referralCode, this.emailActivationCode)
          .subscribe(
            () => {
              this.signupSubmit = false;
              const rand = this.randomString(25);
              // localStorage.setItem('rand', rand);
              localStorage.setItem('resendEmail', this.signupForm.value.email);
              localStorage.setItem('resendActivation', this.emailActivationCode);
              setTimeout(() => {
                this.pageStatus = 'START';
                this.successSwal.show();
                localStorage.removeItem('referralCode');
                this.signupForm.reset();
                grecaptcha.reset();
              }, 500);
            },
            (err) => {
              console.log(err);
              this.signupSubmit = true;
              this.pageError = err.error.err;
              this.pageStatus = 'START';
              if (this.pageError != '') {
                setTimeout(() => {
                  this.errorSwal.show();
                }, 500);
              }
            }
          );
      } else {
        this.pageError = 'Invalid input parameters';
      }
      return false;
    } else {
      this.acknowledgeSwal.show();
    }
  }


  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }


  resolvedSignup(captchaResponse: string) {
    this.googleCaptchaResponseSignup = captchaResponse;
    this.googleCaptchaCheck = true;
  }

  randomString(len, charSet = null) {
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
      var randomPoz = Math.floor(Math.random() * charSet.length);
      randomString += charSet.substring(randomPoz, randomPoz + 1);
    }
    return randomString;
  }


}
