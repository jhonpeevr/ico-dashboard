import { Country } from './country';

export class KYC {
  _id: number;
  userId: string;
  name: string;
  address: string;
  contactno: string;
  state: string;
  countryId: Country;
  status: string;
  timestmap: Date;
  passport: boolean;
  passportImage: string;
  utility: boolean;
  utilityImage: string;
  dateOfBirth: Date;
  SSN: string;
  passportNumber: string;
}
