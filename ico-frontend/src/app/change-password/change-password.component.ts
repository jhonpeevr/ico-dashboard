import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService as AuthServiceInternal, AuthService} from '../_service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService as AuthServiceSocial, FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';
import {UserService} from '../_service/user.service';
import {TokenService} from '../_service/token.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  pageError: string;
  googleCaptchaResponseChangePassword: string;
  googleCaptchaCheck: boolean;
  changePasswordSubmit: boolean = true;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;
  email: string;
  activationCode: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authServiceSocial: AuthServiceSocial,
              private authServiceInternal: AuthServiceInternal,
              private formBuilder: FormBuilder,
              private authService: AuthService) {

    this.email = this.route.snapshot.paramMap.get('email');
    this.activationCode = this.route.snapshot.paramMap.get('activationCode');

  }

  ngOnInit() {

    this.changePasswordForm = this.formBuilder.group({
      password: this.formBuilder.control(null,
        [Validators.required, Validators.minLength(6)]),
      confirmPassword: this.formBuilder.control(null,
        [Validators.required, Validators.minLength(6)]),
    });

    // this.loadScript();
  }

  /*public loadScript() {
    const APP_ID = 'wsxa0rpl';
    const node = document.createElement('script');
    node.type = 'text/javascript';
    node.text = 'window.intercomSettings={app_id:"' + APP_ID + '"}, (function(){function t(){var t=a.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://widget.intercom.io/widget/wsxa0rpl";var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,n=e.Intercom;if("function"==typeof n)n("reattach_activator"),n("update",intercomSettings);else{var a=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Intercom=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}})();';
    document.getElementsByTagName('body')[0].appendChild(node);
  }*/

  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }

  resolvedChangePassword(captchaResponse: string) {
    this.googleCaptchaResponseChangePassword = captchaResponse;
    this.googleCaptchaCheck = true;
  }

  submit() {
    if (this.changePasswordForm.valid) {
      this.pageStatus = 'PROCESSING';

      this.authService.changePassword(this.email, this.activationCode, this.changePasswordForm.controls['password'].value, this.googleCaptchaResponseChangePassword)
        .subscribe(
          () => {
            this.pageStatus = 'START';
            setTimeout(()=> {
              this.successSwal.show();
              this.changePasswordForm.reset();
              grecaptcha.reset();
            },500);
          },
          (err) => {
            this.pageError = err.error.err;
            this.pageStatus = 'START';
            if(this.pageError!=""){
              setTimeout(() => {
                this.errorSwal.show();
              }, 500);
            }
          }
        );

    } else {
      this.pageError = 'Invalid input parameters';
    }
    return false;
  }

}
