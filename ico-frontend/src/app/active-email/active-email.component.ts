import {Component, OnInit, ViewChild} from '@angular/core';
import { AuthService } from '../_service/auth.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
@Component({
  selector: 'app-active-email',
  templateUrl: './active-email.component.html',
  styleUrls: ['./active-email.component.css']
})


export class ActiveEmailComponent implements OnInit {

  constructor(private authService: AuthService,
  private route: ActivatedRoute,
              private router: Router
) { }

  activationErr: string;
  activationCompleted: boolean;
  accountAlreadyActivated: boolean;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  ngOnInit() {
    const email = this.route.snapshot.paramMap.get('email');
    const activationCode = this.route.snapshot.paramMap.get('activationCode');

    this.authService.activateEmail(email, activationCode).subscribe(
      (user) => {
        this.activationCompleted = true;
        setTimeout(() => {
          this.successSwal.show();
        }, 300);
        this.router.navigate(['/login']);
      }, (errResult) => {
        console.log(errResult)
        this.activationErr= errResult.error.err;
        if(this.activationErr!=""){
          setTimeout(() => {
            this.errorSwal.show();
            this.router.navigate(['/login']);
          }, 500);
        }
      }
    );
  }
}
