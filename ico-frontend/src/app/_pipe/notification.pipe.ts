import { Pipe, PipeTransform } from '@angular/core';
import { Notification} from '../_models/notification';

@Pipe({name: 'seenPipe'})
export class NotificationPipe implements PipeTransform {
  transform(notification:Notification[], lang: string[]): number {
    let count=0;
    for (let row of notification){
      if(!row.seen) count++;
    }
    //  console.log('count: ', count)
    return count;
  }
}
