import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService as AuthServiceInternal, AuthService} from '../_service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService as AuthServiceSocial, FacebookLoginProvider, GoogleLoginProvider} from 'angularx-social-login';
import {UserService} from '../_service/user.service';
import {TokenService} from '../_service/token.service';
import {ActivatedRoute, Router} from '@angular/router';
import {SwalComponent} from '@toverux/ngx-sweetalert2';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  pageStatus = 'LOADING'; // LOADING, START, PROCESSING, COMPLETED
  pageError: string;
  googleCaptchaResponseForgotPassword: string;
  googleCaptchaCheck: boolean;
  forgotPasswordSubmit: boolean = true;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(private router: Router,
              private authServiceSocial: AuthServiceSocial,
              private authServiceInternal: AuthServiceInternal,
              private formBuilder: FormBuilder,
              private authService: AuthService) { }

  ngOnInit() {

    this.forgotPasswordForm = this.formBuilder.group({
      email: this.formBuilder.control(null,
        [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]),
    });
    // this.loadScript();
  }

  /*public loadScript() {
    const APP_ID = 'wsxa0rpl';
    const node = document.createElement('script');
    node.type = 'text/javascript';
    node.text = 'window.intercomSettings={app_id:"' + APP_ID + '"}, (function(){function t(){var t=a.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://widget.intercom.io/widget/wsxa0rpl";var e=a.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}var e=window,n=e.Intercom;if("function"==typeof n)n("reattach_activator"),n("update",intercomSettings);else{var a=document,c=function(){c.c(arguments)};c.q=[],c.c=function(t){c.q.push(t)},e.Intercom=c,e.attachEvent?e.attachEvent("onload",t):e.addEventListener("load",t,!1)}})();';
    document.getElementsByTagName('body')[0].appendChild(node);
  }*/


  signInWithGoogle(): void {
    this.authServiceSocial.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authServiceSocial.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(): void {
    this.authServiceSocial.signOut();
  }

  resolvedForgotPassword(captchaResponse: string) {
    this.googleCaptchaResponseForgotPassword = captchaResponse;
    this.googleCaptchaCheck = true;
  }

  submit() {
    if (this.forgotPasswordForm.valid) {
      this.pageStatus = 'PROCESSING';

      this.authService.forgotPassword(this.forgotPasswordForm.controls['email'].value, this.googleCaptchaResponseForgotPassword)
        .subscribe(
          () => {
            this.pageStatus = 'START';
            setTimeout(()=> {
              this.successSwal.show();
              this.forgotPasswordForm.reset();
              grecaptcha.reset();
            },500);
          },
          (err) => {
            this.pageError = err.error.err;
            this.pageStatus = 'START';
            if(this.pageError!=""){
              setTimeout(() => {
                this.errorSwal.show();
              }, 500);
            }
          }
        );

    } else {
      this.pageError = 'Invalid input parameters';
    }
    return false;
  }

}
