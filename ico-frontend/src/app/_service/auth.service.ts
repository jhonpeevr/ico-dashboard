import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {environment} from '../../environments/environment';
import {Message} from '../_models/message';
import {User} from '../_models/user';
import {Observable} from 'rxjs/Observable';
import {TokenService} from './token.service';
import {HttpClient} from '@angular/common/http';
import {Country} from '../_models/country';

@Injectable()
export class AuthService {
  private static activateEmailURL = environment.apiURL + '/auth/activate/';
  private static signupURL = environment.apiURL + '/auth/sign-up';
  private static authURL = environment.apiURL + '/auth';
  private static changePasswordURL = environment.apiURL + '/auth/change-password';
  private static forgotPasswordURL = environment.apiURL + '/auth/forgot-password';
  private static facebookAuthURL = environment.apiURL + '/auth/facebook';
  private static googleAuthURL = environment.apiURL + '/auth/google';
  private static validate2Fa = environment.apiURL + '/auth/validate-twofa';
  private static emailVerification = environment.apiURL + '/auth/email-verifivation';
  private static countryURL = environment.apiURL + '/countries';

  constructor(private http: HttpClient
              ) {
  }

  static logout() {

    localStorage.removeItem('_u');
    localStorage.removeItem('token');
    localStorage.removeItem('referralCode');
    localStorage.removeItem('twofaCheck');
    localStorage.removeItem('resendActivation');
    localStorage.removeItem('resendEmail');
  }

  getAuthStatus(): string {
    return TokenService.token();
  }

  activateEmail(email: string, activationCode: string): Observable<User> {
    return this.http.post<User>(AuthService.activateEmailURL + email + '/' + activationCode, null);
  }


  signUp(data, googleCaptchaCheckCode: string, referralCode: string, emailActivationCode: string): Observable<Message> {
    const postBody = {
      fullName: data.fullName,
      email: data.email,
      password: data.password,
      referralCode: referralCode,
      googleCaptchaCheckCode: googleCaptchaCheckCode,
      userType: 'user',
      emailActivationCode: emailActivationCode,
    };

    return this.http.post<Message>(AuthService.signupURL, postBody);
  }

  login(data): Observable<User> {
    return this.http.post<User>(AuthService.authURL, data);
  }

  changePassword(email: string, activationCode: string, password: string, googleCaptchaResponseChangePass: string) {
    return this.http.post(AuthService.changePasswordURL, {email, activationCode, password, googleCaptchaResponseChangePass});
  }

  forgotPassword(email: string, googleCaptchaResponseForgotPass: string): Observable<Message> {
    return this.http.post<Message>(AuthService.forgotPasswordURL, {email, googleCaptchaResponseForgotPass});
  }



  signUpSocial(data, referralCode): Observable<Message> {

    const postBody = {
      access_token: data.authToken,
      profile: data,
      referralCode: referralCode
    };

    if (data.provider === 'FACEBOOK') {
      return this.http.post<Message>(AuthService.facebookAuthURL, postBody);
    } else if (data.provider === 'GOOGLE') {
      return this.http.post<Message>(AuthService.googleAuthURL, postBody);
    }


  }

  socialSignout(){

  }

  validateTwoFa(code: string) {
    return this.http.post<Message>(AuthService.validate2Fa, {code: code}, TokenService.jwt());
  }

  eMailVerification(email: string, isMailVerification: boolean) {
    return this.http.post<Message>(AuthService.emailVerification, {email, isMailVerification}, TokenService.jwt());
  }

  getAllCountriesData() {
    return this.http.get<Country>(AuthService.countryURL,  TokenService.jwt());
  }
}
