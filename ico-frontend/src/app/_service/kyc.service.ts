import { Http, Response } from '@angular/http';
import { TokenService } from './token.service';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { KYC } from '../_models/kyc';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Message } from '../_models/message';
import {AuthService} from './auth.service';
import {Country} from '../_models/country';
import {CountriesService} from './countries.service';
import {Config} from '../_models/config';

@Injectable()
export class KYCService {
  private static kycURL = environment.apiURL + '/kyc';
  private static kycEnabledURL = environment.apiURL + '/kyc/enabled';
  public kycEnabled$: BehaviorSubject<boolean>;
  kycEnabled: boolean;
  KYC$: BehaviorSubject<KYC> = new BehaviorSubject(null);

  constructor(private http: HttpClient,
              private tokenService: TokenService) {
    if (!this.kycEnabled$) {
      this.kycEnabled$ = new BehaviorSubject(null);
      this.http.get<KYC>(KYCService.kycEnabledURL, TokenService.jwt())
        .subscribe((kycEnabled) => {
        // this.kycEnabled$ = kycEnabled;
        // this.kycEnabled$.next(kycEnabled);
      }, (err) => {
        this.tokenService.processError('[GetKYCEnabled]', err);
      });
    }

    this.http.get<KYC>(KYCService.kycURL, TokenService.jwt())
      .subscribe(
        (KYCData) => {
         this.KYC$.next(KYCData);
        }, (err) => {
          this.tokenService.processError('[GetKYC]', err);
        }
      );

    /*this.tokenService.socket.on('kyc-status-updated-' + TokenService.hash(), (kycStatus) => {
      localStorage.setItem('kycStatus', kycStatus);
    });*/
  }

  saveKYC(kyc, email, passportImage, utilityImage): Observable<Message> {
    // if (!this.kycEnabled) {
    //   return ;
    // }
    const data = new FormData();
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type':  'application/form-data',
    //   })
    // };
    // headers.append( 'Content-Type',  'application/form-data');
    data.append('passportImage', passportImage);
    data.append('utilityImage', utilityImage);
    data.append('email', email);
    data.append('kyc', JSON.stringify(kyc));
    const _u = JSON.parse(localStorage.getItem('_u'));
      if (_u) {
        const httpOptions = {
          headers: new HttpHeaders({
            //'Content-Type':  'application/form-data',
            'x-access-token': _u.token
          })
        };
        return this.http.post<Message>( KYCService.kycURL, data, httpOptions );
      }

    /*return this.http.post(KYCService.kycURL, data, TokenService.jwt())
      .map((response: Response) => {
        return response.json() as Message;
      });*/
  }


  saveKYCwith2FA(kyc, email, code, twoFactorEnabled, passportImage, utilityImage): Observable<Message> {
    const data = new FormData();

    data.append('passportImage', passportImage);
    data.append('utilityImage', utilityImage);
    data.append('email', email);
    data.append('code', code);
    data.append('twoFactorEnabled', twoFactorEnabled);
    data.append('kyc', JSON.stringify(kyc));
    const _u = JSON.parse(localStorage.getItem('_u'));
    if (_u) {
      const httpOptions = {
        headers: new HttpHeaders({
          //'Content-Type':  'application/form-data',
          'x-access-token': _u.token
        })
      };
      return this.http.post<Message>( KYCService.kycURL, data, httpOptions );
    }
  }

  getKYC(): Observable<KYC> {
    /*if (!this.kycEnabled) {
      return new Observable(null);
    }*/
    return this.KYC$.asObservable().share();
  }

  pullKyc() {
    return this.http.get<KYC>(KYCService.kycURL, TokenService.jwt())
      .subscribe(
        (KYCData) => {
          this.KYC$.next(KYCData);
        }, (err) => {
          this.tokenService.processError('[GetKYC]', err);
        }
      );
  }

  getKYCEnabled(): Observable<boolean> {
    return this.kycEnabled$.asObservable();
  }
}
