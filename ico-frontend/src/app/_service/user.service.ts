import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from '../_models/user';
import {environment} from '../../environments/environment';
import {TokenService} from './token.service';
import {HttpClient} from '@angular/common/http';
import {Message} from '../_models/message';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Balances} from '../_models/balances';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserService {
  private static apiUrl = 'http://evareium.io/api/sendWhitepaper';
  private static saveUserInfoUrl = environment.apiURL + '/auth/save-info';
  private static kycUrl = environment.apiURL + '/kyc';
  private static subscribeUrl = environment.apiURL + '/auth/mailchimp';
  private static modifyPasswordURL = environment.apiURL + '/auth/modify-password';


  userBalances$: BehaviorSubject<Balances> = new BehaviorSubject(null);


  constructor(private http: HttpClient, private tokenService: TokenService) {


    this.tokenService.socket.on('balance-changed-' + TokenService.hash(), (userBalances) => {
      console.log('balance changed', userBalances);
      //update the local storage
      if (localStorage.getItem('_u')) {
        let userData = JSON.parse(localStorage.getItem('_u'));
        userData.balanceTKN = userBalances.balanceTKN;
        userData.referralTKN = userBalances.referralTKN;
        userData.purchasedTKN = userBalances.purchasedTKN;
        localStorage.setItem('_u', JSON.stringify(userData));
        this.userBalances$.next(userBalances);
      }
    });


  }


  getUserBalances(): Observable<Balances> {
    return this.userBalances$.asObservable();
  }





  // sendWhitePaper(body: object) {
  //   return this.http.post(UserService.saveUserInfoUrl, body)
  //     .map((response: Response) => {
  //       return response.json() as User;
  //     });
  // }

  // login(body: any) {
  //   return this.http.post(UserService.loginUrl, body).map((response: Response) => {
  //     const user: User = response.json() as User;
  //     if (user.token) {
  //       localStorage.setItem('_u', JSON.stringify(user));
  //       localStorage.setItem('token', user.token);
  //     }
  //     return user;
  //   });
  // }

  // saveKYC(kyc, passportFrontImage, passportBackImage) {
  //   const data = new FormData();
  //   data.append('passportFrontImage', passportFrontImage);
  //   data.append('passportBackImage', passportBackImage);
  //   data.append('kyc', JSON.stringify(kyc));
  //   console.log('KYC PROCEEDED');
  //   return this.http.post(UserService.kycUrl, data, TokenService.jwt())
  //     .map((response: Response) => {
  //       return response.json();
  //     });
  // }

  // subscribe(email: string) {
  //   return this.http.post(UserService.subscribeUrl, {email: email}).map((response: Response) => {
  //     return response.json();
  //   });
  // }

  // getTermsStatus() {
  //   if (localStorage.getItem('term') === 'accepted') {
  //     return true;
  //   }
  //   return null;
  // }

  // setTermsStatus() {
  //   return localStorage.setItem('term', 'accepted');
  // }

  modifyPassword(data) {
    return this.http.post<Message>(UserService.modifyPasswordURL, data, TokenService.jwt());
  }

  modifyPasswordwith2FA(data, code, twoFactorEnabled) {
    return this.http.post<Message>(UserService.modifyPasswordURL, {data, code, twoFactorEnabled}, TokenService.jwt());
  }

}
