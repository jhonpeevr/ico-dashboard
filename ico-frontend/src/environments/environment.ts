export const environment = {
  production: false,
  apiURL: 'http://localhost:3003/api/v1',
  backendSiteURl: 'http://localhost:3003',
  SITE_URL: 'http://localhost:4201',
  googleCaptchaSiteKey: '6LdPjU0UAAAAANFoOBPst45UaqQXZvjzDBZhgaRC',
  facebookAppId: '1759165314391903',
  googleAppId: '254540686204-j7573edv46lfd8gtt0ud69024f4mlggh.apps.googleusercontent.com',
  cryptoCompareURL: 'https://streamer.cryptocompare.com/',
  priceETHURL: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=BTC',
  socketURL: 'http://localhost:8123',
  addressExpiration: 172800,
};
