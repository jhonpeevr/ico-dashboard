export const environment = {
  production: true,
  apiURL: 'https://ico-backend.genesisx.com/api/v1',
  backendSiteURl: 'https://ico-backend.genesisx.com',
  SITE_URL: 'https://ico-backend.genesisx.com',
  googleCaptchaSiteKey: '6LdPjU0UAAAAANFoOBPst45UaqQXZvjzDBZhgaRC',
  facebookAppId: '1759165314391903',
  googleAppId: '896020748672-mjndlsrjmdfv0s4u2466jps5esebfud9.apps.googleusercontent.com',
  cryptoCompareURL: 'https://streamer.cryptocompare.com/',
  priceETHURL: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=BTC',
  socketURL: 'https://genesisx-backend.genesisx.com:8123',
  addressExpiration: 172800,
};
