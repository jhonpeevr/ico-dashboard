export const environment = {
  production: true,
  apiURL: 'http://localhost:3003/api/v1',
  backendSiteURl: 'http://localhost:3003',
  SITE_URL: 'http://localhost:4200',
  googleCaptchaSiteKey: '###',
  facebookAppId: '###',
  googleAppId: '###',
  cryptoCompareURL: 'https://streamer.cryptocompare.com/',
  priceBTCURL: 'https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD',
  priceETHURL: 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD',
  socketURL: 'http://localhost:8123',
};
