import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {TokenService} from './token.service';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {FinHistory} from '../_models/fin-history';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {InvestmentStatics} from '../_models/investmentStatics';
import {WithdrawalStatics} from '../_models/withdrawalStatics';


@Injectable()
export class FinHistoryService {
  private static FIN_HISTORY_LENGTH = 5;

  private static finHistoryInvestmentsURL = environment.apiURL + '/admin/finhistory/investments';
  private static finHistoryAllInvestmentsURL = environment.apiURL + '/admin/finhistory/investments/getallinvestment';
  private static finHistoryWithdrawalsURL = environment.apiURL + '/admin/finhistory/withdrawals';
  private static addressFinHistoryInvestmentsURL = environment.apiURL + '/finhistory/address-invest';
  private static downloadHistoryUrl = environment.apiURL + '/finhistory/generate-receipt';
  private static finHistoryInvestmentStatics = environment.apiURL + '/statistics/investments';
  private static finHistoryWithdrawalStatics = environment.apiURL + '/statistics/withdrawal';

  finHistoryInvestments$: BehaviorSubject<FinHistory[]>;
  finHistoryWithdrawals$: BehaviorSubject<FinHistory[]>;
  investmentStatics$: BehaviorSubject<InvestmentStatics>;
  withdrawalStatics$: BehaviorSubject<WithdrawalStatics>;

  private messageSource = new BehaviorSubject('EMPTY');
  currentMessage = this.messageSource.asObservable();

  constructor(private http: HttpClient,
              private tokenService: TokenService,
              private toastr: ToastrService) {
    if (!this.finHistoryInvestments$) {
      this.finHistoryInvestments$ = new BehaviorSubject(null);
      this.getInvestmentsPageRecord();
      /*this.http.get<FinHistory[]>(FinHistoryService.finHistoryInvestmentsURL, TokenService.jwt())
        .subscribe((finHistoryInvestments) => {
          console.log("finHistoryInvestments ", finHistoryInvestments);
          this.setInvestments(finHistoryInvestments);
        }, (error) => {
          this.tokenService.processError('[FinHistoryInvestments]', error);
        });*/
    }

    if (!this.finHistoryWithdrawals$) {
      this.finHistoryWithdrawals$ = new BehaviorSubject(null);

      this.http.get<FinHistory[]>(FinHistoryService.finHistoryWithdrawalsURL, TokenService.jwt())
        .subscribe((finHistoryWithdrawals) => {
          this.setWithdrawals(finHistoryWithdrawals);
        }, (error) => {
          this.tokenService.processError('FinHistoryWithdrawals', error);
        });
    }

    if (!this.investmentStatics$) {
      this.investmentStatics$ = new BehaviorSubject(null);

      this.http.get<InvestmentStatics>(FinHistoryService.finHistoryInvestmentStatics, TokenService.jwt())
        .subscribe((finHistoryInvestStatics) => {
          this.investmentStatics$.next(finHistoryInvestStatics);
        }, (error) => {
          this.tokenService.processError('FinHistoryInvestmentStatics', error);
        });
    }

    if (!this.withdrawalStatics$) {
      this.withdrawalStatics$ = new BehaviorSubject(null);
      this.http.get<WithdrawalStatics>(FinHistoryService.finHistoryWithdrawalStatics, TokenService.jwt())
        .subscribe((finHistoryWithdrawalStatics) => {
          this.withdrawalStatics$.next(finHistoryWithdrawalStatics);
        }, (error) => {
          this.tokenService.processError('FinHistoryInvestmentStatics', error);
        });
    }

    this.tokenService.socket.on('fh-invest-reload-' + TokenService.hash(), (finHistories) => {
      this.setInvestments(finHistories);
      this.messageSource.next('EMPTY');
      this.toastr.success('', 'Balance Updated');
    });

    this.tokenService.socket.on('fh-withdraw-reload-' + TokenService.hash(), (finHistoryInvestmentsPaginator) => {
      this.setWithdrawals(finHistoryInvestmentsPaginator);
      this.toastr.success('', 'Balance Updated');
    });

  }

  static fixPaginate(finHistories: FinHistory[]): FinHistory[] {
    if (finHistories && finHistories.length > 0) {
      const endValue = FinHistoryService.FIN_HISTORY_LENGTH -
        (finHistories.length % FinHistoryService.FIN_HISTORY_LENGTH);

      for (let i = 0; i < endValue; i++) {
        const f = {};
        f['amount'] = '-';
        f['currencyAbbr'] = '-';
        f['currencyName'] = '-';
        f['type'] = '-';
        f['exchangeRateValue'] = '-';
        f['timestamp'] = '-';
        finHistories.push((<FinHistory> f));
      }
    }

    return finHistories;
  }

  getInvestments(): Observable<FinHistory[]> {
    return this.finHistoryInvestments$.asObservable();
  }

  getAllInvestments(isDay: boolean, isMonth: boolean, isYear: boolean) {
    return this.http.get<any>(FinHistoryService.finHistoryAllInvestmentsURL + '?isDay=' + isDay + '&isMonth=' + isMonth + '&isYear=' + isYear, TokenService.jwt())
  }

  getInvestmentByDate(fromDate: string, toDate: string) {
    return this.http.get<any>(FinHistoryService.finHistoryAllInvestmentsURL + '?fromDate=' + fromDate + '&toDate=' + toDate , TokenService.jwt());
  }


  getWithdrawals(): Observable<FinHistory[]> {
    return this.finHistoryWithdrawals$.asObservable();
  }

  getStatistics(): Observable<InvestmentStatics> {
    return this.investmentStatics$.asObservable();
  }

  getWithdrawalStatistics(): Observable<WithdrawalStatics> {
    return this.withdrawalStatics$.asObservable();
  }

  setInvestments(finHistories: FinHistory[]) {
    if (finHistories) {
      finHistories = FinHistoryService.fixPaginate(finHistories);
      this.finHistoryInvestments$.next(finHistories);
    }
  }

  setWithdrawals(finHistories: FinHistory[]) {
    if (finHistories) {
      finHistories = FinHistoryService.fixPaginate(finHistories);
      this.finHistoryWithdrawals$.next(finHistories);
    }
  }

  getAddressFinHistory(address: string) {
    return this.http.get<FinHistory[]>(FinHistoryService.addressFinHistoryInvestmentsURL + '/' + address, TokenService.jwt());
  }

  downloadReceipt(transactionId: string) {
    const _u = JSON.parse(localStorage.getItem('_u'));
    return this.http.post(FinHistoryService.downloadHistoryUrl, {transactionId: transactionId}, TokenService.jwt())
      ;
  }

  getInvestmentsPageRecord(perPage: number = 10, page: number = 0, query = '') {
    if (!query) {
      query = '';
    }
    this.http.get<FinHistory[]>(FinHistoryService.finHistoryInvestmentsURL + '/' + perPage + '/' + page + '?query=' + query, TokenService.jwt())
      .subscribe((finhistory) => {
        this.finHistoryInvestments$.next(finhistory);
      }, (err) => {
        this.tokenService.processError('[FinHistoryInvestments]', err);
      });
  }

  getWithdrawalPageRecord(perPage: number = 10, page: number = 0, query = '') {
    if (!query) {
      query = '';
    }
    this.http.get<FinHistory[]>(FinHistoryService.finHistoryWithdrawalsURL + '/' + perPage + '/' + page + '?query=' + query, TokenService.jwt())
      .subscribe((finhistory) => {
        this.finHistoryWithdrawals$.next(finhistory);
      }, (err) => {
        this.tokenService.processError('[FinHistoryWithdrawal]', err);
      });
  }

}
