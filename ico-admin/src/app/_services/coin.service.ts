import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {TokenService} from './token.service';
import {environment} from '../../environments/environment';
import {CoinAddress} from '../_models/coinAddress';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpClient} from '@angular/common/http';
import { CoinStatistics } from '../_models/coinStatistics';


@Injectable()
export class CoinService {
  private static generatedAddressesURL = environment.apiURL + '/coin/generated-addresses/all';
  private static generatedAddressURL = environment.apiURL + '/coin/generated-address/';
  private static generateAddressURL = environment.apiURL + '/coin/generate-address';
  private static saveWithdrawalAddress = environment.apiURL + '/coin/save-withdrawal-address';
  private static coinStatisticsURL = environment.apiURL + '/statistics/coin';
  private static assignTokenURL = environment.apiURL + '/admin/assign-token';

  private coinAddresses$: BehaviorSubject<CoinAddress[]> = new BehaviorSubject(null);
  private coinAddress$: BehaviorSubject<CoinAddress> = new BehaviorSubject(null);
  private coinStatistics$: BehaviorSubject<CoinStatistics>;

  public addressStore: CoinAddress;

  constructor(private http: HttpClient,
              private tokenService: TokenService) {

    this.http.get<CoinAddress[]>(CoinService.generatedAddressesURL, TokenService.jwt())
      .subscribe((coinAddresses) => {
        this.coinAddresses$.next(coinAddresses);
      }, (err) => {
        this.tokenService.processError('[CoinAddresses]', err);
      });

    if (!this.coinStatistics$) {
      this.coinStatistics$ = new BehaviorSubject(null);
      this.getAllStatistics();
    }

    //TODO: need socket for stats

    // coin addresses status updated
    // this.tokenService.socket.on('generated-addresses-' + TokenService.hash(), (coinAddresses) => {
    //   console.log('coinAddresses-event', coinAddresses);
    //   this.coinAddresses$.next(coinAddresses);
    // });

  }

  saveAddressForWithdrawal(address: string) {
    return this.http.post(CoinService.saveWithdrawalAddress, {address}, TokenService.jwt());
  }

  saveAddressForWithdrawalwith2FA(address: string, code: string, twoFactorEnabled: boolean) {
    return this.http.post(CoinService.saveWithdrawalAddress, {address, code, twoFactorEnabled}, TokenService.jwt());
  }

  generateAddress(coinAmount: number, coinType: string, tknAmount: number) {
    return this.http.post<CoinAddress[]>(CoinService.generateAddressURL, {coinAmount, coinType, tknAmount}, TokenService.jwt())
      .subscribe((coinAddresses) => {
        this.coinAddresses$.next(coinAddresses);
      }, (error) => {
        this.tokenService.processError('[GenerateAddress]', error);
      });
  }

  getGeneratedAddress(address: string) {
    return this.http.get<CoinAddress>(CoinService.generatedAddressURL + address, TokenService.jwt())
      .subscribe((coinAddress) => {
        this.coinAddress$.next(coinAddress);
      }, (error) => {
        this.tokenService.processError('[GeneratedAddress]', error);
      });
  }


  generateNewAddress(coinAmount: number, coinType: string, tknAmount: number) {
    return this.http.post<CoinAddress[]>(CoinService.generateAddressURL, {coinAmount, coinType, tknAmount}, TokenService.jwt());

  }

  assignToken(id: string, tokens: number, bounsAmount: number) {
    return this.http.post<any>(CoinService.assignTokenURL, {id, tokens, bounsAmount}, TokenService.jwt());
  }

  getCoinAddresses(): Observable<CoinAddress[]> {
    return this.coinAddresses$.asObservable();
  }

  setAddressStore(data) {
    this.addressStore = data;
  }

  getAddressStore() {
    return this.addressStore;
  }

  updateCoinAddressObservable() {
    this.http.get<CoinAddress[]>(CoinService.generatedAddressesURL, TokenService.jwt())
      .subscribe((coinAddresses) => {
        this.coinAddresses$.next(coinAddresses);
      }, (err) => {
        this.tokenService.processError('[CoinAddresses]', err);
      });
  }

  getStatistics(): Observable<CoinStatistics> {
    return this.coinStatistics$.asObservable();
  }

  getAllStatistics() {
    this.http.get<CoinStatistics>(CoinService.coinStatisticsURL, TokenService.jwt())
      .subscribe((statistics) => {
        this.coinStatistics$.next(statistics);
      }, (err) => {
        this.tokenService.processError('[GetCoinStatistics]', err);
      }
    );
  }

}
