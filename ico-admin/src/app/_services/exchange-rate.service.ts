import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {TokenService} from './token.service';
import {environment} from '../../environments/environment';
import {ExchangeRate} from '../_models/exchange-rate';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import * as io from 'socket.io-client';
import {HttpClient} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {CurrencyUsd} from "../_models/currencyUsd";

@Injectable()
export class ExchangeRateService {
  private static exchangeRateURL = environment.apiURL + '/exchange-rate';
  private static bonusURL = environment.apiURL + '/coin/hook/bonus';
  private static currencyUsd  = environment.apiURL + '/exchange-rate/getRates';

  public socket: any = null;

  exchangeRates$: BehaviorSubject<ExchangeRate[]> = new BehaviorSubject(null);
  exchangeUsd$: BehaviorSubject<CurrencyUsd[]> = new BehaviorSubject(null);
  bonus$: BehaviorSubject<number> = new BehaviorSubject(null);
  exchangeETH_BTC$: BehaviorSubject<number> = new BehaviorSubject(null);
  exchangeETH_USD$: BehaviorSubject<number> = new BehaviorSubject(null);
  exchangeBTC_USD$: BehaviorSubject<number> = new BehaviorSubject(null);
  // exchangeLTC_BTC$: BehaviorSubject<number> = new BehaviorSubject(null);
  // exchangeXRP_BTC$: BehaviorSubject<number> = new BehaviorSubject(null);
  // exchangeBCH_BTC$: BehaviorSubject<number> = new BehaviorSubject(null);

  constructor(private http: HttpClient,
              private tokenService: TokenService,
             private toastr: ToastrService
              ) {

  //  exchange rates updated
    this.tokenService.socket.on('exchange-rates-updated', (exchangeRates) => {
      //console.log('balance-changed', exchangeRates);
     // this.toastr.success('Exchange Price  Update!', 'Done');
      this.exchangeRates$.next(exchangeRates);
    });

    this.tokenService.socket.on('exchange-usd-updated', (exchangeRates) => {
      //console.log('balance-changed', exchangeRates);
      // this.toastr.success('Exchange Price  Update!', 'Done');
      this.exchangeUsd$.next(exchangeRates);
    });

    /*this.socket = io(environment.cryptoCompareURL);
    const subscription = ['5~CCCAGG~ETH~BTC', '5~CCCAGG~ETH~USD', '5~CCCAGG~BTC~USD'];
    this.socket.emit('SubAdd', {subs: subscription});
    this.socket.on('m', (message) => {
      const sub = message.substring(0, 16);
      if (subscription.indexOf(sub) > 0 && message.charAt(17) !== '4') {
        const price = parseFloat(message.substring(19, message.indexOf('~', 19)));
        switch (sub) {
          case subscription[0]:
            this.exchangeETH_BTC$.next(price);
            break;
          case subscription[1]:
            this.exchangeETH_USD$.next(price);
            break;
          case subscription[2]:
            this.exchangeBTC_USD$.next(price);
            break;
          // case subscription[1]:
          //   this.exchangeLTC_BTC$.next(price);
          //   break;
          // case subscription[2]:
          //   this.exchangeXRP_BTC$.next(price);
          //   break;
          // case subscription[3]:
          //   this.exchangeBCH_BTC$.next(price);
          //   break;
        }
      }
    });*/

    this.http.get<ExchangeRate[]>(ExchangeRateService.exchangeRateURL, TokenService.jwt())
    // .map((response: Response) => {
    //   return response.json() as ExchangeRate[];
    // })
      .subscribe((exchangeRates) => {
        this.exchangeRates$.next(exchangeRates);
      }, (error) => {
        this.tokenService.processError('[ExchangeRates]', error);
      });

    this.http.get<CurrencyUsd[]>(ExchangeRateService.currencyUsd, TokenService.jwt())
      .subscribe((exchangeRates) => {
        this.exchangeUsd$.next(exchangeRates);
      }, (error) => {
        this.tokenService.processError('[ExchangeRates]', error);
      });
  }

  getExchangeRates(): Observable<ExchangeRate[]> {
    return this.exchangeRates$.asObservable();
  }

  getUsdRates(): Observable<CurrencyUsd[]> {
    return this.exchangeUsd$.asObservable();
  }

  getBonus(): Observable<number> {
    return this.bonus$.asObservable();
  }

  getExchangeETH_BTC(): Observable<number> {
    return this.exchangeETH_BTC$.asObservable();
  }


  getExchangeETH_USD(): Observable<number> {
    return this.exchangeETH_USD$.asObservable();
  }

  getExchangeBTC_USD(): Observable<number> {
    return this.exchangeBTC_USD$.asObservable();
  }

  /*getExchangeLTC_BTC(): Observable<number> {
    return this.exchangeLTC_BTC$.asObservable().share();
  }

  getExchangeXRP_BTC(): Observable<number> {
    return this.exchangeXRP_BTC$.asObservable().share();
  }

  getExchangeBCH_BTC(): Observable<number> {
    return this.exchangeBCH_BTC$.asObservable().share();
  }*/


}
