import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { TokenService } from './token.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Config } from '../_models/config';
import { HttpClient } from '@angular/common/http';
import {Configs} from "../_models/configs";

@Injectable()
export class ConfigService {
  private static configURL = environment.apiURL + '/config';
  private static configsURL = environment.apiURL + '/admin/configs';
  private static configUpdateURL = environment.apiURL + '/admin/configs';

  config$: BehaviorSubject<Config> = new BehaviorSubject(null);
  configs$: BehaviorSubject<Configs[]> = new BehaviorSubject(null);

  constructor(private http: HttpClient,
              private tokenService: TokenService) {
    this.http.get<Config>(ConfigService.configURL, TokenService.jwt())
      .subscribe(
        (config) => {
          this.config$.next(config);
        }, (err) => {
          this.tokenService.processError('[Configs]', err);
        }
      );

    this.http.get<Configs[]>(ConfigService.configsURL, TokenService.jwt())
      .subscribe(
        (configs) => {
          this.configs$.next(configs);
        }, (err) => {
          this.tokenService.processError('[Configs]', err);
        }
      );
    //config updated
    // this.tokenService.socket.on('config-changed', (config) => {
    //   this.config$.next(config);
    // });

  }

  getConfig(): Observable<Config> {
    return this.config$.asObservable();
  }
  getConfigs(): Observable<Configs[]> {
    return this.configs$.asObservable();
  }

  updateConfig(config): any {
    this.http.post<Configs>(ConfigService.configsURL, config, TokenService.jwt())
    .subscribe((newConfig) => {

        this.updateObservable(newConfig);

      }, (err) => {
        this.tokenService.processError('[ConfigUpdate]', err);
      }
    );
  }

  private updateObservable(newconfig: Configs) {
    const configs = [];
    this.configs$.getValue().forEach((oldConfig, index) => {
      let config = oldConfig;
      if (oldConfig._id === newconfig._id) {
        config = newconfig;
      }
      configs.push(config);
    });
    this.configs$.next(configs);
  }

}
