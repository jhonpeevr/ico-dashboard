import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from '../_models/user';
import {UserInfo} from '../_models/userinfo';
import {environment} from '../../environments/environment';
import {TokenService} from './token.service';
import {HttpClient} from '@angular/common/http';
import {Message} from '../_models/message';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Balances} from '../_models/balances';
import {Observable} from 'rxjs/Observable';
import {FinHistoryService} from "./fin-history.service";
import {FinHistory} from "../_models/fin-history";
import { UserStatistics } from '../_models/userStatistics';
import {CoinStatistics} from "../_models/coinStatistics";
import {CoinService} from "./coin.service";
import * as io from 'socket.io-client';

@Injectable()
export class UserService {
  private static apiUrl = 'http://evareium.io/api/sendWhitepaper';
  private static saveUserInfoUrl = environment.apiURL + '/auth/save-info';
  private static kycUrl = environment.apiURL + '/kyc';
  private static subscribeUrl = environment.apiURL + '/auth/mailchimp';
  private static modifyPasswordURL = environment.apiURL + '/auth/modify-password';
  private static getCountUrl = environment.apiURL + '/admin/user/count';
  private static usersURL = environment.apiURL + '/admin/user';
  private static userURL = environment.apiURL + '/admin/user/by-email';
  private static userinfoURL = environment.apiURL + '/admin/user/user-info';
  private static userinfoCountURL = environment.apiURL + '/admin/user/user-info/count';
  private static userInvestmentCountURL = environment.apiURL + '/admin/finhistory/userinvestments/count';
  private static userStatisticsURL = environment.apiURL + '/statistics/user';
  private static userUpdateURL = environment.apiURL + '/admin/user/update';
  private static userBlockURL = environment.apiURL + '/admin/user/block';
  private static userActivateURL = environment.apiURL + '/admin/user/activate';
  private static userinvestmentURL = environment.apiURL + '/admin/finhistory/userinvestments';
  private static useraddressURL = environment.apiURL + '/admin/coin/generated-user-addresses';
  private static userAddressCountURL = environment.apiURL + '/admin/coin/generated-user-addresses/count';

  private users$: BehaviorSubject<User[]>;
  private usersStatistics$: BehaviorSubject<UserStatistics>;

  userBalances$: BehaviorSubject<Balances> = new BehaviorSubject(null);
  userCount$: BehaviorSubject<any> = new BehaviorSubject(null);
  // userInfot$: BehaviorSubject<UserInfo> = new BehaviorSubject(null);
  public socket: any = null;


  constructor(private http: HttpClient, private tokenService: TokenService) {

    this.tokenService.socket.on('user-token-updated', (user) => {
      this.updateObservable(user);
    });

    if (!this.users$) {
      this.users$ = new BehaviorSubject(null);
      this.getUsers();
    }
    if (!this.usersStatistics$) {
      this.usersStatistics$ = new BehaviorSubject(null);
      this.getAllStatistics();
    }

    this.http.get<any>(UserService.getCountUrl, TokenService.jwt())
      .subscribe((count) => {
        this.userCount$.next(count.count);
      }, (error) => {
        this.tokenService.processError('[USERCOUNTERROR]', error);
      });

    this.tokenService.socket.on('balance-changed-' + TokenService.hash(), (userBalances) => {
      console.log('balance changed', userBalances);
      //update the local storage
      if (localStorage.getItem('_u')) {
        let userData = JSON.parse(localStorage.getItem('_u'));
        userData.balanceTKN = userBalances.balanceTKN;
        userData.referralTKN = userBalances.referralTKN;
        userData.purchasedTKN = userBalances.purchasedTKN;
        localStorage.setItem('_u', JSON.stringify(userData));
        this.userBalances$.next(userBalances);
      }
    });

    this.tokenService.socket.on('user-count-changed', (count) => {
      this.userCount$.next(count);
    });


  }

  getUsers(perPage: number = 10, page: number = 0, query = '') {
    if (!query) query = '';
    this.http.get<User[]>(UserService.usersURL + '/' + perPage + '/' + page + '?query=' + query, TokenService.jwt())
      .subscribe((users) => {
        this.users$.next(users);
      }, (err) => {
        this.tokenService.processError('[GetUsers]', err);
      }
    );
  }

  getAllStatistics() {
    this.http.get<UserStatistics>(UserService.userStatisticsURL, TokenService.jwt())
      .subscribe((statistics) => {
          this.usersStatistics$.next(statistics);
        }, (err) => {
          this.tokenService.processError('[GetCoinStatistics]', err);
        }
      );
  }

  getAllUsers(): Observable<User[]> {
    return this.users$.asObservable();
  }

  getAllUsersForExports() {
    return this.http.get<User[]>(UserService.usersURL, TokenService.jwt())
  }

  getSearchRecordToExport(query = '') {
    return this.http.get<User[]>(UserService.usersURL + '?query=' + query, TokenService.jwt())
  }


  getUserByID(email: string) {
    return this.http.get<User>(UserService.userURL + '/' + email, TokenService.jwt());
  }

  getUserInfoByID(id: number, perPage: number = 10, page: number = 0) {
    return this.http.get<any>(UserService.userinfoURL + '/' + id + '/' + perPage + '/' + page, TokenService.jwt());
  }

  getUserInfoCount(id: number) {
    return this.http.get<any>(UserService.userinfoCountURL + '/' + id, TokenService.jwt());
  }

  getUserInvestmentCount(id: string) {
    return this.http.get<any>(UserService.userInvestmentCountURL + '/' + id, TokenService.jwt());
  }
  getUserAddressCount(id: string) {
    return this.http.get<any>(UserService.userAddressCountURL + '/' + id, TokenService.jwt());
  }
  getInvestmentByID(id: string, perPage: number = 10, page: number = 0) {
    return this.http.get<any>(UserService.userinvestmentURL + '/'+ id + '/' + perPage + '/' + page, TokenService.jwt())
  }

  getGeneratedAddressByID(id: string, perPage: number = 10, page: number = 0) {
    return this.http.get<any>(UserService.useraddressURL + '/'+ id + '/' + perPage + '/' + page, TokenService.jwt())
  }

  updateUser(user) {
    this.http.post<User>(UserService.userUpdateURL, user, TokenService.jwt())
      .subscribe((user) => {
      this.updateObservable(user);
    }, (err) => {
      this.tokenService.processError('[UpdateUser]', err);
    });
  }

  blockorActivateUser(_id: string, isUserStatus: boolean) {
    this.http.post<User>(UserService.userBlockURL, {userId: _id, isUserStatus: isUserStatus}, TokenService.jwt())
      .subscribe((user) => {
        this.updateObservable(user);
      }, (err) => {
        this.tokenService.processError('[BlockUser]', err);
      });
  }


  blockUser(_id: string) {
    this.http.post<User>(UserService.userBlockURL, {userId: _id}, TokenService.jwt())
      .subscribe((user) => {
      this.updateObservable(user);
    }, (err) => {
      this.tokenService.processError('[BlockUser]', err);
    });
  }

  activateUser(_id: string) {
    this.http.post<User>(UserService.userActivateURL, {userId: _id}, TokenService.jwt())
      .subscribe((user) => {
      this.updateObservable(user);
    }, (err) => {
      this.tokenService.processError('[ActivateUser]', err);
    });
  }

  getStatistics(): Observable<UserStatistics> {
    return this.usersStatistics$.asObservable();
  }


  getUserBalances(): Observable<Balances> {
    return this.userBalances$.asObservable();
  }

  getUserCount(): Observable<any> {
    return this.userCount$.asObservable();
  }

  // sendWhitePaper(body: object) {
  //   return this.http.post(UserService.saveUserInfoUrl, body)
  //     .map((response: Response) => {
  //       return response.json() as User;
  //     });
  // }

  // login(body: any) {
  //   return this.http.post(UserService.loginUrl, body).map((response: Response) => {
  //     const user: User = response.json() as User;
  //     if (user.token) {
  //       localStorage.setItem('_u', JSON.stringify(user));
  //       localStorage.setItem('token', user.token);
  //     }
  //     return user;
  //   });
  // }

  // saveKYC(kyc, passportFrontImage, passportBackImage) {
  //   const data = new FormData();
  //   data.append('passportFrontImage', passportFrontImage);
  //   data.append('passportBackImage', passportBackImage);
  //   data.append('kyc', JSON.stringify(kyc));
  //   console.log('KYC PROCEEDED');
  //   return this.http.post(UserService.kycUrl, data, TokenService.jwt())
  //     .map((response: Response) => {
  //       return response.json();
  //     });
  // }

  // subscribe(email: string) {
  //   return this.http.post(UserService.subscribeUrl, {email: email}).map((response: Response) => {
  //     return response.json();
  //   });
  // }

  // getTermsStatus() {
  //   if (localStorage.getItem('term') === 'accepted') {
  //     return true;
  //   }
  //   return null;
  // }

  // setTermsStatus() {
  //   return localStorage.setItem('term', 'accepted');
  // }

  modifyPassword(data) {
    return this.http.post<Message>(UserService.modifyPasswordURL, data, TokenService.jwt());
  }

  modifyPasswordwith2FA(data, code, twoFactorEnabled) {
    return this.http.post<Message>(UserService.modifyPasswordURL, {data, code, twoFactorEnabled}, TokenService.jwt());
  }

  private updateObservable(newUser: User) {
    this.getAllStatistics();
    const users = [];
    this.users$.getValue().forEach((oldUser, index) => {
      let user = oldUser;
      if (oldUser._id === newUser._id) {
        user = newUser;
      }
      users.push(user);
    });
    this.users$.next(users);
  }

}
