import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {AddressStatistics} from '../_models/addressStatistics';
import {environment} from '../../environments/environment';
import {TokenService} from './token.service';
import {HttpClient} from '@angular/common/http';
import {Message} from '../_models/message';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import * as io from 'socket.io-client';

//AddressStatistics
@Injectable()
export class AddressService {
  private static apiUrl = 'http://evareium.io/api/sendWhitepaper';
  private static getAddressStatisticsURL = environment.apiURL + '/createAddress/addressesStatistics';
  private static generateAddressURL = environment.apiURL + '/createAddress';
  private static fetchAddressURL = environment.apiURL + '/createAddress/fetchBitgoAddresses';

  private addressStatistics$: BehaviorSubject<AddressStatistics>;

  constructor(private http: HttpClient, private tokenService: TokenService) {


    if (!this.addressStatistics$) {
      this.addressStatistics$ = new BehaviorSubject(null);
      this.getAddressStatistics();
    }
  }



getAddressStatistics() {
  this.http.get<AddressStatistics>(AddressService.getAddressStatisticsURL, TokenService.jwt())
    .subscribe((statistics) => {
        this.addressStatistics$.next(statistics);
      }, (err) => {
        this.tokenService.processError('[GetCoinStatistics]', err);
      }
    );
  }

  getStatistics(): Observable<AddressStatistics> {
    return this.addressStatistics$.asObservable();
  }

  generateAdresses(addressQty: number): Observable<any> {
    return this.http.post<any>(AddressService.generateAddressURL, {addressQty: addressQty}, TokenService.jwt());
  }

  fetchAdresses() {
    return this.http.post<any>(AddressService.fetchAddressURL, {},  TokenService.jwt());
  }
}
