import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { TokenService } from './token.service';
import { environment } from '../../environments/environment';
import { Notification } from '../_models/notification';
import { Feed } from '../_models/feed';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {HttpClient} from '@angular/common/http';
import * as io from 'socket.io-client';
import {UserInfo} from "../_models/userinfo";
import {UserService} from "./user.service";
import {User} from "../_models/user";

@Injectable()
export class NotificationService {
  notifications$: BehaviorSubject<Notification[]>;
  feeds$: BehaviorSubject<Feed[]> = new BehaviorSubject([]);
  public socket: any = null;

  URL: string = environment.apiURL + '/notification';
  notificationURL: string = environment.apiURL + '/admin/notification';
  notificationCountURL: string = environment.apiURL + '/admin/notification/count';
  feedURL: string = environment.apiURL + '/feed';

  constructor(private http: HttpClient,
              private tokenService: TokenService) {

    //  feeds updated
    this.tokenService.socket.on('all-feeds-reload', (feeds) => {
      this.feeds$.next(feeds);
    });

    //  notification updated
    this.tokenService.socket.on('notification-for-all', (notification) => {
      this.addObservable(notification)
    });

    this.tokenService.socket.on('notification-for-user-admin', (notification) => {
      this.addObservable(notification)
    });

    if (!this.notifications$) {
      this.notifications$ = new BehaviorSubject(null);
      this.getAllNotification();
    }

    this.http.get<Feed[]>(this.feedURL, TokenService.jwt())
      .subscribe(
        (feeds) => {
          this.feeds$.next(feeds);
        }, (error) => {
          this.tokenService.processError('[Feeds]', error);
        });

    this.tokenService.socket.on('notifications-' + TokenService.hash(), (notifications) => {
      this.notifications$.next(notifications);
    });
  }

  getAllNotification(perPage: number = 10, page: number = 0, query = ''){
    this.http.get<Notification[]>(this.notificationURL + '/' + perPage + '/' + page + '?query=' + query, TokenService.jwt())
      .subscribe(
        (notifications) => {
          this.notifications$.next(notifications);
        }, (error) => {
          this.tokenService.processError('[Notifications]', error);
        }
      );
  }

  getNotifications(): Observable<Notification[]> {
    return this.notifications$.asObservable();
  }

  updateNotification() {
    this.http.post<Notification[]>(this.URL + '/update-seen', {}, TokenService.jwt()).subscribe((notifications) => {
      //  console.log('notify: ', notifications);
      //  console.log('notifications Update', notifications);
      this.notifications$.next(notifications);
    }, (err) => {
      this.tokenService.processError('[UpdateNotification]', err);
    });

  }

  getAllFeeds(): Observable<Feed[]> {
    return this.feeds$.asObservable();
  }

  getAllNotifications(): Observable<Notification[]> {
    return this.notifications$.asObservable();
  }

  getNotificationCount() {
    return this.http.get<any>(this.notificationCountURL, TokenService.jwt());
  }

  private addObservable(newNotification: Notification[]) {
    let notifications: any = [];
    let newnotifications: any = [];
    newnotifications = newNotification;
    this.notifications$.getValue().forEach((oldNotification, index) => {
      notifications.push(oldNotification);
    });
    notifications.unshift(newnotifications);
    this.notifications$.next(notifications);
  }

}
