import {Injectable} from '@angular/core';
import {Headers, RequestOptions, Response} from '@angular/http';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Message} from '../_models/message';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import * as io from 'socket.io-client';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {catchError, map, tap} from 'rxjs/operators';

import {ToastrService} from 'ngx-toastr';

@Injectable()
export class TokenService {

  public socket: any = null;
  public err = new BehaviorSubject<Message>(null);

  constructor(private router: Router,
              private toastr: ToastrService) {
    this.socket = io(environment.socketURL, {upgrade: false, transports: ['websocket'], query: {token: TokenService.token()}});
    this.socket.on('connect', () => {
      this.socket.send({token: TokenService.token()});
    });

    this.socket.on('referralJoin', (data) => {
      if (data) {
        let lst = this._u();
        if (lst && lst.hasOwnProperty('hash'))
          if (lst.referralCode === data.referralCode)
            this.toastr.success(data.message, 'Referral Joined');
      }
    });

  }

  static hash() {
    if (localStorage.getItem('_u')) {
      const _u = JSON.parse(localStorage.getItem('_u'));

      if (_u && _u.hash) {
        return _u.hash;
      }
    }

    return 0;
  }

  static id() {
    if (localStorage.getItem('_u')) {
      const _u = JSON.parse(localStorage.getItem('_u'));

      if (_u && _u.hash) {
        return _u.hash;
      }
    }

    return 0;
  }

  getFirstName() {
    if (localStorage.getItem('_u')) {
      const _u = JSON.parse(localStorage.getItem('_u'));

      if (_u && _u.firstName) {
        return _u.firstName;
      }
    }

    return 0;
  }

  static token() {
    if (localStorage.getItem('_u')) {
      const _u = JSON.parse(localStorage.getItem('_u'));

      if (_u && _u.token) {
        return _u.token;
      }
    }

    return null;
  }

  static jwt() {
    if (localStorage.getItem('_u')) {
      const _u = JSON.parse(localStorage.getItem('_u'));

      if (_u) {

        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': _u.token
          })
        };
        return httpOptions;
      }
    }

    //return null;
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

  }

  static storage() {
    if (localStorage.getItem('_u')) {
      return JSON.parse(localStorage.getItem('_u'));
    }

    return null;
  }

  _u() {
    if (localStorage.getItem('_u')) {
      return JSON.parse(localStorage.getItem('_u'));
    }

    return null;
  }

  email() {
    if (localStorage.getItem('_u')) {
      const _u = JSON.parse(localStorage.getItem('_u'));

      if (_u && _u.email) {
        return _u.email;
      }
    }

    return null;
  }


  public processError(controller: string, err: HttpErrorResponse) {
    console.error(controller, err);
    if (err.status === 401 || err.status === 403) {
       this.router.navigate(['/login']);
    } else if (err.status === 0) {
      this.router.navigate(['/unavailable']);
    } else {
      this.err.next(err.error);
    }
    // private
    //   handleError<T>(operation = 'operation', result ? : T);
    //   {
    //     return (error: any): Observable<T> => {
    //
    //       // TODO: send the error to remote logging infrastructure
    //       console.error(error); // log to console instead
    //
    //       // TODO: better job of transforming error for user consumption
    //       this.log(`${operation} failed: ${error.message}`);
    //
    //       // Let the app keep running by returning an empty result.
    //       return of(result as T);
    //     };
    //   }

    //
    // if (err.error instanceof ErrorEvent) {
    //   // A client-side or network error occurred. Handle it accordingly.
    //   console.error('An error occurred:', err.error.message);
    // } else {
    //   // The backend returned an unsuccessful response code.
    //   // The response body may contain clues as to what went wrong,
    //   console.error(
    //     `Backend returned code ${err.status}, ` +
    //     `body was: ${err.error}`);
    // }
    // // return an ErrorObservable with a user-facing error message
    // return new ErrorObservable(
    //   'Something bad happened; please try again later.');


  }
}
