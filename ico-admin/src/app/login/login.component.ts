import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {AuthService} from '../_services/auth.service';
import {TokenService} from '../_services/token.service';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {UserService} from "../_services/user.service";
import {User} from "../_models/user";
import {HttpClient} from '@angular/common/http';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  googleCaptchaResponseLogin: string;
  googleCaptchaCheck: boolean;
  pageError: string;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  userIP: any = {};
  deviceInfo = null;

  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(public router: Router,
              public formBuilder: FormBuilder,
              public authService: AuthService,
              public tokenService: TokenService,
              private http: HttpClient,
              private deviceService: DeviceDetectorService) {

    this.form = this.formBuilder.group({
      email: this.formBuilder.control(null, [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)]),
      password: this.formBuilder.control(null, [Validators.required, Validators.minLength(6)]),
      //  captcha: this.formBuilder.control(null, [Validators.required, Validators.maxLength(4), Validators.minLength(4)]),
      twoFactorCode: this.formBuilder.control(null, [Validators.minLength(6), Validators.maxLength(6)])
    });

  }


  ngOnInit() {

    this.http.get('http://ip-api.com/json/?callback=').subscribe((userDetails) => {
      this.userIP = userDetails;
    });

    this.getUserInfo();

  }

  getUserInfo() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
  }

  storeUserInfo() {
    this.authService.storeUserInfo(this.userIP, this.deviceInfo).subscribe((message) => {
    }, (err) => {
      this.pageError = err.error.err;
      this.pageStatus = 'START';
    });
  }

  resolvedLogin(captchaResponse: string) {
    this.googleCaptchaResponseLogin = captchaResponse;
    this.googleCaptchaCheck = true;
  }

  login() {
    this.pageStatus = 'PROCESSING';

    if (this.form.valid) {
      this.authService.login({
        email: this.form.controls['email'].value,
        password: this.form.controls['password'].value,
        code: 'twoFactorCode',
        googleCaptchaResponseLogin: this.googleCaptchaResponseLogin,
        role: 'admin'
      })
        .subscribe(
          (userData) => {
            localStorage.setItem('_u', JSON.stringify(userData));
            localStorage.setItem('token', userData['token']);
            //localStorage.setItem('kycStatus', userData['kycStatus']);
            localStorage.setItem('setup', 'init');
            localStorage.removeItem('referralCode');
            this.pageStatus = 'START';
            this.storeUserInfo();
            this.router.navigate(['/app/home']);
            /*if (userData['twoFaEnabled']) {
              localStorage.setItem('twofaCheck', 'required');
              this.router.navigate(['/two-fa']);
            } else {
              if (userData['isNew']) {
                this.router.navigate(['/account']);
              } else {
                this.router.navigate(['/home']);
              }
            }*/

          },
          (err) => {
            this.tokenService.processError('[loginError]', err);
            this.pageError = err.error.err;
            this.pageStatus = 'START';

            if (this.pageError !== '') {
              setTimeout(() => {
                this.errorSwal.show();
              }, 500);
            }
          }
        );
    } else {
      this.pageError = 'Form is invalid';
    }
  }

}
