import { Pipe, PipeTransform } from '@angular/core';
import { CoinAddress } from '../_models/coinAddress';

@Pipe({name: 'myCoinAddresses'})
export class CoinAddressesPipe implements PipeTransform {
    transform(coinAddresses: CoinAddress[], type: string): CoinAddress[] {
        if (!coinAddresses) {
            return null;
        }
        return coinAddresses.filter((coinAddress) => {
            return coinAddress.currencyId.abbr === type;
        });
    }
}
