import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'myExchangeRate'})
export class ExchangeRatePipe implements PipeTransform {
    transform(rates: any[], types: string): any[] {
        if (!rates) {
            return null;
        }

        if (types.length === 2) {
          return rates.filter((rate) => rate.type === types[0] && rate.currencyIdFirstType === types[1]);
        }
        return rates.filter((rate) => rate.type === types[0]);
    }
}
