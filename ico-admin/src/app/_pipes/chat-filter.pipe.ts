import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chatFilter'
})
export class ChatFilterPipe implements PipeTransform {

  transform(items: any[], value: string): any[] {
    if ( !value || !items) {
      return items;
    }
    return items.filter(it => it.messages[0].name.indexOf(value) > -1);
  }
}
