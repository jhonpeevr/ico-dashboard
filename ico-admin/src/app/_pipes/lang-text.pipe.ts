import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'langText'})
export class LangTextPipe implements PipeTransform {
  transform(text: string, lang: string[]): string {
     // let languages: string[]=['English','Russian','Hindi'];
     if ( text === 'hi') {
       return 'Hindi';
     } else if ( text === 'ru') {
       return 'Russian';
     }else {
       return 'English';
     }
  }
}
