import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'myFinancialHistories'})
export class FinancialHistoriesPipe implements PipeTransform {
    transform(finHistories: any[], type: string[]): any[] {
        if (!finHistories) {
            return null;
        }
        return finHistories.filter((finHistory) => {
            return type.indexOf(finHistory.historyType) > -1;
        });
    }
}
