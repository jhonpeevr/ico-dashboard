export interface CurrencyUsd {
  currencyId: string;
  valueInUSD: number;
}
