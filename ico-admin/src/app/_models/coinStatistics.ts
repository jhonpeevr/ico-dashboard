export interface CoinStatistics {
  totalGeneratedAddresses: number;
  totalPendingAddresses: number;
  totalProcessedAddresses: number;
}
