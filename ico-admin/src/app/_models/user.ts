export interface User {
  _id: number;
  fullName: string;
  email: string;
  timestamp: Date;
  token: string;
  expiresIn: Date;
  balanceTKN: number;
  referralTKN: number;
  purchasedTKN: number;
  country: string;
  twoFaEnabled: boolean;
  kycStatus: string;
}
