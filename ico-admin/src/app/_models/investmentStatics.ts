export interface InvestmentStatics {
  totalNoInvestments: number;
  totalAmountInvestment: number;
}
