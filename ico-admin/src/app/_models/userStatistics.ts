export interface UserStatistics {
  totalUsers: number;
  totalRoleAdmin: number;
  totalRoleUser: number;
  totalActiveUser: number;
  totalBlockUser: number;
  totalEVMBalance: number;
  totalEVTBalance: number;
  totalKycSubmitted: number;
  totalKycConfirmed: number;
}
