export interface WithdrawalStatics {
  totalNoWithdrawal: number;
  totalAmountWithdrawal: number;
}
