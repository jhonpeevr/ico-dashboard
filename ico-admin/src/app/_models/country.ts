export class Country {
  _id: string;
  countryName: string;
  code: string;
}
