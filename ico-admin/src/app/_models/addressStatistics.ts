export interface AddressStatistics {
  totalAddressesCreated: number;
  totalAddressesNotUsed: number;
  totalAddressesUsed: number;
}
