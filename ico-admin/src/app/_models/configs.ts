export interface Configs {
  _id: number;
  key: string;
  value: string;
  type: string;
  editable: boolean;
  displayName: string;
}
