export interface UserInfo {
  _id: number;
  userId: number;
  city: string;
  countryCode: string;
  countryName: string;
  ipAddress: string;
  latitude: number;
  longitude: number;
  browser: string;
  timestamp: Date;
}
