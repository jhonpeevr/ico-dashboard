export interface Config {
  _id: string;
  tknName: string;
  totalTokens: number;
  availableTokens: number;
  soldTokens: number;
  endDate: Date;
  bankCompanyName: string;
  bankName: string;
  bankIBAN: string;
  bankAccount: string;
  bankCountry: string;
  bankState: string;
  bankAddress: string;
  withdrawalEnabled: string;
  tkn2USD: number;
}
