import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';

import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './admin/home/home.component';
import { LoginComponent } from './login/login.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { AdminComponent } from './admin/admin.component';
import { UserManageComponent } from './admin/user-manage/user-manage.component';
import { FinHistoryComponent } from './admin/fin-history/fin-history.component';
import { AssignTokenComponent } from './admin/assign-token/assign-token.component';
import { MessagingCenterComponent } from './admin/messaging-center/messaging-center.component';
import { ConfigComponent } from './admin/config/config.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import { AuthGuard } from './_guards/auth.guard';
import {AuthService} from './_services/auth.service';
import {TokenService} from './_services/token.service';
import { ChartModule } from 'angular-highcharts';
import { DeviceDetectorModule } from 'ngx-device-detector';
import {ChatFilterPipe} from './_pipes/chat-filter.pipe';
import {CoinAddressesPipe} from './_pipes/coin-addresses.pipe';
import {ExchangeRatePipe} from './_pipes/exchange-rate.pipe';
import {FinancialHistoriesPipe} from './_pipes/financial-histories.pipe';
import {LangTextPipe} from './_pipes/lang-text.pipe';
import {NotificationPipe} from './_pipes/notification.pipe';
import {AddressService } from './_services/address.service';

import {
  RECAPTCHA_SETTINGS,
  RecaptchaLoaderService,
  RecaptchaModule,
  RecaptchaSettings
} from 'ng-recaptcha/index';
import {environment} from '../environments/environment';
import { NotFoundComponent } from './not-found/not-found.component';

import { UserService } from './_services/user.service';
import { ConfigService } from './_services/config.service';
import { ExchangeRateService } from './_services/exchange-rate.service';
import { FinHistoryService } from './_services/fin-history.service';
import { CoinService } from './_services/coin.service';
import { KYCService } from './_services/kyc.service';
import { NotificationService } from './_services/notification.service';
import {ToastrModule} from 'ngx-toastr';
import { AddressManageComponent } from './admin/address-manage/address-manage.component';

const globalSettings: RecaptchaSettings = {siteKey: environment.googleCaptchaSiteKey};

const appRoutes: Routes = [
  {path: '', redirectTo: '/app/home', pathMatch: 'full'},
  {
    path: 'app',
    component: AppComponent,
    canActivate: [AuthGuard],
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'users', component: UserManageComponent},
      {path: 'address-manage', component: AddressManageComponent, canActivate: [AuthGuard]},
      {path: 'fn-history', component: FinHistoryComponent, canActivate: [AuthGuard]},
      {path: 'assign-token', component: AssignTokenComponent, canActivate: [AuthGuard]},
      {path: 'message-center', component: MessagingCenterComponent, canActivate: [AuthGuard]},
      {path: 'config', component: ConfigComponent, canActivate: [AuthGuard]},
      {path: '**', redirectTo: '/notfound', pathMatch: 'full'}
    ]
  },
  {path: 'login', component: LoginComponent},
  {path: 'notfound', component: NotFoundComponent},
  {path: 'forgotpassword', component: ForgotpasswordComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ForgotpasswordComponent,
    AdminComponent,
    UserManageComponent,
    FinHistoryComponent,
    AssignTokenComponent,
    MessagingCenterComponent,
    ConfigComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    ChatFilterPipe,
    CoinAddressesPipe,
    ExchangeRatePipe,
    FinancialHistoriesPipe,
    LangTextPipe,
    NotificationPipe,
    AddressManageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    NgxSmartModalModule.forRoot(),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    HttpClientModule,
    HttpModule,
    RecaptchaModule.forRoot(),
    ChartModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
    }),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
    }),
    DeviceDetectorModule.forRoot()
  ],
  providers: [
    AuthGuard,
    AuthService,
    TokenService,
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: globalSettings,
    },
    UserService,
    AuthService,
    ConfigService,
    ExchangeRateService,
    FinHistoryService,
    CoinService,
    KYCService,
    NotificationService,
    NgxSmartModalService,
    AddressService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
