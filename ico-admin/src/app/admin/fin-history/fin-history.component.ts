import { Component, Injectable, OnInit } from '@angular/core';
import { FinHistory } from '../../_models/fin-history';
import { InvestmentStatics } from '../../_models/investmentStatics'
import { WithdrawalStatics } from '../../_models/withdrawalStatics'
import { FinHistoryService} from '../../_services/fin-history.service';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from '../../_services/config.service';
import { Config } from '../../_models/config';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import {ExchangeRateService} from '../../_services/exchange-rate.service';
import {CoinService} from "../../_services/coin.service";
import {UserService} from "../../_services/user.service";

@Component({
  selector: 'app-fin-history',
  templateUrl: './fin-history.component.html',
  styleUrls: ['./fin-history.component.css']
})
export class FinHistoryComponent implements OnInit {
  searchForm: FormGroup;
  p1 = 1;
  investments: Observable<FinHistory[]>;
  withdrawals: Observable<FinHistory[]>;
  config: any;
  statistics:Observable<InvestmentStatics>;
  wstatistics:Observable<WithdrawalStatics>;
  coinstatistics: any;

  investmentSearchForm: FormGroup;
  withdrawalSearchForm: FormGroup;
  perPages = [10, 25, 50];
  perPage = 10;
  pages = [];
  pastRow = 1;
  pastAddressRow = 1;
  pastInvestmentRow = 1;
  prevIndex = 0;
  previnvestmentIndex = 0;
  prevaddressIndex = 0;
  pervPerPage = 10;
  wperPages = [10, 25, 50];
  wperPage = 10;
  wpages = [];
  wpastRow = 1;
  wprevIndex = 0;
  wpervPerPage = 10;
  addressCount: number;
  userCount: number;
  finPages: number;
  nextBtn = 1;
  nextInvestmentBtn = 1;
  nextAddressBtn = 1;
  previousBtn = 0;
  previousInvestmentBtn = 0;
  previousAddressBtn = 0;
  investmentPage: number = 0;
  addressPage: number = 0;

  btcUsd: any;
  ethUsd: any;
  ltcUsd: any;
  users: any;
  userinvestments: any;
  useraddress: any;
  email: string = '';
  investmentID: string;

  constructor(private finHistory: FinHistoryService,
              private  configService: ConfigService,
              public formBuilder: FormBuilder,
              public exchangeRateService: ExchangeRateService,
              public coinService: CoinService,
              private userService: UserService) { }

  ngOnInit() {

    this.searchForm = this.formBuilder.group({
      query: this.formBuilder.control(null, [Validators.required])
    });


    this.configService.getConfig().subscribe((config) => {
      this.config = config;
    });

    this.investments = this.finHistory.getInvestments();
    this.withdrawals = this.finHistory.getWithdrawals();
    this.statistics = this.finHistory.getStatistics();

    this.statistics.subscribe((stat) => {
      if (stat) {
        this.pages = Array(Math.ceil(stat.totalNoInvestments / this.perPage)).fill(0);
      }
    });

    this.wstatistics = this.finHistory.getWithdrawalStatistics();

    this.wstatistics.subscribe((stat) => {
      if (stat) {
        this.wpages = Array(Math.ceil(stat.totalNoWithdrawal / this.wperPage)).fill(0);
      }
    });
    // this.investmentSearchRecord();

    this.exchangeRateService.getUsdRates().subscribe((exchangeRate) => {
      if (exchangeRate) {
        for (const exchange of exchangeRate) {
          if (exchange.currencyId === 'btc') {
            this.btcUsd = exchange.valueInUSD.toFixed(8);
          }
          if (exchange.currencyId === 'eth') {
            this.ethUsd = exchange.valueInUSD.toFixed(8);
          }
          if (exchange.currencyId === 'ltc') {
            this.ltcUsd = exchange.valueInUSD.toFixed(8);
          }
        }
      }
    });

    this.coinService.getStatistics().subscribe((statistics) => {
      this.coinstatistics = statistics;
    });

    this.userService.getAllUsers().subscribe((user) => {
      this.users = user;
    });

    this.userService.getUserCount().subscribe((count) => {
      this.userCount = count;
      if(count != null){
        this.finPages = Math.ceil(count / this.perPage);
      }
    });

  }

  changeUserPerPage(index) {
    if (this.prevIndex !== index) {
      this.userService.getUsers(this.perPage, index, this.searchForm.value.query);
      if (index > this.prevIndex) {
        let a:any = this.pastRow;
        let b:any = this.perPage;
        this.pastRow = parseInt(a) + parseInt(b);
      } else {
        this.pastRow = 1 + (index * this.perPage);
      }
    }
    this.prevIndex = index;
    this.previousBtn = index - 1;
    this.nextBtn = index + 1;
  }

  changePage(index) {
    if (this.prevIndex !== index) {
      this.finHistory.getInvestmentsPageRecord(this.perPage, index,this.investmentSearchForm.value.query);
      if (index > this.prevIndex) {
        this.pastRow = this.pastRow + (index * this.perPage);
      } else {
        this.pastRow = 1 + (index * this.perPage);
      }
    }
    this.prevIndex = index;
  }

  changePerPage() {
    this.statistics.subscribe((stat) => {
      if (stat) {
        this.pages = Array(Math.ceil(stat.totalNoInvestments / this.perPage)).fill(0);
      }
    });
    if (this.pervPerPage !== this.perPage) {
      this.finHistory.getInvestmentsPageRecord(this.perPage, 0, this.investmentSearchForm.value.query);
      this.pervPerPage = this.perPage;
    }
  }
  changeWithdrawalPage(index) {
    if (this.wprevIndex !== index) {
      this.finHistory.getWithdrawalPageRecord(this.wperPage, index,this.withdrawalSearchForm.value.query);
      if (index > this.wprevIndex) {
        this.wpastRow = this.wpastRow + (index * this.wperPage);
      } else {
        this.wpastRow = 1 + (index * this.wperPage);
      }
    }
    this.wprevIndex = index;
  }

  changeWithdrawalPerPage() {
    this.wstatistics.subscribe((stat) => {
      if (stat) {
        this.wpages = Array(Math.ceil(stat.totalNoWithdrawal / this.wperPage)).fill(0);
      }
    });
    if (this.wpervPerPage !== this.wperPage) {
      this.finHistory.getWithdrawalPageRecord(this.wperPage, 0,this.withdrawalSearchForm.value.query);
      this.wpervPerPage = this.wperPage;
    }
  }

  searchRecord() {
    if (this.searchForm.valid) {
      this.userService.getUsers(this.perPage, 0, this.searchForm.value.query);
    }
  }

  reset() {
    this.searchForm.controls['query'].reset();
    // this.searchForm.controls['query'].markAsUntouched(true);
    this.userService.getUsers(this.perPage, 0, null);
  }

  getInvestmentByID(id, email) {

    this.previnvestmentIndex = 0;
    this.previousInvestmentBtn = 0;
    this.nextInvestmentBtn = 1;
    this.pastInvestmentRow = 1;
    this.prevaddressIndex = 0;
    this.previousAddressBtn = 0;
    this.nextAddressBtn = 1;
    this.pastAddressRow = 1;

    this.investmentID = id;
    this.email = email;
    this.userService.getInvestmentByID(id, this.perPage, 0).subscribe((investment) => {
      this.userinvestments = investment;
    })
  }

  getGeneratedAddressByID(id) {
    this.userService.getGeneratedAddressByID(id, this.perPage, 0).subscribe((address) => {
      this.useraddress = address.addresses;
    })
  }

  getUserInvestmentCount(id) {
    this.userService.getUserInvestmentCount(id).subscribe((count) => {
      if(count != null){
        this.investmentPage = Math.ceil(count.count / this.perPage);
      }
    });
  }

  getUserAddressCount(id) {
    this.userService.getUserAddressCount(id).subscribe((count) => {
      if(count != null){
        this.addressCount = count.count;
        this.addressPage = Math.ceil(this.addressCount / this.perPage);
      }
    });
  }

  changeUserInvestmentPerPage(index) {
    if (this.previnvestmentIndex !== index) {
      this.userService.getInvestmentByID(this.investmentID, this.perPage, index).subscribe((investment) => {
        this.userinvestments = investment;

        if (index > this.previnvestmentIndex) {
          let a:any = this.pastInvestmentRow;
          let b:any = this.perPage;
          this.pastInvestmentRow = parseInt(a) + parseInt(b);
        } else {
          this.pastInvestmentRow = 1 + (index * this.perPage);
        }
      });
    }
    this.previnvestmentIndex = index;
    this.previousInvestmentBtn = index - 1;
    this.nextInvestmentBtn = index + 1;
  }

  changeUserAddressPerPage(index) {
    if (this.prevaddressIndex !== index) {
      this.userService.getGeneratedAddressByID(this.investmentID, this.perPage, index).subscribe((address) => {
        this.useraddress = address.addresses;

        if (index > this.prevaddressIndex) {
          let a:any = this.pastAddressRow;
          let b:any = this.perPage;
          this.pastAddressRow = parseInt(a) + parseInt(b);
        } else {
          this.pastAddressRow = 1 + (index * this.perPage);
        }
      });
    }
    this.prevaddressIndex = index;
    this.previousAddressBtn = index - 1;
    this.nextAddressBtn = index + 1;
  }

}
