import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinHistoryComponent } from './fin-history.component';

describe('FinHistoryComponent', () => {
  let component: FinHistoryComponent;
  let fixture: ComponentFixture<FinHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
