import { Component, OnInit, ViewChild } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {User} from '../../_models/user';
import {UserService} from '../../_services/user.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserStatistics} from '../../_models/userStatistics';
import {NgxSmartModalService } from 'ngx-smart-modal';
import {Angular2Csv} from 'angular2-csv';
import {ConfigService} from '../../_services/config.service';
import {TokenService} from '../../_services/token.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {CoinService} from '../../_services/coin.service';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.css']
})

export class UserManageComponent implements OnInit {
  users: any;
  user: any;
  userdetails: any;
  statistics: any;
  email: string;
  searchForm: FormGroup;
  assignTokenForm: FormGroup;
  isUserStatus: boolean;
  isUserStatusClass: string = '';
  isUserStatusClass2FA: string = '';
  blockoractivate: string = '';
  tkn2USD: number;
  perPages = [10, 25, 50];
  perPage = 10;
  pages: any;
  infopages: number;
  pastRow = 1;
  pastInfoRow = 1;
  prevIndex = 0;
  prevUserInfoIndex = 0;
  pervPerPage = 10;
  nextBtn = 1;
  nextInfoBtn = 1;
  previousBtn = 0;
  previousInfoBtn = 0;
  tokens = 0;
  usd = 0;
  tokenWithBonus: number = 0;
  totalTokens = 0;
  PAGE = 'START';
  public pageError: string;
  bonus: number;
  exportUsersData = [];
  exportUsersSearchData = [];

  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              public ngxSmartModalService: NgxSmartModalService,
              public configsService: ConfigService,
              private tokenService: TokenService,
              private coinService: CoinService) {
  }

  ngOnInit() {

    this.searchForm = this.formBuilder.group({
      query: this.formBuilder.control(null, [Validators.required])
    });

    /*this.assignTokenForm = this.formBuilder.group({
      tokens: this.formBuilder.control(null, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
      usdvalue: this.formBuilder.control(null, [Validators.required]),
      bonus: this.formBuilder.control(null)
    });*/


    this.userService.getAllUsers().subscribe((user) => {
      this.users = user;
    });

    this.userService.getStatistics().subscribe((statics) => {
      if(statics != null){
        this.statistics = statics;
        this.pages = Array(Math.ceil(this.statistics.totalUsers / this.perPage));
      }
    });

    this.configsService.getConfig()
      .subscribe((config) => {
          if (config) {
            this.tkn2USD = config['tkn2USD'] || 1; // #1 till config is not updated
            this.bonus = config['bonus'] || 10
          }
        }, (err) => {
          this.tokenService.processError('[GetConfig]', err);
        }
      );

  }

  searchRecord() {
    if (this.searchForm.valid) {
      this.userService.getUsers(this.perPage, 0, this.searchForm.value.query);
    }
  }

  getSearchRecordToExport() {
    if (this.searchForm.valid) {
      this.userService.getSearchRecordToExport(this.searchForm.value.query).subscribe((data) => {
        this.exportUsersSearchData = data;
      });
    }
  }

  reset() {
    this.searchForm.controls['query'].reset();
    this.userService.getUsers(this.perPage, 0, null);
    this.exportUsersSearchData = [];
  }

  exportUsers() {
    const usersExport = [];
    this.userService.getAllUsersForExports().subscribe((users) => {
      if(this.exportUsersSearchData.length > 0){
        this.exportUsersData = this.exportUsersSearchData;
      }else{
        this.exportUsersData = users;
      }
      this.exportUsersData.forEach(function (user) {
        usersExport.push({
          email: user.email,
          country: user.country == undefined ? '' : user.country,
          balanceTKN: user.balanceTKN,
          kycStatus: user.kycStatus,
          timestamp: user.timestamp,
        });
      });
      const head = ['Email', 'Country', 'balanceTKN', 'kycStatus', 'Date'];
      new Angular2Csv(usersExport, 'user-details', {headers: (head)});
    });
  }

  getUserByID(email) {
    this.prevUserInfoIndex = 0;
    this.previousInfoBtn = 0;
    this.nextInfoBtn = 1;
    this.pastInfoRow = 1;

    this.userService.getUserByID(email).subscribe((user) => {
      this.user = user[0];
      this.isUserStatus = this.user.active;
      this.isUserStatusClass = (this.isUserStatus) ? 'active' : '';
      this.blockoractivate = (this.isUserStatus) ? 'Block' : 'Activate';
      this.isUserStatusClass2FA = (this.user.twoFaEnabled) ? 'active' : '';
    });
  }

  getUserInfoByID(id) {
    this.userService.getUserInfoByID(id).subscribe((userinfo) => {
      this.userdetails = userinfo;
    });
  }

  getUserInfoCount(id) {
    this.userService.getUserInfoCount(id).subscribe((count) => {
      if(count != null){
        this.infopages = Math.ceil(count.count / this.perPage);
      }
    });
  }

  changeInfoPerPage(id, index) {
    if (this.prevUserInfoIndex !== index) {
      this.userService.getUserInfoByID(id, this.perPage, index).subscribe((userinfo) => {
        this.userdetails = userinfo;
      });
      if (index > this.prevUserInfoIndex) {
        let a:any = this.pastInfoRow;
        let b:any = this.perPage;
        this.pastInfoRow = parseInt(a) + parseInt(b);
      } else {
        this.pastInfoRow = 1 + (index * this.perPage);
      }
    }
    this.prevUserInfoIndex = index;
    this.previousInfoBtn = index - 1;
    this.nextInfoBtn = index + 1;
  }

  blockORActivate(id) {
    if (confirm('Are you sure?') == true) {
      if (this.isUserStatus) {
        this.isUserStatus = false;
        this.isUserStatusClass = 'active';
        this.blockoractivate = 'Activate';
      } else {
        this.isUserStatus = true;
        this.isUserStatusClass = '';
        this.blockoractivate = 'Block';
      }
      this.userService.blockorActivateUser(id, this.isUserStatus);
    }
  }


  block(user) {
    if (confirm('Are you sure?') == true) {
      this.userService.blockUser(user._id);
    }
  }

  activate(user) {
    if (confirm('Are you sure?') == true) {
      this.userService.activateUser(user._id);
    }
  }

  convertInUSD(user) {
    // this.tknTOUSD = this.searchForm.value.tokens;
    // console.log("this.searchForm.value.tokens ", this.searchForm.value.tokens);
  }

  calculateTokens() {
    this.usd = this.tokens / this.tkn2USD;
    this.tokenWithBonus = (this.tokens * this.bonus) / 100;
  }

  calculateUSD() {
    this.tokens = this.usd * this.tkn2USD;
    this.tokenWithBonus = (this.tokens * this.bonus) / 100;
  }

  changePage(index) {
    if (this.prevIndex !== index) {
      this.userService.getUsers(this.perPage, index, this.searchForm.value.query);
      if (index > this.prevIndex) {
        let a:any = this.pastRow;
        let b:any = this.perPage;
        this.pastRow = parseInt(a) + parseInt(b);
      } else {
        this.pastRow = 1 + (index * this.perPage);
      }
    }
    this.prevIndex = index;
    this.previousBtn = index - 1;
    this.nextBtn = index + 1;
  }

  assignToken(id) {
    this.totalTokens = this.tokens + this.tokenWithBonus;
    this.PAGE = 'PROCESSING';
    this.coinService.assignToken(id, this.tokens, this.tokenWithBonus)
      .subscribe((token) => {
        this.PAGE = 'START';
          setTimeout(() => {
            this.successSwal.show();
            this.tokens = 0;
            this.usd = 0;
            this.tokenWithBonus = 0;
          }, 200);
      }, (error) => {
        this.pageError = error.error.err;
        this.tokenService.processError('[GenerateAddress]', error);
          setTimeout(() => {
            this.errorSwal.show();
          }, 200);
      });
  }

}
