import { Component, Injectable, OnInit } from '@angular/core';
import { ConfigService } from '../../_services/config.service'
import { Observable } from 'rxjs/Observable';
import { Config } from '../../_models/config'
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import {Configs} from "../../_models/configs";

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {

  configs: Configs[];
  configForm: FormGroup;
  constructor(private configService:ConfigService, public  formBuilder:FormBuilder) {
  }

  ngOnInit() {
    this.configService.getConfigs().subscribe((config) => {
      this.configs = config;
    });
    this.configForm= this.formBuilder.group({
      key: this.formBuilder.control({disable:true}, []),
      type: this.formBuilder.control(null, []),
      value: this.formBuilder.control(null,[ Validators.required ])
    })

  }

  updateConfig(config){
    if (confirm('Are you sure?') == true) {
      config.value = this.configForm.value.value;
      this.configService.updateConfig(config);
    }
  }

  populatePopup(config){
    this.configForm.patchValue({
        _id:config._id,
        key:config.key,
        value:config.value,
        type:config.type
      }
    )
  }

}
