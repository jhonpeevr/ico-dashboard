import { Component, OnInit, ViewChild } from '@angular/core';
import {AddressService} from '../../_services/address.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgxSmartModalService } from 'ngx-smart-modal';
import {ConfigService} from '../../_services/config.service';
import {TokenService} from '../../_services/token.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {CoinService} from '../../_services/coin.service';
declare var $: any;

@Component({
  selector: 'app-address-manage',
  templateUrl: './address-manage.component.html',
  styleUrls: ['./address-manage.component.css']
})
export class AddressManageComponent implements OnInit {


  addressStatistics: any;
  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('successAddressSwal') private successAddressSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;
  generateAdreesesform: FormGroup;
  pageStatus = 'START';
  pageError: string;
  isAddressProcess: boolean = false;
  isFetchProcess: boolean = false;

  constructor(private addressService: AddressService,
              private formBuilder: FormBuilder,
              public ngxSmartModalService: NgxSmartModalService,
              public configsService: ConfigService,
              private tokenService: TokenService,
              private coinService: CoinService) {

    this.generateAdreesesform = this.formBuilder.group({
      noofaddress: this.formBuilder.control(null, [Validators.required]),
    });

  }

  ngOnInit() {

    this.getAllStatics();
  }

  getAllStatics() {
    this.addressService.getStatistics().subscribe((statics) => {
      if (statics) {
        this.addressStatistics = statics;
      }
    });
  }

  getAllStatic() {
    this.addressService.getAddressStatistics();
  }

  generateAdreeses() {
    this.pageStatus = 'PROCESSING';
    this.isAddressProcess = true;
    if (this.generateAdreesesform.valid) {
      this.addressService.generateAdresses(this.generateAdreesesform.controls['noofaddress'].value)
        .subscribe(
          (addresses) => {
            this.pageStatus = 'START';
            this.isAddressProcess = false;
            $('#exampleModal').modal('hide');
            this.generateAdreesesform.reset();
            setTimeout(() => {
              this.successSwal.show();
            }, 200);
          },
          (err) => {
            this.pageError = err.error.err;
            this.pageStatus = 'START';
            this.isAddressProcess = false;
            if (this.pageError !== '') {
              setTimeout(() => {
                this.errorSwal.show();
              }, 200);
            }
          }
        );
    } else {
      this.pageError = 'Form is invalid';
    }
  }

  fetchAddresses() {
    this.pageStatus = 'PROCESSING';
    this.isFetchProcess = true;
    this.addressService.fetchAdresses()
      .subscribe(
        (addresses) => {
          this.pageStatus = 'START';
          this.isFetchProcess = false;
          this.getAllStatic();
          setTimeout(() => {
            this.successAddressSwal.show();
          }, 200);
        },
        (err) => {
          this.pageError = err.error.err;
          this.pageStatus = 'START';
          this.isFetchProcess = false;
          if (this.pageError !== '') {
            setTimeout(() => {
              this.errorSwal.show();
            }, 200);
          }
        }
      );
  }

}
