import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignTokenComponent } from './assign-token.component';

describe('AssignTokenComponent', () => {
  let component: AssignTokenComponent;
  let fixture: ComponentFixture<AssignTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
