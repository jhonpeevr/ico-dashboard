import { Component, OnInit, ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from '../../_services/auth.service';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {NotificationService} from '../../_services/notification.service';

@Component({
  selector: 'app-messaging-center',
  templateUrl: './messaging-center.component.html',
  styleUrls: ['./messaging-center.component.css']
})
export class MessagingCenterComponent implements OnInit {

  searchForm: FormGroup;
  p1 = 1;
  perPages = [10, 25, 50];
  perPage = 10;
  pages: any;
  pastRow = 1;
  prevIndex = 0;
  pervPerPage = 10;
  nextBtn = 1;
  previousBtn = 0;

  emailList: any = [];
  notificationForm: FormGroup;
  pageStatus = 'START'; // LOADING, START, PROCESSING, COMPLETED
  pageError: string;
  notifications: any;
  count: number = 0;

  @ViewChild('successSwal') private successSwal: SwalComponent;
  @ViewChild('errorSwal') private errorSwal: SwalComponent;

  constructor(public formBuilder: FormBuilder,
              public authService: AuthService,
              public notificationService: NotificationService,) { }

  ngOnInit() {

    this.notificationForm = this.formBuilder.group({
      email: this.formBuilder.control(null, [Validators.required]),
      subject: this.formBuilder.control(null, [Validators.required]),
      message: this.formBuilder.control(null, [Validators.required]),
      sendToAll: this.formBuilder.control(null)
    });

    this.searchForm = this.formBuilder.group({
      query: this.formBuilder.control(null, [Validators.required])
    });

    this.notificationService.getAllNotifications().subscribe((notifications) => {
      this.notifications = notifications;
    });

    this.notificationService.getNotificationCount().subscribe((count) => {
      if(count != null){
        this.count = count.count;
        this.pages = Array(Math.ceil(this.count / this.perPage));
      }
    });
  }

  changeNotificationsPage(index) {
    if (this.prevIndex !== index) {
      this.notificationService.getAllNotification(this.perPage, index, this.searchForm.value.query);
      if (index > this.prevIndex) {
        let a:any = this.pastRow;
        let b:any = this.perPage;
        this.pastRow = parseInt(a) + parseInt(b);
      } else {
        this.pastRow = 1 + (index * this.perPage);
      }
    }
    this.prevIndex = index;
    this.previousBtn = index - 1;
    this.nextBtn = index + 1;
  }

  searchRecord() {
    if (this.searchForm.valid) {
      this.notificationService.getAllNotification(this.perPage, 0, this.searchForm.value.query);
    }
  }
  reset() {
    this.searchForm.controls['query'].reset();
    this.notificationService.getAllNotification(this.perPage, 0);
  }

  enableDisableEmail() {
    if(this.notificationForm.controls['sendToAll'].value == true){
      this.notificationForm.controls['email'].disable();
      this.notificationForm.controls['email'].valid;
    }else {
      this.notificationForm.controls['email'].enable();
    }
  }
  sendNotification() {
    if (this.notificationForm.valid) {
      this.pageStatus = 'PROCESSING';
      if(this.notificationForm.controls['sendToAll'].value != true) {
        this.emailList = this.notificationForm.controls['email'].value.split(',');
      }
      this.authService.sendNotification(this.emailList, this.notificationForm.controls['subject'].value, this.notificationForm.controls['message'].value, this.notificationForm.controls['sendToAll'].value != null ? this.notificationForm.controls['sendToAll'].value : false)
        .subscribe((notification) => {
            this.pageStatus = 'START';
            setTimeout(()=> {
              this.successSwal.show();
              this.notificationForm.reset();
              this.notificationForm.controls['email'].enable();
            },300);
          },
          (err) => {
            this.pageError = err.error.error.message;
            this.pageStatus = 'START';
            this.notificationForm.controls['email'].enable();
            if(this.pageError!=""){
              setTimeout(() => {
                this.errorSwal.show();
              }, 300);
            }
          }
        );
    } else {
      this.pageError = 'Form is invalid';
    }
  }

}
