import {Component, OnInit} from '@angular/core';
import {CoinService} from '../../_services/coin.service';
import {UserService} from '../../_services/user.service';
import {ConfigService} from '../../_services/config.service';
import {ExchangeRateService} from '../../_services/exchange-rate.service';
import { Chart } from 'angular-highcharts';
import { InvestmentStatics } from '../../_models/investmentStatics'
import { FinHistoryService} from '../../_services/fin-history.service';
import {Observable} from "rxjs/Observable";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  searchForm: FormGroup;
  btcUsd: any;
  ethUsd: any;
  ltcUsd: any;
  investments: any;
  xAxisData = [];
  investmentData = [];

  totalSold: string;
  availableTokens: string;
  tknName: string;
  tkn2USD: string;
  userCount: number;
  statistics: any;
  totalInvestments: number;
  investmentsStatistics: any;
  investmentsByDate: any;
  isDay: boolean = false;
  isMonth: boolean = false;
  isYear: boolean = false;

  constructor(public userService: UserService,
              public coinService: CoinService,
              public exchangeRateService: ExchangeRateService,
              public configService: ConfigService,
              private finHistory: FinHistoryService,
              private formBuilder: FormBuilder) {
  }

  chart = new Chart({
    chart: {
      type: 'line'
    },
    title: {
      text: 'Investments'
    },
    xAxis: {
      categories:  this.xAxisData,
    },
    credits: {
      enabled: false
    },
  });


  ngOnInit() {

    this.searchForm = this.formBuilder.group({
      fromDate: this.formBuilder.control(null, [Validators.required]),
      toDate: this.formBuilder.control(null, [Validators.required])
    });

    this.exchangeRateService.getUsdRates().subscribe((exchangeRate) => {
      if (exchangeRate) {
        for (const exchange of exchangeRate) {
          if (exchange.currencyId === 'btc') {
            this.btcUsd = exchange.valueInUSD.toFixed(8);
          }
          if (exchange.currencyId === 'eth') {
            this.ethUsd = exchange.valueInUSD.toFixed(8);
          }
          if (exchange.currencyId === 'ltc') {
            this.ltcUsd = exchange.valueInUSD.toFixed(8);
          }
        }
      }
    });

    this.configService.getConfig().subscribe((config) => {
      if (config) {
        this.totalSold = config.soldTokens.toFixed(0);
        this.availableTokens = config.totalTokens.toFixed(0);
        this.tknName = config.tknName;
        this.tkn2USD = config.tkn2USD.toString();
      }
    });

    this.userService.getUserCount().subscribe((count) => {
      this.userCount = count;
    });

    this.coinService.getStatistics().subscribe((statistics) => {
      this.statistics = statistics;
    });

     this.finHistory.getStatistics().subscribe((investments)=>{
       this.investmentsStatistics = investments;
     });

    // setTimeout(() => {
    //   this.totalInvestment();
    // }, 300);

    this.getInvestmentByMonth();

    setInterval(() => {
      this.setTimer();
    },1000);

    this.getToday();

  }

  totalInvestment(){
    if(this.investmentsStatistics.totalAmountInvestment != null){
      this.totalInvestments = this.investmentsStatistics.totalAmountInvestment;
    }
    this.chart.addSerie({
      name: 'Line 2',
      data: [ this.totalInvestments ]
    });
  }

  reset() {
    (document.getElementById("toDate") as HTMLInputElement).disabled = true;
    this.searchForm.reset();
    this.chart.removeSerie(this.chart.ref.series.length - 1);
    this.getInvestmentByMonth();
  }

  getInvestmentByDay(){
    this.isDay = true;
    this.isMonth = false;
    this.isYear = false;
    this.investments = [];
    this.xAxisData = [];
    this.investmentData = [];
    this.finHistory.getAllInvestments(this.isDay, this.isMonth, this.isYear).subscribe((finhistory) => {
      this.investments = finhistory.totalInvestmentByDay;
      this.investments.map((invest) => {
        this.xAxisData.push(invest.timestamp.substr(0,10));
        this.investmentData.push(invest.totalAmount);
      });
      this.chart = new Chart({
        xAxis: {
          categories:  this.xAxisData,
        },
        title: {
          text: 'Daily Investments'
        },
        series: [{
          name: 'Investments',
          data: this.investmentData
        }]
      });
    });
  }

  getInvestmentByMonth(){
    this.isDay = false;
    this.isMonth = true;
    this.isYear = false;
    this.investments = [];
    this.xAxisData = [];
    this.investmentData = [];
    this.finHistory.getAllInvestments(this.isDay, this.isMonth, this.isYear).subscribe((finhistory) => {
      this.investments = finhistory.totalInvestmentByMonth;
      this.investments.map((invest) => {
        this.xAxisData.push(invest.timestamp.substr(0,7));
        this.investmentData.push(invest.totalAmount);
      });
      this.chart = new Chart({
        xAxis: {
          categories:  this.xAxisData,
        },
        title: {
          text: 'Monthly Investments'
        },
        series: [{
          name: 'Investments',
          data: this.investmentData
        }]
      });
    });
  }

  getInvestmentByYear(){
    this.isDay = false;
    this.isMonth = false;
    this.isYear = true;
    this.investments = [];
    this.xAxisData = [];
    this.investmentData = [];
    this.finHistory.getAllInvestments(this.isDay, this.isMonth, this.isYear).subscribe((finhistory) => {
      this.investments = finhistory.totalInvestmentByYear;
      this.investments.map((invest) => {
        this.xAxisData.push(invest.timestamp.substr(0,4));
        this.investmentData.push(invest.totalAmount);
      });
      this.chart = new Chart({
        xAxis: {
          categories:  this.xAxisData,
        },
        title: {
          text: 'Yearly Investments'
        },
        series: [{
          name: 'Investments',
          data: this.investmentData
        }]
      });
    });
  }


  getInvestmentByDate(){
    this.investments = [];
    this.xAxisData = [];
    this.investmentData = [];
    this.finHistory.getInvestmentByDate(this.searchForm.value.fromDate, this.searchForm.value.toDate).subscribe((finhistory)=>{
      this.investments = finhistory.totalInvestmentByRange;
      this.investments.map((invest) => {
        this.xAxisData.push(invest.timestamp.substr(0,10));
        this.investmentData.push(invest.totalAmount);
      });
      this.chart = new Chart({
        xAxis: {
          categories:  this.xAxisData,
        },
        title: {
          text: 'Weekly Investments'
        },
        series: [{
          name: 'Investments',
          data: this.investmentData
        }]
      });
    });
  }

  days: number;
  hours: number;
  minutes: number;
  seconds: number;

  setTimer(){
    // Set the date we're counting down to
    var countDownDate = new Date("July 21, 2018 15:37:25").getTime();
    const now = new Date().getTime();
    // Find the distance between now an the count down date
    const distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    this.days = Math.floor(distance / (1000 * 60 * 60 * 24));
    this.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    this.seconds = Math.floor((distance % (1000 * 60)) / 1000);
  }

  getToday() {
    var today = new Date().toISOString().split('T')[0];
    document.getElementById("toDate").setAttribute('max', today);
    document.getElementById("fromDate").setAttribute('max', today);
  }

  setEndDate() {
    var startDate = (document.getElementById('fromDate') as HTMLInputElement).value;
    document.getElementById("toDate").setAttribute('min', startDate);
    (document.getElementById("toDate") as HTMLInputElement).disabled = false;
  };

}
