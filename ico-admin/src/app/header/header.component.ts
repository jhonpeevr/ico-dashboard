import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor() { }

  ngOnInit() {
   /* var file = document.getElementById('navbarText ul li a');
    file.addEventListener('change', function() { document.getElementById('navbarText').classList.remove('in'); });
    var file = document.getElementById('navbarText ul li a');
    file.addEventListener('change', function() { document.getElementById('nav-tog').classList.add('collapsed'); });*/
  }

  logout() {
    localStorage.removeItem('_u');
    localStorage.removeItem('token');
    localStorage.removeItem('setup');
  }

}
